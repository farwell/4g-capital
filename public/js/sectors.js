console.log(jQuery);
$(document).ready(function(){
    var countryField = $("[name='country']");
    countryField.on('change', function(){
    
        var country = $(this).val();
        $.ajax({
            type: 'GET',
            url: '/admin/get-branches',
            data: { country_id: country },
            success: function(data) {
                var branchSelect = $('#branch_id');
                branchSelect.empty();
                $.each(data, function(index, branch) {
                    branchSelect.append($('<option>', {
                        value: branch.id,
                        text: branch.title
                    }));
                });
            }
        });
    });
});


