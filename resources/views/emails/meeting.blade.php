@extends('layouts.email')

@section('content')


<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#662F8E;text-align:center;">Meeting Invite</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: poppins, sans-serif">Hello ,</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    A Meeting on Erevuka Zoom has been created.</p>

    <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
       Topic: {{$topic}}.</p>

    <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    Agenda: {{$agenda}}.</p>

    <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    Host: {{$host}}.</p>

    <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
        Start Time: {{$start_time}}.</p>

  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
      In order to join the meeting, please click on “Join Meeting” .</p>

<p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#23245F;font-size:16px;font-weight: bold;">
  <a href="{{$url}}" class="btn-drk-left" target="_blank">Join Meeting
  </a>
</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
If you're unable to click on the link, you may copy and paste the link below into your browser. 
</p>
<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
  {{$url}}
</p>
<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#7C7C7C;text-align:center;">
Best Regards,
</p>


<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#662F8E;text-align:center;">
contact@4g-capital.com
</p>


@endsection
