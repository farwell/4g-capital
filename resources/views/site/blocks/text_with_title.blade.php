
@php 
use App\Models\Resource;
$resources = Resource::latest()->take(2)->get();
@endphp
@if (Request::path() === 'about')
<section class="general-section">
	<div class="row alignment-class-about">
		<div class="col-lg-9">
			<div class="whatYouGetListing">
    <div class="col-md-12 ">
		<div class="hrHeading">
			<h2 class="line-header ">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="whatYouGetListing ">

					{!! $block->translatedinput('description') !!}
					
				</div>
			</div>
		</div>

    
	</div>
</div>
</div>


<div class="col-md-3 d-none d-md-block">
         
@include('site.includes.components.upcoming',
[
 'resources'=>$resources
])
             
</div>
</div>

</section>
@else 
<section class="general-section">
	<div class="row">
		<div class="col-lg-12">
			<div class="whatYouGetListing">
    <div class="col-md-12 ">
		<div class="hrHeading">
			<h2 class="line-header ">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="whatYouGetListing ">

					{!! $block->translatedinput('description') !!}
					
				</div>
			</div>
		</div>

    
	</div>
</div>
</div>

</div>

</section>




@endif