
@php
use Carbon\Carbon;
@endphp

@extends('layouts.app')

@section('title','Event')



@section('content')
<div class="background-page"> 
    
    @include('site.includes.components.parallax',[
    'image'=> asset("images/banners/session_details.png"),
    'text'=> 'Events'
    ])
    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ route('events')}}" class="active">Back to Events</a>
        </li>
    </ol>
    @endcomponent
    <div class="clearfix">
        <br /> <br />
    </div>
    <section  class="general-section">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10 col-sm-7  offset-1">
                <p class="event_period">{{$pageItem->present()->date}} | {{$pageItem->present()->time}}</p>

                <h3 class="event_title">{{$pageItem->title}}</h3>

                {!!$pageItem->description!!}
            </div> 
        </div>
          
    </div>
</section>
@endsection

