
@php
use Carbon\Carbon;
@endphp

@extends('layouts.app_no_js')

@section('title','Blog')


@section('content')
<div class="background-page">

    @php
    $text = $pageItem->topic;
    @endphp

    @include('site.includes.components.parallax',[
    'image'=> asset("images/banners/session_details.png"),
    'text'=> $text
    ])

    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('blogs') }}">Blogs</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{{$pageItem->title}}</a>
        </li>
    </ol>
    @endcomponent

    <div class="clearfix">
        <br /> <br />
    </div>
    <div class="col-md-12">
        <div class="row alignment-class-sessions">
            <div class="col-md-8 col-sm-7 ">
                <div class="row">
                    <div class="col-md-10 col-sm-7  offset-1">

                        <h3 class="event_title">{{$pageItem->title}}</h3>

                        <p class=" text-grey session-card-calendar">
                         <span class="session-calendar"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon::parse($pageItem->published_at)->isoFormat('MMMM Do YYYY')}} </span>
                        </p>

                        @if( $pageItem->hasImage('inner_image'))
                        <img src="{{$pageItem->image("inner_image", "default")}}" alt="{{$pageItem->title}}" />

                        @endif

                        {!!$pageItem->description!!}

                        <hr>

                        @include('site.includes.components.blog_discuss')

                        @include('site.includes.components.blog_discussview')

                        <div class="clearfix">
                            <br /> <br />
                            <br />
                        </div>
                    </div>
                </div>

            </div>
            @if((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="col-md-4 col-sm-5">

                @include('site.includes.components.other_blogs',
                [
                'blogs'=>$blogs,
                ])
            </div>

            @endif
        </div>

    </div>



@endsection


@section('js')
 <script>
 $(document).ready(function() {
$('.emoji-wysiwyg-editor').on('click',function(e){
   console.log('yes');
    $(this).children().remove();
   });
 });
</script>
<script>
    $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '{{ asset("emojis/lib/img/")}}',
        popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
    });
</script>

<script>
    $(document).ready(function() {
       $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });

$('#comment-discuss').submit(function(e) {
    e.preventDefault();
    // console.log('Yes');
      let formData = new FormData(this);
      $.ajax({
         type:'POST',
         url: '{{ route("blog.comment.store")}}',
          data: formData,
          contentType: false,
          processData: false,
          success: (data) => {
           if($.isEmptyObject(data.error)){

            $(".text-input").empty();

            $("#reloadComments").empty();
             $("#reloadComments").html(data.output);

           }else{
               printErrorMsg(data.error);
           }
          },

      });
 });
});
</script>


<script>

function submitReply(){

     comment = document.getElementById('replycomment_id').value;
     reply = document.getElementById('replycomment_reply').value;

     console.log(comment);
     console.log(reply);

    $.ajax({
         type:'POST',
         url: '{{ route("blog.replies.store")}}',
          data: { id: comment, reply: reply,  token:"{{ csrf_token() }}"},
          dataType:'json',
          success: (data) => {
           if($.isEmptyObject(data.error)){

            $(".text-input").empty();

              var x = document.getElementById('SectionName-'+data.comment);
                if (x.style.display == 'none') {
                    x.style.display = 'block';
                } else {
                    x.style.display = 'none';
                }

                $('#replies-link-'+data.comment).empty();
                $('#replies-link-'+data.comment).html(data.link);

             $('#replySection-'+data.comment).empty();
             $('#replySection-'+data.comment).html(data.output);

           }else{
               printErrorMsg(data.error);
           }
          },

      });
}
</script>


<script>
function loadMore(id){
    $.ajax({
         type:'POST',
         url: '{{ route("blog.comment.load")}}',
          data: {id: id, token:"{{ csrf_token() }}"},
          dataType:'json',
          success: (data) => {
           if($.isEmptyObject(data.error)){

            $(".text-input").empty();


            $("#reloadComments").empty();
             $("#reloadComments").html(data.output);

           }else{
               printErrorMsg(data.error);
           }
          },

      });

}
</script>


<script>

    function saveLikeDislike(comment,type){
      var vm=$(this);

      $.ajax({
         type:'POST',
         url: "{{ route('blog.save_likedislike') }}",
          data: {post: comment,  type: type , token:"{{ csrf_token() }}" },
          dataType:'json',
          beforeSend:function(){
          vm.addClass('disabled');
            },
          success: (data) => {
            if(data.bool==true){
            vm.removeClass('disabled').addClass('active');
            vm.removeAttr('id');
            if(type === 'like'){
                var _prevCount= $("#like-"+comment).text();
            _prevCount++;
            $("#like-"+comment).text(_prevCount);
            }else if(type === 'dislike'){

            var _prevCount= $("#dislike-"+comment).text();
            _prevCount++;
            $("#dislike-"+comment).text(_prevCount);
            }

                }
          },

      });

    }

    </script>

<script>
    function showReplies(id)
    {
        var x=document.getElementById('sectionReplies-'+id);
        if (x.style.display == 'none') {
            x.style.display = 'block';
            document.getElementById('showRepliesLink').innerHTML = 'Hide Replies';
        } else {
            x.style.display = 'none';
            document.getElementById('showRepliesLink').innerHTML ='View Replies';
        }

    }
</script>
<script>
    function ShowAndHide(id) {
        console.log(id);
        var x = document.getElementById('SectionName-'+id);
        if (x.style.display == 'none') {
            x.style.display = 'block';
        } else {
            x.style.display = 'none';
        }
    }
</script>


<script>
  $(document).ready(function() {

// Get current page URL
var url = window.location.href;



// remove # from URL
url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

// remove parameters from URL
url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

// select file name
url = url.split('/')[4];


// console.log(url);

// Loop all menu items
$('.navbar-nav .nav-item').each(function(){

 // select href
 var href = $(this).find('a').attr('href');

 link = href.split('/')[4];

 // Check filename
 if(link === 'blogs'){

  // Add active class
  $(this).addClass('active');
 }
});
});
</script>
@endsection

