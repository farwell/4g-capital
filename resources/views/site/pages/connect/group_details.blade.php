@extends('layouts.app_no_js')

@section('title', 'Group')
@section('css')
    <style>
        <style>.ul-block {
            columns: 2;
            background-color: #F1F2F2 !important;
            padding: 20px;
            color: #0e73b7;
            list-style: none;
        }

        .ul-block li {
            padding: 8px !important;
        }

        .ul-block li::before {
            content: "\2022";
            /* Add content: \2022 is the CSS Code/unicode for a bullet */
            color: #000151;
            /* Change the color */
            font-weight: bold;
            /* If you want it to be bold */
            display: inline-block;
            /* Needed to add space between the bullet and the text */
            width: 1em;
            /* Also needed for space (tweak if needed) */
            margin-left: -1em;
            /* Also needed for space (tweak if needed) */

        }

        .ul-blocks {
            columns: 1;
            padding: 20px;
            color: #757575;
            list-style: none;
        }

        .ul-blocks li {
            padding: 8px !important;
        }

        .ul-blocks li::before {
            content: "\2022";
            /* Add content: \2022 is the CSS Code/unicode for a bullet */
            color: #000151;
            /* Change the color */
            font-weight: bold;
            /* If you want it to be bold */
            display: inline-block;
            /* Needed to add space between the bullet and the text */
            width: 1em;
            /* Also needed for space (tweak if needed) */
            margin-left: -1em;
            /* Also needed for space (tweak if needed) */

        }

        .ol-block {
            column: 2;
            padding: 20px;
            color: #0e73b7;
        }

        .ol-blocks {
            columns: 1;
            padding: 20px;
            color: #0e73b7;
            list-style: none;
        }

        .ol-blocks li {
            padding: 8px !important;
            counter-increment: list;
            list-style-type: none;
            position: relative;
        }

        .ol-blocks li::before {
            content: counter(list) ".";
            color: #000151;
            /* Change the color */
            font-weight: bold;
            /* If you want it to be bold */
            display: inline-block;
            /* Needed to add space between the bullet and the text */
            width: 1em;
            /* Also needed for space (tweak if needed) */
            margin-left: -1em;
            /* Also needed for space (tweak if needed) */
        }

        .single-principle-page a.grey {
            font-weight: 900;
            color: #808285;
        }

        .single-principle-page .top-nav {
            padding: 2% 0;
            border-bottom: 1px solid #808285;
        }

        .single-principle-page .top-nav.no-bd {
            border-bottom: 0;
        }

        .single-principle-page .nav button {
            margin-top: -5px !important;
        }

        .single-principle-page .nav a,
        .single-principle-page .nav span {
            font-weight: 900;
            color: #0071BC;
        }


        @media (max-width: 991px) {

            .single-principle-page .top-nav {
                text-align: center;
            }

            .single-principle-page .nav button,
            .single-principle-page .nav a,
            .single-principle-page .nav span {
                float: none;
                margin: 0.5% 0.5% 1%;
                text-align: center;
            }
        }

        @media (min-width: 992px) {

            .single-principle-page .nav button,
            .single-principle-page .nav a,
            .single-principle-page .nav span {
                float: right;
                margin: 0 0.5%;
            }

        }

        @media(max-width:767px) {

            .tbl-th {
                color: #0071BC;

                font-weight: 900;
                line-height: 1.5 !important;
                font-size: 1em !important;
                /* width: 100%; */
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
            }

        }

        @media (max-width: 450px) {
            .single-principle-page .nav button {
                display: none;
            }

            .ul-block {
                columns: 1;
                background-color: #F1F2F2 !important;
                padding: 20px;
                color: #0e73b7;
            }

            .ul-block li {
                padding: 8px !important;
            }

            .ol-block {
                columns: 1 !important;
                padding: 20px;
            }
        }

        @media (max-width: 340px) {
            .single-principle-page .nav a {
                display: block;
            }

            .single-principle-page .nav span {
                display: none;
            }

            .ul-block {
                columns: 1;
                background-color: #F1F2F2 !important;
                padding: 20px;
                color: #0e73b7;
            }

            .ul-block li {
                padding: 8px !important;
            }

            .ol-block {
                columns: 1 !important;
                padding: 20px;
            }

        }

        .bold {
            font-weight: 900;
        }

        .principle_content .panel {
            margin: 1% 0;
        }

        .single-principle-page .principle-brief {
            padding-top: 35px;
            padding-bottom: 25px;
            font-size: 1.2em;
        }

        .single-principle-page .principle_content .panel-heading {
            background-color: #F1F2F2 !important;
            padding: 0;
            border-radius: 25px;
        }

        .single-principle-page .principle_content .panel-heading .panel-title {
            width: 100%;
            background: none;
            border: none;
        }

        .single-principle-page .principle_content h6 a,
        .single-principle-page .principle_content h6 a:hover {
            color: #575962;
            border-bottom: 0;
        }

        .single-principle-page .principle_content .panel-body {
            margin: auto 2%;
        }

        .single-principle-page .principle_content strong {
            font-weight: 600;
        }

        .single-principle-page .principle_content li {
            /* color:#0071BC; */
        }

        .single-principle-page .principle_content em {
            font-size: 0.8em;
        }

        .single-principle-page .principle_content img {
            max-width: 100%;
            height: auto;
        }

        .single-principle-page .panel-default>.panel-heading+.panel-collapse {
            border: 0;
        }

        .single-principle-page .panel-group .panel-heading .panel-title {
            font-size: 1em;
            text-transform: capitalize;
            padding: 0.5%;
        }

        .single-principle-page .principle_content a {
            color: #404040;
            font-weight: 600;
            font-size: 15px;
        }

        .single-principle-page .principle_content a:hover {
            color: #404040;
        }

        .single-principle-page .principle_content a strong {
            color: #404040;
            font-weight: 900;
        }

        /* Table styling */

        .single-principle-page .principle_content table {
            margin: 2% auto;
            border-collapse: collapse;
            max-width: 80%;
            width: auto !important;
            border: 0;
        }

        .single-principle-page .principle_content tr:first-of-type td,
        .single-principle-page .principle_content tr:first-of-type td strong {
            color: #ffffff;
            background: #21AA55;
            font-weight: 900;
            line-height: 1.5;
            font-size: 16px;
        }

        .single-principle-page .principle_content table tr:first-child td:first-child {
            border-top-left-radius: 10px;
        }

        .single-principle-page .principle_content table tr:first-child td:last-child {
            border-top-right-radius: 10px;
        }

        .single-principle-page .principle_content table tr:last-child td:first-child {
            border-bottom-left-radius: 10px;
        }

        .single-principle-page .principle_content table tr:last-child td:last-child {
            border-bottom-right-radius: 10px;
        }

        .single-principle-page .principle_content tr {
            border-bottom: 1px solid #e9ecef;
            /* cursor: pointer; */
        }

        .single-principle-page .principle_content tr:first-of-type:hover td {
            background: #21AA55;
        }

        .single-principle-page .principle_content tr:hover td {
            background: #fff;
        }

        .single-principle-page .principle_content table tr:last-child {
            border-bottom: 0;
        }

        .single-principle-page .principle_content table tr td:first-of-type {
            padding-left: 40px;
        }

        .single-principle-page .principle_content td,
        th {
            border: 0;
            line-height: 1.2;
            padding: 12px;
            background-color: #F9F9F9;
        }


        .single-principle-page .principle_content div.table {
            background-color: #F9F9F9;
            border-radius: 15px;
        }


        .single-principle-page .principle_content div.table div.tbl-tr {
            border-bottom: 1px solid #e9ecef;
            padding-bottom: 18px;
            padding-top: 0;
            padding-right: 15px;
            margin: 0;
        }

        .single-principle-page .principle_content div.table div.tbl-tr:hover {
            background: #fff;
        }

        .single-principle-page .principle_content div.table div.tbl-tr div.tbl-th,
        .single-principle-page .principle_content div.table div.tbl-tr div.tbl-td {
            padding-left: 30px;
        }


        .single-principle-page .principle_content div.table div.tbl-tr div.tbl-th {
            padding: 20px 0px 5px 30px;
        }

        .single-principle-page .principle_content div.table div.tbl-tr div.tbl-th,
        .single-principle-page .principle_content div.table div.tbl-tr div.tbl-th strong {
            font-size: 0.9em;
            line-height: 1.2;
            text-transform: uppercase;
        }

        .emoji-wysiwyg-editor::before {
            color: grey;
        }

        .emoji-wysiwyg-editor[data-placeholder]:not([data-placeholder=""]):empty::before {
            content: attr(data-placeholder);
        }

        .emoji-wysiwyg-editor::before {
            content: 'Whats'' new?';
        }

        .main-message .emoji-wysiwyg-editor {
            /* height:150px; */
            /* border: 1px solid #00000029; */
        }

        .lead.emoji-picker-container {
            /* width: 300px; */
            margin-bottom: 0;
            display: block;

            input {
                width: 100%;
                height: 50px;
            }
        }

    </style>


@endsection

@section('content')
    @php
    $details_panels = [['id' => '1', 'name' => 'Activity Feed', 'level' => '0', 'icon' => 'our-reach-icon.svg', 'icon-active' => 'our-reach-icon-active.svg'], ['id' => '2', 'name' => 'Resources', 'level' => '0', 'icon' => 'our-reach-icon.svg', 'icon-active' => 'our-reach-icon-active.svg']];

    $mobile_details_panels = [['id' => '1', 'name' => 'Description', 'level' => '0', 'icon' => 'our-reach-icon.svg', 'icon-active' => 'our-reach-icon-active.svg'], ['id' => '2', 'name' => 'Activity Feed', 'level' => '0', 'icon' => 'our-reach-icon.svg', 'icon-active' => 'our-reach-icon-active.svg'], ['id' => '3', 'name' => 'Resources', 'level' => '0', 'icon' => 'our-reach-icon.svg', 'icon-active' => 'our-reach-icon-active.svg']];
    @endphp
    <div class="background-page">
        @include('site.includes.components.parallax', [
            'image' => asset('images/banners/group_header.png'),
            'text' => $group->title,
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('pages', ['key' => $key]) }}"> Groups</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">{{ $group->title }}</a>
                </li>
            </ol>
        @endcomponent

        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {

                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>
        @else
            {{-- <script>
  window.addEventListener('load', function() {
      if(!window.location.hash) {
          window.location = window.location + '#/';
          window.location.reload();
      }
  });
  </script> --}}
        @endif
        <div class="clearfix">
            <br />
        </div>

        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="col-md-12">
                <div class="row alignment-class-connect ">
                    <div class="col-3">
                        <div id="ppic ">
                            @if ($group->hasImage('cover_image'))
                                @php $image = $group->image("cover_image", "default");@endphp

                                <div class="group-image" style="background-image:url('{{ $image }}')"></div>
                            @else
                                @if (!empty($group->cover_image))
                                    <div class="group-image"
                                        style="background-image:url('{{ asset('Connect_Groups/' . $group->cover_image) }}')">
                                    </div>
                                @else
                                    <div class="group-image"
                                        style="background-image:url('{{ asset('resources/cover_image/default-thumbnail.png') }}')">
                                    </div>
                                @endif
                            @endif

                        </div>


                    </div>

                    <div class="col-9">
                        <div class="profile-details group-details">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-8 no-padding-left">
                                        <h1>{{ $group->title }}</h1>
                                    </div>

                                    <div class="col-4">
                                        @if ($group->created_by == Auth::user()->id)
                                            <my-group-filter requests="{{ json_encode($requests) }}"
                                                groupid="{{ $group->id }}">
                                            </my-group-filter>
                                        @endif
                                    </div>
                                </div>
                                {!! $group->description !!}
                            </div>




                            <div class="container-fluid no-padding-left">
                                <div class="row">
                                    <div class="col-12 d-none d-sm-block pr-3 mt-3">
                                        <div class="card sidebar-card group-card">
                                            <article>

                                                <ul class="nav nav-pills nav-fill session-tabs group-tabs" role="tablist">
                                                    @foreach ($details_panels as $key => $item)
                                                        @if ($key === array_key_first($details_panels))
                                                            <li role="presentation"
                                                                class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right">
                                                                <a href="#home{{ $item['id'] }}" aria-controls="home"
                                                                    role="tab" data-toggle="tab"
                                                                    style="text-align: center;
                                                                                                                              ">
                                                                    <span class="group-icon activity-icon"></span>
                                                                    {!! $item['name'] !!}</a>
                                                            </li>
                                                        @else
                                                            <li role="presentation"
                                                                class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right tabz-spacing">
                                                                <a href="#home{{ $item['id'] }}" aria-controls="home"
                                                                    role="tab" data-toggle="tab"><span
                                                                        class="group-icon resource-icon"></span>
                                                                    {!! $item['name'] !!}</a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>


                                            </article>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div role="tabpanel " class="session_details-panel mt-4">

                                <div class="tab-content">
                                    @foreach ($details_panels as $item)
                                        <div role="tabpanel" class="tab-pane {{ $item['id'] == 1 ? 'active' : '' }}"
                                            id="home{{ $item['id'] }}" class="active">


                                            @if ($item['id'] == 1)
                                                @include(
                                                    'site.includes.components.group_activity'
                                                )
                                            @elseif($item['id'] == 2)
                                                <div id="group_resources" class="single-principle-page">
                                                    <div class="col-12">
                                                        <div class="row">
                                                            <div class="col-9">
                                                                <div class="form-group row error-display"
                                                                    style="display:none">
                                                                    <div class="col-md-12">
                                                                        <div class="form-check">
                                                                            <div class="alert alert-danger alert-dismissible fade print-error-msg "
                                                                                role="alert" id="print-error-upload">
                                                                                <ul></ul>
                                                                                <button type="button" class="close"
                                                                                    data-dismiss="alert" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row success-display"
                                                                    style="display:none">
                                                                    <div class="col-md-12">
                                                                        <div class="form-check">
                                                                            <div class="alert alert-success alert-dismissible fade  print-success-msg"
                                                                                role="alert" id="print-success-upload">

                                                                                <button type="button" class="close"
                                                                                    data-dismiss="alert" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 no-padding-left">
                                                        <div class="panel-group principle_content" id="accordion">
                                                            <?php
                                                            $expand = (int) (app('request')->input('collapse') ?: 0);
                                                            ?>
                                                            @foreach ($resourceTypes as $section)
                                                                <div class="panel panel-default"
                                                                    id="panel{{ $loop->index }}">
                                                                    <div class="panel-heading" role="tab"
                                                                        id="heading{{ $loop->index }}">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse"
                                                                                data-parent="#accordion"
                                                                                href="#collapse{{ $loop->index }}"
                                                                                class="{{ $loop->index === $expand ? '' : 'collapsed' }}">
                                                                                {{ $section->title }}</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapse{{ $loop->index }}"
                                                                        class="panel-collapse collapse {{ $loop->index === $expand ? 'in' : null }}">
                                                                        <div class="panel-body">

                                                                            @if (count($section->groupResource) > 0)
                                                                                <table
                                                                                    style="max-width: 100% !important; width: 1512px;"
                                                                                    class="hidden-xs">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="width: 611px; text-align: center;">
                                                                                                <strong>Name of
                                                                                                    Resource</strong>
                                                                                            </td>
                                                                                            <td
                                                                                                style="width: 683.183px; text-align: center;">
                                                                                                <strong>Description</strong>
                                                                                            </td>
                                                                                            <td
                                                                                                style="width: 325.817px; text-align: center;">
                                                                                                <strong>Source</strong>
                                                                                            </td>
                                                                                        </tr>

                                                                                        {!! $section->getContent() !!}

                                                                                    </tbody>
                                                                                </table>
                                                                            @endif

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach

                                                        </div>
                                                    </div>


                                                    <div class="row justify-content-center">

                                                        <button type="button" class="btn btn-overall btn_save"
                                                            style="flex-grow:0" id="uploadResource" onclick="showUpload()">
                                                            <i class="fa fa-upload" aria-hidden="true"></i> Upload
                                                            Resources</button>


                                                    </div>


                                                </div>

                                                <div id="group_resources_create" style="display:none">
                                                    <div class="col-12">
                                                        <div class="row">
                                                            <div class="col-9"></div>
                                                            <div class="col-3"><button type="button"
                                                                    class="btn btn-overall btn_save" style="float:right"
                                                                    id="uploadResource" onclick="showResources()"> <i
                                                                        class="fa fa-upload" aria-hidden="true"></i> Show
                                                                    Resources</button></div>
                                                        </div>
                                                    </div>
                                                    @include(
                                                        'site.includes.components.group_resources_form'
                                                    )
                                                </div>
                                            @elseif($item['id'] == 3)
                                                Forums
                                            @endif


                                        </div>
                                    @endforeach
                                </div>
                            </div>


                        </div>


                    </div>



                </div>

            </div>
        @else
            <div class="col-md-12">
                <div class="row ">

                    <div class="col-4">
                        <div id="ppic ">
                            @if ($group->hasImage('cover_image'))
                                @php $image = $group->image("cover_image", "default");@endphp

                                <div class="group-image" style="background-image:url('{{ $image }}')"></div>
                            @else
                                @if (!empty($group->cover_image))
                                    <div class="group-image"
                                        style="background-image:url('{{ asset('Connect_Groups/' . $group->cover_image) }}')">
                                    </div>
                                @else
                                    <div class="group-image"
                                        style="background-image:url('{{ asset('resources/cover_image/default-thumbnail.png') }}')">
                                    </div>
                                @endif
                            @endif

                        </div>

                    </div>
                    <div class="col-8 mt-4">
                        <h2>{{ $group->title }}</h2>
                    </div>

                    <div class="col-12">
                        <div class="card sidebar-card group-card group-card-details">
                            <article>


                                <ul class="nav nav-pills nav-fill session-tabs group-tabs " role="tablist">
                                    @foreach ($mobile_details_panels as $key => $item)
                                        @if ($key === array_key_first($mobile_details_panels))
                                            <li role="presentation"
                                                class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right-group">
                                                <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                                    data-toggle="tab"
                                                    style="text-align: center;
                                                                                                              ">{!! $item['name'] !!}</a>
                                            </li>
                                        @else
                                            <li role="presentation"
                                                class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right-group tabz-spacing">
                                                <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                                    data-toggle="tab">{!! $item['name'] !!}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>

                            </article>
                        </div>
                    </div>
                    <div role="tabpanel " class="session_details-panel mt-4">

                        <div class="tab-content">
                            @foreach ($mobile_details_panels as $item)
                                <div role="tabpanel" class="tab-pane {{ $item['id'] == 1 ? 'active' : '' }}"
                                    id="home{{ $item['id'] }}" class="active">
                                    @if ($item['id'] == 1)
                                        <div class="col-12">
                                            <div class="profile-details group-details">
                                                <p>
                                                    {!! $group->description !!}
                                                </p>
                                            </div>

                                        </div>
                                    @elseif($item['id'] == 2)
                                        @include(
                                            'site.includes.components.group_activity'
                                        )
                                    @elseif($item['id'] == 3)
                                        <div id="group_resources" class="single-principle-page">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group row error-display" style="display:none">
                                                            <div class="col-md-12">
                                                                <div class="form-check">
                                                                    <div class="alert alert-danger alert-dismissible fade print-error-msg "
                                                                        role="alert" id="print-error-upload">
                                                                        <ul></ul>
                                                                        <button type="button" class="close"
                                                                            data-dismiss="alert" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row success-display" style="display:none">
                                                            <div class="col-md-12">
                                                                <div class="form-check">
                                                                    <div class="alert alert-success alert-dismissible fade  print-success-msg"
                                                                        role="alert" id="print-success-upload">

                                                                        <button type="button" class="close"
                                                                            data-dismiss="alert" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-12 no-padding-left">
                                                <div class="panel-group principle_content" id="accordion">
                                                    <?php
                                                    $expand = (int) (app('request')->input('collapse') ?: 0);
                                                    ?>
                                                    @foreach ($resourceTypes as $section)
                                                        <div class="panel panel-default" id="panel{{ $loop->index }}">
                                                            <div class="panel-heading" role="tab"
                                                                id="heading{{ $loop->index }}">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                                        href="#collapse{{ $loop->index }}"
                                                                        class="{{ $loop->index === $expand ? '' : 'collapsed' }}">
                                                                        {{ $section->title }}</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse{{ $loop->index }}"
                                                                class="panel-collapse collapse {{ $loop->index === $expand ? 'in' : null }}">
                                                                <div class="panel-body">

                                                                    @if (count($section->groupResource) > 0)
                                                                        <table
                                                                            style="max-width: 100% !important; width: 1512px;"
                                                                            class="hidden-xs">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td
                                                                                        style="width: 611px; text-align: center;">
                                                                                        <strong>Name of Resource</strong>
                                                                                    </td>
                                                                                    <td
                                                                                        style="width: 683.183px; text-align: center;">
                                                                                        <strong>Description</strong>
                                                                                    </td>
                                                                                    <td
                                                                                        style="width: 325.817px; text-align: center;">
                                                                                        <strong>Source</strong>
                                                                                    </td>
                                                                                </tr>

                                                                                {!! $section->getContent() !!}

                                                                            </tbody>
                                                                        </table>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                            <div class="row justify-content-center">

                                                <button type="button" class="btn btn-overall btn_upload "
                                                    id="uploadResource" onclick="showUpload()"
                                                    style="flex-grow: 0; background:#6FC6B8; border-color:#6FC6B8;padding:5px !important;margin:0">
                                                    <i class="fa fa-upload" aria-hidden="true"></i> Upload
                                                    Resources</button>

                                            </div>





                                        </div>

                                        <div id="group_resources_create" style="display:none">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-10"></div>
                                                    <div class="col-2"><button type="button"
                                                            class="btn btn-overall btn_upload " id="uploadResource"
                                                            onclick="showResources()"
                                                            style="flex-grow: 0; background:#6FC6B8; border-color:#6FC6B8;padding:5px !important;margin:0">
                                                            <i class="fa fa-upload" aria-hidden="true"></i> Show
                                                            Resources</button></div>
                                                </div>
                                            </div>
                                            @include(
                                                'site.includes.components.group_resources_form'
                                            )
                                        </div>
                                    @elseif($item['id'] == 4)
                                        Forums
                                    @endif


                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        @endif
    @endsection

    @section('js')
        {{-- <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description');
    CKEDITOR.replace( 'description1' );
    CKEDITOR.replace( 'description2' );
    CKEDITOR.replace( 'description3' );
    CKEDITOR.replace( 'description4' );
</script> --}}
        <script>
            $(function() {
                // Initializes and creates emoji set from sprite sheet
                window.emojiPicker = new EmojiPicker({
                    emojiable_selector: '[data-emojiable=true]',
                    assetsPath: '{{ asset('emojis/lib/img/') }}',
                    popupButtonClasses: 'fa fa-smile-o',
                    position: "bottom"
                });

                window.emojiPicker.discover();
            });
        </script>

        <script>
            function showHide(id) {
                $("#" + id).toggle();
            };

            function showHideReply(id) {
                $("#" + id).toggle();
            };
        </script>
        <script>
            function sendLike(id, data) {
                $.ajax({
                    url: "{{ url('/profile/connect/group/reactions/') }}/" + id,
                    type: "post",
                    dataType: 'json',
                    data: {
                        data,
                        "_token": "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        console.log(response)
                        document.getElementById("like-" + id).innerHTML = response.likes;
                        document.getElementById("dislike-" + id).innerHTML = response.dislikes;
                    },
                    error: function(response) {
                        // console.log(response)
                    }
                });
            };
        </script>
        <script>
            function sendReplyLike(activityId, replyId, data) {
                console.log(activityId, replyId, data)
                $.ajax({
                    url: "{{ url('/profile/connect/group/reply/reacttions/create/') }}/" + activityId + "/" +
                        replyId,
                    type: "post",
                    dataType: 'json',
                    data: {
                        data,
                        "_token": "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        console.log(document.getElementById("dislike-" + replyId))
                        document.getElementById("like-reply-" + replyId).innerHTML = response.likes;
                        document.getElementById("dislike-reply-" + replyId).innerHTML = response.dislikes;
                    },
                    error: function(response) {
                        console.log(response)
                    }
                });
            }
        </script>


        <script>
            function showUpload() {
                $('#group_resources_create').css('display', 'block');
                $('#group_resources').css('display', 'none');
            }

            function showResources() {
                $('#group_resources_create').css('display', 'none');
                $('#group_resources').css('display', 'block');

            }
        </script>


        <script>
            $('#resource_type').on('change', function() {
                var type = $(this).val();
                if (type == 1) {
                    document.getElementById("articles_additional").style.display = "block";
                    document.getElementById("videos_additional").style.display = "none";
                    document.getElementById("audios_additional").style.display = "none";
                    document.getElementById("publications_additional").style.display = "none";


                } else if (type == 2) {
                    document.getElementById("articles_additional").style.display = "none";
                    document.getElementById("videos_additional").style.display = "none";
                    document.getElementById("audios_additional").style.display = "block";
                    document.getElementById("publications_additional").style.display = "none";


                } else if (type == 3) {
                    document.getElementById("articles_additional").style.display = "none";
                    document.getElementById("videos_additional").style.display = "block";
                    document.getElementById("audios_additional").style.display = "none";
                    document.getElementById("publications_additional").style.display = "none";


                } else if (type == 4) {
                    document.getElementById("articles_additional").style.display = "none";
                    document.getElementById("videos_additional").style.display = "none";
                    document.getElementById("audios_additional").style.display = "none";
                    document.getElementById("publications_additional").style.display = "block";


                } else {
                    document.getElementById("additional_text").style.display = "none";
                }

            })
        </script>



        <script>
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#group-upload-form').submit(function(e) {
                    e.preventDefault();

                    console.log('Yes');
                    let formData = new FormData(this);

                    console.log(formData);

                    //   $('#image-input-error').text('');

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('group.resource') }}',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            console.log(data);
                            if ($.isEmptyObject(data.error)) {

                                printSuccessMsg(data.success);
                            } else {
                                printErrorMsg(data.error);
                            }
                        },

                    });
                });

                function printErrorMsg(msg) {
                    $(".print-error-msg").find("ul").html('');
                    $(".success-display").css('display', 'none');
                    $(".error-display").css('display', 'block');
                    $(".print-error-msg").addClass('show');
                    $.each(msg, function(key, value) {
                        $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
                    });

                    var elmnt = document.getElementById("print-error-upload");
                    elmnt.scrollIntoView();
                }


                function printSuccessMsg(msg) {
                    // $(".print-success-msg").find("ul").html('');
                    $(".error-display").css('display', 'none');
                    $(".success-display").css('display', 'block');
                    $(".print-success-msg").addClass('show');
                    $(".print-success-msg").html(msg);


                    document.getElementById("group-upload-form").reset();
                    var elmnt = document.getElementById("print-success-upload");
                    $("#group_resources").load(window.location.href + " #group_resources");
                    $('#group_resources_create').css('display', 'none');
                    $('#group_resources').css('display', 'block');

                    elmnt.scrollIntoView();
                }
            });
        </script>




        <script>
            $(document).ready(function() {
                //Scroll to view
                @if (count($resourceTypes) > $expand)
                    $('html, body').animate({scrollTop:$('#panel{{ $expand }}').position().top}, 'slow');
                @endif
                //Locate all tables
                var tables = $(".single-principle-page .principle_content table");
                //Loop each table
                tables.each(function(index) {
                    var numRows = $(this).find("tr").length;
                    var numCols = $(this).find("td").length / numRows;
                    var data = [];
                    //Loop each row getting all columns and adding to data
                    $(this).find("tr").each(function(iRowNum) {
                        var rowData = [];
                        $(this).find("td").each(function(iColNum) {
                            rowData.push($(this).html());
                        });
                        data.push(rowData);
                    });

                    //Loop creating div
                    //Create table div
                    var divTbl = $("<div class='table visible-xs-block'></div>");
                    var headers;
                    data.forEach(function(tr, trIndex) {
                        if (!trIndex) {
                            headers = tr;
                            return;
                        }
                        //Add table row
                        var divRow = $("<div class='tbl-tr'></div>");
                        //Get elements
                        tr.forEach(function(td, tdIndex) {
                            var divHeader = $("<div class='tbl-th'>" + headers[tdIndex] +
                                "</div>");
                            divRow.append(divHeader);
                            var divData = $("<div class='tbl-td'>" + td + "</div>");
                            divRow.append(divData);
                        });
                        divTbl.append(divRow);
                    });

                    $(this).after(divTbl);
                    $(this).addClass('hidden-xs');
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $('.show-table').removeClass('hidden-xs');

            });


            $(document).ready(function() {

                // Get current page URL
                var url = window.location.href;



                // remove # from URL
                url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

                // remove parameters from URL
                url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

                // select file name
                url = url.split('/')[4];




                // Loop all menu items
                $('.navbar-nav .nav-item').each(function() {

                    // select href
                    var href = $(this).find('a').attr('href');

                    link = href.split('/')[3];


                    // Check filename
                    if (link === 'groups') {

                        // Add active class
                        $(this).addClass('active');
                    }
                });
            });
        </script>

        <script>
            $(document).ready(function() {

                var input = document.getElementById('imageInput');
                var attach = document.getElementById('fileInput');

                var replyInput = document.getElementById('replyimageInput');
                var replyGif = document.getElementById('replygifInput');

                var infoArea = document.getElementById('file-upload-filename');

                var replyInfoArea = document.getElementById('reply-upload-filename');

                input.addEventListener('change', showFileName);
                attach.addEventListener('change', showAttachName);

                // replyInput.addEventListener( 'change', showReplyFileName );

                function showFileName(event) {

                    // the change event gives us the input it occurred in 
                    var input = event.srcElement;

                    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
                    var fileName = input.files[0].name;

                    // use fileName however fits your app best, i.e. add it into a div
                    infoArea.textContent = 'File name: ' + fileName;
                }


                function showAttachName(event) {

                    // the change event gives us the input it occurred in 
                    var input = event.srcElement;

                    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
                    var fileName = input.files[0].name;

                    // use fileName however fits your app best, i.e. add it into a div
                    infoArea.textContent = 'File name: ' + fileName;
                }


                function showReplyFileName(event) {

                    // the change event gives us the input it occurred in 
                    var input = event.srcElement;

                    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
                    var fileName = input.files[0].name;

                    // use fileName however fits your app best, i.e. add it into a div
                    replyInfoArea.textContent = 'File name: ' + fileName;
                }



            });
        </script>

    @endsection
