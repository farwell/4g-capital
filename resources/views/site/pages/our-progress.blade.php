@extends('layouts.app_no_js')

@section('title', 'Background')
@php
use App\Models\Country;

$data = Country::mapData();

@endphp
@section('content')
    <div class="background-page">
        @if ($pageItem->hasImage('hero_image'))
            @php $image = $pageItem->image('hero_image', 'default');
                $text = $pageItem->header_title;
            @endphp
            @include('site.includes.components.parallax', [
                'image' => $image,
                'text' => $text,
            ])
        @endif
        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">{!! $pageItem->title !!}</a>
                </li>
            </ol>
        @endcomponent
        <div class="container-fluid p-0 ">
            {!! $pageItem->renderBlocks(false) !!}

        </div>
    @endsection

    @section('js')
<script src="{{asset('maps/highmaps.js')}}"></script>
<script src="{{asset('maps/africa.js')}}"></script>

{{-- <script src="{{asset('maps/africa.js')}}"></script>
<script src="{{asset('maps/africa.js')}}"></script> --}}
<script> 
var width = $(window).width();
var height = $(window).height();

        var data = [

            @foreach($data as  $key=>$value)
            ['{{$value['code']}}','{{json_encode($value, TRUE)}}'],
            @endforeach
        ];
       var data2 = [...data];

       console.log(data2);
     
        // tooltips create...generate
        $(document).ready(function () {
          var generate_tooltips = function () {
            console.log(this.point);
            var bodies = JSON.parse(this.point.value.replace(/&quot;/g,'"'));
          
            var common_name = bodies.common_name;
            var users = bodies.users;
            var certified = bodies.certified;
            var country_flag = bodies.country_flag;
           // alert(this.point.trans);
    
              var mintype=" ";    
              
              if(common_name === 'Côte d&#039;Ivoire'){
                common_name = "COTE D IVOIRE";
              }

                var flag ="<img src='{{ url('/images/flags') }}"+"/" + country_flag + "' width='20px'/>" ;

                return '<div>'+ flag +' <span>' + common_name  +'</span>' + '<br/> Number of Staff Registered: '+ 
                users + ' <br/> Number of Staff Certified: '+ certified + '</div>'; 
            
             
            
          };

            var maps = Highcharts.mapChart('map', {
                chart: {
                    map: 'custom/africa',
                    backgroundColor: "rgba(0,0,0,0)",
                    events: {
                        load: function() {
                            console.log('loaded')
                        }
                    }
                },

                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },

                mapNavigation: {
                    enabled: false,
                    buttonOptions: {
                        verticalAlign: 'bottom'
                    }
                },

                colorAxis: {
                    min: 0
                },
                tooltip: {
                  formatter: generate_tooltips,
                  backgroundColor: '#333333',
                  borderColor:'#333333',
                  useHTML: true,
                  style: {
                    color: '#ffffff',
                  }
                },


                series: [{
                    data: data.map(elem => {
                        elem.push('#FF6C0D');
                        //console.log(elem);
                        return elem;
                    }),
                    keys: ['hc-key', 'value', 'color'],
                    borderWidth: '1px',
                    borderColor: '#000000',
                    name: '',
                    states: {
                        hover: {
                            color: '#015488'
                        }
                    },
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}'
                    },
                    
                }],
                plotOptions: {
                    series: {
                        point: {
                            events: {
                             
                                click: function () {
                                
                                   location.href = '{{ url("/our-progress") }}'+'/' + this.name;
                                }
                            }
                        }
                    }
                 },
                
                
            });
         
            $('.stats').on('mouseout', function(e) {

            $.each(data2, function(key, value) {
                value[2] = 'white';
            });
            maps.series[0].setData(data2);
            });


            $('#countries').on('mouseenter', function(e) {
                $.ajax({
                    url: '{{ url('/our-progress/countries') }}',
                    type: 'GET',
                    success: function(response) {
                        let list = response;
                        let localData = JSON.parse(JSON.stringify(data));

                        $.each(localData, function(key, value) {
                            if (list.includes(value[0])) {
                                value[2] = "#015488";
                            }
                        });
                       
                         maps.series[0].setData(localData);
                    }
                });
            });

       


        $('#countries').on('mouseleave', function(e) {
                $.ajax({
                    url: '{{ url('/our-progress/countries') }}',
                    type: 'GET',
                    success: function(response) {
                        let list = response;
                        let localData = JSON.parse(JSON.stringify(data));

                        $.each(localData, function(key, value) {
                            if (list.includes(value[0])) {
                                value[2] = "#FF6C0D";
                            }
                        });
                       
                         maps.series[0].setData(localData);
                    }
                });
            });

        });
    </script>



   
@endsection