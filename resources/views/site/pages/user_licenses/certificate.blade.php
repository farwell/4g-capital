<html>

<head>
    <style>
        @font-face {
            font-family: 'Arial';
            src: url('{{ storage_path("fonts/arial.ttf") }}') format("truetype");
            font-weight: normal;
            font-style: normal;
            font-variant: normal;

            font-family: 'CenturyGothic';
            src: url('{{ storage_path("fonts/Century_Gothic.ttf") }}') format("truetype");
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
        }

        @page {
            size: a4 landscape;
            margin: 0;
            padding: 0; // you can set margin and padding 0
        }

        body {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            margin: 0 !important;
            width: 100% !important;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
            font-family: 'CenturyGothic';

        }

        table {
            page-break-inside: auto;
            border: 0;
            font-family: 'CenturyGothic';
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto;
            border: 0;
            font-family: 'CenturyGothic';
        }

        th {
            border: 0;
            font: inherit;
            font-size: 100%;
            margin: 0;
            padding: 0;
            text-align: left;
            color: #1D2F5D;
            font-family: 'CenturyGothic';


        }

        .tableContent img {
            border: 0 !important;
            display: block !important;
            outline: none !important;
        }

        a {
            color: #382F2E;
        }

        a.blend {
            color: #fff;
        }

        p,
        h1,
        h2,
        ul,
        ol,
        li,
        div {
            margin: 0;
            padding: 0;
        }

        h1,
        h2 {
            font-weight: normal;
            background: transparent !important;
            border: none !important;
        }

        .header {
            margin-top: 4rem;
            margin-bottom: 2rem;
        }

        @media only screen and (max-width: 480px) {

            table[class="MainContainer"],
            td[class="cell"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="specbundle"] {
                width: 100% !important;
                float: left !important;
                font-size: 1.5em;
                line-height: 17px !important;
                display: block !important;
                padding-bottom: 15px !important;
            }

            td[class="specbundle2"] {
                width: 80% !important;
                float: left !important;
                font-size: 1.3em;
                line-height: 17px !important;
                display: block !important;
                padding-bottom: 10px !important;
                padding-left: 10% !important;
                padding-right: 10% !important;
            }

            td[class="spechide"] {
                display: none !important;
            }

            img[class="banner"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="left_pad"] {
                padding-left: 15px !important;
                padding-right: 15px !important;
            }

        }

        @media only screen and (max-width: 540px) {

            table[class="MainContainer"],
            td[class="cell"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="specbundle"] {
                width: 100% !important;
                float: left !important;
                font-size: 13px !important;
                line-height: 17px !important;
                display: block !important;
                padding-bottom: 15px !important;
            }

            td[class="specbundle2"] {
                width: 80% !important;
                float: left !important;
                font-size: 13px !important;
                line-height: 17px !important;
                display: block !important;
                padding-bottom: 10px !important;
                padding-left: 10% !important;
                padding-right: 10% !important;
            }

            td[class="spechide"] {
                display: none !important;
            }

            img[class="banner"] {
                width: 100% !important;
                height: auto !important;
            }

            td[class="left_pad"] {
                padding-left: 15px !important;
                padding-right: 15px !important;
            }

        }

        img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            width: auto;
            /* max-width: 100%; */
            clear: both;
            display: block;
            float: none;
        }

        img.logo {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            float: none;
            display: initial;
            padding: 0 20px;
        }

        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;

        }

        .left {
            display: block;
            float: left;
            margin: 2rem;

        }

        .right {
            display: block;
            float: right;
            margin: 5rem;

        }

        img {
            min-width: 100px;
        }

        hr.class-1 {
            border-top: 0.5px solid #ced4da;
        }
    </style>
</head>


<body paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; background-image: url('https://dev.academy.4g-capital.com/images/certificateAssets/CIC-design.png');
    background-size:100% 100%;
    background-repeat: no-repeat;" offset="0" toppadding="0" leftpadding="0">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td style="text-align:center"><img
                                        src="{{ asset('https://dev.academy.4g-capital.com/images/certificateAssets/4g-logo-new.jpg') }}"
                                        style="margin-top:5rem;width:400px;margin-bottom:2%"></td>
                            </tr>
                            <tr>
                                <td style="text-align:center;">
                                    <p style="font-size:60px;letter-spacing: 8px;  font-weight:bold;color:#1B134D;">CERTIFICATE</p>
                                    <p style="font-size:32px;letter-spacing: 6px; font-weight:bold;color:#1B134D;">OF COMPLETION</p><br>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center;">
                                    <span style="font-size:22px;letter-spacing: 4px; color:#1B134D;">This certificate is proudly
                                        presented to</span><br><br>
                                    <span style="font-size:48px; color:#C331B1"><b>{{$name}}</b></span><br>
                                    <hr class="class-1" style="margin-left:34%; width:33%; margin-top:-1px; border:1px solid #1B134D;">
                                    <span style="font-size:20px;color:#1B134D;">As recognition for successfully completing the course </span> <br>
                                    <span style="font-size:20px;color:#1B134D; font-weight:bold;">"{!! wordwrap($course_name, 40,
                                        "<br />\n") !!}"</span><br><br>

                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center;font-size:12px;color:#6f727a"> {{ $cert_number }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td style="text-align:left; width:50%">

                                    <p
                                        style="font-size:20px; height:auto; margin-left:35%;margin-bottom:0;margin-top:2rem; font-weight:bold;">
                                        {{ $date }}</p>
                                    <hr class="class-1" style="margin-left:28%; width:30%;border:1px solid #1B134D;" />
                                    <p style="margin-left:38%; line-height:1em;font-size:20px; font-weight:bold;color:#1B134D;">DATE</p>
                                </td>
                                <td>
                                    <img src="{{ asset('https://academy.4g-capital.com/images/certificateAssets/sign.png') }}"
                                        style="height:auto; margin-left:48%;margin-bottom:0; width:140px; margin-top:2em;">
                                    <hr class="class-1" style="margin-left:45%; width:34%;border:1px solid #1B134D;">
                                    <p style="margin-left:48%; line-height:1em;font-size:18px; color:#1B134D;">Caroline Njeru <br>
                                        Training Manager</p>

                                </td>

                            </tr>
                        </tbody>
                    </table>

                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>