@extends('layouts.app')

@section('title', 'My Courses')
@section('css')
    <style>
        .emoji-wysiwyg-editor::before {
            color: grey;
        }

        .emoji-wysiwyg-editor[data-placeholder]:not([data-placeholder=""]):empty::before {
            content: attr(data-placeholder);
        }

        .emoji-wysiwyg-editor::before {
            content: 'Whats'' new?';
        }

        .main-message .emoji-wysiwyg-editor {
            /* height:150px; */
            /* border: 1px solid #00000029; */
        }

        .lead.emoji-picker-container {
            /* width: 300px; */
            margin-bottom: 0;
            display: block;

            input {
                width: 100%;
                height: 50px;
            }
        }

        .live {
            color: blue;
        }

    </style>

@endsection
@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax',[
        'image'=>asset("images/banners/course_header.png"),
        'text'=>'My Profile'
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">My Training Feed</a>
                </li>
            </ol>
        @endcomponent
        @if (Session::has('redirectMessage'))
            <script>
                jQuery(document).ready(function($) {
                    $("#notificationModal").addClass('show');
                });
            </script>
        @else
            {{-- <script>
        window.addEventListener('load', function() {
            if(!window.location.hash) {
                window.location = window.location + '#training-feed';
                window.location.reload();
            }
        });
      </script> --}}
        @endif

        <div class="profile-row">

            <div class="row">

                <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                    @include('site.includes.components.timeline')

                </div>
            </div>

            @if ((new \Jenssegers\Agent\Agent())->isDesktop())

                <div class="row no-gutters">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                        <div class="container-fluid">

                            <!-- Filter section -->

                            <!-- End of filter section -->
                            <!-- Course list -->
                            <div class="clearfix"><br /></div>
                            <!-- Course list -->
                            <div class="tab-content">
                                <div id="home" class="container-fluid tab-pane active">
                                    <div class="course-card detail-card training-feed">
                                        <article>
                                            <div class="course-content">
                                                <div class="help">
                                                </div>
                                                @if (!count($licenses))
                                                    <div class="text-center col-12">
                                                        <br /><br /><br />

                                                        <p style="text-align:center">
                                                            Follow colleagues and engage each other on your training
                                                            progress.
                                                        </p>

                                                    </div>
                                                @endif

                                                @foreach ($licenses as $license)
                                                    @php
                                                         $configLms = config()->get("settings.lms.live");   
                                                        $name = ucwords($license->user->first_name . ' ' . $license->user->last_name);
                                                        
                                                        if (empty($license->user->profile_pic)) {
                                                            $image = '/images/profile/images.jpg';
                                                        } else {
                                                            $image = '/uploads/' . $license->user->profile_pic;
                                                        }
                                                        if (empty($license->user->roles->title)) {
                                                            $position = 'Position';
                                                        } else {
                                                            $position = $license->user->roles->title;
                                                        }
                                                        
                                                        $unique = uniqid();

                                                        
                                                    @endphp
 
                                                    <training-feed class="d-none d-lg-flex" title="{{ $name }}"
                                                        image="{{ $image }}" course="{{ $license->course->name }}"
                                                        date="{{ Carbon\Carbon::parse($license->enrolled_date)->isoFormat('Do MMMM YYYY') }}"
                                                        position="{{ $position }}"
                                                        link="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->id . '/courseware' }}"
                                                        start="{{ Carbon\Carbon::parse($license->course->start)->isoFormat('Do MMMM YYYY') }}"
                                                        end="{{ Carbon\Carbon::parse($license->course->start)->isoFormat('Do MMMM YYYY') }}"
                                                        enrolled="{{ $license->getEnrollments($license->course->id) }}"
                                                        likes="{{ $license->getLikes($license->user->id, $license->course->id) }}"
                                                        likeaction="{{ $license->getLikeAction($license->user->id, $license->course->id) }}"
                                                        unique="{{ $unique }}" follow="{{ $license->user->id }}"
                                                        courseid="{{ $license->course->id }}"
                                                        followers="{{ $license->getfollowers($license->user->id) }}"
                                                        commentcount="{{ $license->getCommentCount($license->user->id, $license->course->id) }}"
                                                        commentaction="{{ $license->getCommentAction() }}"
                                                       
                                                        csrf="{{ csrf_token() }}"></training-feed>
                                                @endforeach
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            @else
                <div class="row no-gutters">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                        <div class="course-card detail-card training-feed">
                            <article>
                                <div class="course-content">
                                    <div class="help">
                                    </div>
                                    @if (!count($licenses))
                                        <div class="text-center col-12">
                                            <br /><br /><br />

                                            <p style="text-align:center">
                                                Follow colleagues and engage each other on your training
                                                progress.
                                            </p>

                                        </div>
                                    @endif

                                    @foreach ($licenses as $license)
                                        @php
                                             $configLms = config()->get("settings.lms.live");   
                                            $name = ucwords($license->user->first_name . ' ' . $license->user->last_name);
                                            
                                            if (empty($license->user->profile_pic)) {
                                                $image = '/images/profile/images.jpg';
                                            } else {
                                                $image = '/uploads/' . $license->user->profile_pic;
                                            }
                                            if (empty($license->user->roles->title)) {
                                                $position = 'Position';
                                            } else {
                                                $position = $license->user->roles->title;
                                            }
                                            
                                            $unique = uniqid();
                                        @endphp
                                        <training-feed-mobile class="d-block d-lg-none" title="{{ $name }}"
                                            image="{{ $image }}" course="{{ $license->course->name }}"
                                            date="{{ Carbon\Carbon::parse($license->enrolled_date)->isoFormat('Do MMMM YYYY') }}"
                                            position="{{ $position }}" 
                                            link="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->id . '/courseware' }}"
                                            start="{{ Carbon\Carbon::parse($license->course->start)->isoFormat('Do MMMM YYYY') }}"
                                            end="{{ Carbon\Carbon::parse($license->course->start)->isoFormat('Do MMMM YYYY') }}"
                                            enrolled="{{ $license->getEnrollments($license->course->id) }}"
                                            likes="{{ $license->getLikes($license->user->id, $license->course->id) }}"
                                            likeaction="{{ $license->getLikeAction($license->user->id, $license->course->id) }}"
                                            unique="{{ $unique }}" follow="{{ $license->user->id }}"
                                            courseid="{{ $license->course->id }}"
                                            followers="{{ $license->getfollowers($license->user->id) }}"
                                            commentcount="{{ $license->getCommentCount($license->user->id, $license->course->id) }}"
                                            commentaction="{{ $license->getCommentAction() }}"
                                           
                                            csrf="{{ csrf_token() }}">></training-feed-mobile>
                                    @endforeach
                                </div>
                            </article>
                        </div>

                    </div>
                </div>

            @endif

        </div>
    </div>


    </div>
    </div>
    <!-- End of future courses list -->

    <div class="clearfix"><br /></div>
@endsection
