@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="container-fluid ">
        <div class="row justify-content-center">
            <div class="card resource-filter mb-2 mt-4">
                <div class="col-12">
                    <div class="row justify-content-center">


                        <div class="col-12">
                            <form action="{{ route('home') }}" method="POST" class="homeForm">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-1  mt-4 mb-4 col-spacing">
                                        <img src="{{ asset('images/icons/filter_icon.svg') }}"
                                            style="float: right;" />
                                    </div>

                                    <div class="col-7 no-padding-right ">
                                        <div class="row">

                                            <div class="col-3 mt-4 mb-4 ">
                                                <label class="filter_by"> Find courses by</label>
                                            </div>
                                            <div class="col-5">
                                                <select name="category" id="" class="form-control mt-4 mb-4 " required>
                                                    <option value=" ">Department/Job role</option>
                                                    @foreach ($jobRoles as $theme)
                                                        <option value="{{ $theme->id }}"> {{ $theme->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('category')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            </div>

                                            <div class="col-3 col-sm-4  ">
                                        
                                        <input type="text" name="search" class="form-control mt-4 mb-4" placeholder="Search for a course"/>
                                        @error('search')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                     </div>

                                        </div>

                                    </div>

                                    <div class="col-4 ">
                                        <button type="submit"
                                            class="btn btn-overall btn_black_bg mt-4 mb-4">Search</button>

                                        <button type="reset" class="btn btn-overall btn_cancel mt-4 mb-4"
                                            onClick="window.location.href=window.location.href">Reset</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@else
    <div class="container-fluid">

        <div class="card resource-filter mb-3 mt-3">
            <div class="col-12">
                <div class="row">

                    <div class="col-12">
                        <form action="{{ route('home') }}" method="POST">
                            @csrf
                            <div class="row">

                                <div class="col-sm-8 col-xs-12">
                                    <select name="category" id="" class="form-control mt-4 mb-2 minimal" required>
                                        <option value=" ">Department/Job role</option>
                                        @foreach ($jobRoles as $theme)
                                            <option value="{{ $theme->id }}"> {{ $theme->title }}</option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <button type="submit" class="btn btn-overall btn_black_bg mt-4 mb-4">Search</button>
                                    <button type="reset" class="btn btn-overall btn_cancel mt-4 mb-4"
                                        onClick="window.location.href=window.location.href">Reset</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>
    </div>


@endif
