

@php
    $flashMessage = session()->get('site_success');
@endphp
<div class="modal fade" id="ModalMessage" tabindex="-1" role="dialog" aria-labelledby="ModalMessage" aria-hidden="true">
  <div class="modal-dialog notification-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <div class="row">
            <div class="col-12">
              <div class="noti-header"><h4 class="text-center  noti-success-header">CONGRATULATIONS</h4></div>
              <div class="col light">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noti-text noti-success" >
                     You have successfully purchased and enrolled into the module(s). If you purchased a Group License, please check your email for enrollment user codes to be used by your team.
                     If this was a single user license purchase, please click on My Modules then click on continue with course to get started with your training!.
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noti-sbutt" style="text-align: center; margin-top: 10px">
                  @if(isset($flashMessage['action']))
                  <a href="{{ $flashMessage['link'] }}" class="btn btn-action">
                      {{ $flashMessage['action'] }}
                  </a>
                  @else
                  <button type="button" class="btn btn-overall purple"  data-dismiss="modal"  onclick="document.getElementById('ModalMessage').style.display='none'" >Dismiss</button>
                  @endif
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
