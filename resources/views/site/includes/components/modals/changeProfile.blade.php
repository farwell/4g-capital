<div class="modal fade popup-modal" id="changeProfile" tabindex="-1" role="dialog"
            aria-labelledby="edit-header-txt" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="margin-top:15px">

                        <h5 id="edit-header-txt" class="text-center"> Please complete your profile by filling in the
                            below information! </h5>
                    </div>
                    <div class="modal-body">
                        <!-- <img src="{{ asset("/images/thumbs.png") }}" alt="" class="popuperror-img"> -->
                        <form action="" method="post" class="" id="profileChange">
                            {{ csrf_field() }}
                            <div class="form-body">

                                @if(Auth::user())
                                   @if(Auth::user()->authentication_type == 3)
                                    <div class="form-group">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        First Name
                                    </label>
                                    <div class="col-12">
                                        <div class="form-group has-feedback">

                                            <input type="text" name="first_name" id="first_name"
                                                class="form-control m-input "
                                                placeholder="First Name" value="">
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Last Name
                                    </label>
                                    <div class="col-12">
                                        <div class="form-group has-feedback">

                                            <input type="text" name="last_name" id="last_name"
                                                class="form-control m-input "
                                                placeholder="Last Name" value="">
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                       Email
                                    </label>
                                    <div class="col-12">
                                        <div class="form-group has-feedback">

                                            <input type="text" name="secondary_email" id="secondary_email"
                                                class="form-control m-input "
                                                placeholder="Email" value="">
                                        </div>
                                    </div>
                                </div>
                                   @endif
                                @endif

                                <div class="form-group">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Country
                                    </label>
                                    <div class="col-12">
                                        <div class="form-group has-feedback">

                                            <select name="country_id" id="country" class="form-control m-input "
                                                required>
                                                <option value=""> Select Country</option>
                                                @foreach (App\Models\Country::published()->orderByTranslation("title")->get() as $country)
                                                    <option value="{{ $country->id }}">{{ $country->title }}
                                                    </option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Branch
                                    </label>
                                    <div class="col-12">
                                        <div class="form-group has-feedback">
                                            <select class="form-control" name="branch_id" id="branch"
                                                class="form-control m-input " required>
                                                <option value="">Select Branch</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Job Role
                                    </label>
                                    <div class="col-12">
                                        <div class="form-group has-feedback">

                                            <select name="job_role_id" id="job_role" class="form-control m-input "
                                                required>
                                                <option value="">Select Job Role</option>
                                                @foreach (App\Models\JobRole::orderByTranslation("title")->get() as $role)
                                                    <option value="{{ $role->id }}">{{ $role->title }}</option>
                                                @endforeach

                                            </select>
                                            <div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-text-input" class="col-4 col-form-label">
                                        Gender
                                    </label>
                                    <div class="col-12">

                                        <div class="form-group has-feedback">

                                            <select name="gender_id" id="profile-gender"
                                                class="form-control m-input " required>
                                                <option value=""> Select Gender</option>
                                                @foreach (App\Models\Gender::all() as $gender)
                                                    <option value="{{ $gender->id }}">{{ $gender->title }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-12 text-center">
                                            <button type="submit" class="btn btn-overall btn_black_bg "
                                                id="btn-savePass">
                                                Save
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>
        </div>
