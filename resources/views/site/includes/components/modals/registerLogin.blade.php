   
<div class="modal fade bd-example-modal-lg" id="registerLoginModal" tabindex="-1" role="dialog" aria-labelledby="registerLoginModal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content error-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="registerLoginModalLabel">Please register or Sign in to continue</h5>
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
               Don't worry its painless. You'll enjoy access to our courses by joining the Erevuka community.
            </div>
            <div class="modal-footer">
                <a href="{{ route('user.login') }}" class="dismiss-modal modal-accept">
                    Register/Login
                </a>
                <button type="button" class="btn dismiss-modal modal-accept"  data-dismiss="modal">Dismiss</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

