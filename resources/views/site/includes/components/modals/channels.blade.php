<div class="modal fade" id="channelsModal" tabindex="-1" role="dialog" aria-labelledby="channelsModal" aria-hidden="true">

  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div id="first_form">
        @if(isset($fullAmount)&& isset($invoice_number))
      <form method="POST" action="{{route('ipay.pay')}}" class="grey" id="pay-form">
          @csrf
          <input type="hidden" id="invoice" class="form-control" name="invoice_number" value="{{$invoice_number}}"/>
          <input type="hidden" id="fullamount" class="form-control" name="fullAmount" value="{{$tots}}" />
            <div class="checkout-panel">
        <div class="panel-body">
          <h2 class="title">Cart</h2>

          <div class="progress-line">
            <div class="step active"></div>
            <div class="step "></div>
            <div class="step"></div>
          </div>
          <div class="payment-method">
            <div class="row">

              <div class="col-md-6">
              <label for="PhoneNumber" class="text-purple">Kindly enter the phone number that your payment confirmation should be sent through for verification.</label>

            <input type="text" id="phone" class="form-control" name="phone" style="border-radius:10px;"  placeholder="0712345678" />

            <small id="error_message" style="color:red"> </small>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group flex-group1" style="align-items:center">
            <div style="width: 23%;display: flex;">
              <button class="btn btn-primary btn-primary-lg" type="submit" id="pricing-form-submit" style="background-color:#23245f;" onclick="paymentSubmit()">{{ __('Submit') }}</button>&nbsp;&nbsp;
              <button type="button" class="btn btn-outline-primary btn-outline-primary-lg" data-dismiss="modal">Close</button>
            </div>
        </div>
        </div>

        </form>

        @endif
        </div>


      </div>

    </div>
  </div>
</div>




@section('js')

<script type="text/javascript">

function paymentSubmit() {

  $('#error_message').empty();

  if($('#phone').val() == ''){

    $('#error_message').append('Please fill in the phone number before submitting');

  }else{

    $('#pay-form').submit();
  }
}
</script>
@endsection
