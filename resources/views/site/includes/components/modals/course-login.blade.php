<!-- Modal -->
<div class="modal fade notification" id="companyPurchase" tabindex="-1" role="dialog" aria-labelledby="companyPurchaseLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content error-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="notificationModalLabel">Sorry</h5>
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
                It seems your email domain doesn't match the email domains submitted for your company license or your company doesn't have a license to access this course. Kindly check with your HR Manager. 
                Interested in taking this course?
                Please send your request to <a href="mailto:{{ env('SUPPORT_EMAIL') }}">
                    {{ env('SUPPORT_EMAIL') }}
                </a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-confirm" id="closeBtn" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>