

<section class="general-section">
    <div class="col-md-12 ">
		<div class="hrHeading">
			<h2  class="header-with-line about-sidebar-title"> Blog </h2>
            <hr>
		</div>
		<div class="row">
			<div class="col-lg-12">
                @foreach($blogs as $item)
				<div class="card upcoming-card mb-3 blog-content"" >
                    <div class="row g-0">
                      <div class="col-md-4 no-padding-right">
                        @if( $item->hasImage('blog_image'))
                        <img src="{{$item->image("blog_image", "default")}}" alt="{{$item->title}}" class="img-fluid upcoming-image"/>
                        @endif
                      </div>
                      <div class="col-md-8 no-padding-left">
                        <div class="card-body">
                          <h5 class="card-title">{{ $item->title}}</h5>
                          <p class="card-text">
                            <?php
                                $str = $item->description;
                                if (strlen($str) > 60) {
                                    $str = substr($str, 0, 60) . '...';
                                }
                                echo $str;
                                ?>
                          </p>
                          
         
                          <p class="readmore ">
                            <span> <a href="{{route('blog.details',[$item->id])}}" class="btn btn-overall btn_more_details">Read More </a></span> 
                          </p>
                  
                        </div>
                      </div>
                    </div>
                  </div>
            @endforeach
			</div>
		</div>
	</div>

</section>