<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css">
    
    <!-- with v4.1.0 Krajee SVG theme is used as default (and must be loaded as below) - include any of the other theme CSS files as mentioned below (and change the theme property of the plugin) -->
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.css"
        media="all" rel="stylesheet" type="text/css" />

    <!-- important mandatory libraries -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
 

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>


    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <link href="{{ asset('css/mobile.css') }}" rel="stylesheet" media="screen and (max-width: 840px)">
    @endif
 
    <style>
        #app {
            height:77vh;
            margin: 0;
        }
    @media (max-width: 1444px) {
        #app {
            height: 85vh;
            margin: 0;
        }
    }
    @media (max-width: 1368px) {
        #app {
            height: 87vh;
            margin: 0;
        }
    }
    @media (max-width:1350px){
        #app {
        height: 89vh;
        margin: 0;
        }
    }
     @media (max-width: 1281px) {
        #app {
            height: 81vh;
            margin: 0;
        }
     }

     @media(max-width:768px){
        .sticky-top{
            padding-bottom: 40px;
        }
     }
     
    </style>
 <script>(function(d){var s = d.createElement("script");s.setAttribute("data-account", "kyz0QDqj5Z");s.setAttribute("src", "https://cdn.userway.org/widget.js");(d.body || d.head).appendChild(s);})(document)</script><noscript>Please ensure Javascript is enabled for purposes of <a href="https://userway.org">website accessibility</a></noscript>
    @yield('css')
</head>

<body oncontextmenu="return false;">
    <div id="app">

        @include('layouts.partials._header')

        @if (Session::has('course_success'))
        <script>
            jQuery(document).ready(function($) {

                $("#CourseSuccess").addClass('show');
            });
        </script>
    @elseif(Session::has('course_errors'))
        <script>
            jQuery(document).ready(function($) {
                $("#CourseErrors").addClass('show');
            });
        </script>

   
    @endif

        @include('site.includes.components.modals.courseerrors')
        @include('site.includes.components.modals.coursemessages')
        @include('site.includes.components.modals.contact-form')
        @include('site.includes.components.modals.ticket')
        @include('site.includes.components.modals.tour')
        @yield('content')

        <!-- Back to top button -->

        @include('layouts.partials._footer')

        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <a id="tour_videos" data-toggle="modal" data-target="#tourModal"><span>Learn to navigate this site</span></a>
        @endif
    </div>
    <script src="{{ asset('assets/jquery-ui-1.12.1/external/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/jquery-ui-1.12.1/jquery-ui.js') }}"></script>
    <script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    <script src="{{ asset('emojis/lib/js/nanoscroller.min.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/tether.min.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/config.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/util.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/jquery.emojiarea.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/emoji-picker.js') }}"></script>

    <script src="{{ asset('js/upload.js') }}"></script>
    <script src="{{ asset('js/side-menu.js') }}"></script>

    @yield('js')

    <script>
        function makePayment() {

            $('#channelsModal').modal('show');

        }
    </script>
    <script>
        $(document).on("click", ".confrimpayment", function(e) {

            e.preventDefault();

            var _self = $(this);

            var url = _self.data('val');
            _self.css('display', 'none');
            window.open(url, '_self');
        });
    </script>
    <script>
        function confirmTransaction() {
            $('#img').show();
            setTimeout(function() {
                $('#img').hide();
                /*submit the form after 5 secs*/
                $('#transaction').submit();
            }, 15000)


        }



        $(function() {

            $('#navbarSupportedContent')
                .on('shown.bs.collapse', function() {
                    $('.navbar-toggler-icon').addClass('hidden');
                    $('#navbar-close').removeClass('hidden');
                })
                .on('hidden.bs.collapse', function() {
                    $('.navbar-hamburger').removeClass('hidden');
                    $('#navbar-close').addClass('hidden');
                });

        });
    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PRD4JKF" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


    <script>
        PreventIllegalKeyPress = function (e) {
      if (e.target) t = e.target; //Firefox and others
      else if (e.srcElement) t = e.srcElement; //IE
      
      if (e.keyCode == 116) { //prevent F5 for refresh
          e.preventDefault();
      }
      if (e.keyCode == 123) { //prevent F12 for inspect
          e.preventDefault();
      }
      if (e.keyCode == 122) { //F11 leave fullscreen
          e.preventDefault();
      } else if (e.altKey && e.keyCode == 115) { //Alt + F4
          e.preventDefault();
      } else if (e.altKey && e.keyCode == 37) { //Alt + left
          e.preventDefault();
      } else if (e.altKey && e.keyCode == 39) { //Alt + right
          e.preventDefault();
      } else if (e.ctrlKey && e.keyCode == 82) { //Ctrl + R (reload)
          e.preventDefault();
      }
      };
      
      
      $(document).keydown(function (e) {
      
      PreventIllegalKeyPress(e);
      
      });
      </script>
</body>

</html>
