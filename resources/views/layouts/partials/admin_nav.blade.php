@php
$activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
    <div class="container">
        @if($activeRoute == 'admin.courses.index')
        <ul class="nav__list">
            <li class="nav__item {{ $activeRoute == 'admin.courses.index' ? 's--on' : '' }}">
                <a href="{{ route('admin.courses.index') }}" class="">
                    Courses
                </a>
            </li>

            <li class="nav__item {{ $activeRoute == 'admin.courseCategories.index' ? 's--on' : '' }}">
                <a href="{{ route('admin.courseCategories.index') }}" class="">
                    Course Categories
                </a>
            </li>
        </ul>
        @elseif($activeRoute == 'admin.user-evaluations.index')
        <ul class="nav__list">
            <li class="nav__item {{ $activeRoute == 'admin.user-evaluations.index' ? 's--on' : '' }}">
                <a href="{{ route('admin.user-evaluations.index') }}" class="">
                    User Evaluations
                </a>
            </li>
            <li class="nav__item {{ $activeRoute == 'admin.evaluation.evaluations.index' ? 's--on' : '' }}">
                <a href="{{ route('admin.evaluation.evaluations.index') }}" class="">
                    Evaluation Questions
                </a>
            </li>
            <li class="nav__item {{ $activeRoute == 'admin.evaluation.evaluationOptions.index' ? 's--on' : '' }}">
                <a href="{{ route('admin.evaluation.evaluationOptions.index') }}" class="">
                    Evaluation Options
                </a>
            </li>
        </ul>
        @endif
    </div>
</nav>
