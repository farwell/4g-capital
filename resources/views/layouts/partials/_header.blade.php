<?php
use App\Models\Menus;

$mainmenus = Menus::where('menu_type', 1)->orWhere('menu_type',4)
    ->published()
    ->orderBy('position', 'asc')
    ->get();
$sidemenus = Menus::where('menu_type', 2)
    ->published()
    ->orderBy('position', 'asc')
    ->get();

$startmenus = Menus::where('menu_type',4)
    ->published()
    ->orderBy('position', 'asc')
    ->get();
?>

<nav class="navbar sticky-top navbar-expand-md navbar-light bg-orange shadow-sm">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>

            <div id="navbar-close" class="hidden">
                <span class="glyphicon glyphicon-remove"></span>
            </div>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('images/icons/logo.png') }}" alt="{{ config('app.name', 'Laravel') }}">
        </a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->

            <div style="float:right">
                <ul class="navbar-nav mr-auto ">

                </ul>
            </div>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                @if (Auth::check())
                    {{-- @php dd(Request::url()); @endphp --}}
                    {{-- <li class="nav-item">
                      @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                      <a class="nav-link active" href="{{route($el['route'])}}"> {{$el['title']}}</a>
                      @else
                      <a class="nav-link" href="{{route($el['route'])}}"> {{$el['title']}}</a>
                      @endif
                    </li> --}}
                    <?php foreach ($mainmenus as $el) : ?>
                    <li class="nav-item">
                        @if (Request::segment(1) == $el['key'] || strpos(\Route::current()->getName(), $el['key']) !== false)
                            <a class="nav-link active" href="{{ route('pages', ['key' => $el['key']]) }}">
                                {{ $el['title'] }}</a>
                        @else
                            <a class="nav-link" href="{{ route('pages', ['key' => $el['key']]) }}">
                                {{ $el['title'] }}</a>
                        @endif
                    </li>
                    <?php endforeach; ?>

                 @else
                 <?php foreach ($startmenus as $el) : ?>
                 <li class="nav-item">
                     @if (Request::segment(1) == $el['key'] || strpos(\Route::current()->getName(), $el['key']) !== false)
                         <a class="nav-link active" href="{{ route('first', ['key' => $el['key']]) }}">
                             {{ $el['title'] }}</a>
                     @else
                         <a class="nav-link" href="{{ route('first', ['key' => $el['key']]) }}">
                             {{ $el['title'] }}</a>
                     @endif
                 </li>
                 <?php endforeach; ?>


                @endif
                <!-- Notifications -->
                @if (!Auth::check())
                @else
                    @php $notify= array(); @endphp
                    @foreach (auth()->user()->unreadNotifications as $notification)
                        @if ($notification->data['type'] == 1)
                            @php $notify[] = $notification; @endphp
                        @endif
                    @endforeach
                    <li class="nav-item dropdown no-arrow  notification">
                        @if ($notify)
                            <a style="padding-top: 8px;" class="nav-link " href="#" id="alertsDropdown"
                                role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                title="Your notifications will show here">

                                <span class="header-icon icon-notifications"></span>


                                <!-- Counter - Alerts -->

                                <span class="badge badge-danger badge-counter">{{ count($notify) }}</span>

                            </a>
                        @else
                            <a style="padding-top: 8px;" class="nav-link " href="#">

                                <span class="header-icon icon-notifications"></span>


                                <!-- Counter - Alerts -->

                                <span class="badge badge-danger badge-counter"></span>
                            </a>
                        @endif
                        @if ($notify)
                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in notifications-menu"
                                aria-labelledby="alertsDropdown" id="noti-drop">
                                <span class="dropdown-menu-arrow"></span>
                                <h6 class="dropdown-header">
                                    Notifications
                                </h6>
                                @foreach ($notify as $notification)
                                    @if ($notification->data['type'] == 1)
                                        <a class="dropdown-item d-flex align-items-center dropdown-wrap"
                                            href="{{ route('single.notification', [$notification->id]) }}"
                                            title="Click to View">
                                            <div class="mr-3">
                                                <div class="icon-circle bg-primary">
                                                    <i class="fas fa-file-alt text-white"></i>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="small text-gray-500">
                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                </div>
                                                <span class="font-weight-bold">{{ $notification->data['title'] }}
                                                </span>
                                            </div>
                                        </a>
                                    @elseif($notification->data['type'] == 2)
                                        <a class="dropdown-item d-flex align-items-center dropdown-wrap"
                                            href="{{ route('single.notification', [$notification->id]) }}"
                                            title="Click to View">
                                            <div class="mr-3">
                                                <div class="icon-circle bg-primary">
                                                    <i class="fas fa-file-alt text-white"></i>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="small text-gray-500">
                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                </div>
                                                <span class="font-weight-bold">{{ $notification->data['data'] }}
                                                    {{ $notification->data['type'] }}</span>
                                            </div>
                                        </a>
                                    @elseif($notification->data['type'] == 4)
                                        <a class="dropdown-item d-flex align-items-center dropdown-wrap"
                                            href="{{ route('single.notification', [$notification->id]) }}"
                                            title="Click to View">
                                            <div class="mr-3">
                                                <div class="icon-circle bg-primary">
                                                    <i class="fas fa-file-alt text-white"></i>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="small text-gray-500">
                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                </div>
                                                <span class="font-weight-bold">{{ $notification->data['data'] }}
                                                    {{ $notification->data['type'] }}</span>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach

                                {{-- <a class="dropdown-item text-center small text-gray-500"
                                    href="{{ route('markAsRead') }}">Mark all as read</a> --}}
                            </div>
                        @else
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="alertsDropdown" id="noti-drop">

                                <span class="dropdown-menu-arrow"></span>
                                <h6 class="dropdown-header">
                                    No new notification
                                </h6>
                            </div>
                        @endif

                    </li>
                @endif
                <!-- End Notifications -->


                <!-- Authentication Links -->
                @if (!Auth::check())
                    {{-- <li class="nav-item section-auth-2">

                        @if (Route::current()->getName() === 'register')
                            <button type="button" class="btn btn-overall btn_register_active"
                                onclick="location.href='{{ route('register') }}'">Register</i></button>
                        @else
                            <button type="button" class="btn btn-overall btn_register"
                                onclick="location.href='{{ route('register') }}'">Register</i></button>

                        @endif
                    </li> --}}
                    <li class="nav-item section-auth-2">

                        @if (Route::current()->getName() === 'login')
                            <button type="button" class="btn btn-overall btn_login_active"
                                onclick="location.href='{{route('login') }}'">Sign in </button>
                        @else
                            <button type="button" class="btn btn-overall btn_login"
                                onclick="location.href='{{route('login') }}'">Sign in </button>
                        @endif
                    </li>
                @else
                    <li class="nav-item dropdown profile-dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @if (Auth::user()->profile_pic != null)
                                @php $ppic = Auth::user()->profile_pic; @endphp
                            @else
                                @php $ppic = 'images.jpg'; @endphp
                            @endif
                            <img src="{{ asset('uploads/' . $ppic) }}" alt="{{ Auth::user()->name }}"> &nbsp;
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right profile-menu" aria-labelledby="navbarDropdown">
                            <span class="dropdown-menu-arrow"></span>

                            @foreach ($sidemenus as $side)
                                <a class="dropdown-item"
                                    href="{{ route($side['key']) }}">{{ $side['title'] }}</a>
                            @endforeach

                            <a class="dropdown-item menu-breaker" href=""><span></span></a>
                            <a class="dropdown-item" href="{{ route('profile.edit') }}/">Edit Profile</a>
                            @if ((Auth::user()->is_admin && Auth::user()->role === 'ADMIN') || (Auth::user()->is_admin && Auth::user()->role == 'SUPERADMIN') )
                                <a class="dropdown-item" href="/admin" target="_blank">Admin Section</a>
                            @endif
                            @if (Auth::user()->is_admin && Auth::user()->role === 'HR')
                                <a class="dropdown-item " href="/admin" target="_blank">HR Dashboard</a>
                            @endif
                            <a class="dropdown-item " href="{{ route('logout') }}/" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                <span>{{ __('Logout') }}</span>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                style="display: none;">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
