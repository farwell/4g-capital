<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->


    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <!-- Fonts -->
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <script>(function(d){var s = d.createElement("script");s.setAttribute("data-account", "kyz0QDqj5Z");s.setAttribute("src", "https://cdn.userway.org/widget.js");(d.body || d.head).appendChild(s);})(document)</script><noscript>Please ensure Javascript is enabled for purposes of <a href="https://userway.org">website accessibility</a></noscript>
    @yield('css')
</head>

<body oncontextmenu="return false;">
    <div id="app">



        @include('layouts.partials._header')

        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {

                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>
        @endif

        @include('site.includes.components.modals.courseerrors')
        @include('site.includes.components.modals.coursemessages')
        @include('site.includes.components.modals.contact-form')
        @include('site.includes.components.modals.tour')
        <main>
        @yield('content')
        </main>
        <!-- Back to top button -->

        @include('layouts.partials._footer')
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <a id="tour_videos" data-toggle="modal" data-target="#tourModal"><span>Learn to navigate this site</span></a>
        @endif
    </div>
    @yield('js')
    <script>
        var $star_rating = $('.star-rating .fa');

        var SetRatingStar = function() {
            return $star_rating.each(function() {
                if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data(
                        'rating'))) {
                    return $(this).removeClass('fa-star-o').addClass('fa-star');
                } else {
                    return $(this).removeClass('fa-star').addClass('fa-star-o');
                }
            });
        };

        $star_rating.on('click', function() {
            console.log($star_rating.siblings('input.rating-value').val($(this).data('rating')));
            $star_rating.siblings('input.rating-value').val($(this).data('rating'));
            return SetRatingStar();
        });

        SetRatingStar();
        $(document).ready(function() {

        });
    </script>
    <!-- <script src="{{ asset('js/app.js') }}" type="text/javascript"></script> -->

    <script>
        PreventIllegalKeyPress = function (e) {
      if (e.target) t = e.target; //Firefox and others
      else if (e.srcElement) t = e.srcElement; //IE
      
      if (e.keyCode == 116) { //prevent F5 for refresh
          e.preventDefault();
      }
      if (e.keyCode == 123) { //prevent F12 for inspect
          e.preventDefault();
      }
      if (e.keyCode == 122) { //F11 leave fullscreen
          e.preventDefault();
      } else if (e.altKey && e.keyCode == 115) { //Alt + F4
          e.preventDefault();
      } else if (e.altKey && e.keyCode == 37) { //Alt + left
          e.preventDefault();
      } else if (e.altKey && e.keyCode == 39) { //Alt + right
          e.preventDefault();
      } else if (e.ctrlKey && e.keyCode == 82) { //Ctrl + R (reload)
          e.preventDefault();
      }
      };
      
      
      $(document).keydown(function (e) {
      
      PreventIllegalKeyPress(e);
      
      });
      </script>
</body>

</html>
