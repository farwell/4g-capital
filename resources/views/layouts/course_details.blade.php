<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" type="text/javascript"></script>
    
  
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>

    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <link href="{{ asset('css/mobile.css') }}" rel="stylesheet" media="screen and (max-width: 840px)">
    @endif

    <script>(function(d){var s = d.createElement("script");s.setAttribute("data-account", "kyz0QDqj5Z");s.setAttribute("src", "https://cdn.userway.org/widget.js");(d.body || d.head).appendChild(s);})(document)</script><noscript>Please ensure Javascript is enabled for purposes of <a href="https://userway.org">website accessibility</a></noscript>
    @yield('css')

    
</head>


<body oncontextmenu="return false;">
    <div id="app">
        @include('layouts.partials._header')

        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {

                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>

       
        @endif

      

        @include('site.includes.components.modals.courseerrors')
        @include('site.includes.components.modals.coursemessages')
        @include('site.includes.components.modals.contact-form')
        @include('site.includes.components.modals.notification')
        @include('site.includes.components.modals.ticket')
        @include('site.includes.components.modals.tour')
        <main>
        @yield('content')
      </main>
        <!-- Back to top button -->

        @include('layouts.partials._footer')
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <a id="tour_videos" data-toggle="modal" data-target="#tourModal"><span>Learn to navigate this site</span></a>
        @endif
    </div>
    <script src="{{ asset('assets/jquery-ui-1.12.1/external/jquery/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/jquery-ui-1.12.1/jquery-ui.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


    <script src="{{ asset('emojis/emoji-picker-main/lib/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/util.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/jquery.emojiarea.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/emoji-picker.js') }}" type="text/javascript"></script>

    @yield('js')


    <script>
        PreventIllegalKeyPress = function (e) {
      if (e.target) t = e.target; //Firefox and others
      else if (e.srcElement) t = e.srcElement; //IE
      
      if (e.keyCode == 116) { //prevent F5 for refresh
          e.preventDefault();
      }
      if (e.keyCode == 123) { //prevent F12 for inspect
          e.preventDefault();
      }
      if (e.keyCode == 122) { //F11 leave fullscreen
          e.preventDefault();
      } else if (e.altKey && e.keyCode == 115) { //Alt + F4
          e.preventDefault();
      } else if (e.altKey && e.keyCode == 37) { //Alt + left
          e.preventDefault();
      } else if (e.altKey && e.keyCode == 39) { //Alt + right
          e.preventDefault();
      } else if (e.ctrlKey && e.keyCode == 82) { //Ctrl + R (reload)
          e.preventDefault();
      }
      };
      
      
      $(document).keydown(function (e) {
      
      PreventIllegalKeyPress(e);
      
      });
      </script>

</body>

</html>
