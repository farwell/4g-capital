@php
	$activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
	<div class="container">
    <ul class="nav__list">
		<li class="nav__item {{$activeRoute == 'admin.courseCreations.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.courseCreations.index') }}" class="">
                {{ __('Course Creation') }}
            </a>
        </li>

      

    </ul>
	</div>
</nav>
