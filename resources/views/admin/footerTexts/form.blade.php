@extends('twill::layouts.form')

@section('contentFields')
@formField('wysiwyg', [
    'name' => 'description',
    'label' => 'Description',
    'placeholder' => 'Text',
    'maxlength' => 2000,
    'translated' => true,
])
@stop
