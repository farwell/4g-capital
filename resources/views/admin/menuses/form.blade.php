@extends('twill::layouts.form')

@section('contentFields')
@formField('input', [
		'name' => 'key',
		'label' => 'Key',
		'maxlength' => 100
	])
@formField('select', [
	'name' => 'menu_type',
	'label' => 'Menu Type',
	'placeholder' => 'Select Menu Type',
	'options' => collect($menuTypeList ?? ''),
])
@formField('select', [
	'name' => 'parent_id',
	'label' => 'Parent Menu',
	'placeholder' => 'Select Parent',
	'options' => collect($menuList ?? ''),
])
    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'translated' => true,
        'maxlength' => 100
    ])
@stop
