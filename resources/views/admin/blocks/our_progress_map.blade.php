@formField('input', [
'name' => 'title',
'label' => 'Title',
'maxlength' => 250,
'translated' => true,
])

@formField('wysiwyg', [
    'name' => 'description',
	'label' => 'Description',
	'maxlength' => 1000,
    'translated' => true,
])
