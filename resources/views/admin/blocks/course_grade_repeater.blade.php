
@twillRepeaterTitle('Grades')
@formField('input', [
    'name' => 'type',
    'label' => 'Assignment Type Name',
    'maxlength' => 200,
    'note' => 'The general category for this type of assignment, for example, Homework or Midterm Exam. This name is visible to learners.',
    ])

        @formField('input', [
        'name' => 'short_label',
        'label' => 'Abbreviation',
        'maxlength' => 200,
        'note' => "This short name for the assignment type (for example, HW or Midterm) appears next to assignments on a learner's Progress page.",
        ])

        @formField('input', [
        'name' => 'weight',
        'label' => 'Weight of Total Grade',
        'note' => "The weight of all assignments of this type as a percentage of the total grade, for example, 40. Do not include the percent symbol.",
        ])

      

