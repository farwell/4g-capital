@extends('twill::layouts.form')

@section('contentFields')
@formField('select', [
	'name' => 'user',
	'label' => 'Select User',
	'placeholder' => 'Select User',
    'min'=> 1,
	'options' => collect($userList ?? ''),
])

@formField('multi_select', [
	'name' => 'courses',
	'label' => 'Select Mandatory Course for User',
	'placeholder' => 'Select Course',
    'min'=> 1,
    'unpack' => false,
	'options' => collect($courseList ?? ''),
])
@stop
