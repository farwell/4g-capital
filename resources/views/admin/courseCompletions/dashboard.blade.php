
@php
    use Carbon\Carbon;
    $month = [];
    for ($m = 1; $m <= 12; $m++) {
        $month[] = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
    }

    $year = date("Y");
    $previousyear = $year -1;
    
    $years = range($previousyear, $previousyear + 1);
    //$years= range(now()->year, now()->year + 5);
@endphp
@extends('layouts.reports')
@push('extra_css')
	<link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
	<style>
		.report_visual {
			margin-bottom: 30px;
		}
        .container {
            width: 100% !important;
        }

        .buttons {
        min-width: 310px;
        text-align: center;
        margin-bottom: 1.5rem;
        font-size: 0;
    }

    .buttons button {
        cursor: pointer;
        border: 1px solid silver;
        border-right-width: 0;
        background-color: #f8f8f8;
        font-size: 1rem;
        padding: 0.5rem;
        transition-duration: 0.3s;
        margin: 0;
        margin-top: 15px;
        margin-bottom: 30px;
    }

    .buttons button:first-child {
        border-top-left-radius: 0.3em;
        border-bottom-left-radius: 0.3em;
    }

    .buttons button:last-child {
        border-top-right-radius: 0.3em;
        border-bottom-right-radius: 0.3em;
        border-right-width: 1px;
    }

    .buttons button:hover {
        color: white;
        background-color: rgb(158 159 163);
        outline: none;
    }



    .buttons button.active {
        background-color: rgb(247, 163, 92);
        color: white;
    }

	</style>

	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
<div class="listing">

	<div id="container-fluid">
        <div class="row">
            <div class="col-6">
				<div class="report_visual" id="registrationDistribution" style="height: 500px">

				</div>
			</div>

			<div class="col-6">
				<div class="report_visual" id="userDistribution" style="height: 500px">

				</div>
			</div>


      <div class="col-6">
                    <div class="buttons col-12 " style="background-color:#ffff;display:grid">
                        <div class="mt-4 mb-2 ">
                        
                            <select name="year" id="completion-year" class="form-control  minimal col-3" style="float: right">
                                <option value="0">To date</option>
                                @foreach ($years as $key => $y)
                                    @if ($y === Carbon::now()->format('Y'))
                                        <option value="{{ $y }}" selected> {{ $y }}</option>
                                    @else
                                        <option value="{{ $y }}"> {{ $y }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <label style="float: right;margin-right:1rem; margin-top:8px; color:#000" >Filter by year</label>
                        </div>

                        <div class="report_visual" id="completion_yearly_container" style="height: 500px">

                        </div>

                    </div>

                </div>

                <div class="col-6">
                    <div class="buttons col-12 " style="background-color:#ffff;display:grid">
                        <div class="mt-4 mb-2 ">
                        
                            <select name="year" id="dashboard-year" class="form-control  minimal col-3" style="float: right">
                                <option value="0">To date</option>
                                @foreach ($years as $key => $y)
                                    @if ($y === Carbon::now()->format('Y'))
                                        <option value="{{ $y }}" selected> {{ $y }}</option>
                                    @else
                                        <option value="{{ $y }}"> {{ $y }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <label style="float: right;margin-right:1rem; margin-top:8px; color:#000" >Filter by year</label>
                        </div>

                        <div class="report_visual" id="yearly_container" style="height: 500px">

                        </div>

                    </div>

                </div>

        <div class="col-6">
                <div class="report_visual" id="genderDistribution" style="height: 547px">

                </div>
            </div>

            <div class="col-6">
                <div class="row">

                    <div class="buttons col-12 " style="background-color:#ffff">


                        @foreach($month as $m)
                        @if($m === Carbon::now()->format('F'))
                        <button id="{{ $m }}" class="active">
                            {{ $m }}
                        </button>


                        @else
                         <button id="{{ $m }}">
                            {{ $m }}
                        </button>

                         @endif

                        @endforeach

                        <div class="report_visual" id="container" style="height: 355px">

                        </div>
                    </div>

                </div>
            </div>


            
        <div class="col-6">
        <div style="background: #ffff; padding:20px" >
            <div class="row">
                <div class="col-10">
                    <h3 style="font-size: 18px;text-align:center;padding-top:20px"> Top 10 users with most
                        completed courses</h3>
                </div>
                <div class="col-2">
                    <a href="{{ route('admin.reports.courseCompletions.index') }}" id="viewMore" class="btn btn-primary">
                        View More</a>
                </div>
            </div>

            <table class="table table--sized">
                <tr>
                    <th class="tablehead__cell f--small">
                        <b>Name</b>
                    </th>

                    <th class="tablehead__cell f--small">
                       <b> Number of completed courses</b>

                    </th>
                </tr>

                @foreach ($userCompletions as $value)
                    <tr class="tablerow">
                        <td class="tablecell text-center">{{ $value['name'] }}</td>
                        <td class="tablecell text-center">{{ $value['total'] }}</td>

                    </tr>
                @endforeach


            </table>



        </div>

    </div>



    <div class="col-6">
				<div class="report_visual" id="femaleCompletion" style="height: 600px">

				</div>
			</div>




    </div>

    <div class="row col-12">
    <div class="col-6">
				<div class="report_visual" id="maleCompletion" style="height: 600px">

				</div>
                
			</div>


    
    </div>
        

           







    </div>

    
    

</div>


</div>
@stop
@push('extra_js')
<script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<script>
        const dataPrev = {
            2016: [
                ['South Korea', 0],
                ['Japan', 0],
                ['Australia', 0],
                ['Germany', 11],
                ['Russia', 24],
                ['China', 38],
                ['Great Britain', 29],
                ['United States', 46]
            ],
        };

        const data = {
            2016: [
                ['South Korea', 0],
                ['Japan', 0],
                ['Australia', 0],
                ['Germany', 17],
                ['Russia', 19],
                ['China', 26],
                ['Great Britain', 27],
                ['United States', 46]
            ],

        };

        const chart = Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: '{{ $enrollmentCompletionGraph['chart_title'] }}',

            },

            plotOptions: {
                series: {
                    grouping: false,
                    borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                shared: true,
                headerFormat: '<span style="font-size: 15px">{point.point.name}</span><br/>',
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y} </b><br/>'
            },
            xAxis: {
                type: 'category',
                accessibility: {
                    description: 'Courses'
                },

            },
            yAxis: [{
                title: {
                    text: 'Numbers'
                },
                showFirstLabel: false
            }],
            series: [{
                color: 'rgb(158, 159, 163)',
                pointPlacement: -0.2,
                linkedTo: 'main',
                dataSorting: {
                    enabled: true,
                    matchByName: true
                },
                data: [
                    @foreach ($enrollmentCompletionGraph['enrollment_data'] as $key => $value)
                        {
                            name: '{{ $value['name'] }}',
                            y: {{ $value['total'] }}
                        },
                    @endforeach
                ],
                name: 'Enrollments'
            }, {
                name: 'Completions',
                id: 'main',
                dataSorting: {
                    enabled: true,
                    matchByName: true
                },
                dataLabels: [{
                    enabled: true,
                    inside: true,
                    style: {
                        fontSize: '16px'
                    }
                }],
                data: [
                    @foreach ($enrollmentCompletionGraph['completion_data'] as $key => $value)
                        {
                            name: '{{ $value['name'] }}',
                            y: {{ $value['total'] }}
                        },
                    @endforeach
                ]
            }],
            exporting: {
                allowHTML: true
            }
        });


        const years = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
            'November', 'December'
        ];

        years.forEach(year => {
            const btn = document.getElementById(year);
            var res;
            btn.addEventListener('click', () => {

                document.querySelectorAll('.buttons button.active')
                    .forEach(active => {
                        active.className = '';
                    });
                btn.className = 'active';


                chart.update({
                    title: {
                        text: `Monthly Enrollments vs Completions ${year}`
                    },

                    series: [{
                        name: year,
                        dataSorting: {
                            enabled: true,
                            matchByName: true
                        },
                        data: [
                            $.ajax({
                                url: '{{ url('/admin/reports/graph/eresults') }}?emonth=' +
                                    year,
                                type: 'GET',
                                dataType: 'json',
                                success: function(res) {
                                    $.each(res, function(key, value) {
                                        chart.series[0].addPoint([value
                                            .name, parseInt(
                                                value.total)
                                        ]);
                                    })
                                }
                            })
                        ],
                    }, {
                        name: year,
                        data: [
                            $.ajax({
                                url: '{{ url('/admin/reports/graph/cresults') }}?cmonth=' +
                                    year,
                                type: 'GET',
                                success: function(res) {
                                    $.each(res, function(key, value) {


                                        chart.series[1].addPoint([value
                                            .name, parseInt(
                                                value.total)
                                        ]);
                                    })
                                }
                            })
                        ]
                    }]
                }, true, false, {
                    duration: 800
                });
            });
        });
    </script>


<script>
Highcharts.chart('registrationDistribution', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'spline'
    },
    title: {
        text: 'Registration Trends {{ now()->year }}'
    },
    subtitle: {
        text: 'Monthly Registrations'
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        accessibility: {
            description: 'Months of the year'
        }
    },
    yAxis: {
        title: {
            text: 'Number of Registrations'
        },
        labels: {
            formatter: function () {
                return this.value ;
            }
        }
    },
    tooltip: {
        crosshairs: true,
        shared: true
    },
    plotOptions: {
        spline: {
            marker: {
                radius: 4,
                lineColor: '#666666',
                lineWidth: 1
            }
        }
    },
    series: [

    {
        name: 'Completed Registrations',
        marker: {
                symbol: 'square'
            },
        data:[
        @foreach ($completedRegistrationoption as $key => $value)
		   {{($value.',')}}
		@endforeach

        ]
    },
    {
        name: 'Pending Registration Completion',
        marker: {
                symbol: 'diamond'
            },
        data:[
        @foreach ($pendingRegistrationoption as $key => $value)
		   {{($value.',')}}
		@endforeach

        ]



    }

]
});

</script>


<script>
    Highcharts.chart('genderDistribution', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },

        title: {
            text: '{{ $genderChartOptions['chart_title']}}'
        },

        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>Total: {point.y}'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Total: {point.y}'
                },
                showInLegend: true
            }
        },

        credits: {
            enabled: false
        },


        series: [
            {
                name: 'Users',
                colorByPoint: true,
                data: [
                    @foreach ($genderChartOptions['series_data'] as $key => $value)
				{name: '{{($value["name"])}}', y:{{$value['total']}} },
				@endforeach
                ]
            },
        ],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
</script>


<script>
    Highcharts.chart('femaleCompletion', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },

        title: {
            text: '{{ $completionFemaleChartOptions['chart_title']}}'
        },
        subtitle: {
        text: '{{ $completionFemaleChartOptions['subtitle']}}'
    },

        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>Total: {point.y}'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Total: {point.y}'
                },
                showInLegend: true
            }
        },

        credits: {
            enabled: false
        },


        series: [
            {
                name: 'Users',
                colorByPoint: true,
                data: [
                    @foreach ($completionFemaleChartOptions['series_data'] as $key => $value)
				{name: '{{($value["name"])}}', y:{{$value['total']}} },
				@endforeach
                ]
            },
        ],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
</script>

<script>
    Highcharts.chart('maleCompletion', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },

        title: {
            text: '{{ $completionMaleChartOptions['chart_title']}}'
        },
        subtitle: {
        text: '{{ $completionMaleChartOptions['subtitle']}}'
    },

        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>Total: {point.y}'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Total: {point.y}'
                },
                showInLegend: true
            }
        },

        credits: {
            enabled: false
        },


        series: [
            {
                name: 'Users',
                colorByPoint: true,
                data: [
                    @foreach ($completionMaleChartOptions['series_data'] as $key => $value)
				{name: '{{($value["name"])}}', y:{{$value['total']}} },
				@endforeach
                ]
            },
        ],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
</script>


<script>
    Highcharts.chart('userDistribution', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },

        title: {
            text: '{{ $completionChartOptions['chart_title']}}'
        },

        subtitle: {
            text: '{{ $completionChartOptions['chart_title']}}'
        },

        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>Total: {point.y}'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Total: {point.y}'
                },
                showInLegend: true
            }
        },

        credits: {
            enabled: false
        },


        series: [
            {
                name: 'Users',
                colorByPoint: true,
                data: [
                    @foreach ($completionChartOptions['series_data'] as $key => $value)
				{name: '{{($value["name"])}}', y:{{$value['total']}} },
				@endforeach
                ]
            },
        ],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
    </script>

<script>
    Highcharts.chart('userJobs', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },

        title: {
            text: '{{ $completionChartOptions['chart_title']}}'
        },

        subtitle: {
            text: '{{ $completionChartOptions['subtitle']}}'
        },

        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>Total: {point.y}'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Total: {point.y}'
                },
                showInLegend: true
            }
        },

        credits: {
            enabled: false
        },


        series: [
            {
                name: 'Gender',
                colorByPoint: true,
                data: [

                ]
            },
        ],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
    </script>



<script>
    const completion_yearly = Highcharts.chart('completion_yearly_container', {
        chart: {
            type: 'column'
        },
        title: {
            text: '{{ $yearlyCompletionGraph['chart_title'] }}',

        },

        plotOptions: {
            series: {
                grouping: false,
                borderWidth: 0
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            shared: true,
            headerFormat: '<span style="font-size: 15px">{point.point.name}</span><br/>',
            pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y} </b><br/>'
        },
        xAxis: {
            type: 'category',
            accessibility: {
                description: 'Courses'
            },

        },
        yAxis: [{
            title: {
                text: 'Numbers'
            },
            showFirstLabel: false
        }],
        series: [{
            color: 'rgb(247, 163, 92)',
            pointPlacement: -0.2,
            linkedTo: 'main',
            dataSorting: {
                enabled: true,
                matchByName: true
            },
            data: [
                @foreach ($yearlyCompletionGraph['completion_data'] as $key => $value)
                    {
                        name: '{{ $value['name'] }}',
                        y: {{ $value['total'] }}
                    },
                @endforeach
            ],
            name: 'Enrollments'
        }],
        exporting: {
            allowHTML: true
        }
    });


  var select = document.getElementById('completion-year');

  select.addEventListener('change', (e) => {
   var year = e.target.value;

   completion_yearly.update({
                title: {
                    text: `Top 5 Course Completions of ${year}`
                },
                series: [{
                    dataSorting: {
                        enabled: true,
                        matchByName: true
                    },
                    data: [
                        $.ajax({
                            url: '{{ url('/admin/reports/graph/yearlyCRresults') }}?cyear=' +
                                year,
                            type: 'GET',
                            dataType: 'json',
                            success: function(res) {
                                $.each(res, function(key, value) {
                                    completion_yearly.series[0].addPoint([value
                                        .name, parseInt(
                                            value.total)
                                    ]);
                                })
                            }
                        })
                    ],
                    name: 'Completions',
                }]
            }, true, false, {
                duration: 800
            });
         });




</script>


<script>
    const yearly = Highcharts.chart('yearly_container', {
        chart: {
            type: 'column'
        },
        title: {
            text: '{{ $yearlyEnrollmentGraph['chart_title'] }}',

        },

        plotOptions: {
            series: {
                grouping: false,
                borderWidth: 0
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            shared: true,
            headerFormat: '<span style="font-size: 15px">{point.point.name}</span><br/>',
            pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y} </b><br/>'
        },
        xAxis: {
            type: 'category',
            accessibility: {
                description: 'Courses'
            },

        },
        yAxis: [{
            title: {
                text: 'Numbers'
            },
            showFirstLabel: false
        }],
        series: [{
            color: 'rgb(247, 163, 92)',
            pointPlacement: -0.2,
            linkedTo: 'main',
            dataSorting: {
                enabled: true,
                matchByName: true
            },
            data: [
                @foreach ($yearlyEnrollmentGraph['enrollment_data'] as $key => $value)
                    {
                        name: '{{ $value['name'] }}',
                        y: {{ $value['total'] }}
                    },
                @endforeach
            ],
            name: 'Enrollments'
        }],
        exporting: {
            allowHTML: true
        }
    });


  var select = document.getElementById('dashboard-year');

  select.addEventListener('change', (e) => {
   var year = e.target.value;

   yearly.update({
                title: {
                    text: `Top 5 Course Enrollments of ${year}`
                },
                series: [{
                    dataSorting: {
                        enabled: true,
                        matchByName: true
                    },
                    data: [
                        $.ajax({
                            url: '{{ url('/admin/reports/graph/yearlyEResults') }}?eyear=' +
                                year,
                            type: 'GET',
                            dataType: 'json',
                            success: function(res) {
                                $.each(res, function(key, value) {
                                  yearly.series[0].addPoint([value
                                        .name, parseInt(
                                            value.total)
                                    ]);
                                })
                            }
                        })
                    ],
                    name: 'Enrollments',
                }]
            }, true, false, {
                duration: 800
            });
         });




</script>

@endpush
