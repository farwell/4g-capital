@extends('twill::layouts.form')

@section('contentFields')

@formField('input', [
		'name' => 'short_description',
		'label' => 'Short Description',
        'maxlength' => 200,
		'translated' => true,
])

@formField('date_picker', [
		'name' => 'event_date',
		'label' => 'Event Date',
	])

@formField('time_picker', [
    'name' => 'event_time',
    'label' => 'Event time',
])

@formField('input', [
		'name' => 'external_link',
		'label' => 'Link to Event',
		'translated' => false,
])


@formField('medias',[
    'name' => 'event_image',
    'label' => 'Event Cover Image',
])


@formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Event Article',
        'toolbarOptions' => [
      ['header' => [2, 3, 4, 5, 6, false]],
      'bold',
      'italic',
      'underline',
      'strike',
      ["script" => "super"],
      ["script" => "sub"],
      "blockquote",
      "code-block",
      ['list' => 'ordered'],
      ['list' => 'bullet'],
      ['indent' => '-1'],
      ['indent' => '+1'],
      ["align" => []],
      ["direction" => "rtl"],
      'link',
      'image',
      "clean",
    ],
        'placeholder' => 'Article',
		'maxlength' => 2000,
		'translated' => true,
])
@stop
