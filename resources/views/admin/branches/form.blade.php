@extends('twill::layouts.form')

@section('contentFields')
@formField('select', [
    'name' => 'country_id',
    'label' => 'Country',
    'placeholder' => 'Select Country',
    'options' => collect($countryList ?? ''),
    ])

    @formField('select', [
    'name' => 'sector_id',
    'label' => 'Sector',
    'searchable'=>true,
    'placeholder' => 'Select sector ',
    'options' => collect($sectorList ?? ''),
    ])
@stop
