@extends('twill::layouts.form')

@section('contentFields')

@formField('select', [
    'name' => 'message_type_id',
    'label' => 'Type',
    'placeholder' => 'Select Type',
    'options' => collect($messageTypeList ?? '')
])


@formConnectedFields([
    'fieldName' => 'message_type_id',
    'fieldValues' => 2,
    
])
    @formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select a jobrole',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($roleList ?? ''),
        ])
    
    
        @formField('input', [
        'name' => 'subject',
        'label' => 'Subject',
        'maxlength' => 100
        ])
    
        @formField('wysiwyg', [
        'name' => 'message',
        'label' => 'Message',
        'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6, false]],
        'bold',
        'italic',
        'underline',
        'strike',
        ["script" => "super"],
        ["script" => "sub"],
        "blockquote",
        "code-block",
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        ['indent' => '-1'],
        ['indent' => '+1'],
        ["align" => []],
        ["direction" => "rtl"],
        'link',
        'image',
        "clean",
        ],
        'placeholder' => 'Message',
        'maxlength' => 2000,
    
        ])
@endformConnectedFields



@formConnectedFields([
    'fieldName' => 'message_type_id',
    'fieldValues' => 3,
    
])
        @formField('input', [
        'name' => 'subject',
        'label' => 'Subject',
        'maxlength' => 100
        ])

        @formField('multi_select', [
        'name' => 'target_id',
        'label' => "Select a target to send message to (if it's everyone select All)",
        'placeholder' => 'Select target',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($targetList ?? ''),
        ])
     
    
        @formField('wysiwyg', [
        'name' => 'message',
        'label' => 'Message',
        'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6, false]],
        'bold',
        'italic',
        'underline',
        'strike',
        ["script" => "super"],
        ["script" => "sub"],
        "blockquote",
        "code-block",
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        ['indent' => '-1'],
        ['indent' => '+1'],
        ["align" => []],
        ["direction" => "rtl"],
        'link',
        'image',
        "clean",
        ],
        'placeholder' => 'Message',
        'maxlength' => 2000,
    
        ])
@endformConnectedFields




@formConnectedFields([
    'fieldName' => 'message_type_id',
    'fieldValues' => 1,
    
])
    

        @formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList ?? ''),
        ])

        @formField('input', [
        'name' => 'subject',
        'label' => 'Subject',
        'maxlength' => 100
        ])
    
        @formField('wysiwyg', [
        'name' => 'message',
        'label' => 'Message',
        'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6, false]],
        'bold',
        'italic',
        'underline',
        'strike',
        ["script" => "super"],
        ["script" => "sub"],
        "blockquote",
        "code-block",
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        ['indent' => '-1'],
        ['indent' => '+1'],
        ["align" => []],
        ["direction" => "rtl"],
        'link',
        'image',
        "clean",
        ],
        'placeholder' => 'Message',
        'maxlength' => 2000,
    
        ])
@endformConnectedFields



@stop
