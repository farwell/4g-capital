@extends('layouts.admin.users')
@push('extra_css')
    <link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
    <style>
        .report_visual {
            margin-bottom: 30px;
        }

        .container {
            width: 100% !important;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
    <div class="listing">

        <div id="container-fluid">

            <div class="row justify-content-center">

                <div class="col-12">
                    <form action="{{ route('admin.userCodeStore') }}" method="POST" class="homeForm">
                        @csrf
                        <div class="row justify-content-center">

                            @error('category')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                          

                            <div class="col-12">
                                <div class="row">

                                    <div class="col-2 mt-4 mb-4 ">
                                        <label class="filter_by" style="color:#000"> Number of User codes </label>
                                    </div>
                                    <div class="col-4">
                                        <input type="text" placeholder="Number of User codes"
                                            class="form-control mt-4 mb-4 " name="numbers">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 ">
                                <button type="submit" class="mt-4 mb-4"
                                    style="
                                margin-top: 40px;
                                margin-left: 20px;
                                color: rgb(255, 255, 255);
                                background: #1a8f36;
                                border:1px solid #1a8f36;
                                padding: 10px;
                                text-decoration: none;">Submit</button>

                                <button type="reset" class=" mt-4 mb-4"
                                    style="
                            margin-top: 40px;
                            margin-left: 20px;
                            color: rgb(255, 255, 255);
                            background: rgb(255, 0, 0);
                            border:1px solid rgb(255, 0, 0);
                            padding: 10px;
                            text-decoration: none;"
                                    onClick="window.location.href=window.location.href">Reset</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
@stop



@push('extra_js')
    <script src="{{ twillAsset('main-free.js') }}" crossorigin></script>
@endpush
