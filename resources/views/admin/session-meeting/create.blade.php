@extends('layouts.admin_no_nav')

@push('extra_css')
<link href="{{ asset('/css/reports.css') }}" rel="stylesheet">

	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush


@section('title', trans('admin.sessionmeetings.actions.index'))

@section('customPageContent')
    <div class="container-xl">

                <div class="card">
        
        <session-meeting-form
            :action="'{{ url('admin/sessionmeetings') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate enctype="multipart/form-data">
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.session-meeting.actions.create') }}
                </div>

                <div class="card-body">
                    @include('admin.session-meeting.components.form-elements')
                </div>
                                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </session-meeting-form>

        </div>

        </div>

    
@endsection

@push('extra_js')
<script src="{{ asset('/js/admin.js') }}"></script> 


@endpush