<form action="{{ route('admin.courseCreation.settings') }}" method="POST" @if($customForm) ref="customForm" @else
        v-on:submit.prevent="submitForm" @endif>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="item_id" value="{{ $item->id }}">
   <input type="hidden" name="course_id" value="{{ $item->course_id }}">
    <div class="container">
        <div class="wrapper-inner wrapper--reverse" v-sticky data-sticky-id="publisher" data-sticky-offset="80">

            <section class="col col--primary" data-sticky-top="publisher">
                @formField('radios', [
                "name" => "self_paced",
                "label" => "Course Pacing",
                "inline" => true,
                "note" => 'Instructor-paced courses progress at the pace that the course author sets. You can configure
                release
                dates for course content and due dates for assignments.
                Self-paced courses do not have release dates for course content or due dates for assignments. Learners
                can
                complete course material at any time before the course end date.',
                "options" => [
                [
                "value" => 0,
                "label" => "Instructor-Paced",
                ],
                [
                "value" => 1,
                "label" => "Self-Paced",
                ],
                ],
                ])
                <div class="col-12">

                    <div class="col-sm-6">
                        @formField('date_picker', [
                        "name" => "start",
                        "label" => "Course Start date",
                        "minDate" => "2017-09-10 12:00",
                        'required' => true,
                        ])
                    </div>
                    <div class="col-sm-6">
                        @formField('date_picker', [
                        "name" => "end",
                        "label" => "Course End date",
                        "minDate" => "2017-09-10 12:00",
                        'required' => true,
                        ])

                    </div>
                </div>

                <div class="col-12">
                    <div class="col-sm-6">
                        @formField('date_picker', [
                        "name" => "enrol_start",
                        "label" => "Enrollment Start date",
                        "minDate" => "2017-09-10 12:00",
                        'required' => true,
                        ])

                    </div>

                    <div class="col-sm-6">
                        @formField('date_picker', [
                        "name" => "enrol_end",
                        "label" => "Enrollment End date",
                        "minDate" => "2017-09-10 12:00",
                        'required' => true,
                        ])

                    </div>
                </div>

                 @formField('select', [
                'name' => 'course_category_id',
                'label' => 'Course Category ',
                'options' => collect(app(App\Repositories\CourseCategoryRepository::class)->listAll('title') ?? '')
                ])
                @formField('input', [
                'name' => 'effort',
                'label' => 'Course Effort',
                'maxlength' => 100,
                'required' => true,
                'note' => 'Estimated time for course completion',
                'placeholder' => '00:00',
                ])
                @formField('input', [
                'name' => 'short_description',
                'label' => 'Short Description',
                'maxlength' => 150,
                'required' => true,
                'note' => 'Hint message goes here',
                'type' => 'textarea',
                'rows' => 3
                ])

                @formField('wysiwyg', [
                'name' => 'overview',
                'label' => 'Course Overview',
                'placeholder' => 'overview',
                'maxlength' => 200,
                'note' => 'Hint message',
                ])

                @formField('medias', [
                'name' => 'cover_image',
                'label' => 'Cover image',
                'note' => 'Course Cover Image',
                ])


            </section>
 <aside class="col col--aside">
                <div class="publisher" data-sticky-target="publisher">
                    <a17-publisher>
                        @yield('publisherRows')
                    </a17-publisher>
                    <a17-page-nav placeholder="Go to page" previous-url="{{ $parentPreviousUrl ?? '' }}"
                        next-url="{{ $parentNextUrl ?? '' }}"></a17-page-nav>
                    @hasSection('sideFieldset')
                    <a17-fieldset title="{{ $sideFieldsetLabel ?? 'Options' }}" id="options">
                        @yield('sideFieldset')
                    </a17-fieldset>
                    @endif
                    @yield('sideFieldsets')
                </div>
            </aside>
        </div>
    </div>
    <a17-spinner v-if="loading"></a17-spinner>
</form>
