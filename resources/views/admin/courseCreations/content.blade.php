@php
$chapters = getCourseChapters($item->course_id);

//dd($chapters);


$translate = $translate ?? false;
    $translateTitle = $translateTitle ?? $translate ?? false;
    $reorder = $reorder ?? false;
    $nested = $nested ?? false;
    $bulkEdit = $bulkEdit ?? true;
    $create = true ;
    $skipCreateModal = $skipCreateModal ?? false;
    $controlLanguagesPublication = $controlLanguagesPublication ?? true;
@endphp

@if($chapters)
@if(count($chapters['child_info']['children'] ) > 0)

<!-- display panels -->



@include('admin.courseCreations.components.sectionPanels', ['chapters' =>$chapters])


@else
<p style="text-align:center;padding:10px"> <button class="button button--small button--validate" data-toggle="modal"
    data-target="#modalSection">Create section</button> </p>
@endif
@else

<!-- display button to create first chapter -->
<p style="text-align:center;padding:10px"> <button class="button button--small button--validate" data-toggle="modal" data-target="#modalSection">Create section</button> </p>


@endif

<div class="modal modal--form sectionModal" id="modalSection" tabindex="-1" role="dialog" aria-labelledby="modallabel1" aria-hidden= "true">
     <div class="modal__window">
     <div class="modal__header">
      Create Section
     </div>
     <div class="modal__content">
      <form action="{{ route('admin.courseCreation.blocks') }}" novalidate method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="item_id" value="{{ $item->id }}">
    <input type="hidden" name="course_id" value="{{ $item->course_id }}">
    <input type="hidden" name="parent" id="parentLocator">
    <input type="hidden" name="type" id="creationType">
    <input type="hidden" name="category" value="chapter">
    <div style="padding:10px">
        @formField('input', [
         "name" => "section_name",
        "class" => "form-control spacing-input",
        "label" => "Display name",
        "id" => "sectionName",
        'required' => true,
                            ])

    <button class="button button--small button--validate" type="submit" style="margin-top:10px">Submit</button>
                    </div>
    </form>

     </div>
     </div>
</div>


<div class="modal modal--form subsectionModal" id="modalSubsection" tabindex="-1" role="dialog" aria-labelledby="modallabel1" aria-hidden= "true">
     <div class="modal__window">
     <div class="modal__header">
      Create Subsection
     </div>
     <div class="modal__content">
      <form action="{{ route('admin.courseCreation.blocks') }}" novalidate method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="item_id" value="{{ $item->id }}">
    <input type="hidden" name="course_id" value="{{ $item->course_id }}">
    <input type="hidden" name="parent" id="parentLocator">
    <input type="hidden" name="type" id="creationType">
    <input type="hidden" name="category" value="sequential">
    <div style="padding:10px">
        @formField('input', [
         "name" => "section_name",
        "class" => "form-control spacing-input",
        "label" => "Display name",
        "placeholder"=> "Display name",
        "id" => "sectionName",
        'required' => true,
                            ])

    <div id="graderSection" style="margin-top:20px; display:none">
       <label class="input__label">Grade as: </label><br>
       <select class="form-control filter-select" name="grader" id="subsectionGrader">
         <option>Select Grading</option>

       </select>
     </div>


 <div id="preSection" style="display:none">
     <div style="margin-bottom:20px;">
        <input type="checkbox" id="subsection_is_prereq" name="is_prereq"  >
        <label for="is_prereq"> Make this subsection available as a prerequisite to other content</label>
     </div>

    <div>
     <label class="input__label">Prerequisite:  </label><br>
       <select class="form-control filter-select" name="prereq" id="subsectionPrerequisite" style="margin-bottom:1rem">
         <option>Select Prerequisite</option>

       </select>

        @formField('input', [
         "name" => "prereq_min_score",
        "class" => "form-control spacing-input",
        "label" => "Minimum Score:",
        "id" => "prereq-score",

                            ])

           @formField('input', [
         "name" => "prereq_min_completion",
        "class" => "form-control spacing-input",
        "label" => "Minimum Completion:",
        "id" => "prereq-Completion",

                            ])

    </div>
    </div>

    <button class="button button--small button--validate" type="submit" style="margin-top:10px">Submit</button>
                    </div>
    </form>

     </div>
     </div>
</div>







<div class="modal modal--form unitModal" id="modalUnit" tabindex="-1" role="dialog" aria-labelledby="modallabel1" aria-hidden= "true">
     <div class="modal__window">
     <div class="modal__header">
      Create Unit
     </div>
     <div class="modal__content">
      <form action="{{ route('admin.courseCreation.blocks') }}" novalidate method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="item_id" value="{{ $item->id }}">
    <input type="hidden" name="course_id" value="{{ $item->course_id }}">
    <input type="hidden" name="parent" id="parentLocator">
    <input type="hidden" name="type" id="creationType">
    <input type="hidden" name="category" value="vertical">
    <div style="padding:10px">
        @formField('input', [
         "name" => "section_name",
        "class" => "form-control spacing-input",
        "label" => "Display name",
        "placeholder"=> "Display name",
        "id" => "sectionName",
        'required' => true,
                            ])

    <button class="button button--small button--validate" type="submit" style="margin-top:10px">Submit</button>
                    </div>
    </form>

     </div>
     </div>
</div>


<div class="modal modal--form modalDelete" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modallabel1" aria-hidden= "true">
     <div class="modal__window">
     <div class="modal__header">
      Delete this <span id="categoryTitle"></span>
     </div>
     <div class="modal__content">
      <form action="{{ route('admin.courseCreation.blocks') }}" novalidate method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="item_id" value="{{ $item->id }}">
    <input type="hidden" name="course_id" value="{{ $item->course_id }}">
    <input type="hidden" name="parent" id="parentLocator">
    <input type="hidden" name="type" id="creationType">
    <input type="hidden" name="category" id="categoryType">
   <p>Deleting this <span id="categoryTitle"></span> is permanent and cannot be undone.</p>

    <button class="button button--small button--validate button-destroy" type="submit" style="margin-top:10px">Yes, delete this <span id="categoryTitle"></span></button>
                    </div>
    </form>

     </div>
     </div>
</div>
