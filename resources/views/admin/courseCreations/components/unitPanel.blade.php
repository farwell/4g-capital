@php 
$unitList = $section['child_info']['children'];

@endphp


@foreach($unitList as $key => $unit)


<div class="accordion accordion-flush" id="accordionUnit">
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-heading-unit-{{$key}}">
        <button class="accordion-button collapsed" type="button" onclick="window.open('{{route('admin.unit.creation', ['item' => $item->id, 'locator'=> $unit['id']])}}','_self')">
        {{$unit['display_name']}}
      </button>
     
      <button class="button button-unitEdit button-sectionEdit button-modalDelete" data-toggle="modal" data-target="#modalDelete" data-id="{{$unit['id']}}" data-type="delete" data-category="unit"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </h2>
    <div id="flush-unit-{{$key}}" class="accordion-collapse collapse" aria-labelledby="flush-heading-unit-{{$key}}" data-bs-parent="#accordionUnit">
      <div class="accordion-body">

      </div>
    </div>
  </div>
</div>

@endforeach



