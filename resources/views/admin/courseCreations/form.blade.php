@extends("layouts.admin.outer_form")

@push("extra_css")
@push('extra_css')
    <style>
        .input[data-v-74021712] {
            margin-top: 50px;
            position: relative;
        }

        @media screen and (min-width: 600px) {
            .input__note[data-v-74021712] {
                display: inline;
                right: 0 !important;
                top: 0px !important;
                left:unset !important;
                position: absolute;
                font-size: 11px;
                max-width: 80%;

            }
        }
    </style>
@endpush
@endpush

@php
$customForm = true;


@endphp




@section("contentFields")
<a17-custom-tabs :tabs="[
            { name: 'settings', label: 'Schedule & details' },
            { name: 'advanced', label: 'Advanced Settings' },
            { name: 'grading', label: 'Grading' },
            { name: 'content', label: 'Content' },
        ]">


    <div class="custom-tab custom-tab-content custom-tab--settings">

      @include('admin.courseCreations.settings', [$item])

    </div>

    <div class="custom-tab custom-tab-content custom-tab--advanced">

        @include('admin.courseCreations.advanced', [$item])

    </div>

    <div class="custom-tab custom-tab-content custom-tab--grading">
        @include('admin.courseCreations.grading', [$item])
    </div>

    <div class="custom-tab custom-tab-content custom-tab--content">
        @include('admin.courseCreations.content', [$item])
    </div>

</a17-custom-tabs>
@stop


