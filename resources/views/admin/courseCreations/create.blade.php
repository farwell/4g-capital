@push('extra_css')
    <style>
        .input[data-v-74021712] {
            margin-top: 50px;
            position: relative;
        }

        @media screen and (min-width: 600px) {
            .input__note[data-v-74021712] {
                display: inline;
                left: 0;
                top: 80px;
                position: absolute;
                font-size: 11px;
            }
        }
    </style>
@endpush

@formField('input', [
    'name' => $titleFormKey ?? 'title',
    'label' => 'Course Name',
    'required' => true,
    'note' => 'The public display name for your course. This cannot be changed.',
])


@formField('input', [
    'name' => 'org',
    'label' => 'Organization Name',
    'required' => true,
    'note' => 'The name of the organization sponsoring the course. Note: The organization name is part of the course URL. This cannot be changed.',
])

@formField('input', [
    'name' => 'number',
    'label' => 'Course Number',
    'required' => true,
    'placeholder' => 'e.g. CS101',
    'note' => 'The unique number that identifies your course within your organization. Note: This is part of your course URL, so no spaces or special characters are allowed and it cannot be changed.',
])

@formField('input', [
    'name' => 'run',
    'label' => 'Course Run',
    'required' => true,
    'placeholder' => 'e.g. 2023_Q4',
    'note' => 'The term in which your course will run. Note: This is part of your course URL, so no spaces or special characters are allowed and it cannot be changed.',
])
