<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
            <tr style="text-align:center;">
                <td>
                    <img src="{{asset('/images/icons/logo_blue.png')}}" height="40" style="margin: 0 0 5px 0;"/>
                </td>
            </tr>
            
            <tr>
                <td>
                    © Copyright 2023 - 4G Capital Insurance.
                </td>
            </tr>
        </table>
    </td>
</tr>
