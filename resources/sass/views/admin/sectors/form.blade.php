
@extends('twill::layouts.form')

@section('contentFields')
{{-- @formField('select', [
    'name' => 'country_id',
    'label' => 'Country',
    'placeholder' => 'Select Country',
    'options' => collect($countryList ?? ''),
]) --}}
	
@formField('multi_select', [
	'name' => 'branch_id',
	'label' => 'Select branches for this sector',
	'placeholder' => 'Select Branches',
    'min'=> 1,
    'searchable'=> true,
    'unpack' => false,
	'options' => collect($branchList ?? ''),
])

@formField('multi_select', [
	'name' => 'courses',
	'label' => 'Select Mandatory course for this sector',
	'placeholder' => 'Select Course',
    'min'=> 1,
    'unpack' => false,
	'options' => collect($courseList ?? ''),
])





@stop



