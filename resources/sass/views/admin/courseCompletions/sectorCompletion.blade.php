@extends('layouts.reports')
@push('extra_css')
	<link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
	<style>
		.report_visual {
			margin-bottom: 30px;
		}
        .container {
            width: 100% !important;
        }

	</style>

	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
<div class="listing">

	<div id="container-fluid">

        <div class="row">

            <div class="col-12">
                <form action="{{ route('admin.sector.completion') }}" method="POST" class="homeForm">
                    @csrf
                    <div class="row justify-content-center">

                        @error('category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                       
                        <div class="col-9 no-padding-right ">
                            <div class="row">

                                <div class="col-2 mt-4 mb-4 ">
                                    <label class="filter_by" style="color:#000"> Filter sectors by</label>
                                </div>
                                <div class="col-4">
                                    <select name="country" id="" class="form-control mt-4 mb-4 ">
                                        <option value=''>Select Country</option>
                                        @foreach (App\Models\Country::published()->orderByTranslation('title')->get() as $theme)
                                            <option value="{{ $theme->id }}"> {{ $theme->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-5">

                                    <input type="text" placeholder="Free text search ... eg Sector name" class="form-control mt-4 mb-4 " name="search">
                                   
                                </div>

                            </div>

                        </div>

                        <div class="col-3 ">
                            <button type="submit"
                                class="mt-4 mb-4" style="
                                margin-top: 40px;
                                margin-left: 20px;
                                color: rgb(255, 255, 255);
                                background: #1a8f36;
                                border:1px solid #1a8f36;
                                padding: 10px;
                                text-decoration: none;">Search</button>

                            <button type="reset" class=" mt-4 mb-4" style="
                            margin-top: 40px;
                            margin-left: 20px;
                            color: rgb(255, 255, 255);
                            background: rgb(255, 0, 0);
                            border:1px solid rgb(255, 0, 0);
                            padding: 10px;
                            text-decoration: none;"
                                onClick="window.location.href=window.location.href">Reset</button>

                                <button id="exportButton" style="
                                margin-top: 40px;
                                margin-left: 20px;
                                color: rgb(255, 255, 255);
                                background: #1a8f36;
                                border:1px solid #1a8f36;
                                padding: 10px;
                                text-decoration: none;" class=" mt-4 mb-4"> Export</button>

                        </div>
                    </div>
                </form>

            </div>

        </div>
        <div class="row">
            <div class="col">
			  <table class="table">
                <thead></thead>
                <tr class="tablehead">
                    <th> #</th>
                    <th> Sector</th>
                    <th> Number of Registrations</th>
                    <th> Completed all mandatory modules</th>
                    <th> Completed at least one mandatory module</th>
                                
                </tr>
            </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($sectors as $sector)
                    <tr class="tablerow">
                    <td class="tablecell text-center">{{ $i++}}</td>
                    <td class="tablecell text-center">{{ $sector->title }}</td>
                    <td class="tablecell text-center">{{ $sector->registered }}</td>
                    <td class="tablecell text-center">{{ $sector->completed }}</td>
                    <td class="tablecell text-center">{{ $sector->completedOne }}</td>
                </tr>
                  @endforeach
                </tbody>
              </table>

              {!! $sectors->links() !!}
			</div>

    </div>

</div>
</div>
@stop
@push('extra_js')
<script src="{{ twillAsset('main-free.js') }}" crossorigin></script>

<!-- Include SheetJS library from CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.8/xlsx.full.min.js"></script>

<!-- Add event listener to export button -->
<script>
  // Function to export table data to Excel
  function exportToExcel() {
    // Get table element
    var table = document.querySelector('table');

    // Convert table to worksheet
    var ws = XLSX.utils.table_to_sheet(table);

    // Add title to row 1
    var title = "SECTOR COMPLETION REPORT"; // Replace with your desired title
    XLSX.utils.sheet_add_aoa(ws, [[title]], { origin: "A1" });

    // Shift table rows to start from row 2
    XLSX.utils.sheet_add_aoa(ws, [], { origin: "A2" });

    // Create workbook and add worksheet
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    // Export workbook to Excel file
    XLSX.writeFile(wb, "sector_completion.xlsx");
  }

  // Add event listener to export button
  document.getElementById('exportButton').addEventListener('click', exportToExcel);
</script>

@endpush
