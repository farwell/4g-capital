@extends('layouts.reports')
@push('extra_css')
	<link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
	<style>
		.report_visual {
			margin-bottom: 30px;
		}
        .container {
            width: 100% !important;
        }

	</style>

	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
<div class="listing">

	<div id="container-fluid">

        <div class="row">

            <div class="col-12">
               {{-- <form action="{{ route('admin.sector.completion') }}" method="POST" class="homeForm">
                    @csrf
                    <div class="row justify-content-center">

                        @error('category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                       
                        <div class="col-9 no-padding-right ">
                            <div class="row">

                                <div class="col-2 mt-4 mb-4 ">
                                    <label class="filter_by" style="color:#000"> Filter branches by</label>
                                </div>
                                <div class="col-4">
                                    <select name="country" id="" class="form-control mt-4 mb-4 ">
                                        <option value=''>Select Country</option>
                                        @foreach (App\Models\Country::published()->orderByTranslation('title')->get() as $theme)
                                            <option value="{{ $theme->id }}"> {{ $theme->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-5">

                                    <input type="text" placeholder="Free text search ... eg Sector name" class="form-control mt-4 mb-4 " name="search">
                                   
                                </div>

                            </div>

                        </div>

                        <div class="col-3 ">
                            <button type="submit"
                                class="mt-4 mb-4" style="
                                margin-top: 40px;
                                margin-left: 20px;
                                color: rgb(255, 255, 255);
                                background: #1a8f36;
                                border:1px solid #1a8f36;
                                padding: 10px;
                                text-decoration: none;">Search</button>

                            <button type="reset" class=" mt-4 mb-4" style="
                            margin-top: 40px;
                            margin-left: 20px;
                            color: rgb(255, 255, 255);
                            background: rgb(255, 0, 0);
                            border:1px solid rgb(255, 0, 0);
                            padding: 10px;
                            text-decoration: none;"
                                onClick="window.location.href=window.location.href">Reset</button>
                        </div>
                    </div>
                </form>--}}

            </div>

        </div>
        <div class="row">
            <div class="col">
            <button id="exportButton" class=" btn btn-primary btn-sm d-flex justify-content-end float-right mb-2"><i class="fa fa-file-excel-o mx-2 my-1" ></i> Export</button>

			  <table class="table">
                <thead>
                <tr class="tablehead">
                    

                    <th style="text-align: left;padding-left:1.8%;font-weight:bold"> #</th>
                                <th style="text-align: left;padding-left:2.5%;font-weight:bold"> Jobrole</th>
                                <th style="text-align: left;padding-left:2.5%;width:25%;font-weight:bold;"> Courses </th>
                                <th style="text-align: left;padding-left:2.5%;width:10%;font-weight:bold;"> Enrollments count </th>
                                <th style="text-align: left;padding-left:2.5%;width:10%;font-weight:bold;"> Completed all mandatory modules</th>
                                <th style="text-align: left;padding-left:2.5%;width:10%;font-weight:bold;"> Completed at least one mandatory module</th>
                               
                </tr>
            </thead>
                <tbody>
               
                @if(count($jobRoles)>0)
                @php
            $perPage = $jobRoles->perPage();
            $currentPage = $jobRoles->currentPage();
            $count = ($currentPage - 1) * $perPage + 1;
            @endphp
                @foreach($jobRoles as $jobRole)
                    <tr class="tablerow">
                   

                        <td class="tablecell" style="width:5%">
                        {{ $count++ }}</td>
                            <td class="tablecell" style="width:5%">
                            {{$jobRole->title }}</td>

                            <td class="tablecell" style="text-align:left; padding-left:2.5%; width:10%">{{ $jobRole->courses->pluck('name')->implode(', ') }}
                        </td>
                       
                        <td class="tablecell" style="text-align:left; padding-left:2.5%; width:10%">

                   
                     
                        @php
                        $users = \App\Models\User::where('job_role_id', $jobRole->id)->get();
                        @endphp

                          {{$users->count()}}
                        </td>
                        <td class="tablecell" style="text-align:left; padding-left:2.5%; width:10%">

                        @php
                      



                                                    $completedCount = 0;

                        $users = \App\Models\User::where('job_role_id', $jobRole->id)->get();

                            foreach ($users as $key => $user) {
                                $compulsory = $jobRole->courses->pluck('id')->toArray();
                                $completed = App\Models\CourseCompletion::where('user_id', '=', $user->id)->pluck('course_id')->toArray();

                                if (count($compulsory) === count($completed) && count(array_diff($compulsory, $completed)) === 0) {
                                    $completedCount++;
                                }
                            }

                           
               

   
              
                        @endphp
                            {{$completedCount }}
                        </td>

                        <td class="tablecell" style="text-align:left; padding-left:2.5%; width:10%">

                        @php
                      



                                                    $completedCount = 0;

                        $users = \App\Models\User::where('job_role_id', $jobRole->id)->get();

                            foreach ($users as $key => $user) {
                                $compulsory = $jobRole->courses->pluck('id')->toArray();
                                $completed = App\Models\CourseCompletion::where('user_id', '=', $user->id)->pluck('course_id')->toArray();

                               
               if (count($completed) > 0 || count(array_diff($compulsory, $completed)) === 0) {
                $completedCount++;
               }
                            }

                           
               

   
              
                        @endphp
                            {{$completedCount }}
                        </td>
                       
                                     
                    </tr>
                  @endforeach

                  @else
                <tr class="tablerow">
                        <td class="tablecell" colspan="10" style="width:10%">
                        Records Not found for this resource
                        </td>
                </tr>

                @endif
                </tbody>
              </table>

              {!! $jobRoles->links() !!}
			</div>

    </div>

</div>
</div>
@stop
@push('extra_js')
<script src="{{ twillAsset('main-free.js') }}" crossorigin></script>

<!-- Include SheetJS library from CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.8/xlsx.full.min.js"></script>

<!-- Add event listener to export button -->
<script>
  // Function to export table data to Excel
  function exportToExcel() {
    // Get table element
    var table = document.querySelector('table');

    // Convert table to worksheet
    var ws = XLSX.utils.table_to_sheet(table);

    // Add title to row 1
    var title = "JOB ROLE COMPLETION REPORT"; // Replace with your desired title
    XLSX.utils.sheet_add_aoa(ws, [[title]], { origin: "A1" });

    // Shift table rows to start from row 2
    XLSX.utils.sheet_add_aoa(ws, [], { origin: "A2" });

    // Create workbook and add worksheet
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    // Export workbook to Excel file
    XLSX.writeFile(wb, "job_role_completion.xlsx");
  }

  // Add event listener to export button
  document.getElementById('exportButton').addEventListener('click', exportToExcel);
</script>


@endpush
