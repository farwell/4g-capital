@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.user-evaluation.actions.edit', ['name' => $userEvaluation->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <user-evaluation-form
                :action="'{{ $userEvaluation->resource_url }}'"
                :data="{{ $userEvaluation->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.user-evaluation.actions.edit', ['name' => $userEvaluation->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.user-evaluation.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </user-evaluation-form>

        </div>
    
</div>

@endsection