@formField('input', [
    'name' => 'faq_title',
    'label' => 'FAQ Title',
    'maxlength' => 200,
    'translated' => true,
])
@formField('select', [
    'name' => 'display',
    'label' => 'Display',
    'placeholder' => 'Select where to Display',
    'options' => [
        [
            'value' => 'not_authenticated',
            'label' => 'Not Authenticated'
        ],
        [
            'value' => 'authenticated',
            'label' => 'Authenticated'
        ],
    ]
])

@formField('wysiwyg', [
    'name' => 'faq_description',
	'label' => 'Text Description',
	'toolbarOptions' => [
        'bold',
        'italic',
        ['list' => 'bullet'],
        ['list' => 'ordered'],
        [ 'script' => 'super' ],
        [ 'script' => 'sub' ],
        'link',
        'clean',
        'image',
    ],
    
	'translated' => true,
    'note' => 'Use this when uploading images'
])

@formField('medias', [
    'name' => 'image',  //role
    'label' => 'Image',
	'withVideoUrl' => false,
	'max' => 1,
])

@formField('files', [
	'name' => 'video',
	'label' => 'Video',
	'noTranslate' => true,
])

