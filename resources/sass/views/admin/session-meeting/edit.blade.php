@extends('layouts.admin_no_nav')

@push('extra_css')
<link href="{{ asset('/css/reports.css') }}" rel="stylesheet">

	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush


@section('title', trans('admin.sessionmeetings.actions.index'))

@section('customPageContent')

    <div class="container-xl">
        <div class="card">

            <session-meeting-form
                :action="'{{ $sessionMeeting->resource_url }}'"
                :data="{{ $sessionMeeting->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.session-meeting.actions.edit', ['name' => $sessionMeeting->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.session-meeting.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </session-meeting-form>

        </div>
    
</div>

@endsection

@push('extra_js')
<script src="{{ asset('/js/admin.js') }}"></script> 


@endpush