@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
    'name' => 'key',
    'label' => 'Key',
    'maxlength' => 100
    ])
    @formField('select', [
    'name' => 'menu_type',
    'label' => 'Menu Type',
    'placeholder' => 'Select Menu Type',
    'options' => collect($menuTypeList ?? ''),
    ])
    @formField('input', [
    'name' => 'header_title',
    'label' => 'Header Title',
    'translated' => true,
    'maxlength' => 500
    ])
    @formField('wysiwyg', [
    'name' => 'description',
    'label' => 'Description',
    'translated' => true,
    'maxlength' => 2000
    ])

    @formField('medias',[
    'name' => 'hero_image',
    'label' => 'Hero image',
    ])

@formField('medias',[
    'name' => 'landing_image',
    'label' => 'Authenticate Home image',
    ])


    @formField('files', [
    'name' => 'slide_show',
    'label' => 'Slide Show',
    'max' => 5,
    'note' => 'Minimum image width: 1500px',
    'withVideoUrl' => true,
    ])

    @formField('input', [
    'name' => 'link_text',
    'label' => 'Link Text',
    'maxlength' => 200,
    'translated' => true,
    ])
    @formField('input', [
    'name' => 'url',
    'label' => 'URL',
    'translated' => true,
    ])

    @formField('select', [
    'name' => 'variant',
    'label' => 'Variant',
    'placeholder' => 'Select a variant',
    'options' => [
    [
    'value' => 'green_bg',
    'label' => 'Green Background'
    ],
    [
    'value' => 'black_bg',
    'label' => 'Black Background'
    ],
    [
    'value' => 'white_bg_green_border',
    'label' => 'White Background Green Border'
    ],

    [
    'value' => 'white_bg_black_border',
    'label' => 'White Background black Border'
    ]
    ]
    ])

    @formField('block_editor', [

    'blocks' => ['gallery', 'image_with_text', 'image_top_text', 'quote', 'faq',
    'paragraph','text_btn','text_with_title','track_record','testimonial','client','top_courses','courses','limited_courses','branch_in_panel','our_progress_map','session_in_panel','groups','video_text_grid']

    ])
@stop




@section('sideFieldsets')
    @formFieldset([ 'id' => 'seo', 'title' => 'SEO'])
    @formField('input', [
    'name' => 'meta_title',
    'label' => 'Title',
    'translated' => true,
    'maxlength' => 100,
    ])

    @formField('input', [
    'name' => 'meta_description',
    'label' => 'Description',
    'translated' => true,
    'maxlength' => 200,
    ])
    @endformFieldset
@stop
