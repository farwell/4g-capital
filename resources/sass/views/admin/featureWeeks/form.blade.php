@extends('twill::layouts.form')

@section('contentFields')


@formField('files', [
	'name' => 'resource_video',
	'label' => 'Video',
	'note' => 'This applies for video resources',
])
    @formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Description',
        'placeholder' => 'Introduction Text',
		'maxlength' => 2000,
		'translated' => true,
    ])
@stop
