
<style>
    .course-cat{
        width:100%;
    }
</style>
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.course.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('course_category_id'), 'has-success': fields.course_category_id && fields.course_category_id.valid }">
    <label for="course_category_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.course_category_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <select v-model="form.course_category_id" class="form-control"  placeholder="Select Course Category" label="title" track-by="course_category_id">
            @foreach( $categories as $category)
            <option   style="min-width: 100%;"  name="course_category_id" value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
            </select>

        {{-- <input type="text" v-model="form.course_category_id" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('course_category_id'), 'form-control-success': fields.course_category_id && fields.course_category_id.valid}" id="course_category_id" name="course_category_id" placeholder="{{ trans('admin.course.columns.course_category_id') }}"> --}}
        <div v-if="errors.has('course_category_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('course_category_id') }}</div>
    </div>
</div>




<div class="form-group row align-items-center" :class="{'has-danger': errors.has('more_info'), 'has-success': fields.more_info && fields.more_info.valid }">
    <label for="more_info" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.more_info') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <quill-editor v-model="form.more_info" :options="wysiwygConfig" name="more_info"  id="more_info"/>
            {{-- <textarea class="form-control" v-model="form.more_info" v-validate="''" id="more_info" name="more_info" :options="wysiwygConfig"></textarea> --}}
        </div>
        <div v-if="errors.has('more_info')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('more_info') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('effort'), 'has-success': fields.effort && fields.effort.valid }">
    <label for="effort" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.effort') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.effort" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('effort'), 'form-control-success': fields.effort && fields.effort.valid}" id="effort" name="effort" placeholder="{{ trans('admin.course.columns.effort') }}">
        <div v-if="errors.has('effort')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('effort') }}</div>
    </div>
</div>


<div class="form-check row" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="status" type="checkbox" v-model="form.status" v-validate="''" data-vv-name="status"  name="status_fake_element">
        <label class="form-check-label" for="status">
            {{ trans('admin.course.columns.status') }}
        </label>
        <input type="hidden" name="status" :value="form.status">
        <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
    </div>
</div>

@include('brackets/admin-ui::admin.includes.media-uploader', [
                            'mediaCollection' => app(App\Models\Course::class)->getMediaCollection('course_video'),
                            'label' => 'Course Video'
                        ])

