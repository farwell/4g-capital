@extends('twill::layouts.form')

@section('contentFields')
@formField('input', [
		'name' => 'key',
		'label' => 'Key',
		'maxlength' => 100
	])
    @formField('input', [
        'name' => 'description',
        'label' => 'Link',
        'translated' => true,
        'maxlength' => 100
    ])
@stop
