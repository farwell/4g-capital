@extends('twill::layouts.form')

@section('contentFields')
@formField('multi_select', [
	'name' => 'courses',
	'label' => 'Select Mandatory Course for Job Role',
	'placeholder' => 'Select Course',
    'min'=> 1,
    'unpack' => false,
	'options' => collect($courseList ?? ''),
])


@stop
