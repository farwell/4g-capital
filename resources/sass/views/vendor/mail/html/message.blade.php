@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @include('mail::header')
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @include('mail::footer')
    @endslot
@endcomponent
