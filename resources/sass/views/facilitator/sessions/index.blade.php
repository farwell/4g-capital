@extends('layouts.dashboard')

@section('title','Sessions')


@section('content')

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
      <div class="kt-portlet__head-label">
        <span class="kt-portlet__head-icon">
          <i class="kt-font-brand flaticon2-line-chart"></i>
        </span>
        <h3 class="kt-portlet__head-title">
          All Meetings
        </h3>
      </div>
      <div class="kt-portlet__head-toolbar">
        <div class="kt-portlet__head-wrapper">
          <div class="kt-portlet__head-actions">
            <a href="{{route('sessions.form')}}" class="btn btn-success btn-elevate btn-icon-sm">
              <i class="fas fa-plus"></i>
              Create Meeting
            </a>
          </div>
        </div>
      </div>
    </div>
  
    <div class="kt-portlet__body">
  
      <table class="table" id="courses" width="100%">
        <thead>
          <tr>
            <th>Meeting ID</th>
            <th>Topic</th>
            <th>Agenda</th>
            <th>Start Time</th>
            <th>URL</th>
            <th>Tools</th>
          </tr>
        </thead>
       <tbody> 
         
        
        @foreach($data['meetings'] as $meeting)
        <tr>
          
       <td>{{$meeting['id']}} </td>
       <td>{{$meeting['topic']}} </td>
       <td>{{$meeting['agenda']}} </td>
       <td>{{$meeting['start_time']}} </td>
       <td><a href="{{$meeting['join_url']}}">{{$meeting['join_url']}}</a> </td>
       <td>
        <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('sessions.edit', $meeting['id']) }}">
          <i class="fas fa-edit"></i>
        </a> 
        <a
        title="Delete"
        class="btn btn-sm btn-clean btn-icon btn-icon-sm"
        data-href="{{route('sessions.delete', $meeting['id']) }}"
        data-toggle="modal"
        data-target="#deleteConfirmModal"
        >
        <i class="far fa-trash-alt"></i>
      </a></td>
        </tr>
        @endforeach
       </tbody> 
      </table>
  
    </div>
  </div>
  
  @endsection




