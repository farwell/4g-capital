<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

      <!-- Scripts -->

      <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
      <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>
      <!-- Fonts -->
  
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/main.css') }}" rel="stylesheet">
      
      <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
   
      @if ((new \Jenssegers\Agent\Agent())->isMobile())
          <link href="{{ asset('css/mobile.css') }}" rel="stylesheet" media="screen and (max-width: 840px)">
      @endif
      <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
   
      <script>(function(d){var s = d.createElement("script");s.setAttribute("data-account", "kyz0QDqj5Z");s.setAttribute("src", "https://cdn.userway.org/widget.js");(d.body || d.head).appendChild(s);})(document)</script><noscript>Please ensure Javascript is enabled for purposes of <a href="https://userway.org">website accessibility</a></noscript>
    @yield('css')
</head>

<body oncontextmenu="return false;">
    <div id="app">

        @include('layouts.partials._header')

        @include('site.includes.components.modals.courseerrors')
        @include('site.includes.components.modals.coursemessages')
         @include('site.includes.components.modals.contact-form')
         @include('site.includes.components.modals.ticket')
         @include('site.includes.components.modals.tour')
        @include('site.includes.components.modals.notification')
         <main>
        @yield('content')
         </main>
        <!-- Back to top button -->

        @if(Auth::check())

         @include('site.includes.components.apps')
        @endif

        @include('layouts.partials._footer')

        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <a id="tour_videos" data-toggle="modal" data-target="#tourModal"><span>Learn to navigate this site</span></a>
        @endif
    </div>
    
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    @yield('js')

    @if(Auth::user())
        @if(Route::current()->getName() === "home.link" && Auth::user()->initial_profile == 0)
          <script>
          $(document).ready(function() {
            $('#changePass').modal('show');
        });
          </script>


        @endif
        @endif

        <div class="modal fade popup-modal" id="changePass" tabindex="-1" role="dialog" aria-labelledby="edit-header-txt" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header" style="margin-top:15px">
  
              <h5 id="edit-header-txt" class="text-center"> Please complete your profile by filling in the below information! </h5>
            </div>
            <div class="modal-body">
              <!-- <img src="{{asset('/images/thumbs.png')}}" alt="" class="popuperror-img"> -->
              <form action="" method="post" class="" id="profileChange">
                {{ csrf_field() }}
                <div class="form-body">
  
                  <div class="form-group">
                    <label for="example-text-input" class="col-4 col-form-label">
                     Country 
                    </label>
                    <div class="col-12">
                        <div class="form-group has-feedback">

                            <select name="country_id" id="country" class="form-control m-input " required>
                              <option value=""> Select Country</option>
                              @foreach (App\Models\Country::published()->orderByTranslation('title')->get() as $country)

                              <option value="{{ $country->id }}">{{ $country->title }}</option>
                                  
                              @endforeach

                            </select>
                        </div>
                    </div>
                  </div>
  
  
                  <div class="form-group">
                    <label for="example-text-input" class="col-4 col-form-label">
                    Branch
                    </label>
                    <div class="col-12">
                      <div class="form-group has-feedback">
                        <select class="form-control" name="branch_id" id="branch" class="form-control m-input " required>
                            <option value="">Select Branch</option>
                        </select>
                      </div>
                    </div>
                  </div>
  
                  <div class="form-group">
                    <label for="example-text-input" class="col-4 col-form-label">
                    Job Role
                    </label>
                    <div class="col-12">
                      <div class="form-group has-feedback">

                        <select name="job_role_id" id="job_role" class="form-control m-input " required>
                            <option value="">Select Job Role</option>
                            @foreach (App\Models\JobRole::orderByTranslation('title')->get() as $role)

                            <option value="{{ $role->id }}">{{ $role->title }}</option>
                                
                            @endforeach

                          </select>
                        <div>
                      </div>
                    </div>
                    </div>
                  </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-4 col-form-label">
                           Gender
                        </label>
                        <div class="col-12">
                            
                            <div class="form-group has-feedback">

                                <select name="gender_id" id="profile-gender" class="form-control m-input " required>
                                    <option value=""> Select Gender</option>
                                    @foreach (App\Models\Gender::all() as $gender)
                                        <option value="{{ $gender->id }}">{{ $gender->title }}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
  
  
                  <div class="form-group">
                    <div class="row">
                      <div class="col-lg-4"></div>
                      <div class="col-lg-12 text-center">
                        <button type="submit" class="btn btn-overall btn_black_bg " id="btn-savePass">
                          Save
                        </button>
                      </div>
                    </div>
                  </div>
  
              </div>
  
              </form>
            
            </div>
        
          </div>
  
        </div>
      </div>



    @if(Auth::user())
    @if(Route::current()->getName() === "home.link"  && Auth::user()->initial_profile == 0)

    <script>
        $(document).ready(function() {
        $('#country').on('change', function() {
           var countryID = $(this).val();
           if(countryID) {
               $.ajax({
                   url: '/getBranch/'+countryID,
                   type: "GET",
                   data : {"_token":"{{ csrf_token() }}"},
                   dataType: "json",
                   success:function(data)
                   {
                     if(data){
                        $('#branch').empty();
                        $('#branch').append('<option value=" " hidden>Choose Branch</option>'); 
                        $.each(data, function(key, branch){
                            $('select[name="branch_id"]').append('<option value="'+ branch.id +'">' + branch.title+ '</option>');
                        });
                    }else{
                        $('#branch').empty();
                    }
                 }
               });
           }else{
             $('#branch').empty();
           }
        });
        });
    </script>

    <script>
            $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        var urlPass="{{route('update.profile',[ Auth::user()->id ])}}";

        $('#btn-savePass').click(function (e) {

                e.preventDefault();

                $.ajax({
                    data: $('#profileChange').serialize(),
                    url: urlPass,
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                    console.log(data);
                    if(data.status == 'ok'){
                        $('#changePass').modal('hide');
                        $("#notificationModal").find(".modal-body").append(data.success);
                        $('#notificationModal').modal('show');


                    }else{

                    }
                    // $('#changePass').modal('hide');
                    },

                });
            });

        });
    </script>
    @endif
    @endif



    <script>
        function myFunction() {
            var elmnt = document.getElementById("home-scroll");
            elmnt.scrollIntoView();
        }


        $(function() {

            $('#navbarSupportedContent')
                .on('shown.bs.collapse', function() {
                    $('.navbar-toggler-icon').addClass('hidden');
                    $('#navbar-close').removeClass('hidden');
                })
                .on('hidden.bs.collapse', function() {
                    $('.navbar-hamburger').removeClass('hidden');
                    $('#navbar-close').addClass('hidden');
                });

        });
    </script>

    


@if(!Auth::check())
<script>
  jQuery(document).ready(function($) {
 function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

was_reloaded = getCookie('was_reloaded')
console.log(was_reloaded)
if (was_reloaded != 'yes') {
        document.cookie = "was_reloaded=yes; path=/; max-age=3600;"
    location.reload();
} 
});
</script> 
@endif

<script>
  PreventIllegalKeyPress = function (e) {
if (e.target) t = e.target; //Firefox and others
else if (e.srcElement) t = e.srcElement; //IE

if (e.keyCode == 116) { //prevent F5 for refresh
    e.preventDefault();
}
if (e.keyCode == 123) { //prevent F12 for inspect
    e.preventDefault();
}
if (e.keyCode == 122) { //F11 leave fullscreen
    e.preventDefault();
} else if (e.altKey && e.keyCode == 115) { //Alt + F4
    e.preventDefault();
} else if (e.altKey && e.keyCode == 37) { //Alt + left
    e.preventDefault();
} else if (e.altKey && e.keyCode == 39) { //Alt + right
    e.preventDefault();
} else if (e.ctrlKey && e.keyCode == 82) { //Ctrl + R (reload)
    e.preventDefault();
}
};


$(document).keydown(function (e) {

PreventIllegalKeyPress(e);

});
</script>

</body>

</html>
