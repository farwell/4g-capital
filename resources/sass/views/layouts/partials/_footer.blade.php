@php
use App\Models\Socialmedia;
use App\Models\Menus;
use App\Models\FooterText;

// $facebook = 'facebook';
// $twitter = 'twitter';
// $youtube = 'youtube';

// $socialfacebook = Socialmedia::where('key',$facebook)->published()->first();
// $socialtwitter = Socialmedia::where('key',$twitter)->published()->first();
// $socialyoutube = Socialmedia::where('key',$youtube)->published()->first();

// $mainmenus = Menus::where('menu_type',1)->published()->orderBy('position', 'asc')->get();

$mainoffice = FooterText::where('id', 1)
    ->published()
    ->first();

$fieldoffice = FooterText::where('id', 2)
    ->published()
    ->first();

$touch = FooterText::where('id', 3)
    ->published()
    ->first();

@endphp
@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <footer>
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-12">
                    <div class="row">
                       
                            <div class="col-md-10 ">
                                <div class="footer-text copyright">
                                    <ul>
                                        <li><small>Copyright &copy; <?php echo date('Y'); ?> {{ config('app.name', 'Laravel') }} | All Rights Reserved</small>&nbsp;&nbsp;&nbsp;<small class="bordered-left">Powered By <a href="https://farwell-consultants.com"  aria-label="Farwell Innovations(opens in new tab)"  target="_blank">Farwell Innovations Ltd</a></small></li>
                                    </ul>
                                </div>
                            </div>
                      
                       
                        <div class="col-md-2 ">
                                <div class="row">
                                    <div class="form-group col-md-12 ">
                                      
                                        @if(!empty($touch))

                                        <a class="btn btn-overall btn_get_touch" target="_blank" href=" https://farwell-consultants.com/farwellticket/farwellticket/index.php?category=19&a=add"><span><i class="support-icon" ></i>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $touch->title }} </a></span></a>


                                     <!--    <a class="btn btn-overall btn_get_touch" title="Support" data-toggle="modal"  data-target="#ticketModal"><i class="support-icon" ></i>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $touch->title }} </a> -->
                                        
                                      @endif
                                    
                                    </div>
                                </div>
                        </div>
                    </div>


                </div>


            </div>
        </div>

     
        <div class="row footer-partner"> 
        </div>
    </footer>
@endif

@if ((new \Jenssegers\Agent\Agent())->isMobile())


    <footer>

        <div class="col-md-12 col-12 mobile-suport-row">
            <div class="row">
                <div class="col-sm-8 col-xs-6">
                    <div class="footer-text copyright">
                        <ul>
                            <li><small>Copyright &copy; <?php echo date('Y'); ?> {{ config('app.name', 'Laravel') }} | All Rights Reserved | Powered By <a href="https://farwell-consultants.com"  aria-label="Farwell Innovations(opens in new tab)"  target="_blank">Farwell Innovations Ltd</a></small></li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-4 col-xs-6">
                    
                    @if(!empty($touch))
                                        <button class="btn btn-overall btn_get_touch" title="Support"
                                            data-toggle="modal"
                                            data-target="#ticketModal"> <i class="support-icon" ></i>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $touch->title }} 
                                        </button>
                                      @endif
                                    
                    <a id="tour_videos" data-toggle="modal" data-target="#tourModal"><span>Learn to navigate this site</span></a>         
                                
                </div>

            </div>


        </div>

        <div class="row footer-partner"> 
        </div>

    </footer>

@endif
