@php
use PragmaRX\Countries\Package\Countries;
@endphp

@extends('layouts.app')

@section('title','My Courses')

@section('content')
<div class="courses-page">
 
  @include('site.includes.components.parallax',[
  'image'=>asset("images/banners/session_header.png"),
  'text'=>'Training uptake per branch'
  ])

  @component('site.includes.components.breadcrumbs')
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{{ route('connect.activity') }}">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('pages', ['key' => 'our-progress']) }}"> Our Progress</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
      <a href="{{ url()->current() }}" class="active">{{ $name }} Branches</a>
    </li>
  </ol>
  @endcomponent

  @if(Session::has('course_success'))
    <script>
    jQuery(document).ready(function($){
      console.log("it has");
     $("#CourseSuccess").addClass('show');
  });
  </script>
  @elseif(Session::has('course_errors'))
  <script>
  jQuery(document).ready(function($){
   $("#CourseErrors").addClass('show');
  });
  </script>
  @else
      {{-- <script>
  window.addEventListener('load', function() {
      if(!window.location.hash) {
          window.location = window.location + '#/';
          window.location.reload();
      }
  });
  </script> --}}
  @endif
  <div class="clearfix">
    <br /> 
</div>

@if((new \Jenssegers\Agent\Agent())->isDesktop())

<div class="col-md-12">     
    <div class="row alignment-class-connect">
   
        <div class="col-md-12 col-12">
         
            <section class="general-section business-general home-section-filter">
                <div class="container-fluid">
                    @include('site.includes.components.country_branches_filter',['name'=> $name])
                </div>
            </section>
           
             

              @if(!count($branches))
      
              <div class="text-center col-12">
                  <br /><br /><br />
                  <p>
                    There are no branches found.
                  </p>
                  
                
              </div>
              @endif

            <div class="row my-course">
                @foreach ($branches as $branch)
                <div class="col-lg-2 col-md-2 col-sm-6 col-12">
                    @include(
                        'site.includes.components.branch-card',
                        ['branch' => $branch]
                    )
                </div>
            @endforeach
            </div>
            
  
         
        
    </div>
    
</div>


@else
<div class="col-md-12">     
    <div class="row ">
    
        <section class="general-section business-general home-section-filter">
            <div class="container-fluid">
                @include('site.includes.components.country_branches_filter',['name'=> $name])
            </div>
        </section>

          @if(!count($branches))
  
          <div class="text-center col-12">
              <br /><br /><br />
              <p>
                  There are no branches from this  {{ $name }}.
              </p>
              
            
          </div>
          @endif

        <div class="row my-course">
            @foreach ($branches as $branch)
            <div class="col-lg-2 col-md-2 col-sm-6 col-12">
                @include(
                    'site.includes.components.branch-card',
                    ['branch' => $branch]
                )
            </div>
        @endforeach
        </div>
      
        
        
    </div>
    
</div>
@endif

</div>
<div class="clearfix"><br /></div>
@endsection

