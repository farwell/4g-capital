@extends('layouts.app_no_js')

@section('title','Background')

@section('content')
<div class="background-page">
    @include('site.includes.components.parallax',[
    'image'=> asset("images/banners/connect_banner.png"),
     'text'=> "Connect"
    ])

    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">Community</a>
        </li>
    </ol>
    @endcomponent
    <div class="clearfix">
        <br /> 
    </div>

    @if((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="col-md-12">     
        <div class="row alignment-class-connect profile-row">
          <div class="col-3">
              @include('site.includes.components.connect.side_menu')
          </div>

            <div class="col-md-9 col-12">
                <div class="card community-card">
                    <article>
                        <div class="row">
                            <div class="col-5">
                                <div class="community-card-content mt-2 ml-4">
                                <h1 class="first-h1">Connect with the</h1>
                                <h1 class="second-h1">Feminist Community</h1>
                                <p> Engage and share strategies that advocate to challenge patriarchy and change the trajectory of the daily lived realities of young women in Eritrea, Ethiopia, Kenya, Somaliland, South Sudan, Tanzania and Uganda. </p>
                              </div>

                              <div class="community-status">
                                  <div class="row">
                                    <div class="col-4">
                                        <div id="participants" class="stats">
                                            <p class="community-partcipants"><span class="connect-icon icon-participants"></span> <span class="status-numbers">{{sprintf("%02d",count($participants))}}</span> Participants</p>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div id="countries" class="stats">
                                            <p class="community-countries"><span class="connect-icon icon-countries"></span> <span class="status-numbers pr-3">{{sprintf("%02d",count($countries))}}</span> Countries</p>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div id="issues" class="stats">
                                            <p class="community-issues"><span class="connect-icon icon-issues"></span> <span class="status-numbers pr-3">{{sprintf("%02d",count($issues))}} </span> Issues</p>
                                        </div>
                                    </div>

                                  </div>
                               

                              </div>
                            </div>
                              <div class="col-6">
                                  <div id="map" >
                     
                                  </div>
          
                              </div>
    
                        </div>
                    </article>
                </div>


            </div>
            
            
        </div>
        
    </div>


    @else
    <div class="col-md-12">     
        <div class="row ">
          <div class="col-12">
            @include('site.includes.components.connect.side_menu')
          </div>

            <div class="col-md-12 col-12">
                <div class="card community-card">
                    <article>
                        <div class="row">
                            <div class="col-12">

                                <div class="community-card-content mt-4 ml-2">
                                    <h1 class="first-h1">Connect with the</h1>
                                    <h1 class="second-h1">Feminist Community</h1>
                                    <p> Engage and share strategies that advocate to challenge patriarchy and change the trajectory of the daily lived realities of young women in Eritrea, Ethiopia, Kenya, Somaliland, South Sudan, Tanzania and Uganda. </p>
                                  </div>
                               
                              </div>
                              <div class="col-12">
                                  <div id="map" >
                     
                                  </div>
          
                              </div>

                              <div class="col-12">
                                <div class="community-status">
                                    <div class="row">
                                      <div class="col-4">
                                          <div id="participants" class="stats">
                                              <p class="community-partcipants"><span class="connect-icon icon-participants"></span> <span class="status-numbers">{{sprintf("%02d",count($participants))}}</span> Participants</p>
                                          </div>
                                      </div>
  
                                      <div class="col-4">
                                          <div id="countries" class="stats">
                                              <p class="community-countries"><span class="connect-icon icon-countries"></span> <span class="status-numbers">{{sprintf("%02d",count($countries))}}</span> Countries</p>
                                          </div>
                                      </div>
                                      <div class="col-4">
                                          <div id="issues" class="stats">
                                              <p class="community-issues"><span class="connect-icon icon-issues"></span> <span class="status-numbers">{{sprintf("%02d",count($issues))}} </span> Issues</p>
                                          </div>
                                      </div>
  
                                    </div>
                                 
  
                                </div>

                              </div>
    
                        </div>
                    </article>
                </div>


            </div>
            
            
        </div>
        
    </div>
    @endif
@endsection


@section('js')
<script src="{{asset('maps/highmaps.js')}}"></script>
<script src="{{asset('maps/africa.js')}}"></script>

{{-- <script src="{{asset('maps/africa.js')}}"></script>
<script src="{{asset('maps/africa.js')}}"></script> --}}
<script> 
var width = $(window).width();
var height = $(window).height();

        var data = [

            @foreach($data as  $key=>$value)
            ['{{$key}}','{{json_encode($value, TRUE)}}'],
            @endforeach
        ];
       var data2 = [...data];
     
        // tooltips create...generate
        $(document).ready(function () {
          var generate_tooltips = function () {

            var bodies = JSON.parse(this.point.value.replace(/&quot;/g,'"'));
          
            var common_name = bodies.common_name;
            var users = bodies.users;
            var country_flag = bodies.country_flag;
           // alert(this.point.trans);
    
              var mintype=" ";    
              
              if(common_name === 'Côte d&#039;Ivoire'){
                common_name = "Côte d'Ivoire";
              }
              console.log(country_flag);

            var flag ="<img src='{{ url('/images') }}"+"/" + country_flag + "' width='20px'/>" ;

            console.log(flag);

            return '<div>'+ flag +' <span>' + common_name  +'</span>' + '<br/> Number of participants:'+ 
              users + '</div>'; 
          };

            var maps = Highcharts.mapChart('map', {
                chart: {
                    map: 'custom/africa',
                    // width: 500,
                    // height: 600,
                    backgroundColor: "rgba(0,0,0,0)",
                    events: {
                        load: function() {
                            console.log('loaded')
                        }
                    }
                },

                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },

                mapNavigation: {
                    enabled: false,
                    buttonOptions: {
                        verticalAlign: 'bottom'
                    }
                },

                colorAxis: {
                    min: 0
                },
                tooltip: {
                  formatter: generate_tooltips,
                  backgroundColor: '#333333',
                  borderColor:'#333333',
                  useHTML: true,
                  style: {
                    color: '#ffffff',
                  }
                },


                series: [{
                    data: data.map(elem => {
                        elem.push('white');
                        //console.log(elem);
                        return elem;
                    }),
                    keys: ['hc-key', 'value', 'color'],
                    borderWidth: '1px',
                    borderColor: '#000000',
                    name: '',
                    states: {
                        hover: {
                            color: '#F18A21'
                        }
                    },
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}'
                    },
                    
                }],
                plotOptions: {
                    series: {
                        point: {
                            events: {
                             
                                click: function () {
                                
                                   location.href = '{{ url("/profile/community") }}'+'/' + this.name;
                                }
                            }
                        }
                    }
                 },
                
                
            });
         
            $('.stats').on('mouseout', function(e) {

            $.each(data2, function(key, value) {
                value[2] = 'white';
            });
            maps.series[0].setData(data2);
            });

            $('#participants').on('mouseenter', function(e) {
                $.ajax({
                    url: '{{ url('/profile/community/participants') }}',
                    type: 'GET',
                    success: function(response) {
                        let list = response;
                        let localData = JSON.parse(JSON.stringify(data));

                        $.each(localData, function(key, value) {
                            if (list.includes(value[0])) {
                                value[2] = "#f18a21";
                            }
                        });
                       
                         maps.series[0].setData(localData);
                    }
                });
            });
   

            $('#countries').on('mouseenter', function(e) {
                $.ajax({
                    url: '{{ url('/profile/community/countries') }}',
                    type: 'GET',
                    success: function(response) {
                        let list = response;
                        let localData = JSON.parse(JSON.stringify(data));

                        $.each(localData, function(key, value) {
                            if (list.includes(value[0])) {
                                value[2] = "#6BC7B8";
                            }
                        });
                       
                         maps.series[0].setData(localData);
                    }
                });
            });

        });
    </script>



   
@endsection
