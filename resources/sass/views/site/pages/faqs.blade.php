@extends('layouts.course_details')

@section('title','FAQ')

@section('css')
<style>
#app{
min-height: 100vh;
}
.accordion .accordion-header .accordion-button:after {
    font-family: 'FontAwesome';  
    content: "\f068";
    float: right; 
    margin-right:15px;
}
.accordion .accordion-header .accordion-button.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\f067"; 
}

@media(max-width:768px){
#app{
min-height: 89vh;
}
.background-page {
    min-height: 31rem;
}
}
</style>

@endsection

@section('content')
<div class="background-page">
    @if( $pageItem->hasImage('hero_image'))
    @php $image = $pageItem->image("hero_image", "default") ; 
    $text = $pageItem->header_title;
    @endphp
    @include('site.includes.components.parallax',[
    'image'=> $image,
    'text'=>$text
    ])
    @endif
    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{!! $pageItem->title !!}</a>
        </li>
    </ol>
    @endcomponent

    <div class="clearfix">
        <br /> <br />
    </div>


    @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row alignment-class-about">

            
            <div class="col-md-12">
                {!! $pageItem->renderBlocks(false) !!}

            </div> 

        
        </div>

    @else 
    <div class="row ">

            
        <div class="col-md-12">
            {!! $pageItem->renderBlocks(false) !!}

        </div> 

    
    </div>


    @endif


@endsection
