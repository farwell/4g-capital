@extends('layouts.app_no_js')

@section('title', 'My Courses')

@section('css')
    <style>
        /* .emoji-picker-icon {
                                                                                                                                                            position: absolute;

                                                                                                                                                            font-size: 20px;
                                                                                                                                                            opacity: 0.7;
                                                                                                                                                            z-index: 100;
                                                                                                                                                            transition: none;
                                                                                                                                                            color: black;
                                                                                                                                                            -moz-user-select: none;
                                                                                                                                                            -khtml-user-select: none;
                                                                                                                                                            -webkit-user-select: none;
                                                                                                                                                            -o-user-select: none;
                                                                                                                                                            user-select: none;
                                                                                                                                                            border: none;
                                                                                                                                                            border-radius: 3px;
                                                                                                                                                             padding: 0.4rem;
                                                                                                                                                        }
                                                                                                                                                        .main-message .emoji-picker-icon{
                                                                                                                                                            right: 68%;
                                                                                                                                                            top:50%;
                                                                                                                                                        } */
        .emoji-wysiwyg-editor::before {
            color: grey;
        }

        .emoji-wysiwyg-editor[data-placeholder]:not([data-placeholder=""]):empty::before {
            content: attr(data-placeholder);
        }

        .emoji-wysiwyg-editor::before {
            content: 'Write ''a ''comment...';


        }

        .main-message .emoji-wysiwyg-editor {
            /* height:150px; */
            /* border: 1px solid #00000029; */
        }

        .lead.emoji-picker-container {
            /* width: 300px; */
            margin-bottom: 0;
            display: block;

            input {
                width: 100%;
                height: 50px;
            }
        }


        .modal.fade .modal-dialog {
            display: flex;
            align-items: center;
            min-height: calc(100% - 0rem);

        }

    </style>

@endsection
@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax', [
            'image' => asset('images/banners/group_header.png'),
            'text' => $forum->title,
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('pages', ['key' => 'forums']) }}">Forums</a>
                </li>

                @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{ url()->current() }}" class="active">{{ $forum->title }}</a>
                    </li>
                @else
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="{{ url()->current() }}" class="active">{{ $forum->title }}</a>
                    </li>
                @endif
            </ol>
        @endcomponent
        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {

                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>
        @else
            {{-- <script>
    window.addEventListener('load', function() {
    if(!window.location.hash) {
      window.location = window.location + '#/';
      window.location.reload();
    }
    });
    </script> --}}
        @endif
        <div class="profile-row">

            <div class="row alignment-class-connect">
                {{-- <div class="col-md-3">
                    <div class="card forum-sidebar sidebar-card">
                        <article>
                            <div class="row mt-1 mr-3 ml-2">

                                <div class="col-12">
                                    <a href="javascript:;"
                                        class="btn btn-overall btn_create_group btn-add_topic btn_not_inline"
                                        onclick="showTopicForm()" style="float:left"><i class="fa fa-plus"></i> Add
                                        Topic</a>
                                </div>
                            </div>
                            <ul class="list-group mt-2 ">


                                @foreach ($topics as $topic)
                                    <li class="list-unstyled border-0 p-2 list-nav">
                                        @if ($topic->id == $forum->forum_topic)
                                            <a href="#" class="ml-3"
                                                style="color:#F87522">{{ $topic->title }}</a>
                                        @else
                                            <a href="{{ route('connect.forum') }}"
                                                class="ml-3">{{ $topic->title }}</a>
                                        @endif
                                    </li>
                                    <hr class="side-hr">
                                @endforeach
                            </ul>
                        </article>
                    </div>

                </div> --}}
                @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                    <div class="col-md-11 mt-4 ml-5">

                        @unless(empty($activities))
                            @foreach ($activities as $activity)
                                <div class="card shadow connectCard mt-3">
                                    <div class="card-body d-flex connectCardBody">
                                        <div class="d-flex">
                                            <div class="commentImg">
                                                @if ($activity->user->profile_pic)
                                                    <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                                        class="img-fluid connectImg" />
                                                @else
                                                    <img src="{{ asset('uploads/images.jpg') }}"
                                                        class="img-fluid connectImg" />
                                                @endif
                                            </div>
                                            <div class="upperRow">
                                                <div class="commentsContainer">
                                                    <div class="upperCommentRow">
                                                        <span class="name">
                                                            {{ $activity->user->username }}
                                                            <small class="time">
                                                                {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                                            </small>
                                                        </span>
                                                        {{-- <span class="time">
                                                    {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                                </span> --}}
                                                    </div>
                                                    <div class="commentMain">
                                                        <p class="connect-font">{!! $activity->description !!}</p>
                                                    </div>

                                                    <div class="d-flex align-items-center" style="">
                                                        <small class="float-left">
                                                            <span title="Likes" class=""
                                                                onclick="sendLike({{ $activity->id }}, 'like')">
                                                                <i class="fa fa-thumbs-up" aria-hidden="true"
                                                                    id="likeUp-{{ $activity->id }}"></i> <span
                                                                    class="activity-like-count "
                                                                    id="like-{{ $activity->id }}"
                                                                    style="font-size:14px">{{ $activity->reactions->sum('likes') }}</span>
                                                            </span>
                                                            <span title="Dislikes" class=""
                                                                onclick="sendLike({{ $activity->id }}, 'dislike')">
                                                                <i class="fa fa-thumbs-down" aria-hidden="true"
                                                                    id="likeDown-{{ $activity->id }}"></i> <span
                                                                    class="activity-like-count "
                                                                    id="dislike-{{ $activity->id }}"
                                                                    style="font-size:14px">{{ $activity->reactions->sum('dislikes') }}</span>
                                                            </span>
                                                            {{-- <a href="javascript:;"onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');" class="replylink" >Reply</a> --}}
                                                            <span>
                                                                <img src="{{ asset('svgs/comment.svg') }}" id=""
                                                                    class="img-fluid inputImg"
                                                                    onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');"
                                                                    width="20" /><span
                                                                    class="like-count">{{ $activity->replies->count() }}</span>
                                                            </span>

                                                            </form>
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <form action="{{ route('forum.reply.create', [$activity->id]) }}" method="POST"
                                            enctype="multipart/form-data" style="width: 100%; margin-top:10px">
                                            @csrf

                                            <div class="card-body connectCardBody">
                                                <div class="">
                                                    <div class="connectImgSection">
                                                        <div class="connectRight">
                                                            <div class="main-message">
                                                                <p class="lead emoji-picker-container">
                                                                    <textarea type="text" name="body" class="connectInput connect-font form-control" placeholder="Write a comment"
                                                                        id="example1" data-emojiable="true"
                                                                        data-emoji-input="unicode"></textarea>
                                                                </p>

                                                            </div>
                                                            <div class="card-footer">
                                                                <div>
                                                                    <label for="imageInput" title="attach images"><img
                                                                            src="{{ asset('svgs/images.svg') }}"
                                                                            class="img-fluid inputImg" width="20" /></label>
                                                                    <input type="file" name="images[]" id="imageInput" multiple>


                                                                    <label for="fileInput" title="attach files"><img
                                                                            src="{{ asset('svgs/attach.svg') }}"
                                                                            class="img-fluid inputImg" width="20" /></label>
                                                                    <input type="file" name="files[]" id="fileInput" multiple>

                                                                    <div id="file-upload-filename"
                                                                        style="font-size: 12px; color:#6bc7b8"></div>
                                                                </div>
                                                                <div>
                                                                    <span><button type="submit"
                                                                            class="btn connect-button">Post</button></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                        <div class="card-body pt-0" id="response-{{ $activity->id }}" style="display: block">
                                            {{-- <summary>Show Replies</summary> --}}
                                            <span class="showReplies pt-0">
                                                @include(
                                                    'site.pages.connect.forum_replies'
                                                )
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endunless


                    </div>
                @else
                    <div class="col-md-11 ">

                        @unless(empty($activities))
                            @foreach ($activities as $activity)
                                <div class="card shadow connectCard mt-3">
                                    <div class="card-body d-flex connectCardBody">
                                        <div class="d-flex">
                                            <div class="commentImg">
                                                @if ($activity->user->profile_pic)
                                                    <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                                        class="img-fluid connectImg" />
                                                @else
                                                    <img src="{{ asset('uploads/images.jpg') }}"
                                                        class="img-fluid connectImg" />
                                                @endif
                                            </div>
                                            <div class="upperRow">
                                                <div class="commentsContainer">
                                                    <div class="upperCommentRow">
                                                        <span class="name">
                                                            {{ $activity->user->username }}
                                                            <small class="time">
                                                                {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                                            </small>
                                                        </span>
                                                        {{-- <span class="time">
                                                    {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                                </span> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="commentMain">
                                            <p class="connect-font">{!! $activity->description !!}</p>
                                        </div>

                                        <div class="d-flex align-items-center" style="">
                                            <small class="float-left">
                                                <span title="Likes" class=""
                                                    onclick="sendLike({{ $activity->id }}, 'like')">
                                                    <i class="fa fa-thumbs-up" aria-hidden="true"
                                                        id="likeUp-{{ $activity->id }}"></i> <span
                                                        class="activity-like-count " id="like-{{ $activity->id }}"
                                                        style="font-size:14px">{{ $activity->reactions->sum('likes') }}</span>
                                                </span>
                                                <span title="Dislikes" class=""
                                                    onclick="sendLike({{ $activity->id }}, 'dislike')">
                                                    <i class="fa fa-thumbs-down" aria-hidden="true"
                                                        id="likeDown-{{ $activity->id }}"></i> <span
                                                        class="activity-like-count " id="dislike-{{ $activity->id }}"
                                                        style="font-size:14px">{{ $activity->reactions->sum('dislikes') }}</span>
                                                </span>
                                                {{-- <a href="javascript:;"onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');" class="replylink" >Reply</a> --}}
                                                <span>
                                                    <img src="{{ asset('svgs/comment.svg') }}" id=""
                                                        class="img-fluid inputImg"
                                                        onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');"
                                                        width="20" /><span
                                                        class="like-count">{{ $activity->replies->count() }}</span>
                                                </span>

                                                </form>
                                            </small>
                                        </div>

                                        <form action="{{ route('forum.reply.create', [$activity->id]) }}" method="POST"
                                            enctype="multipart/form-data" style="width: 100%; margin-top:10px">
                                            @csrf

                                            <div class="card-body connectCardBody">
                                                <div class="">
                                                    <div class="connectImgSection">
                                                        <div class="connectRight">
                                                            <div class="main-message">
                                                                <p class="lead emoji-picker-container">
                                                                    <textarea type="text" name="body" class="connectInput connect-font form-control" placeholder="Write a comment"
                                                                        id="example1" data-emojiable="true"
                                                                        data-emoji-input="unicode"></textarea>
                                                                </p>

                                                            </div>
                                                            <div class="card-footer">
                                                                <div>
                                                                    <label for="imageInput" title="attach images"><img
                                                                            src="{{ asset('svgs/images.svg') }}"
                                                                            class="img-fluid inputImg" width="20" /></label>
                                                                    <input type="file" name="images[]" id="imageInput" multiple>


                                                                    <label for="fileInput" title="attach files"><img
                                                                            src="{{ asset('svgs/attach.svg') }}"
                                                                            class="img-fluid inputImg" width="20" /></label>
                                                                    <input type="file" name="files[]" id="fileInput" multiple>

                                                                    <div id="file-upload-filename"
                                                                        style="font-size: 12px; color:#6bc7b8"></div>
                                                                </div>
                                                                <div>
                                                                    <span><button type="submit"
                                                                            class="btn connect-button">Post</button></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                        <div class="card-body pt-0" id="response-{{ $activity->id }}" style="display: block">
                                            {{-- <summary>Show Replies</summary> --}}
                                            <span class="showReplies pt-0">
                                                @include(
                                                    'site.pages.connect.forum_replies'
                                                )
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endunless


                    </div>

                @endif
            </div>
        </div>
    </div>
@endsection
@section('js')

    <script>
        $(function() {
            // Initializes and creates emoji set from sprite sheet
            window.emojiPicker = new EmojiPicker({
                emojiable_selector: '[data-emojiable=true]',
                assetsPath: '{{ asset('emojis/lib/img/') }}',
                popupButtonClasses: 'fa fa-smile-o',
                position: "bottom"
            });

            window.emojiPicker.discover();
        });
    </script>
    <script>
        function showHide(id) {
            $("#" + id).toggle();
        };

        function showHideReply(id) {
            $("#" + id).toggle();
        };
    </script>
    <script>
        function sendLike(id, data) {
            $.ajax({
                url: "{{ url('/profile/connect/forum/reactions/') }}/" + id,
                type: "post",
                dataType: 'json',
                data: {
                    data,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response) {
                    console.log(response)
                    document.getElementById("like-" + id).innerHTML = response.likes;
                    document.getElementById("dislike-" + id).innerHTML = response.dislikes;
                },
                error: function(response) {
                    // console.log(response)
                }
            });
        };
    </script>
    <script>
        function sendReplyLike(activityId, replyId, data) {
            console.log(activityId, replyId, data)
            $.ajax({
                url: "{{ url('/profile/connect/forum/reply/reacttions/create/') }}/" + activityId + "/" +
                    replyId,
                type: "post",
                dataType: 'json',
                data: {
                    data,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response) {
                    console.log(document.getElementById("dislike-" + replyId))
                    document.getElementById("like-reply-" + replyId).innerHTML = response.likes;
                    document.getElementById("dislike-reply-" + replyId).innerHTML = response.dislikes;
                },
                error: function(response) {
                    console.log(response)
                }
            });
        }
    </script>
    <script>
        $(document).ready(function() {

            // Get current page URL
            var url = window.location.href;



            // remove # from URL
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

            // remove parameters from URL
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

            // select file name
            url = url.split('/')[4];




            // Loop all menu items
            $('.navbar-nav .nav-item').each(function() {

                // select href
                var href = $(this).find('a').attr('href');

                link = href.split('/')[3];


                // Check filename
                if (link === 'forums') {

                    // Add active class
                    $(this).addClass('active');
                }
            });
        });
    </script>


    <script>
        $(document).ready(function() {

            var input = document.getElementById('imageInput');
            var gif = document.getElementById('gifInput');
            var attach = document.getElementById('fileInput');

            var replyInput = document.getElementById('replyimageInput');
            var replyFile = document.getElementById('replyfileInput');

            var infoArea = document.getElementById('file-upload-filename');

            var replyInfoArea = document.getElementById('reply-upload-filename');

            input.addEventListener('change', showFileName);

            attach.addEventListener('change', showAttachName);

            replyInput.addEventListener('change', showReplyInputName);
            replyFile.addEventListener('change', showReplyFileName);

            function showFileName(event) {

                // the change event gives us the input it occurred in 
                var input = event.srcElement;

                // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
                var fileName = input.files[0].name;

                // use fileName however fits your app best, i.e. add it into a div
                infoArea.textContent = 'File name: ' + fileName;
            }



            function showAttachName(event) {

                // the change event gives us the input it occurred in 
                var input = event.srcElement;

                // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
                var fileName = input.files[0].name;

                // use fileName however fits your app best, i.e. add it into a div
                infoArea.textContent = 'File name: ' + fileName;
            }


            function showReplyInputName(event) {

                // the change event gives us the input it occurred in 
                var input = event.srcElement;

                // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
                var fileName = input.files[0].name;

                // use fileName however fits your app best, i.e. add it into a div
                replyInfoArea.textContent = 'File name: ' + fileName;
            }

            function showReplyFileName(event) {

                // the change event gives us the input it occurred in 
                var input = event.srcElement;

                // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
                var fileName = input.files[0].name;

                // use fileName however fits your app best, i.e. add it into a div
                replyInfoArea.textContent = 'File name: ' + fileName;
            }

        });
    </script>

    {{-- <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

    <script>
        CKEDITOR.replace( 'description');
        CKEDITOR.replace( 'description-reply');
    
    </script> --}}
@endsection
