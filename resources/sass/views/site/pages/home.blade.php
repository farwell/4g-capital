@extends('layouts.app_no_js')

@section('content')
    @php
        
        $variant = $pageItem->variant;
        $url = $pageItem->url;
        $linkText = $pageItem->link_text;
        
    @endphp


    @if ((new \Jenssegers\Agent\Agent())->isDesktop())

        @if (!Auth::check())
            @if ($pageItem->hasImage('hero_image'))

                @if (Auth::check())
                    @php
                        $gender = Auth::user()->gender_id;
                    @endphp
                    @if (session('message') && empty($gender))
                        <div class="alert alert-info">
                            {!! session('message') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        {{ session()->forget('message') }}
                    @endif
                    <div class="overlay">
                        <section class='device-banner-auth'
                            style="background:url('{{ $pageItem->image('hero_image', 'default') }}');height: auto;
                                            background-position: center;
                                            background-repeat: no-repeat;
                                            background-size: cover;">
                            <div class="row ">
                                <div class="col-md-12">
                                    <div class='home-banner-auth'>
                                        <div class="banner-section-text">
                                        @else
                                            <section class='device-banner'
                                                style="background:url('{{ $pageItem->image('hero_image', 'default') }}');
                                        background-position: center;
                                        background-repeat: no-repeat;
                                        background-size: cover;">
                                                <div class="row ">
                                                    <div class="col-md-12">
                                                        <div class='device-banner home-banner'>
                                                            <div class="banner-section-text">
                @endif
            @else
                <div class="overlay" style="height: 100%">
                    <section class='device-banner'>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class='device-banner home-banner'>
                                    <div class="banner-section-text">
            @endif
        @else
            @if ($pageItem->hasImage('landing_image'))

                @if (Auth::check())
                    @php
                        $gender = Auth::user()->gender_id;
                    @endphp
                    @if (session('message') && empty($gender))
                        <div class="alert alert-info">
                            {!! session('message') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        {{ session()->forget('message') }}
                    @endif
                    <div class="overlay">
                        <section class='device-banner-auth'
                            style="background:url('{{ $pageItem->image('landing_image', 'default') }}');height: auto;
                                        background-position: center;
                                        background-repeat: no-repeat;
                                        background-size: cover;">
                            <div class="row ">
                                <div class="col-md-12">
                                    <div class='home-banner-auth'>
                                        <div class="banner-section-text">
                                        @else
                                            <section class='device-banner'
                                                style="background:url('{{ $pageItem->image('landing_image', 'default') }}');
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover;">
                                                <div class="row ">
                                                    <div class="col-md-12">
                                                        <div class='device-banner home-banner'>
                                                            <div class="banner-section-text">
                @endif
            @else
                <div class="overlay" style="height: 100%">
                    <section class='device-banner'>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class='device-banner home-banner'>
                                    <div class="banner-section-text">
            @endif


        @endif



        <h1>{!! $pageItem->header_title !!}</h1>
        <p>{!! $pageItem->description !!}</p>

        @if ($variant)
            @if ($variant && $variant === 'green_bg')
                @guest
                    <a href="{{ route('login') }}" class="btn btn-overall btn_green_bg">
                        {{ $linkText }}
                    </a>
                @else
                    <a href="{{ $url }}" class="btn btn-overall btn_green_bg">
                        {{ $linkText }}
                    </a>
                @endif
            @elseif ($variant && $variant === 'black_bg')
                @guest
                    <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                        {{ $linkText }}

                    </a>
                @else
                    <a href="{{ $url }} " class="btn btn-overall btn_black_bg">
                        {{ $linkText }}

                    </a>
                @endif
            @elseif ($variant && $variant === 'white_bg_green_border')
                @guest
                    <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                        {{ $linkText }}

                    </a>
                @else
                    <a href="{{ $url }} " class="btn btn-overall btn_white_bg_green_border">
                        {{ $linkText }}

                    </a>
                @endif
            @elseif ($variant && $variant === 'white_bg_black_border')
                @guest
                    <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                        {{ $linkText }}

                    </a>
                @else
                    <a href="{{ $url }} " class="btn btn-overall btn_white_bg_black_border">
                        {{ $linkText }}

                    </a>
                    @endif
                @else
                    <a href="{{ $url }}" class="btn btn-overall btn_green_bg">
                        {{ $linkText }}

                    </a>

                    @endif
                    @endif
                    </div>
                    </div>
                    </div>

                    </div>

                    </section>
                    </div>
                @else
                    @if (!Auth::check())
                        @if ($pageItem->hasImage('hero_image'))

                            @if (Auth::check())
                                <div class="overlay">
                                    <section class='device-banner-auth'
                                        style="background:url('{{ $pageItem->image('hero_image', 'default') }}');height: auto;
                                            background-position: center;
                                            background-repeat: no-repeat;
                                            background-size: cover;">
                                        <div class="row ">
                                            <div class="col-md-12">
                                                <div class='home-banner-auth'>
                                                    <div class="banner-section-text">
                                                    @else
                                                        <section class='device-banner'
                                                            style="background:url('{{ $pageItem->image('hero_image', 'default') }}');
                                        background-position: center;
                                        background-repeat: no-repeat;
                                        background-size: cover;">
                                                            <div class="row ">
                                                                <div class="col-md-12">
                                                                    <div class='device-banner home-banner'>
                                                                        <div class="banner-section-text">
                            @endif
                        @else
                            <div class="overlay" style="height: 100%">
                                <section class='device-banner'>
                                    <div class="row ">
                                        <div class="col-md-12">
                                            <div class='device-banner home-banner'>
                                                <div class="banner-section-text">
                        @endif
                    @else
                        @if ($pageItem->hasImage('landing_image'))

                            @if (Auth::check())
                                <div class="overlay">
                                    <section class='device-banner-auth'
                                        style="background:url('{{ $pageItem->image('landing_image', 'default') }}');height: auto;
                                        background-position: center;
                                        background-repeat: no-repeat;
                                        background-size: cover;">
                                        <div class="row ">
                                            <div class="col-md-12">
                                                <div class='home-banner-auth'>
                                                    <div class="banner-section-text">
                                                    @else
                                                        <section class='device-banner'
                                                            style="background:url('{{ $pageItem->image('landing_image', 'default') }}');
                                    background-position: center;
                                    background-repeat: no-repeat;
                                    background-size: cover;">
                                                            <div class="row ">
                                                                <div class="col-md-12">
                                                                    <div class='device-banner home-banner'>
                                                                        <div class="banner-section-text">
                            @endif
                        @else
                            <div class="overlay" style="height: 100%">
                                <section class='device-banner'>
                                    <div class="row ">
                                        <div class="col-md-12">
                                            <div class='device-banner home-banner'>
                                                <div class="banner-section-text">
                        @endif



                    @endif
                    <div class="row ">
                        <div class="col-sm-12 col-xs-12">
                            <div class='device-banner home-banner'>
                                <div class="banner-section-text">
                                    <h1><strong>{!! $pageItem->header_title !!}</strong></h1>
                                    <p>{!! $pageItem->description !!}</p>
                                    @if ($variant)
                                        @if ($variant && $variant === 'green_bg')
                                            @guest
                                                <a href="{{ route('login') }}" class="btn btn-overall btn_green_bg">
                                                    {{ $linkText }}
                                                </a>
                                            @else
                                                <a href="{{ $url }}" class="btn btn-overall btn_green_bg">
                                                    {{ $linkText }}
                                                </a>
                                            @endif
                                        @elseif ($variant && $variant === 'black_bg')
                                            @guest
                                                <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                                                    {{ $linkText }}

                                                </a>
                                            @else
                                                <a href="{{ $url }} " class="btn btn-overall btn_black_bg">
                                                    {{ $linkText }}

                                                </a>
                                            @endif
                                        @elseif ($variant && $variant === 'white_bg_green_border')
                                            @guest
                                                <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                                                    {{ $linkText }}

                                                </a>
                                            @else
                                                <a href="{{ $url }} " class="btn btn-overall btn_white_bg_green_border">
                                                    {{ $linkText }}

                                                </a>
                                                @endif
                                            @elseif ($variant && $variant === 'white_bg_black_border')
                                                @guest
                                                    <a href="{{ route('login') }}" class="btn btn-overall btn_black_bg">
                                                        {{ $linkText }}

                                                    </a>
                                                @else
                                                    <a href="{{ $url }} " class="btn btn-overall btn_white_bg_black_border">
                                                        {{ $linkText }}

                                                    </a>
                                                    @endif
                                                @else
                                                    <a href="{{ $url }}" class="btn btn-overall btn_green_bg">
                                                        {{ $linkText }}

                                                    </a>

                                                    @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                    {{-- <button onclick="myFunction()" title="" class="btn-arrow arrow bounce d-none d-md-block" aria-hidden="true" style="" id="arrow-button">
    <i class="fas fa-chevron-down"></i>
  </button> --}}
                                    </section>
                                    </div>
                                    @endif


                                    @if (Auth::user())
                                        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                                            <section class="general-section business-general home-section-filter">
                                                <div class="container-fluid">
                                                    @include('site.includes.components.home_filter', [
                                                        'jobRoles' => $jobRoles,
                                                    ])
                                                </div>
                                            </section>
                                        @else
                                            <section class="business-general home-section-filter">
                                                <div class="container-fluid">
                                                    @include('site.includes.components.home_filter', [
                                                        'jobRoles' => $jobRoles,
                                                    ])
                                                </div>
                                            </section>

                                        @endif
                                    @endif
                                    @if (!Auth::check())
                                    @else
                                        {!! $pageItem->renderBlocks(
                                            true,
                                            [],
                                            ['jobRole' => $jobRole, 'courseCategories' => $courseCategories, 'search' => $search],
                                        ) !!}

                                    @endif
                                @endsection
                                @section('js')
                                    <script>
                                        function resetForm() {
                                            document.getElementById("homeForm").reset();
                                        }
                                    </script>
                                @endsection
