@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="form-group row justify-content-center ">
    <div class="col-12 col-md-8 ">
        <a href="{{ url('auth/google') }}" style="margin-top: 20px;" class="btn btn-lg btn-signin-google btn-block">
            <i class="signin-button-icon signin-button"></i> <p>Sign in with Google </p>
          </a> 
    </div>
    
</div>

@elseif((new \Jenssegers\Agent\Agent())->isTablet())

<div class="form-group row justify-content-center ">
    <div class="col-7 ">
        <a href="{{ url('auth/google') }}" style="margin-top: 20px;" class="btn btn-lg btn-signin-google btn-block">
            <i class="signin-button-icon signin-button"></i> <p>Sign in with Google </p>
          </a> 
    </div>
    
</div>

@else 
<div class="form-group row justify-content-center ">
    <div class="col-10">
        <a href="{{ url('auth/google') }}" style="margin-top: 20px;" class="btn btn-lg btn-signin-google btn-block">
            <i class="signin-button-icon signin-button"></i> <p>Sign in with Google </p>
          </a> 
    </div>
    
</div>


@endif
