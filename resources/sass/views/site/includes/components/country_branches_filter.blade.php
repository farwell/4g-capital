@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="container-fluid ">
        <div class="row justify-content-center">
            <div class="card resource-filter mb-2 mt-4">
                <div class="col-12">
                    <div class="row justify-content-center">

                        <div class="col-12">
                            <form action="{{ route('progress.country',['name'=>$name]) }}" method="POST" class="homeForm">
                                @csrf
                                <div class="row justify-content-center">

                                    @error('category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                   
                                    <div class="col-9 no-padding-right ">
                                        <div class="row">

                                            <div class="col-3 mt-4 mb-4 ">
                                                <label class="filter_by"> Filter branches by</label>
                                            </div>
                                            <div class="col-4">
                                                <select name="country" id="" class="form-control mt-4 mb-4 ">
                                                    <option value=''>Select Country</option>
                                                    @foreach (App\Models\Country::published()->orderByTranslation('title')->get() as $theme)
                                                        <option value="{{ $theme->id }}"> {{ $theme->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-5">

                                                <input type="text" placeholder="Free text search ... eg branch name" class="form-control mt-4 mb-4 " name="search">
                                               
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-3 ">
                                        <button type="submit"
                                            class="btn btn-overall btn_black_bg mt-4 mb-4">Search</button>

                                        <button type="reset" class="btn btn-overall btn_cancel mt-4 mb-4"
                                            onClick="window.location.href=window.location.href">Reset</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@else
    <div class="container-fluid">

        <div class="card resource-filter mb-3 mt-3">
            <div class="col-12">
                <div class="row">

                    <div class="col-12">
                        <form action="{{ route('progress.country',['name'=>$name]) }}" method="POST">
                            @csrf
                            <div class="row">
                                @error('category')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                           
                                <div class="col-sm-4 col-xs-12">
                                    <select name="country" id="" class="form-control mt-4 mb-2 minimal">
                                        <option value=''>Select Country</option>
                                        @foreach (App\Models\Country::published()->orderByTranslation('title')->get() as $theme)
                                            <option value="{{ $theme->id }}"> {{ $theme->title }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-sm-5 col-xs-12">
                                    <input type="text" placeholder="Search" class="form-control mt-4 mb-4 " name="search">
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <button type="submit" class="btn btn-overall btn_black_bg mt-4 mb-4">Search</button>
                                    <button type="reset" class="btn btn-overall btn_cancel mt-4 mb-4"
                                        onClick="window.location.href=window.location.href">Reset</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>
    </div>


@endif
