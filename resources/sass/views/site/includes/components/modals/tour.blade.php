<div class="modal fade bd-example-modal-lg" id="tourModal" tabindex="-1" role="dialog" aria-labelledby="tourModal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered site-modal">
    <div class="modal-content ticket-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="tourModalLabel">Site Tour</h5>
            </div>
            
            <div class="modal-body" style="padding-bottom:0">
              <div class="video-wrapper">
                 <video controls playsinline muted  id="bgvid" style="height:unset;margin-top:0"  poster="{{ url('site_tour/Site_Tour_Video_Cover.png') }}">
                     <source src="{{url('site_tour/4G_Capital_Site_Tour.mp4')}}" type="video/mp4">
                  </video>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dismiss-modal modal-accept"  data-dismiss="modal" onclick="document.getElementById('tourModal').style.display='none'">Dismiss</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
