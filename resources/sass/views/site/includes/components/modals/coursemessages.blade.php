
@php
if(session()->get('course_success')){
    $flashMessage = session()->get('course_success');
  }else{
      $flashMessage = '';
  }
    
@endphp
<div class="modal fade bd-example-modal-lg" id="CourseSuccess" role="dialog" aria-labelledby="CourseSuccess" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content message-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="CourseSuccessLabel">@if($flashMessage)
                    {{ $flashMessage['title'] }}
                    @endif</h5>
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
                @if($flashMessage)
                {!! $flashMessage['content'] !!}
                @endif
            </div>
            <div class="modal-footer">
                @if(isset($flashMessage['action']))
                <a href="{{ $flashMessage['link'] }}" class="btn btn-action">
                    {{ $flashMessage['action'] }}
                </a>
                @else
                <button type="button" class="btn dismiss-modal modal-accept"  data-dismiss="modal"  onclick="document.getElementById('CourseSuccess').style.display='none'">Dismiss</button>
                @endif
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
