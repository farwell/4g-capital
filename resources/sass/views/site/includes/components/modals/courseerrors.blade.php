@php
  if(session()->get('course_errors')){
    $flashMessage = session()->get('course_errors');
  }else{
      $flashMessage = '';
  }
   
  
@endphp
    
<div class="modal fade bd-example-modal-lg" id="CourseErrors" tabindex="-1" role="dialog" aria-labelledby="CourseErrors" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content error-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="CourseErrorsLabel">
                     @if($flashMessage)
                    {{ $flashMessage['title'] }}
                    @endif
                </h5>
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
                @if($flashMessage)
                {!! $flashMessage['content'] !!}
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dismiss-modal modal-accept"  data-dismiss="modal"  onclick="document.getElementById('CourseErrors').style.display='none'">Dismiss</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

