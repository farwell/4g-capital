
<div class="modal" tabindex="-1" role="dialog" id="newCourseModal"  aria-labelledby="newCourseModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body" >
      <div class="row modal-row">
          <div class="col-md-7 col-12 no-padding">
            <div class="solid-blue">

               <button type="button" class="close" onclick="newClose()" ><img src="{{asset('/images/newcourse/X_symbol.svg')}}" class="newcourse-arrow"/></button>

            <div class="content">
           <h1> New  Course Alert!</h1>

           <p> The Customer Experience in the Modern Day training module is now live.</p>

           <a href="{{url('/courses/detail/course-v1:FarwellInnovationsLtd+CSE101+2020_1')}}"> View Course <img src="{{asset('/images/newcourse/Arrow.svg')}}" class="newcourse-arrow"/></a>
         </div>
          </div>
          </div>
          <div class="col-md-5 d-md-block d-none">
             <img src="{{asset('/images/newcourse/Erevuka_logo.svg')}}" class="newcourse-logo"/>
             <img src="{{asset('/images/newcourse/Building.svg')}}" class="newcourse-building"/>
          </div>
      </div>

      </div>
    </div>
  </div>
</div>
