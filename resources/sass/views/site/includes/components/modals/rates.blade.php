<div class="modal fade bd-example-modal-lg" id="rateModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel2" aria-hidden="true" style="margin-top: 15%;border-radius:36px;">
    <div class="modal-dialog modal-lg" style="border-radius:36px;">
        <div class="modal-content">

            <div class="modal-body" style="text-align:center;padding:0">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 20px;font-size: 1.5rem;">
                  <span aria-hidden="true">&times;</span>
              </button>
              <div class="row">
                <div class="col-md-6 col-12 bg-teal-green" style="padding: 40px 20px; background-color: #05b4cb;color: #fff">
                  <h6 class="modal-title" id="exampleModalLabel" style="margin-top:30px; ">Purchase a Single Module License</h6>
                  <ul style="text-align: left;font-size: 13px;padding: 6px; ">
                   <li style="padding:5px">This enables access to one module for one person.</li>
                   <li style="padding:5px">Upon completing the transaction, you will be able to access the course and take the training.</li>

                  </ul>
                  <a href="#" class="btn btn-overall purple" style="margin-top:10px;font-size:12px; padding:0.4rem;" id="single_purchase">
                      Purchase course
                  </a>
                </div>
                <div class="col-md-6 col-12" style="padding: 40px 20px;color:#000">

                  <h6 class="modal-title" id="exampleModalLabel" style="margin-top:30px;">Purchase a Group License </h6>
                  <ul style="text-align: left;font-size: 13px;padding: 6px;padding-left: 20px;">
                   <li style="padding:5px">This allows you to purchase multiple licenses for one course to be accessed by a group of users as indicated in the price plan below.</li>
                   <li style="padding:5px">Once you complete the transaction you will be sent codes via email to share with your team members so that they can enroll and take the modules.</li>
                  </ul>
                            <form method="POST" action="{{route('uwishlist.purchase-group')}}" class="grey">
                      @csrf
                      <input type="hidden" name="course" value="" id="rate_course">
                      <div class="form-group editable">

                          @php $rates = \App\BandRate::all();@endphp
                          <select id="future_bundles" class="form-control rate-select" name="bundles" style="border-radius:36px;" required>
                            <option value=" "> Select Price Plan </option>
                            @foreach( $rates as $rate)
                              <option value="{{$rate->id}}">{{$rate->range}} {{$rate->currency}} {{$rate->price}}</option>

                              @endforeach
                          </select>
                          @if ($errors->has('future_bundles'))
                              <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('future_bundles') }}</strong>
                            </span>
                          @endif
                      </div>
                      <div class="form-group ">
                          <div class="form-group flex-group">
                              <button type="submit" class="btn btn-overall purple"  id="pricing-form-submit" style="font-size:12px;">
                                  {{ __('Submit') }}
                              </button>&nbsp;&nbsp;
                              <button type="button" class="btn-arsenic-inverse btn-reset" data-dismiss="modal" style="padding: 0.2rem 0.4rem;border-radius:36px; font-size:12px;">Close</button>
                          </div>
                      </div>
                  </form>






              </div>

              </div>



            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>
