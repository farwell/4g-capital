<div class="row ceo-row" style="margin-top:0">
  <div class="app-pattern">
    <div class="container-fluid app-container">

      <h1>Access your academy training anytime anywhere.</h1>
        <p>Download the academy mobile app.</p>
          
        <ul class="app-list">
            <li><a href="{{asset('mobile-app/4g-capital-academy.apk')}}" target="_blank"><img src="{{asset('images/icons/android_icon.svg')}}" alt="android">Download</a> </li>
          
        </ul>
    </div>
  </div>
</div>
