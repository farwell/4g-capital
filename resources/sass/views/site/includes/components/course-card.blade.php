<div class="course-card">
  <article>
    <div class="thumbnail">
      @if ($course['course_image_uri'])
           <img src="{{asset($course['course_image_uri'])}}" alt="{{$course->name}}" />
     
      @endif
        <div class="overlay">
         
            @if(!empty($course->enrollment))
              @if(!empty($course->completion))
               @if($course->completion == 1)
              <div class="overlay-inside-right" data-toggle="tooltip" data-placement="top" title="">
                <i class="progress-icon icon-completed" title="Completed"></i>
               
              </div>
              @else 
              <div class="overlay-inside-right" data-toggle="tooltip" data-placement="top" title="">
                <i class="progress-icon icon-retake" title="Retake Course"></i>
               
              </div>
              @endif
             @else 
             <div class="overlay-inside-right" data-toggle="tooltip" data-placement="top" title="">
              <i class="progress-icon icon-progress" title="In Progress"></i>
             
            </div>
             
             @endif
            
           @else
            @if(!empty($course->mandatory))
           <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
            <i class="pro-icon icon-pro"></i>
          </div>
            @endif
           @endif
        </div>
    </div>
   
    <div class="course-card-content">
      <p class="title ">
        <a href="{{route('course.detail',['key' => $course->link_selected])}}" class="text-center text-purple">{{ $course['name'] }}</a></p>
     
    </div>

    
    <div class="card-footer ">
        
      <p class=" text-grey session-card-calendar">
          <span class="session-calendar"><img src="{{asset('images/icons/time_stamp.svg')}}" title="clock"/> {{$course->effort_selected}}</span> 
          <span class="card-span-icon" ><a href="{{route('course.detail',['key' => $course->link_selected])}}" class="btn btn-overall btn_white_bg_green_border_green_text">More Details <i class="fas fa-arrow-right"></i></a></span>
        </p>
</div>

  </article>
</div>

