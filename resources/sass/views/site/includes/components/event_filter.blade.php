<form action="{{route('events')}}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-row align-items-center">
      <div class="col-auto col-3">
        <select name="event_filter" id="" class="form-control filter_select">
          <option value=""> Filter Events</option>
          <option value="1"> Happening this month </option>
          <option value="2"> All Events </option>
        </select>
      </div> 
      <div class="col-auto ">
        <button type="submit" class="btn btn-overall btn_event_filter ">Filter</button>
      </div>
    </div>
  </form>