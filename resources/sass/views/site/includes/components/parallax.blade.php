<div class="jarallax">
    <img class="jarallax-img" src="{{ $image }}" alt="">
    <div class="content">
        <div class="wrap">
            @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                <h1>{!! $text !!}</h1>
                {{-- <p>{!! $description !!}</p> --}}
            @else
                <h3>{!! $text !!}</h3>
                {{-- <p>{!! $description !!}</p> --}}
            @endif

        </div>
        <div class="ik-hr-white">
            <span class="hr-inner">
                <span class="hr-inner-style"></span>
            </span>
        </div>
    </div>
    <!-- <a href="#next-section" title="" class="scroll-down-link " aria-hidden="true" >
      <i class="fas fa-chevron-down"></i>
    </a> -->
</div>
<div id="next-section"></div>
