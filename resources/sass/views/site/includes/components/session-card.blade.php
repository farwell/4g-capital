<div class="course-card mt-3 live-sessions-card ">
    <article>
        <div class="thumbnail">

            @if ($session->hasImage('session_image'))
                <img src="{{ $session->image('session_image', 'default') }}" alt="{{ $session->topic }}" />
            @endif

        </div>

        <div class="course-card-content session-card-content">


            <p class=" text-grey session-card-calendar">

                <span class="session-calendar"> <i class="fa fa-calendar" aria-hidden="true"></i>
                    {{ Carbon\Carbon::parse($session->start_time)->isoFormat('Do MMMM YYYY') }} </span>
                <span class="session-time"> <i class="fa fa-calendar" aria-hidden="true"></i>
                    {{ Carbon\Carbon::parse($session->start_time)->format('H:i A') }} </span>

            </p>
            <p class="title ">
                <a href="{{ route('session.details', [$session->id]) }}"
                    class=" text-purple">{{ $session->topic }}</a>
            </p>

            <?php
            $str = $session['agenda'];
            if (strlen($str) > 60) {
                $str = substr($str, 0, 90) . '...';
            }
            
            ?>
            {!! $str !!}

            <p class="readmore ">
                <a href="{{ route('session.details', [$session->id]) }}" class="btn btn-overall btn_more_details">More
                    Details </a>
            </p>

        </div>

    </article>
</div>
