

<section class="general-section">
    <div class="col-md-12 ">
    <div class="hrHeading latest-resources">
      <h2  class="header-with-line about-sidebar-title"> Related Resources</h2>
    </div>
    <br>
    <div class="row">
      <div class="col-lg-12">
                @foreach($related as $item)
                <div class="course-card sessions-card related_card">
                  <article>
                    <div class="thumbnail">
              
                      @if( $item->hasImage('resource_image'))
                      <img src="{{$item->image("resource_image", "default")}}" alt="{{$item->title}}" />
                     
                      @endif
                        <div class="overlay">
                            @if($item->course_id)
                          <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$item->title}}">
                            <i class="pro-icon icon-pro"></i>
                          </div>
                          @endif
                        </div>
                    </div>
                
                    <div class="course-card-content session-card-content ">
                      @if($item->course_id)
                      <p class="title "><a href="{{route('course.detail',['slug'=>$item->course_id])}}" class=" text-purple">{{ $item->title}}</a></p>
              
                      @else 
                      <p class="title "><a href="{{route('resource.details',['id'=>$item->id])}}" class=" text-purple">{{ $item->title}}</a></p>
                      @endif
                      
                        <?php
                        $str = $item['description'];
                        if (strlen($str) > 200) {
                          $str = substr($str, 0, 200) . '...';
                        }
                        
                        ?>
                      {!!$str!!}
                     
                    </div>
                    <div class="card-footer ">
                      
                              <p class=" text-grey session-card-calendar">
                        
                                  <span class="session-calendar">{{$item->video_length}} Min </span> 
                                  <span class="session-time">  {{Carbon\Carbon::parse($item->created_at)->isoFormat('Do MMMM YYYY')}}</span>


                                  <span class="card-span-icon" ><img src="{{asset('images/resource_categories/'.$item->type->icon_name)}}" title="{{$item->type->title}}"/></span>
                                </p>
                          
                  </div>
                  </article>
                </div>
            @endforeach
      </div>
    </div>
  </div>
  
  </section>