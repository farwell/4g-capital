@if((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container-fluid">
    <div class="row">
        <div class="col-12 d-none d-sm-block pr-3 ">
            <div class="card sidebar-card">
                <article>
            <ul class="list-group mt-4">

                <?php foreach (config('app-constants.connect-nav') as $el) : ?>
                <li class="list-unstyled border-0 p-2 list-nav">
                
                  @if (\Route::current()->getName() == $el['route'] || (strpos(\Request::url(), $el['route']) !== false))
                  <a class="nav-link active" href="{{route($el['route'])}}"> <span class="side-icon {{$el['icon']}}"></span>  <span>{{$el['title']}}</span></a>
                  @else
                  <a class="nav-link" href="{{route($el['route'])}}"><span class="side-icon {{$el['icon']}}"></span><span> {{$el['title']}}</span></a>
                  @endif
                </li>
              <?php endforeach; ?>
               
            </ul>
                </article>
            </div>
        </div>
       
    </div>
</div>
@else 
<ul class="nav nav-pills course-tabs connect_mobile_menu" role="tablist">
    <?php foreach (config('app-constants.connect-nav') as $el) : ?>
      <li class="nav-item list-nav">
        @if (\Route::current()->getName() == $el['route'] || (strpos(\Request::url(), $el['route']) !== false))
      <a class="nav-link active first-tab" href="{{route($el['route'])}}/"><span class="side-icon {{$el['icon']}}"></span></a>
      @else
      <a class="nav-link first-tab" href="{{route($el['route'])}}/"><span class="side-icon {{$el['icon']}}"></span></a>
      @endif
    </li>
    <?php endforeach; ?>
    </ul>


@endif