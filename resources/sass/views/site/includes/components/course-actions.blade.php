@php
 $configLms = config()->get("settings.lms.live");   

@endphp

<section class="detailed-tree">
    <div class="start">
        @if ($course->start >= Carbon\Carbon::today()->toDateString())
            <p class="span-space"><strong>Starts:</strong>
                {{ Carbon\Carbon::parse($course->start)->isoFormat('Do MMMM YYYY') }}</p>
        @else
            @if ($course->end <= Carbon\Carbon::today()->toDateString())
                <p class="span-space"><strong>Ended:</strong>
                    {{ Carbon\Carbon::parse($course->end)->isoFormat('Do MMMM YYYY') }}</p>
            @else
                <p class="span-space"><strong>Started:</strong>
                    {{ Carbon\Carbon::parse($course->start)->isoFormat('Do MMMM YYYY') }}</p>
            @endif
        @endif
        <p class="span-space"><strong>Effort:</strong> {{ $course->effort_selected }}</p>
        @guest
            <p>
                <a href="{{ route('user.login') }}" class="btn btn-overall btn_green_bg">Enrol for this course <i
                        class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                </a>
            </p>
        @else
            @if (!$licensed)
                <p><a class="btn btn-overall btn_green_bg" href="{{ route('course.enroll', $course->course_id) }}/">Enroll
                        into Course</a></p>
            @else
                @if ($enrolled)
                    <p><a class="btn btn-overall btn_green_bg"
                            href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}"
                            target="_blank">Take Course</a></p>
                @else
                    <p><a class="btn btn-overall btn_green_bg"
                            href="{{ route('course.enroll', $course->course_id) }}/">Enroll into Course</a></p>
                @endif
            @endif
        @endguest

    </div>
</section>
