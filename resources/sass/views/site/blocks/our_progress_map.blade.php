@php
use App\Models\Country;
use App\Models\User;
use App\Models\Branch;
use App\Models\CourseCompletion;

$countries = Country::published()->get();
$branches = Branch::all();
$participants =User::where('published','=',1)->get();

$certified = CourseCompletion::getCertified();

@endphp

@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <section class="general-section special-margin-top">
        <div class="container-fluid">
            <div class="row alignment-class">
                <div class="col-md-12 ">
                 
                    <div class="card community-card">
                        <article>
                            <div class="row">
                                <div class="col-6">
                                    <div class="community-card-content progress-map mt-2 ml-4">
                                        <h2>{{$block->translatedinput('title') }}</h2>

                                        {!! $block->translatedinput('description') !!}
                                      </div>
        
    
                                  <div class="community-status">
                                      <div class="row">
                                        <div class="col-5">
                                            <div id="countries" class="stats">
                                                <p class="community-countries"><span class="connect-icon icon-countries"></span> <span class="status-numbers pr-3">{{sprintf("%02d",count($countries))}}</span> Our Footprint</p>
                                            </div>
                                        </div>

                                        <div class="col-5">
                                            <div id="participants" class="stats">
                                                <p class="community-partcipants"><span class="connect-icon icon-participants"></span> <span class="status-numbers">{{sprintf("%02d",count($participants))}}</span> Staff Registered</p>
                                            </div>
                                        </div>

                                      
                                      </div>

                                      <div class="row mt-3">
                                        <div class="col-5">
                                            <div id="certified" class="stats">
                                                <p class="community-certified"><span class="connect-icon icon-participants-certified"></span> <span class="status-numbers">{{sprintf("%02d",count($certified))}}</span> Staff Certified</p>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div id="branches" class="stats">
                                                <p class="community-branches"><i class="fa fa-building branch-icon-size"></i> <span class="status-numbers pr-3">{{sprintf("%02d",count($branches))}}</span> Branches </p>
                                            </div>
                                        </div>
                                      </div>
                                   
    
                                  </div>
                                </div>
                                  <div class="col-6">
                                      <div id="map" >
                         
                                      </div>
              
                                  </div>
        
                            </div>
                        </article>
                    </div>

            </div>

        </div>

    </section>
@else
<div class="col-12">     
    <div class="row ">
        
            <div class="card community-card">
                <article>
                    <div class="row">
                        <div class="col-12">

                            <div class="community-card-content mt-4 ml-2">
                                <h2>{{$block->translatedinput('title') }}</h2>
                                {!! $block->translatedinput('description') !!}
                              </div>
                           
                          </div>
                          <div class="col-12 d-flex flex-row-reverse">
                              <div id="map" >
                 
                              </div>
      
                          </div>

                          <div class="col-12">
                            <div class="community-status">
                                <div class="row">
                                  <div class="col-5 offset-1">
                                    <div id="participants" class="stats">
                                        <p class="community-partcipants"><span class="connect-icon icon-participants"></span> <span class="status-numbers">{{sprintf("%02d",count($participants))}}</span> Staff Registered</p>
                                    </div>
                                  </div>

                                  <div class="col-5">
                                    <div id="certified" class="stats">
                                        <p class="community-certified"><span class="connect-icon icon-participants-certified"></span> <span class="status-numbers">{{sprintf("%02d",count($certified))}}</span> Staff Certified</p>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-5 offset-1">
                                    <div id="countries" class="stats">
                                        <p class="community-countries"><span class="connect-icon icon-countries"></span> <span class="status-numbers pr-3">{{sprintf("%02d",count($countries))}}</span> Our Footprint</p>
                                    </div>
                                  </div>

                                  <div class="col-5">
                                    <div id="branches" class="stats">
                                        <p class="community-branches"><i class="fa fa-building branch-icon-size"></i> <span class="status-numbers pr-3">{{sprintf("%02d",count($branches))}}</span> Branches </p>
                                    </div>
                                  </div>

                                </div>
                             

                            </div>

                          </div>

                    </div>
                </article>
            </div>


       
        
        
    </div>
    
</div>
@endif
