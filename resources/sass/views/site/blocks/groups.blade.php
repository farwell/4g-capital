@php
use App\Models\Group;

if (!empty($jobRole)) {
    $groups = Group::whereHas('translations', function ($query) use ($jobRole) {
        $query->where('title', 'LIKE', '%' . $jobRole . '%');
    })
        ->published()
        ->orderBy('position', 'desc')
        ->paginate(12);
} else {
    $groups = Group::published()
        ->orderBy('position', 'desc')
        ->paginate(12);
}

@endphp


@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="col-md-12">

        <div class="row profile-lower">

            <div class="col-md-12 col-12">

                <div class="row alignment-class ">
                    @if (count($groups) > 0)

                        @if (!empty($jobRole))
                            @foreach ($groups as $group)
                                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                                    @include(
                                        'site.includes.components.groups-cards',
                                        ['group' => $group]
                                    )
                                </div>
                            @endforeach
                        @else
                            @foreach ($groups as $group)
                                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                                    @include(
                                        'site.includes.components.groups-cards',
                                        ['group' => $group]
                                    )
                                </div>
                            @endforeach

                        @endif
                    @else
                        <p class="text-center"> There are no Groups </p>
                    @endif



                </div>

            </div>


        </div>

    </div>
@else
    <div class="col-md-12 col-12">

        <div class="row ">
            @if (count($groups) > 0)

                @if (!empty($jobRole))
                    @foreach ($groups as $group)
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            @include('site.includes.components.groups-cards', [
                                'group' => $group,
                            ])
                        </div>
                    @endforeach
                @else
                    @foreach ($groups as $group)
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            @include('site.includes.components.groups-cards', [
                                'group' => $group,
                            ])
                        </div>
                    @endforeach

                @endif
            @else
                <p class="text-center"> There are no Groups </p>
            @endif



        </div>


    </div>



@endif
