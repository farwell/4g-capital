
@php

$image = $block->imageObject('cta_text_image');
$ratio = $image->ratio ?? 2/3;
$ratioPercentage = ($ratio * 100);
@endphp
<section class="background-banner">
  <div class="row justify-content-center">
      <div class="col-md-5 offset-md-1">
          <div class="banner-section-text">

            {!! $block->translatedinput('image_subtitle') !!}
             
          </div>
      </div>
      <div class="col-md-6 text-center">
         
          <div class="image-section"> 
            <img src="{{ $block->image('cover', 'default') }}" alt="">
            {!!  (new \App\Helpers\Front)->responsiveImage([
              'options' => [
                'src' => $block->image('cta_text_image', 'default') ?? '',
                'w' => $image->width ?? 600,
                'h' => $image->height ?? 400,
                'ratio' => $ratio,
                'fit' => 'crop',
              ],
              'html' => [
                'alt' => $image->altText ?? ''
              ],
              'sizes' => [
                [ 'media' => 'small', 'w' => 730 ],
                [ 'media' => 'medium',  'w' => 426 ],
                [ 'media' => 'large', 'w' => 500 ],
                [ 'media' => 'xlarge',  'w' => 600 ],
                [ 'media' => 'xxlarge',  'w' => 680 ]
              ]
            ]) !!}

        
          </div>
      </div>
  </div>
</section>