@php
    
    use App\Models\JobroleCourse;
    use App\Models\JobRole;
    use App\Models\Course;
    
    if ((new \Jenssegers\Agent\Agent())->isDesktop()) {
        if (!empty($jobRole)) {
            $role = JobroleCourse::where('job_role_id', $jobRole)
                ->with('courses')
                ->simplePaginate(4);
            $jobr = JobRole::where('id', $jobRole)->first();
        } elseif (!empty($search)) {
            $role = JobroleCourse::whereHas('courses', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%');
            })
                ->where('job_role_id', Auth::user()->job_role_id)
                ->with('courses')
                ->simplePaginate(4);
    
            // $role = Course::where(function ($query) use($search) {
            //     $query->where('name', 'LIKE', '%' . $search . '%');
            //     })->where('status', 1)->published()->paginate(4);
        } else {
            $term = Auth::user()->job_role_id;
            $role = JobroleCourse::where('job_role_id', Auth::user()->job_role_id)
                ->with('courses')
                ->simplePaginate(4);
            //
        }
    } else {
        if (!empty($jobRole)) {
            $role = JobroleCourse::where('job_role_id', $jobRole)
                ->with('courses')
                ->paginate(2);
            $jobr = JobRole::where('id', $jobRole)->first();
        } else {
            $role = JobroleCourse::where('job_role_id', Auth::user()->job_role_id)
                ->with('courses')
                ->paginate(2);
        }
    }
    
@endphp

<section class="general-section">
    <div class="container-fluid mobile-container-fluid">
        <div class="col-12 ">
            
            <div class="hrHeading">
                <h2 class="line-header child_header">
                @if (!empty($jobRole))
                        {{ 'Courses for ' . $jobr->title }}
                    @else
                        {{ $block->translatedinput('title') }}
                    @endif
                </h2>
            </div>
        </div>
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="col-12 c_categories-tabs ">
                <ul class="nav course-category-tab c_category-tabs" id="myTab" role="tablist">
                    @foreach ($courseCategories as $key => $parent)
                        @if ($key == 0)
                        @elseif($key == 1)
                            <li class="nav-item first-nav-item">
                                <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="parent-{{ $parent->id }}-tab"
                                    data-toggle="tab" href="#parent-{{ $parent->id }}" role="tab"
                                    aria-controls="parent-{{ $parent->id }}" aria-selected="true">
                                    {{ $parent->title }}
                                </a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="parent-{{ $parent->id }}-tab"
                                    data-toggle="tab" href="#parent-{{ $parent->id }}" role="tab"
                                    aria-controls="parent-{{ $parent->id }}" aria-selected="true">
                                    {{ $parent->title }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>

            <div class="tab-content" id="myTabContent">
                @foreach ($courseCategories as $key => $parent)
                    @if ($key == 0)
                        <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}"
                            id="parent-{{ $parent->id }}" role="tabpanel"
                            aria-labelledby="parent-{{ $parent->id }}-tab">
                            <div class="row alignment-class mt-3">

                                @if (count($role) > 0)
                                    @foreach ($role as $related)
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            @include('site.includes.components.course-card', [
                                                'course' => $related->courses,
                                            ])
                                        </div>
                                    @endforeach
                                @else
                                    <p class="text-center no-courses" style="margin-left:40%"> There are no courses for your Job role</p>
                                @endif
                            </div>


                            <div class="row justify-content-center  pagination-spacing">{!! $role->links() !!}
                            </div>

                        </div>
                    @else
                        <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}"
                            id="parent-{{ $parent->id }}" role="tabpanel"
                            aria-labelledby="parent-{{ $parent->id }}-tab">
                            <div class="row alignment-class mt-5">

                                @if (count($parent->courses) > 0)
                                
                                    @foreach ($parent->courses as $related)
                                 
                                        @if ($related->mandatory)
                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                @include('site.includes.components.course-card', [
                                                    'course' => $related,
                                                ])
                                            </div>
                                        @endif
                                    @endforeach
                                @else
                                    <p class="text-center no-courses" style="margin-left:40%"> There are no courses for this category</p>
                                @endif
                            </div>


                            <div class="row justify-content-center  pagination-spacing">{!! $role->links() !!}
                            </div>


                        </div>
                    @endif
                @endforeach
            </div>
        @else
        <div class="col-12 c_categories-tabs ">
            <ul class="nav course-category-tab c_category-tabs" id="myTab" role="tablist">
                @foreach ($courseCategories as $key => $parent)
                    @if ($key == 0)
                    @elseif($key == 1)
                        <li class="nav-item first-nav-item">
                            <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="parent-{{ $parent->id }}-tab"
                                data-toggle="tab" href="#parent-{{ $parent->id }}" role="tab"
                                aria-controls="parent-{{ $parent->id }}" aria-selected="true">
                                {{ $parent->title }}
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="parent-{{ $parent->id }}-tab"
                                data-toggle="tab" href="#parent-{{ $parent->id }}" role="tab"
                                aria-controls="parent-{{ $parent->id }}" aria-selected="true">
                                {{ $parent->title }}
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="row mt-3">
                @if (count($role) > 0)
                    @foreach ($role as $related)
                        <div class="col-md-3 col-sm-4 col-12">
                            @include('site.includes.components.course-card', [
                                'course' => $related->courses,
                            ])
                        </div>
                    @endforeach
                @else
                    <p class="text-center no-courses" style="margin-left:40%"> There are no courses for your Job role</p>
                @endif
            </div>

            <div class="row justify-content-center  pagination-spacing">{!! $role->links() !!}</div>

        </div>


        @endif

    </div>
</section>
