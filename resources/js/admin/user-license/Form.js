import AppForm from '../app-components/Form/AppForm';

Vue.component('user-license-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                course_id:  '' ,
                user_id:  '' ,
                company_license_id:  '' ,
                enrolled_at:  '' ,
                expired_at:  '' ,
                
            }
        }
    }

});