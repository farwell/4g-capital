import AppListing from '../app-components/Listing/AppListing';

Vue.component('session-meeting-listing', {
    mixins: [AppListing]
});