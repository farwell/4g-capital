<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\CourseController;
use App\Http\Controllers\Front\UserLicenseController;
use App\Http\Controllers\Front\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('courses/process-reflection', [CourseController::class, 'processReflection'])->name('process-reflection');
Route::post('courses/progress', [UserLicenseController::class, 'courseProgress'])->name('courseProgress');
Route::post('mobile/request-auth', [UserController::class, 'requestAuth'])->name('request-auth');
Route::post('mobile/request-dashboard', [UserController::class, 'requestDashboard'])->name('request-dashboard');
Route::post('mobile/request-mandatory', [UserController::class, 'requestMandatory'])->name('request-mandatory');
Route::get('mobile/request-course-data', [CourseController::class, 'requestCourses'])->name('request-courses');

