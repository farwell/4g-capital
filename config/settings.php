<?php

return [
    'app' => [
        'APP_DOMAIN' => '4g-capital.com',
        'VERIFY_SSL' => true,
    ],

    'lms' => [
        'status' => 'test',
        'live' => [
            'EDX_KEY' => '973ab319c64c8ff8d24b',
            'EDX_SECRET' => '17ea50c4fee1b3f6909d88658caf378db3f0998d',
            'EDX_CONNECT' => true,
            'COOKIE_DOMAIN' => 'courses.4g-capital.com',
            'LMS_DOMAIN' => 'https://courses.4g-capital.com',
            'LMS_BASE' => 'https://courses.4g-capital.com',
            'CMS_BASE' => 'https://studio.4g-capital.com',
            'LMS_REGISTRATION_URL' => 'https://courses.4g-capital.com/user_api/v1/account/registration/',
            'LMS_LOGIN_URL' => 'https://courses.4g-capital.com/user_api/v1/account/login_session/',
            'LMS_RESET_PASSWORD_PAGE' => 'https://courses.4g-capital.com/user_api/v1/account/password_reset/',
            'LMS_RESET_PASSWORD_API_URL' => 'https://courses.4g-capital.com/user_api/v1/account/password_reset/',
            'edxWebServiceApiToken' => 'c90685ce8bc75a8e03ad35deb28a5fade4a4cc87',
            'default_token' => '133ad7f0ecd269b63637a522dbb529c50475a493'
        ],
        
    ],

    'fix_page_css' => [
        'case-study-index'
    ],


];
