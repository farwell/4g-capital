<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use Carbon\Carbon;

class NotificationsController extends Controller
{
    //
    public function singleNotification($id){
    
     $notification = Notification::where('id', $id)->first();

      if($notification->data['type'] == 1){

        $notification->read_at = Carbon::now();
        $notification->save();

        return redirect()->route('pages',['key'=>'groups']);

      }else if($notification->data['type'] == 3){

        $notification->read_at = Carbon::now();
        $notification->save();

        return redirect()->route('message.index');

      }else if($notification->data['type'] == 2){

        $notification->read_at = Carbon::now();
        $notification->save();

        return redirect()->route('connect.activity');

      }
      
      
    }
}
