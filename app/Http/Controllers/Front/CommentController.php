<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Repositories\PageRepository;
use A17\Twill\Http\Controllers\Front\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Comment;
use App\Models\CommentReply;
use App\Models\LikeDislike;
use App\Models\Session;

class CommentController extends Controller
{
    protected $pageKey;
    public function __construct()
    {
		
        parent::__construct();
    }
    public function posts ()
    {
        $itemPage = app(PageRepository::class)->getPage($this->forumkey);
		$this->seo->title = !empty($title=$itemPage->seo_title) ? $title : "{$itemPage->title}";
        $this->seo->canonical = $itemPage->seo_canonical;
        $this->seo->description = mb_substr($itemPage->present()->seo_description,0, 255);
        $this->seo->image = $itemPage->present()->imageSeo;
        return view('site.pages.discussionindex',[
			        'pageItem' => $itemPage,                
			
		]);
    }

    public function storeComment(Request $request)
    {
        $output = '';
        $replies = '';
        $user = Auth::user();

        $comment = Comment::create([
            'user_id' => $user->id,
            'session_meeting_id' => $request->id,
            'comment' => $request->comment,
            'emoji' => 1,
        ]);
        $comment->save();


      $session = Session::where('id','=',$comment->session_meeting_id)->first();

         foreach($session->comments as $comments){
            if($comments->user->profile_pic !=null){
                $ppic = $comments->user->profile_pic;
            }else{
                $ppic = 'images.jpg'; 
            }
            $output .='<div class="container" style="margin-top:1rem" >
                      <div class="main d-flex">
                     <div class="card" style="width: 100%;border: 0px;">
                    <div class="row no-gutters">
                    <div class="col-2 col-md-1"> 
                    <img class="card-img avatar-small rounded-circle disc-img" src="'.asset("uploads/".$ppic).'" alt="avatar">
                    </div>
                    <div class="col-10">
                    <div class="card-body card-small" style="padding-bottom: 0;">
                    <div class="card-text">
                    <p style="color:#424242; font-weight:bold;margin-bottom:7px">'.$comments->user->name.'&nbsp; <small style="font-size:11px">'.$comments->created_at->diffForHumans().'</small></p>
                    <p>'.$comments->comment.'</p>
                    </div>
                    </div>
                 </div>
                 <div class="replies" style="">
                 <small class="float-left">
                 <span title="Likes"  onClick="saveLikeDislike('.$comments->id.',"like")" class="mr-2  mr-1-sm" >
                            <i class="fa fa-thumbs-up"></i> <span class="like-count" id="like-'.$comments->id.'" style="font-size:14px">'.$comments->likes().'</span>
                        </span>
                        <span title="Dislikes"  onClick="saveLikeDislike('.$comments->id.',"dislike")" class="mr-2  mr-1-sm" >
                           <i class="fa fa-thumbs-down"></i> <span class="dislike-count" id="dislike-'.$comments->id.'" style="font-size:14px">'.$comments->dislikes().'</span>
                        </span>
                 <a href="javascript:;" class="replylink" onclick="ShowAndHide('.$comments->id.')">Reply</a>
                 </small>
                 </div>
                </div>
                 <div class="row">';
                 if(count($comments->replies) > 0){
                 $output.=' <div class="replieslink mt-2">
                 <small class="float-left" style="margin-left: 2rem">
                  <a href="javascript:;" onclick="showReplies('.$comments->id.')" id="showRepliesLink"> View Replies</a>    
                 </small>
                 </div>
                 ';
                }
              $output.='
              </div>
              <div class="container-fluid" id="SectionName-'.$comments->id.'" style="display: none;">
                <div class="main  d-flex" >
                <div class="card " style="border: 0px;width:100%">
                <div class="row no-gutters">
                <div class="col-2 col-md-1" >
                    
                    <img class="card-img avatar-small rounded-circle disc-img" src="'.asset("uploads/images.jpg").'" alt="avatar">
                </div>
                <div class="col-10">
                    <form method="POST" action="#" id="reply-discussion">
                        <div class="card-body card-small">
                        
                            <div class="card-text">
                                <input type="text" name="id" value="'.$comments->id.'" hidden id="replycomment_id">
                                <textarea id="replycomment_reply" class="form-control text-input" name="reply" placeholder="What are your thoughts?" data-emojiable="true" data-emoji-input="unicode"></textarea>
                            </div>
                            <div class="submitbuttons">
                                <button type="button" class="btn btn-outline-dark btn-cancel" onclick="ShowAndHide('.$comments->id.')">Cancel</button>
                                <a href="javascript:;" onclick="submitReply()"  class="button button-submit"> Submit Comment</a>
                            </div>                                       
                        </div>                    
                            </form>                  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
              <div id="replySection-'.$comments->id.'">';
              if(!empty($comments->replies)){
         
                $output .='<div class="container" id="sectionReplies-'.$comments->id.'" style="display: none; margin-left:50px;">';
                 foreach($comments->replies as $reply){
    
                    if($reply->user->profile_pic !=null){
                        $ppics = $reply->user->profile_pic;
                    }else{
                        $ppics = 'images.jpg'; 
                    }
    
                    $output .='<div class="main  d-flex">
                    <div class="card " style="width:100%;border: 0px;">
                    <div class="row no-gutters">
                    <div class="col-2 col-md-1" >
                    <img class="card-img avatar-small rounded-circle disc-img" src="'. asset("uploads/".$ppics).'" alt="avatar">
                    </div>
                    <div class="col-10">         
                    <div class="card-body card-small" style="padding-bottom:0">            
                    <div class="card-text">
                    <p style="color:#424242; font-weight:bold;margin-bottom:7px">'.$reply->user->name.'&nbsp;<small style="font-size:11px">'.$reply->created_at->diffForHumans().'</small></p>  
                    <p>'.$reply->reply.'</p> 
                    </div>                            
                    </div>                                    
                    </div>
                    </div>
                    </div>
                </div>';
                 }
                $output .='</div>';
             }
          $output.='</div>         
            </div>
            </div>
            </div>';
        
       
         }

       

        return response()->json(['success'=>'Resource has been uploaded successfully.','output'=>$output]);
    }

  public function loadComment(Request $request){
   
    $output = '';

    $session = Session::where('id','=',$request->id)->first();

    foreach($session->comments as $comments){
       if($comments->user->profile_pic !=null){
           $ppic = $comments->user->profile_pic;
       }else{
           $ppic = 'images.jpg'; 
       }
       $output .='<div class="container" style="margin-top:1rem" >
                 <div class="main d-flex">
                <div class="card" style="width: 100%;border: 0px;">
               <div class="row no-gutters">
               <div class="col-2 col-md-1"> 
               <img class="card-img avatar-small rounded-circle disc-img" src="'.asset("uploads/".$ppic).'" alt="avatar">
               </div>
               <div class="col-10">
               <div class="card-body card-small" style="padding-bottom: 0;">
               <div class="card-text">
               <p style="color:#424242; font-weight:bold;margin-bottom:7px">'.$comments->user->name.'&nbsp; <small style="font-size:11px">'.$comments->created_at->diffForHumans().'</small></p>
               <p>'.$comments->comment.'</p>
               </div>
               </div>
            </div>
            <div class="replies" style="">
            <small class="float-left">
            <span title="Likes"  onClick="saveLikeDislike('.$comments->id.',"like")" class="mr-2  mr-1-sm" >
                       <i class="fa fa-thumbs-up"></i> <span class="like-count" id="like-'.$comments->id.'" style="font-size:14px">'.$comments->likes().'</span>
                   </span>
                   <span title="Dislikes"  onClick="saveLikeDislike('.$comments->id.',"dislike")" class="mr-2  mr-1-sm" >
                      <i class="fa fa-thumbs-down"></i> <span class="dislike-count" id="dislike-'.$comments->id.'" style="font-size:14px">'.$comments->dislikes().'</span>
                   </span>
            <a href="javascript:;" class="replylink" onclick="ShowAndHide('.$comments->id.')">Reply</a>
            </small>
            </div>
           </div>
            <div class="row">';
            if(count($comments->replies) > 0){
            $output.=' <div class="replieslink mt-2">
            <small class="float-left" style="margin-left: 2rem">
             <a href="javascript:;" onclick="showReplies('.$comments->id.')" id="showRepliesLink"> View Replies</a>    
            </small>
            </div>
            ';
           }
         $output.='
         </div>
         <div class="container-fluid" id="SectionName-'.$comments->id.'" style="display: none;">
           <div class="main  d-flex" >
           <div class="card " style="border: 0px;width:100%">
           <div class="row no-gutters">
           <div class="col-2 col-md-1" >
               
               <img class="card-img avatar-small rounded-circle disc-img" src="'.asset("uploads/images.jpg").'" alt="avatar">
           </div>
           <div class="col-10">
               <form method="POST" action="#" id="reply-discussion">
                   <div class="card-body card-small">
                   
                       <div class="card-text">
                           <input type="text" name="id" value="'.$comments->id.'" hidden id="replycomment_id">
                           <textarea id="replycomment_reply" class="form-control text-input" name="reply" placeholder="What are your thoughts?" data-emojiable="true" data-emoji-input="unicode"></textarea>
                       </div>
                       <div class="submitbuttons">
                           <button type="button" class="btn btn-outline-dark btn-cancel" onclick="ShowAndHide('.$comments->id.')">Cancel</button>
                           <a href="javascript:;" onclick="submitReply()"  class="button button-submit"> Submit Comment</a>
                       </div>                                       
                   </div>                    
                       </form>                  
                       </div>
                   </div>
               </div>
           </div>
       </div>
         <div id="replySection-'.$comments->id.'">';
         if(!empty($comments->replies)){
    
           $output .='<div class="container" id="sectionReplies-'.$comments->id.'" style="display: none; margin-left:50px;">';
            foreach($comments->replies as $reply){

               if($reply->user->profile_pic !=null){
                   $ppics = $reply->user->profile_pic;
               }else{
                   $ppics = 'images.jpg'; 
               }

               $output .='<div class="main  d-flex">
               <div class="card " style="width:100%;border: 0px;">
               <div class="row no-gutters">
               <div class="col-2 col-md-1" >
               <img class="card-img avatar-small rounded-circle disc-img" src="'. asset("uploads/".$ppics).'" alt="avatar">
               </div>
               <div class="col-10">         
               <div class="card-body card-small" style="padding-bottom:0">            
               <div class="card-text">
               <p style="color:#424242; font-weight:bold;margin-bottom:7px">'.$reply->user->name.'&nbsp;<small style="font-size:11px">'.$reply->created_at->diffForHumans().'</small></p>  
               <p>'.$reply->reply.'</p> 
               </div>                            
               </div>                                    
               </div>
               </div>
               </div>
           </div>';
            }
           $output .='</div>';
        }
     $output.='</div>         
       </div>
       </div>
       </div>';
   
  
    }

   return response()->json(['success'=>'Resource has been uploaded successfully.','output'=>$output]);

    }



    public function storeReplies(Request $request)
    {

        $output = '';
        $links = ' ';

        $user = Auth::user();

        $reply = CommentReply::create([
            'user_id'=>$user->id,
            'comment_id' => $request->id,
            'reply'=>$request->reply
         ]);
        $reply->save();
         
        $comments = Comment::where('id','=',$request->id)->first();

         if(count($comments->replies)> 0){
         
            $links .='<small class="float-left" style="margin-left: 2rem">
            <a href="javascript:;" onclick="showReplies('.$comments->id.')" id="showRepliesLink"> Hide Replies</a>    
            </small>';

         }

         if(!empty($comments->replies)){
         
            $output .='<div class="container" id="sectionReplies-'.$comments->id.'" style="display: block; margin-left:50px;">';
             foreach($comments->replies as $reply){

                if($reply->user->profile_pic !=null){
                    $ppic = $reply->user->profile_pic;
                }else{
                    $ppic = 'images.jpg'; 
                }

                $output .='<div class="main  d-flex">
                <div class="card " style="width:100%;border: 0px;">
                <div class="row no-gutters">
                <div class="col-2 col-md-1" >
                <img class="card-img avatar-small rounded-circle disc-img" src="'. asset("uploads/".$ppic).'" alt="avatar">
                </div>
                <div class="col-10">         
                <div class="card-body card-small" style="padding-bottom:0">            
                <div class="card-text">
                <p style="color:#424242; font-weight:bold;margin-bottom:7px">'.$reply->user->name.'&nbsp;<small style="font-size:11px">'.$reply->created_at->diffForHumans().'</small></p>  
                <p>'.$reply->reply.'</p> 
                </div>                            
                </div>                                    
                </div>
                </div>
                </div>
            </div>';
             }
            $output .='</div>';
         }

     

        return response()->json(['success'=>'Resource has been uploaded successfully.', 'comment' =>$request->id, 'output'=> $output, 'link'=>$links]);
    }

    public function viewReplies()
    {
        $comments = Comment::with('comment_replies')->get();
        return $comments;
    }

 // Save Like Or dislike
 function save_likedislike(Request $request){
    $data=new LikeDislike;
    $data->comment_id=$request->post;
    $data->user_id = Auth::user()->id;
    if($request->type=='like'){
        $data->like=1;
    }else{
        $data->dislike=1;
    }
    $data->save();
    return response()->json([
        'bool'=>true
    ]);
}
}
