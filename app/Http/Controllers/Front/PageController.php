<?php

namespace App\Http\Controllers\Front;

// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PageRepository;
use App\Repositories\ResourceRepository;
use A17\Twill\Http\Controllers\Front\Controller;
use App\Models\Session as SessionMeeting;
use App\Models\Resource;
use App\Models\SessionResource;
use App\Models\User;
use App\Models\Event;
use App\Models\Blog;
use App\Models\ResourceType;
use App\Models\JobRole;
use App\Models\ResourceTheme;
use App\Models\Forum;
use App\Models\GeneralMessage;
use App\Models\ForumTopic;
use App\Models\Branch;
use App\Models\Country;
use Carbon\Carbon;
use Auth;
use Validator;
use Mail;
use Redirect;
use Session;
use App;
use App\Models\CourseCategory;
use Notification;
use DB;
use App\Notifications\GeneralMessageSent;
use Spatie\GoogleCalendar\Event as Events;
use Monarobase\CountryList\CountryListFacade as CountryList;
use PragmaRX\Countries\Package\Countries;
use PragmaRX\Countries\Package\Services\Config;


class PageController extends Controller
{
  //

  protected $pageKey;
  protected $country;

  public function __construct()
  {
    $this->homeKey = PageRepository::PAGE_HOMEPAGE;

    $this->middleware('auth', ['except' => ['index', 'contactStore', 'noneAuthPages']]);
    parent::__construct();
  }

  public function homeLink(Request $request){
   

    $data = $request->all();
    if ($request->isMethod('post')) {
      if ($request->input('category')) {

        $type = $request->input('category');
     
      }

      if ($request->input('search')) {
        $search = $request->input('search');
        
      
      }

    }else{

      $type = '';
      $search = '';
    }

    $courseCategories = CourseCategory::with(['courses'=>function($query){
      //$query->where('company_id',Auth::user()->company_id);
   }])->published()->orderBy('position', 'asc')->get();
    $jobRoles = JobRole::published()->orderBy('position', 'asc')->get();
    $itemPage = app(PageRepository::class)->getPage($this->homeKey);
    $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
    $this->seo->canonical = $itemPage->seo_canonical;
    $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
    $this->seo->image = $itemPage->present()->imageSeo;

    return view('site.pages.home', [
      'pageItem' => $itemPage,
      'jobRoles' => $jobRoles,
      'jobRole' => $type,
      'search' => $search,
      'courseCategories'=>$courseCategories
    ]);
  }


  public function index(Request $request)
  {

    if(Auth::check()){
      return redirect()->route('home.link');
    }
    
    if ($request->isMethod('post')) {
    $data = $request->all();
   
      // $validator = Validator::make($data, [
      //   'category'    => 'required',
      //   ]);
       
      if ($request->input('category')) {

        $type = $request->input('category');
        
      }

      if ($request->input('search')) {
        $search = $request->input('search');
        
       
      }

    }else{

      $type = '';
      $search ='';
    }
    $courseCategories = CourseCategory::with(['courses'=>function($query){

      //$query->where('company_id',Auth::user()->company_id);
   }])->published()->orderBy('position', 'asc')->get();

    $jobRoles = JobRole::published()->orderBy('position', 'asc')->get();
    $itemPage = app(PageRepository::class)->getPage($this->homeKey);
    $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
    $this->seo->canonical = $itemPage->seo_canonical;
    $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
    $this->seo->image = $itemPage->present()->imageSeo;

    return view('site.pages.home', [
      'pageItem' => $itemPage,
      'jobRoles' => $jobRoles,
      'jobRole' => $type ?? null,
      'search' => $search ?? null,
      'courseCategories'=>$courseCategories
    ]);
  }

  public function noneAuthPages(Request $request, $key)
  {
    

 
    $data = $request->all();
    if ($request->isMethod('post')) {
         
      $validator = Validator::make($data, [
        'category'    => 'required',
        ]);
       
        if ($validator->fails()) {
          $errors = $validator->errors();
    
          return Redirect::back()->withErrors($errors);
        }

      if ($request->input('key') === 'groups') {
        $type = $request->input('search');
        $forums = Forum::latest()->get();
      }

      if ($request->input('key') === 'our-progress') {
        $type = $request->input('branch');
        $forums = Forum::latest()->get();
      }
      if ($request->input('category')) {

        $type = $request->input('category');
        $forums = Forum::latest()->get();
      }


      if ($request->input('search')) {

        $type = $request->input('search');
        $forums = Forum::latest()->get();
      }
      if ($request->input('theme')) {

        $forums = Forum::where('forum_topic', $request->input('theme'))->latest()->get();
      }
    } else {
      $forums = Forum::latest()->get();
      $type = '';
      $search = '';
    }


    $types = Forum::getTypes();

    $jobRoles = JobRole::published()->orderBy('position', 'asc')->get();
    $topics = ForumTopic::all();
    $itemPage = app(PageRepository::class)->getPage($key);
    $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
    $this->seo->canonical = $itemPage->seo_canonical;
    $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
    $this->seo->image = $itemPage->present()->imageSeo;

    if ($key === 'home') {
      return redirect()->route('home');
    } else {
      return view('site.pages.' . $key, [
        'pageItem' => $itemPage,
        'jobRoles' => $jobRoles,
        'jobRole' => $type,
        'search'=>$search,
        'topics' => $topics,
        'types' => $types,
        'forums' => $forums,

      ]);
    }
  }


  public function pages(Request $request, $key)
  {

   
    $data = $request->all();
    if ($request->isMethod('post')) {
         
      // $validator = Validator::make($data, [
      //   'category'    => 'required',
      //   ]);
       
      //   if ($validator->fails()) {
      //     $errors = $validator->errors();
    
      //     return Redirect::back()->withErrors($errors);
      //   }

      if ($request->input('key') === 'groups') {
        $type = $request->input('search');
        $forums = Forum::latest()->get();
      }

      if ($request->input('key') === 'our-progress') {
        $type = $request->input('branch');
        $forums = Forum::latest()->get();
      }
      if ($request->input('category')) {

        $type = $request->input('category');
        $forums = Forum::latest()->get();
      }

      if ($request->input('search')) {
        $search = $request->input('search');
        
        $forums = Forum::latest()->get();
      }
      if ($request->input('theme')) {

        $forums = Forum::where('forum_topic', $request->input('theme'))->latest()->get();
      }
    } else {
      $forums = Forum::latest()->get();
      $type = '';
      $search = '';
    }


    $types = Forum::getTypes();

    $jobRoles = JobRole::published()->orderBy('position', 'asc')->get();

    $courseCategories = CourseCategory::with(['courses'=>function($query){
      //$query->where('company_id',Auth::user()->company_id);
   }])->published()->orderBy('position', 'asc')->get();

    $topics = ForumTopic::all();
    $itemPage = app(PageRepository::class)->getPage($key);
    $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
    $this->seo->canonical = $itemPage->seo_canonical;
    $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
    $this->seo->image = $itemPage->present()->imageSeo;

    if ($key === 'home') {

    
      return redirect()->route('home');
    } else {
      return view('site.pages.' . $key, [
        'pageItem' => $itemPage,
        'jobRoles' => $jobRoles,
        'jobRole' => $type ?? null,
        'topics' => $topics,
        'types' => $types,
        'search'=> $search ?? null,
        'forums' => $forums ?? null,
        'courseCategories' => $courseCategories

      ]);
    }
  }

  public function resourceDetails(Request $request, $id)
  {
    $itemPage = Resource::where('id', '=', $id)->first();
    $related = Resource::where('content_bucket_id', '=', $itemPage->content_bucket_id)->latest()->take(2)->get();

    return view('site.pages.resources_details', [
      'pageItem' => $itemPage,
      'related' => $related,


    ]);
  }


  public function save_likedislike(Request $request)
  {
    $data = new LikeDislike;
    $data->comment_id = $request->post;
    $data->user_id = Auth::user()->id;
    if ($request->type == 'like') {
      $data->like = 1;
    }
    $data->save();
    return response()->json([
      'bool' => true
    ]);
  }


  public function contactStore(Request $request)
  {
    //Validate
    $request->validate([
      'name' => ['required', 'string', 'max:255'],
      'email' => ['required', 'email'],
      'phone' => ['required', 'string', 'max:255'],
      'subject' => ['required', 'string', 'max:255'],
      'comment' => ['required', 'string'],
    ]);

    //Get message
    $message = new GeneralMessage;
    $message->name = $request->input('name');
    $message->email = $request->input('email');
    $message->contact_phone = $request->input('phone');
    $message->subject = $request->input('subject');
    $message->contact_message = $request->input('comment');
    $message->save();

    //Send email
    $contact_emails = explode(',', env('CONTACT_EMAILS'));
    foreach ($contact_emails as $email) {
      Notification::route('mail', $email)
        ->notify(new GeneralMessageSent($message));
    }


    //Show session message
    $redirectMessage = [
      'title' => 'Message sent',
      'content' => 'Thank you for your enquiry. We will respond to you as soon as possible.',
      // 'action'=>'Google',
      // 'link'=>'google.com'
    ];
    Session::flash('course_success', $redirectMessage);
    return redirect()->back();
  }

  protected function getRepository()
  {
    return App::make("App\\Repositories\\ResourceRepository");
  }


  public function sessionDetail(Request $request, $id)
  {
    $itemPage = SessionMeeting::where('id', '=', $id)->with('comments.replies')->first();
    $upcoming = SessionMeeting::where('start_time', '>=', Carbon::now())->first();
    $previous = SessionMeeting::where('start_time', '<', Carbon::today()->toDateString())->limit(1)->orderBy('position', 'desc')->get();


    // dd(count($articles));
    return view(
      'site.pages.session_details',
      [
        'pageItem' => $itemPage,
        'upcoming' => $upcoming,
        'previous' => $previous,
      ]
    );
  }


  public function getBranch($id)
  {

    $branch = Branch::where('country_id', $id)->orderByTranslation('title')->get();
    return response()->json($branch);
  }



  public function progressCountry(Request $request, $name)
  {


    if ($request->isMethod('post')) {

      if(empty($request->input('country')) && empty($request->input('search'))){
      
            $errors = "Please Select one option";
      
            return Redirect::back()->withErrors($errors);
      }else{

      if ($request->input('country')) {

        $country = DB::table('country_translations')->where('country_id', '=', $request->input('country'))->first();
        
        if ($country) {

          $branches = Branch::where('country_id', $country->country_id)->published()->get();
        } else {

          $branches = array();
        }

        $name = $country->title;

      } else if ($request->input('search')) {


        $keyword = $request->input('search');

        $branches = Branch::where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->get();

       
      }
    }

    } else {

      $country = DB::table('country_translations')->where('title', '=', $name)->first();

      if ($country) {
        $branches = Branch::where('country_id', $country->country_id)->published()->get();
      } else {
        $branches = array();
      }
      
    }


    return view('site.pages.country_branches', ['branches' => $branches, 'name' => $name]);
  }


  public function progressCountries()
  {
    $data = [];

    $countries = Country::published()->get();
  
    foreach ($countries as $country) {
      array_push($data, strtolower($country['code']));
    }
    return $data;
  }
}
