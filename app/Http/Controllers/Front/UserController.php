<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Front\RegisterRequest;
use App\Http\Requests\Front\VerifyRequest;
use App\Http\Requests\Front\LoginRequest;
use Monarobase\CountryList\CountryListFacade as CountryList;
use App\Helpers\Front;
use App\Models\TwillUsersAuth;
use App\Models\User;
use App\Models\CourseCompletion;
use App\Models\JobroleCourse;
use App\Models\UserLicense;
use App\Models\Projecttopic;
use App\Models\ProjectUser;
use Carbon\Carbon;
use AfricasTalking\SDK\AfricasTalking;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;
use Redirect;
use Validator;

use App\Edx\EdxAuthUser;
use App\Models\JobRole;
use App\Models\Branch;
use App\Models\Country;
use App\Models\TwillPosition;

use Auth;
use Toastr;
use Session;
use Mail;
use App;
use App\Models\Gender;

class UserController extends Controller
{
  //

  public function __construct()
  {
    $this->repository = $this->getRepository();
  }


  /**
   * @param RegisterRequest $request
   */

  public function showRegistrationForm()
  {

    abort(403, 'You are not allowed on this page');
    // return view('site.auth.register');
  }



  public function completeProfile(Request $request, $user = null)
  {
    if ($request->ajax()) {

      $validator = Validator::make($request->except('_token'), [
        'country_id' => 'required',
        'branch_id'    => 'required',
        'job_role_id' => 'required',
        'gender_id' => 'required',
      ]);
      if ($validator->fails()) {
        return response()->json(['message' => $validator->errors()], 401);
      } else {


        $user = User::where('id', '=', $user)->first();

        if ($request->get('first_name') && $request->get('last_name')) {
          $user->name = $request->get('first_name') . ' ' . $request->get('last_name');
        } else {
          $user->name = $user->name;
        }

        $user->country_id = $request->get('country_id');
        $user->branch_id = $request->get('branch_id');
        $user->job_role_id = $request->get('job_role_id');
        $user->gender_id = $request->get('gender_id');
        $user->first_name = ($request->get('first_name')) ? $request->get('first_name') : $user->first_name;
        $user->last_name = ($request->get('last_name')) ? $request->get('last_name') : $user->last_name;
        $user->secondary_email = ($request->get('secondary_email')) ? $request->get('secondary_email') : $user->secondary_email;
        $user->initial_profile = 1;
        $saved = $user->save();
        if ($saved) {
          return response()->json(['success' => '<h1 style="text-align:center" class="success_title">Thank you !</h1> <h3 style="text-align:center">Your profile has been updated</h3>', 'status' => 'ok']);
        }
      }
    }
    return redirect()->back()->with('modal_error', '<h3 style="text-align:center">invalid request</h3>');
  }


  public function UpdatePassword(Request $request, $user = null)
  {

    if ($request->ajax()) {


      $validator = Validator::make($request->except('_token'), [
        'oldpwd' => 'required',
        'pwd'    => 'required|min:6',
        'cpwd' => 'required|same:pwd',
      ]);
      if ($validator->fails()) {
        return response()->json(['message' => $validator->errors()], 401);
      } else {


        $pass = Hash::make($request->get('oldpwd'));

        $user = User::where('id', '=', $user)->first();


        if ($user->password = $pass) {


          $edxReset = $this->resetEdxPassword($user, $request->get('pwd'));


          if ($edxReset) {

            $user->password = Hash::make($request->get('pwd'));
            $user->require_new_password = 0;
            $saved = $user->save();
            if ($saved) {

              // $this->resetEdxPassword($user, $request->get('pwd'));



              return response()->json(['success' => '<h1 style="text-align:center">Congratulations</h1> <h3 style="text-align:center">You have changed your password</h3>', 'status' => 'ok']);
            }
          }
        } else {
          return response()->json(['message' => $validator->errors()], 401);
        }
      }
    } elseif ($request->isMethod('post')) {

      $validator = Validator::make($request->except('_token'), [
        'oldpwd' => 'required',
        'pwd'    => 'required|min:6',
        'cpwd' => 'required|same:pwd',
      ]);
      if ($validator->fails()) {
        //Log::info($validator);
        return redirect()->back()->withInfo('Please Correct the errors below')->withInput()->withErrors($validator);
        //return $this->invalid_request('no','enter valid information','warning');
      } else {
        $pass = Hash::make($request->get('oldpwd'));

        $user = User::where('id', '=', $user)->first();

        if ($user->password = $pass) {


          $edxReset = $this->resetEdxPassword($user, $request->get('pwd'));


          if ($edxReset) {

            $user->password = Hash::make($request->get('pwd'));
            $user->require_new_password = 0;
            $saved = $user->save();
            if ($saved) {

              // $this->resetEdxPassword($user, $request->get('pwd'));



              return response()->json(['success' => '<h1 style="text-align:center">Congratulations</h1> <h3 style="text-align:center">You have changed your password</h3>', 'status' => 'ok']);
            }
          }
        } else {
          return redirect()->back()->withInfo('Please check that the Old Password is correct');
        }
      }
    }
    return redirect()->back()->with('modal_error', '<h3 style="text-align:center">invalid request</h3>');
  }



  public function profile(Request $request)
  {
    //Get user
    $user = Auth::user();


    //dd($user);
    if ($request->isMethod('post')) {

      //Validate
      $request->validate([
        'first_name' => ['required', 'string', 'max:255'],
        'last_name' => ['required', 'string', 'max:255'],

      ]);

      //Get updates
      $updates = $request->all();


      //Save changes
      $user = User::find($user->id);

      if ($user->update($updates)) {
        return Redirect::back()->with('register_success', 'Your profile has been successfully updated.');
      } else {
        //Show session message

        return Redirect::back()->with('register_error', 'There was a problem updating your profile.Please refresh the page and try again.');
      }
    }

    $roles = JobRole::all();
    $branches = Branch::all();
    $countries = Country::all();
    $genders = Gender::all();



    if (strpos(Auth::user()->email, 'noemail')) {
      $user->email = '';
    }

    return view('site.pages.user.profile', ['user' => $user, 'roles' => $roles, 'branches' => $branches, 'countries' => $countries, 'genders' => $genders]);
  }


  public function picture(Request $request)
  {
    if (!Auth::check()) {
      return response('Unauthorized', 403);
    }

    $user = Auth::user();

    // //Save to storage
    // $ppic = $request->file('ppic');
    // $extension = $ppic->getClientOriginalExtension();
    // $filename = $ppic->getFilename() . '.' . $extension;


    // Storage::disk('public')->put($filename,  File::get($ppic));

    // //Delete old file
    // Storage::disk('public')->delete($user->profile_pic);

    //Save to user
    $user->profile_pic = $this->Fileupload($request, 'ppic', 'uploads');
    if ($user->save()) {

      return Redirect::back()->with('register_success', 'Your profile photo has been successfully updated.');
    } else {
      return Redirect::back()->with('register_error', 'Profile photo updating failed. Please refresh the page and try again.');
    }
  }



  //     /**
  // 	 * Get the guard to be used during registration.
  // 	 *
  // 	 * @return \Illuminate\Contracts\Auth\StatefulGuard
  // 	 */
  protected function guard()
  {
    return Auth::guard();
  }

  // 	/**
  //      * @return \A17\Twill\Repositories\ModuleRepository
  //      */
  protected function getRepository()
  {
    return App::make("App\\Repositories\\UserRepository");
  }

  protected function validationToken($user, $length)
  {

    $validation = new TwillUsersAuth();
    $validation->user_id  = $user->id;
    $validation->token = $this->generateRandomString($length);
    $validation->save();


    return $validation;
  }


  public function Fileupload($request, $file = 'file', $folder = null)
  {
    $upload = null;
    //create folder if none
    if (!File::exists($folder)) {
      File::makeDirectory($folder);
    }
    //chec fo file
    if (!is_null($request->file($file))) {
      $extension = $request->file($file)->getClientOriginalExtension();
      $fileName = rand(11111, 99999) . uniqid() . '.' . $extension;
      $upload = $request->file($file)->move($folder, $fileName);
      if ($upload) {
        $upload = $fileName;
      } else {
        $upload = null;
      }
    }
    return $upload;
  }
  private function edxRegister($user)
  {

    $configLms = config()->get("settings.lms.live");
    $configApp = config()->get("settings.app");


    //Validate user
    if ($user === null) {
      return;
    }
    //Package data to be sent
    if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
      $email = $user->email;
    } else {
      $email = $user->email . '@local.com';
    }
    $data = [
      'email' => $email,
      'name' => $user->first_name . ' ' . $user->last_name,
      'username' => $user->username,
      'honor_code' => 'true',
      'password' => request()->get('password'),
      'country' => 'KE',
      'terms_of_service' => 'true',
    ];

    $headers = array(
      'Content-Type' => 'application/x-www-form-urlencoded',
      'cache-control' => 'no-cache',
      'Referer' => env('APP_URL') . '/register',
    );

    $client = new \GuzzleHttp\Client();

    try {

      $response = $client->request('POST', $configLms['LMS_REGISTRATION_URL'], [
        'form_params' => $data,
        'headers' => $headers,
      ]);

      return true;
    } catch (\GuzzleHttp\Exception\ClientException $e) {

      $responseJson = $e->getResponse();
      $response = json_decode($responseJson->getBody()->getContents(), true);
      //Error, delete user
      $user->delete();

      //Delete password resets
      //PasswordReset::where('email', '=', $user->email)->delete();
      $errors = [];
      foreach ($response as $key => $error) {
        //Return error
        $errors[] = $error;
      }
      //echo "CATCH 1";

      return $errors[0];
    } catch (\Exception $e) {

      //Error, delete user
      $user->delete();
      //Delete password resets
      //PasswordReset::where('email', '=', $user->email)->delete();
      //echo "CATCH 2";
      //echo $e->getMessage();
      //die;
      return $e->getMessage();
    }
  }



  private function resetEdxPassword($user, $password)
  {
    $configLms = config()->get("settings.lms.live");
    $configApp = config()->get("settings.app");

    //if password reset, perform edx password resets




    $email = $user->email;

    $client = new \GuzzleHttp\Client();
    $response = $client->request('GET', $configLms['LMS_RESET_PASSWORD_PAGE']);
    //var_dump($response);die;
    $csrfToken = null;
    foreach ($response->getHeader('Set-Cookie') as $key => $cookie) {
      if (strpos($cookie, 'csrftoken') === FALSE) {
        continue;
      }
      $csrfToken = explode('=', explode(';', $cookie)[0])[1];
      break;
    }

    if (!$csrfToken) {
      //Error, reactivate reset
      Toastr::error("There was a problem logging you in. Please try again later or report to support.");
      return;
    }

    $data = [
      'email' => $email,
      'password' => $password,
      'csrfmiddlewaretoken' => $csrfToken
    ];



    $headers = [
      'Content-Type' => ' application/x-www-form-urlencoded ; charset=UTF-8',
      'Accept' => ' text/html,application/json',
      'X-CSRFToken' => $csrfToken,
      'Cookie' => ' csrftoken=' . $csrfToken,
      'Origin' => $configLms['LMS_BASE'],
      'Referer' => $configLms['LMS_BASE'],
      'X-Requested-With' => ' XMLHttpRequest',
    ];
    $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);
    $cookieJar = \GuzzleHttp\Cookie\CookieJar::fromArray([
      'csrftoken' => $csrfToken
    ], env('LMS_DOMAIN'));

    $client = new \GuzzleHttp\Client();
    try {

      $response = $client->request('POST', $configLms['LMS_RESET_PASSWORD_API_URL'], [
        'form_params' => $data,
        'headers' => $headers,
        'cookies' => $cookieJar
      ]);

      //var_dump($response);die;

      return true;
    } catch (\GuzzleHttp\Exception\ClientException $e) {
      $responseJson = $e->getResponse();
      $response = json_decode($responseJson->getBody()->getContents(), true);

      dd($response);
      //Error, delete user

      return $response;
    } catch (\Exception $e) {
      //Error, reactivate reset
      return $e->getMessage();
      // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
    }
  }

  protected function generateRandomString($length)
  {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
  }


  public function requestAuth(Request $request)
  {



    $validator = Validator::make($request->except('_token'), [
      'email' => 'required',

    ]);

    if ($validator->fails()) {
      return response()->json(['value' => 'The email field is required', 'error' => true], 400);
    }


    $user = User::where('email', $request->input('email'))->first();


    if ($user) {

      $position = TwillPosition::where('user_id', $user->id)->first();

      // Mail::send('emails.reset',['name' => $user->first_name.' '.$user->last_name, 'username' => $user->email, 'password'=>$position->position],
      // function ($message) use ($user) {
      //   $message->from('support@farwell-consultants.com', 'The 4G Capital ELearning Mobile APP Credentials');
      //   $message->to($user->email, $user->first_name);
      //   $message->subject('The 4G Capital ELearning Mobile APP');
      // });


      $success = $position->position;

      return response()->json(['value' => $success, 'success' => true], 200);
    } else {

      $success = "The email address provided is not attached to any user account. Please visit the academy website and register.";


      return response()->json(['value' => $success, 'error' => true], 401);
    }
  }


  public function requestDashboard(Request $request)
  {
    $all_data = array();

    $validator = Validator::make($request->except('_token'), [
      'username' => 'required',

    ]);

    if ($validator->fails()) {
      return response()->json(['value' => 'The email field is required', 'error' => true], 400);
    }

    $user = User::where('username', $request->input('username'))->first();


    $enrolled = UserLicense::where('user_id', $user->id)->get();
    $mandatory = JobroleCourse::where('job_role_id', $user->job_role_id)->get();
    $completed = CourseCompletion::where('user_id', $user->id)->where('score', '>=', 0.8)->get();


    return response()->json([
      'assignedTitle' => 'Courses Assigned',
      'enrolledTitle' => 'Courses Enrolled',
      'completedTitle' => 'Courses Completed',
      'assignedCount' => count($mandatory),
      'enrolledCount' => count($enrolled),
      'completedCount' => count($completed)
    ], 200);
  }



  public function requestMandatory(Request $request)
  {
    $validator = Validator::make($request->except('_token'), [
      'username' => 'required',

    ]);

    if ($validator->fails()) {
      return response()->json(['value' => 'The username field is required', 'error' => true], 400);
    }


    $user = User::where('username', $request->input('username'))->first();


    $mandatory_courses = JobroleCourse::where('job_role_id', $user->job_role_id)->with('courses')->get()->pluck('courses.course_id');

    return response()->json(['value' => $mandatory_courses]);
  }
}
