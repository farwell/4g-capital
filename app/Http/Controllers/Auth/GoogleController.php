<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Socialite;
use Auth;
use Exception;
use App;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Models\TwillPosition;
use Toastr;
use Config;
use Illuminate\Support\Facades\Session;
use Input;
use View;
use DB;
use Log;
use App\Edx\EdxAuthUser;
use Ixudra\Curl\Facades\Curl;
use Carbon\Carbon;
use Illuminate\Auth\AuthManager;

class GoogleController extends Controller

{

    use AuthenticatesUsers;

    public function __construct(AuthManager $authManager)
    {
        $this->authManager = $authManager;
        $this->repository = $this->getRepository();
        $this->middleware('guest')->except('logout');
    }


    public function redirectToGoogle()

    {

        return Socialite::driver('google')->redirect();
    }


    public function handleGoogleCallback()

    {

        return  App::environment(['local', 'staging']) ? $this->handleGoogleLocal() : $this->handleGoogleLive();




    }



    public function handleGoogleLocal(){
        try {

            $user = Socialite::driver('google')->stateless()->user();


            // only allow people with @company.com to login

            $domain = explode("@", $user->email)[1];

            if ($domain === "4g-capital.com" || $domain === "farwell-consultants.com") {

                $finduser = User::where('google_id', $user->id)->first();

                $findemail = User::where('email', $user->email)->first();



                if ($finduser) {
                    if (empty($finduser->reset_pd)) {

                        $randomString = $finduser->first_name . '_' . $finduser->last_name . '_' . $finduser->id;

                        $position = new TwillPosition();
                        $position->user_id = $finduser->id;
                        $position->position = $randomString;


                        $position->save();

                        $finduser->reset_pd = 1;
                        $finduser->save();
                    }

                    Auth::login($finduser, true);
                    $pd =  TwillPosition::where('user_id', $finduser->id)->first();

                    if ($finduser->role === "SUPERADMIN" || $finduser->role === "ADMIN" || $finduser->role === "HR") {


                        $this->authManager->guard('twill_users')->loginUsingId($finduser->id);


                        $finduser->last_login_at = Carbon::now();
                        $finduser->save();

                        return redirect()->route('home.link');
                    } else {


                        $finduser->last_login_at = Carbon::now();
                        $finduser->save();
                        session()->flash('message', 'Please update information on the  <a href="' . url('/profile/edit') . '">profile</a> page.');
                        return redirect()->route('home.link');
                    }
                } elseif ($findemail) {


                    if (empty($findemail->reset_pd)) {

                        $randomString = $findemail->first_name . '_' . $findemail->last_name . '_' . $findemail->id;

                        $position = new TwillPosition();
                        $position->user_id = $findemail->id;
                        $position->position = $randomString;



                        $position->save();

                        $findemail->reset_pd = 1;
                        $findemail->save();
                    }

                    Auth::login($findemail, true);
                    $pd =  TwillPosition::where('user_id', $findemail->id)->first();

                    if ($findemail->role === "SUPERADMIN" || $findemail->role === "ADMIN" || $findemail->role === "HR") {


                        $this->authManager->guard('twill_users')->loginUsingId($findemail->id);



                        $findemail->last_login_at = Carbon::now();
                        $findemail->google_id = $user->id;
                        $findemail->save();

                        return redirect()->route('home.link');
                    } else {


                        $findemail->last_login_at = Carbon::now();
                        $findemail->google_id = $user->id;
                        $findemail->save();
                        session()->flash('message', 'Please update information on the  <a href="' . url('/profile/edit') . '">profile</a> page.');
                        return redirect()->route('home.link');
                    }
                } else {


                    $newUser = $this->repository->createForGoogle($user);

                    if (!$newUser) {
                        $user->delete();
                        return Redirect::back()->with('register_error', 'An error occured while submitting your registration. Please refresh the page and try again.');
                    } else {

                        $randomString = $newUser->first_name . '_' . $newUser->last_name . '_' . $newUser->id;

                        $position = new TwillPosition();
                        $position->user_id = $newUser->id;
                        $position->position = $randomString;

                        $position->save();


                        $newUser->reset_pd = 1;
                        $newUser->username = $usern->username;
                        $newUser->save();


                        Auth::login($newUser, true);


                        $newUser->last_login_at = Carbon::now();
                        $newUser->save();

                        return redirect()->route('home.link');
                    }
                }
            } else {
                $course_errors = [
                    'title' => 'Sorry',
                    'content' => 'The email domain is not allowed for this platform',
                ];

                Session::flash('course_errors', $course_errors);
                return redirect()->to('/login');
            }
        } catch (Exception $e) {
            //dd($e);
            return redirect('auth/google');
        }

    }

    public function handleGoogleLive(){

        try {

            $user = Socialite::driver('google')->stateless()->user();


            // only allow people with @company.com to login

            $domain = explode("@", $user->email)[1];

            if ($domain === "4g-capital.com" || $domain === "farwell-consultants.com") {

                $finduser = User::where('google_id', $user->id)->first();

                $findemail = User::where('email', $user->email)->first();



                if ($finduser) {



                    $usern = EdxAuthUser::where('username', $finduser->username)->first();

                    if ($usern) {
                        if (empty($finduser->reset_pd)) {

                            $randomString = $finduser->first_name . '_' . $finduser->last_name . '_' . $finduser->id;

                            $position = new TwillPosition();
                            $position->user_id = $finduser->id;
                            $position->position = $randomString;

                            $ff = $this->resetEdxPassword($finduser, $randomString);


                            $position->save();

                            $finduser->reset_pd = 1;
                            $finduser->save();
                        }

                        Auth::login($finduser, true);
                        $pd =  TwillPosition::where('user_id', $finduser->id)->first();

                        if ($finduser->role === "SUPERADMIN" || $finduser->role === "ADMIN" || $finduser->role === "HR") {


                            $this->authManager->guard('twill_users')->loginUsingId($finduser->id);


                            $edxStatus = $this->edxLogin($finduser, $pd->position);

                            if (!$edxStatus) {
                                Auth::logout();
                                return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
                            } else {

                                $et = TwillPosition::where('user_id', $finduser->id)->first();
                                $et->etock = $_COOKIE['edinstancexid'];
                                $et->save();

                                $finduser->last_login_at = Carbon::now();
                                $finduser->save();

                                return redirect()->route('home.link');
                            }
                        } else {

                            $edxStatus = $this->edxLogin($finduser, $pd->position);

                            if (!$edxStatus) {
                                Auth::logout();
                                return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
                            } else {

                                $et =  TwillPosition::where('user_id', $finduser->id)->first();
                                $et->etock = $_COOKIE['edinstancexid'];
                                $et->save();

                                $finduser->last_login_at = Carbon::now();
                                $finduser->save();
                                session()->flash('message', 'Please update information on the  <a href="' . url('/profile/edit') . '">profile</a> page.');
                                return redirect()->route('home.link');
                            }
                        }
                    } else {

                        $randomString = $finduser->first_name . '_' . $finduser->last_name . '_' . $finduser->id;

                        $position = new TwillPosition();
                        $position->user_id = $finduser->id;
                        $position->position = $randomString;

                        $position->save();

                        $edxResponse =  $this->edxRegister($finduser, $randomString);

                        if ($edxResponse !== true) {

                            return Redirect::back()->with('register_error', 'An error occured while submitting your registration. Please refresh the page and try again.');
                        } else {


                            $edxuser = EdxAuthUser::where('username', $finduser->username)->first();
                            $edxuser->first_name = $finduser->first_name;
                            $edxuser->last_name = $finduser->last_name;
                            $edxuser->email = $finduser->email;
                            $edxuser->is_active =  1;
                            $edxuser->save();


                            Auth::login($finduser, true);

                            $edxStatus = $this->edxLogin($finduser, $randomString);


                            if (!$edxStatus) {
                                Auth::logout();
                                return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
                            } else {

                                $et =  TwillPosition::where('user_id', $finduser->id)->first();
                                $et->etock = $_COOKIE['edinstancexid'];
                                $et->save();

                                $finduser->last_login_at = Carbon::now();
                                $finduser->save();

                                return redirect()->route('home.link');
                            }
                        }
                    }
                } elseif ($findemail) {

                    $usern = EdxAuthUser::where('username', $findemail->username)->first();

                    if ($usern) {
                        if (empty($findemail->reset_pd)) {

                            $randomString = $findemail->first_name . '_' . $findemail->last_name . '_' . $findemail->id;

                            $position = new TwillPosition();
                            $position->user_id = $findemail->id;
                            $position->position = $randomString;

                            $ff = $this->resetEdxPassword($findemail, $randomString);


                            $position->save();

                            $findemail->reset_pd = 1;
                            $findemail->save();
                        }

                        Auth::login($findemail, true);
                        $pd =  TwillPosition::where('user_id', $findemail->id)->first();

                        if ($findemail->role === "SUPERADMIN" || $findemail->role === "ADMIN" || $findemail->role === "HR") {


                            $this->authManager->guard('twill_users')->loginUsingId($findemail->id);


                            $edxStatus = $this->edxLogin($findemail, $pd->position);



                            if (!$edxStatus) {
                                Auth::logout();
                                return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
                            } else {

                                $et =  TwillPosition::where('user_id', $findemail->id)->first();
                                $et->etock = $_COOKIE['edinstancexid'];
                                $et->save();

                                $findemail->last_login_at = Carbon::now();
                                $findemail->google_id = $user->id;
                                $findemail->save();

                                return redirect()->route('home.link');
                            }
                        } else {

                            $edxStatus = $this->edxLogin($findemail, $pd->position);

                            if (!$edxStatus) {
                                Auth::logout();
                                return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
                            } else {

                                $et =  TwillPosition::where('user_id', $findemail->id)->first();
                                $et->etock = $_COOKIE['edinstancexid'];
                                $et->save();

                                $findemail->last_login_at = Carbon::now();
                                $findemail->google_id = $user->id;
                                $findemail->save();
                                session()->flash('message', 'Please update information on the  <a href="' . url('/profile/edit') . '">profile</a> page.');
                                return redirect()->route('home.link');
                            }
                        }
                    } else {

                        $randomString = $findemail->first_name . '_' . $findemail->last_name . '_' . $findemail->id;

                        $position = new TwillPosition();
                        $position->user_id = $findemail->id;
                        $position->position = $randomString;

                        $position->save();

                        $edxResponse =  $this->edxRegister($findemail, $randomString);

                        if ($edxResponse !== true) {

                            return Redirect::back()->with('register_error', 'An error occured while submitting your registration. Please refresh the page and try again.');
                        } else {


                            $edxuser = EdxAuthUser::where('username', $findemail->username)->first();
                            $edxuser->first_name = $findemail->first_name;
                            $edxuser->last_name = $findemail->last_name;
                            $edxuser->email = $findemail->email;
                            $edxuser->is_active =  1;
                            $edxuser->save();


                            Auth::login($findemail, true);

                            $edxStatus = $this->edxLogin($findemail, $randomString);


                            if (!$edxStatus) {
                                Auth::logout();
                                return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
                            } else {

                                $et =  TwillPosition::where('user_id', $findemail->id)->first();
                                $et->etock = $_COOKIE['edinstancexid'];
                                $et->save();

                                $findemail->last_login_at = Carbon::now();
                                $findemail->google_id = $user->id;
                                $findemail->save();

                                return redirect()->route('home.link');
                            }
                        }
                    }
                } else {


                    $newUser = $this->repository->createForGoogle($user);

                    if (!$newUser) {
                        $user->delete();
                        return Redirect::back()->with('register_error', 'An error occured while submitting your registration. Please refresh the page and try again.');
                    } else {

                        $randomString = $newUser->first_name . '_' . $newUser->last_name . '_' . $newUser->id;

                        $position = new TwillPosition();
                        $position->user_id = $newUser->id;
                        $position->position = $randomString;

                        $position->save();


                        $usern = EdxAuthUser::where('email', $newUser->email)->first();


                        if ($usern) {

                            $this->resetEdxPassword($newUser, $randomString);

                            $usern->first_name = $newUser->first_name;
                            $usern->last_name = $newUser->last_name;
                            $usern->is_active =  1;
                            $usern->save();

                            $newUser->reset_pd = 1;
                            $newUser->username = $usern->username;
                            $newUser->save();


                            Auth::login($newUser, true);

                            $edxStatus = $this->edxLogin($newUser, $randomString);


                            if (!$edxStatus) {
                                Auth::logout();
                                return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
                            } else {

                                $et =  TwillPosition::where('user_id', $newUser->id)->first();
                                $et->etock = $_COOKIE['edinstancexid'];
                                $et->save();

                                $newUser->last_login_at = Carbon::now();
                                $newUser->save();

                                return redirect()->route('home.link');
                            }
                        } else {
                            $edxResponse =  $this->edxRegister($newUser, $randomString);

                            if ($edxResponse !== true) {

                                return Redirect::back()->with('register_error', 'An error occured while submitting your registration. Please refresh the page and try again.');
                            } else {


                                $edxuser = EdxAuthUser::where('username', $newUser->username)->first();
                                $edxuser->first_name = $newUser->first_name;
                                $edxuser->last_name = $newUser->last_name;
                                $edxuser->email = $newUser->email;
                                $edxuser->is_active =  1;
                                $edxuser->save();


                                Auth::login($newUser, true);

                                $edxStatus = $this->edxLogin($newUser, $randomString);


                                if (!$edxStatus) {
                                    Auth::logout();
                                    return Redirect::back()->with('login_error', 'The username or password you entered did not match our records. Please confirm you have registered');
                                } else {

                                    $et =  TwillPosition::where('user_id', $newUser->id)->first();

                                    $et->etock = $_COOKIE['edinstancexid'];
                                    $et->save();

                                    $newUser->last_login_at = Carbon::now();
                                    $newUser->save();

                                    return redirect()->route('home.link');
                                }
                            }
                        }
                    }
                }
            } else {
                $course_errors = [
                    'title' => 'Sorry',
                    'content' => 'The email domain is not allowed for this platform',
                ];

                Session::flash('course_errors', $course_errors);
                return redirect()->to('/login');
            }
        } catch (Exception $e) {
            //dd($e);
            return redirect('auth/google');
        }
    }


    protected function getRepository()
    {
        return App::make("App\\Repositories\\UserRepository");
    }



    private function edxRegister($user, $pass)
    {

        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");


        //Validate user
        if ($user === null) {
            return;
        }
        //Package data to be sent
        if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
            $email = $user->email;
        }
        $data = [
            'email' => $email,
            'name' => $user->first_name . ' ' . $user->last_name,
            'username' => $user->username,
            'honor_code' => 'true',
            'password' => $pass,
            'country' => 'KE',
            'terms_of_service' => 'true',
            'is_active' => 1,
        ];

        $headers = array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'cache-control' => 'no-cache',
            'Referer' => env('APP_URL') . '/register',
        );

        $client = new \GuzzleHttp\Client();

        try {

            $response = $client->request('POST', $configLms['LMS_REGISTRATION_URL'], [
                'form_params' => $data,
                'headers' => $headers,
            ]);

            return true;
        } catch (\GuzzleHttp\Exception\ClientException $e) {

            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user
            // $user->delete();

            //Delete password resets
            //PasswordReset::where('email', '=', $user->email)->delete();
            $errors = [];
            foreach ($response as $key => $error) {
                //Return error
                $errors[] = $error;
            }
            //echo "CATCH 1";

            return $errors[0];
        } catch (\Exception $e) {

            //Error, delete user
            // $user->delete();
            //Delete password resets
            //PasswordReset::where('email', '=', $user->email)->delete();
            //echo "CATCH 2";
            //echo $e->getMessage();
            //die;
            return $e->getMessage();
        }
    }




    private function edxLogin($user, $password)
    {
        //dd("got it");

        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");

        $email = $user->email;
        $password = $password;



        //Get CSRF Token

        $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);

        $response = $client->request('GET', $configLms['LMS_LOGIN_URL']);

        $csrfToken = null;
        foreach ($response->getHeader('Set-Cookie') as $key => $cookie) {
            if (strpos($cookie, 'csrftoken') === false) {
                continue;
            }
            $csrfToken = explode('=', explode(';', $cookie)[0])[1];
            break;
        }

        if (!$csrfToken) {
            //Error, reactivate reset
            return false;
        }

        $data = [
            'email' => $email,
            'password' => $password,
        ];
        $headers = [
            'Content-Type' => ' application/x-www-form-urlencoded ; charset=UTF-8',
            'Accept' => ' text/html,application/json',
            'X-CSRFToken' => $csrfToken,
            'Cookie' => ' csrftoken=' . $csrfToken,
            'Origin' => $configLms['LMS_BASE'],
            'Referer' => $configLms['LMS_BASE'] . '/login',
            'X-Requested-With' => ' XMLHttpRequest',
        ];
        $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);
        $cookieJar = \GuzzleHttp\Cookie\CookieJar::fromArray(
            [
                'csrftoken' => $csrfToken
            ],
            $configLms['LMS_DOMAIN']
        );
        $response = $client->request(
            'POST',
            $configLms['LMS_LOGIN_URL'],
            [
                'form_params' => $data,
                'headers' => $headers,
                'cookies' => $cookieJar
            ]
        );
        //set cookies

        if (!$response->hasHeader('Set-Cookie')) {
            return false;
        }


        $loggedInCookies = $response->getHeader('Set-Cookie');



        $setCookies = [];
        foreach ($loggedInCookies as $userCookie) {
            //format cookies
            $cookieDetails = (explode(';', $userCookie));

            $ourCookie = [];
            foreach ($cookieDetails as $cookieDetail) {


                $key = strtolower(trim(explode('=', $cookieDetail)[0]));

                $value = isset(explode('=', $cookieDetail)[1]) ? trim(explode('=', $cookieDetail)[1]) : 1;


                if (in_array(strtolower($key), ['__cfduid', 'csrftoken', 'edxloggedin', 'sessionid', 'openedx-language-preference'])) {
                    $ourCookie['name'] = $key;
                    $ourCookie['value'] = $value;
                } else {
                    $ourCookie[$key] = $value;
                }
            }
            // var_dump($ourCookie);

            if (isset($ourCookie['name'])) {
                if ($ourCookie['name'] != 'edx-user-info' && !isset($ourCookie['domain'])) {
                    if ($ourCookie['name'] == 'csrftoken') {
                        $ourCookie['domain'] = '';
                    } else {
                        $ourCookie['domain'] = '.' . $configApp['APP_DOMAIN'];
                    }
                    //Set the cookie

                }
                if ($ourCookie['domain'] == $configLms['COOKIE_DOMAIN']) {

                    $ourCookie['domain'] = '.' . $configApp['APP_DOMAIN'];
                }

                $setCookies[] = ['name' => $ourCookie['name'], 'value' => $ourCookie['value'], 'domain' => $ourCookie['domain']];
            }
        }


        $data = [
            'grant_type' => 'password',
            'client_id' => $configLms['EDX_KEY'],
            'client_secret' => $configLms['EDX_SECRET'],
            'username' => auth()->user()->username,
            'password' => $password,
            //'token_type'=>'jwt',
        ];
        $tokenUrl = $configLms['LMS_BASE'] . '/oauth2/access_token/';
        //Get authorization token
        $accessResponse = Curl::to($tokenUrl)
            ->withData($data)
            ->withResponseHeaders()
            ->returnResponseObject()
            ->post();



        if ($accessResponse->status !== 200) {
            return false;
        }
        //Set access token
        $accessToken = json_decode($accessResponse->content, true);

        $setCookies[] = ['name' => 'edinstancexid', 'value' => $accessToken['access_token'], 'expiry' => $accessToken['expires_in']];
        $setCookies[] = ['name' => 'edx-company-info', 'value' => $user->company_id, 'expiry' => $accessToken['expires_in']];


        foreach ($setCookies as $cookie) {
            $cookie['expiry'] = 0;
            if (!isset($cookie['domain'])) {
                $cookie['domain'] = '.' . $configApp['APP_DOMAIN'];
            }
            setrawcookie($cookie['name'], $cookie['value'], $cookie['expiry'], '/', $cookie['domain']);
        }

        return true;
    }



    private function resetEdxPassword($user, $password)
    {
        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");

        //if password reset, perform edx password resets
        $email = $user->email;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $configLms['LMS_RESET_PASSWORD_PAGE']);
        //var_dump($response);die;
        $csrfToken = null;
        foreach ($response->getHeader('Set-Cookie') as $key => $cookie) {
            if (strpos($cookie, 'csrftoken') === FALSE) {
                continue;
            }
            $csrfToken = explode('=', explode(';', $cookie)[0])[1];
            break;
        }

        if (!$csrfToken) {
            //Error, reactivate reset
            Toastr::error("There was a problem logging you in. Please try again later or report to support.");
            return;
        }

        $data = [
            'email' => $email,
            'password' => $password,
            'csrfmiddlewaretoken' => $csrfToken
        ];
        $headers = [
            'Content-Type' => ' application/x-www-form-urlencoded ; charset=UTF-8',
            'Accept' => ' text/html,application/json',
            'X-CSRFToken' => $csrfToken,
            'Cookie' => ' csrftoken=' . $csrfToken,
            'Origin' => $configLms['LMS_BASE'],
            'Referer' => $configLms['LMS_BASE'],
            'X-Requested-With' => ' XMLHttpRequest',
        ];
        $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);
        $cookieJar = \GuzzleHttp\Cookie\CookieJar::fromArray([
            'csrftoken' => $csrfToken
        ], env('LMS_DOMAIN'));

        $client = new \GuzzleHttp\Client();
        try {

            $response = $client->request('POST', $configLms['LMS_RESET_PASSWORD_API_URL'], [
                'form_params' => $data,
                'headers' => $headers,
                'cookies' => $cookieJar
            ]);

            //var_dump($response);die;
            //dd($response);
            return true;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset
            return $e->getMessage();
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}
