<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\JobRoleRepository;
use App\Repositories\MessageTypeRepository;
use Illuminate\Support\Facades\DB;

class PlatformMessageController extends ModuleController
{
    protected $moduleName = 'platformMessages';



    protected function formData($request)
{
    $userList = DB::table('twill_users')->pluck('email','id');
    $targetList = DB::table('targets')->pluck('title','id');
    $collection = collect(['All']);
    $merged = $collection->merge(app(JobRoleRepository::class)->listAll('title'));
    $merged->all();

    return [
        'roleList' => $merged,
        'targetList' =>$targetList,
        'userList' =>$userList,
        'messageTypeList' => app(MessageTypeRepository::class)->listAll('title'), 
        
        
    ];

}
    
}
