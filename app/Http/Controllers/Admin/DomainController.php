<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class DomainController extends ModuleController
{
    protected $moduleName = 'domains';

    protected $titleColumnKey = 'name';
}
