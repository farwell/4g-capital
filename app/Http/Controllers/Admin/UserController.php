<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\User;
use App\Models\TwillPosition;
use A17\Twill\Models\Enums\UserRole;
use Illuminate\Config\Repository as Config;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Edx\EdxAuthUser;
use Carbon\Carbon;
use Storage;
use App\Http\Requests\Admin\UserRequest;

class UserController extends ModuleController
{
    //
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var string
     */
    protected $namespace = 'A17\Twill';

    /**
     * @var string
     */
    protected $moduleName = 'users';

    /**
     * @var string[]
     */
    protected $indexWith = ['medias'];

    /**
     * @var array
     */
    protected $defaultOrders = ['id' => 'desc'];

    /**
     * @var array
     */
    protected $defaultFilters = [
        'search' => 'search',
    ];

    /**
     * @var array
     */
    protected $filters = [
        'role' => 'role',
    ];

    /**
     * @var string
     */
    protected $titleColumnKey = 'name';

    /**
     * @var array
     */
    protected $indexColumns = [
        'name' => [
            'title' => 'Name',
            'field' => 'name',
        ],
        'email_value' => [
            'title' => 'Email',
            'field' => 'email_value',
            'sort' => true,
        ],
        'job_role_value' => [
            'title' => 'Job Role',
            'field' => 'job_role_value',
            'sort' => true,
            'sortKey' => 'job_role',
        ],
        'branch_value' => [
            'title' => 'Branch',
            'field' => 'branch_value',
            'sort' => true,
            'sortKey' => 'branch',
        ],
        'country_value' => [
            'title' => 'Country',
            'field' => 'country_value',
            'sort' => true,
            'sortKey' => 'country',
        ],
        'created_at' => [
            'title' => 'Registered on',
            'field' => 'created_at',
            'sort' => true,

        ],
        'last_login_at' => [
            'title' => 'Last Login',
            'field' => 'last_login_at',
            'sort' => true,

        ],
    ];

    /**
     * @var array
     */
    protected $indexOptions = [
        'permalink' => false,
    ];

    /**
     * @var array
     */
    protected $fieldsPermissions = [
        'role' => 'manage-users',
    ];

    public function __construct(Application $app, Request $request, AuthFactory $authFactory, Config $config)
    {
        parent::__construct($app, $request);

        $this->authFactory = $authFactory;
        $this->config = $config;

        $this->removeMiddleware('can:edit');
        $this->removeMiddleware('can:delete');
        $this->removeMiddleware('can:publish');
        $this->middleware('can:manage-users', ['only' => ['index']]);
        $this->middleware('can:edit-user,user', ['only' => ['store', 'edit', 'update', 'destroy', 'bulkDelete', 'restore', 'bulkRestore']]);
        $this->middleware('can:publish-user', ['only' => ['publish']]);

        if ($this->config->get('twill.enabled.users-image')) {
            $this->indexColumns = [
                'image' => [
                    'title' => 'Image',
                    'thumb' => true,
                    'variant' => [
                        'role' => 'profile',
                        'crop' => 'default',
                    ],
                ],
            ] + $this->indexColumns;
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function indexData($request)
    {
        return [
            'defaultFilterSlug' => 'published',
            'create' => $this->getIndexOption('create') && $this->authFactory->guard('twill_users')->user()->can('manage-users'),
            'roleList' => Collection::make(UserRole::toArray()),
            'single_primary_nav' => [
                'users' => [
                    'title' => twillTrans('twill::lang.user-management.users'),
                    'module' => true,
                ],
            ],
            'customPublishedLabel' => twillTrans('twill::lang.user-management.enabled'),
            'customDraftLabel' => twillTrans('twill::lang.user-management.disabled'),
        ];
    }

    /**
     * @param Request $request
     * @return array
     * @throws \PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException
     * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
     */
    protected function formData($request)
    {
        $user = $this->authFactory->guard('twill_users')->user();
        $with2faSettings = $this->config->get('twill.enabled.users-2fa') && $user->id == $request->route('user');

        if ($with2faSettings) {
            $user->generate2faSecretKey();

            $qrCode = $user->get2faQrCode();
        }

        return [
            'roleList' => Collection::make(UserRole::toArray()),
            'single_primary_nav' => [
                'users' => [
                    'title' => twillTrans('twill::lang.user-management.users'),
                    'module' => true,
                ],
            ],
            'customPublishedLabel' => twillTrans('twill::lang.user-management.enabled'),
            'customDraftLabel' => twillTrans('twill::lang.user-management.disabled'),
            'with2faSettings' => $with2faSettings,
            'qrCode' => $qrCode ?? null,
        ];
    }

    /**
     * @return array
     */
    protected function getRequestFilters()
    {
        return json_decode($this->request->get('filter'), true) ?? ['status' => 'published'];
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $items
     * @param array $scopes
     * @return array
     */
    public function getIndexTableMainFilters($items, $scopes = [])
    {
        $statusFilters = [];

        array_push($statusFilters, [
            'name' => twillTrans('twill::lang.user-management.active'),
            'slug' => 'published',
            'number' => $this->repository->getCountByStatusSlug('published'),
        ], [
            'name' => twillTrans('twill::lang.user-management.disabled'),
            'slug' => 'draft',
            'number' => $this->repository->getCountByStatusSlug('draft'),
        ]);

        if ($this->getIndexOption('restore')) {
            array_push($statusFilters, [
                'name' => twillTrans('twill::lang.user-management.trash'),
                'slug' => 'trash',
                'number' => $this->repository->getCountByStatusSlug('trash'),
            ]);
        }

        return $statusFilters;
    }

    /**
     * @param string $option
     * @return bool
     */
    protected function getIndexOption($option)
    {
        if (in_array($option, ['publish', 'delete', 'restore'])) {
            return $this->authFactory->guard('twill_users')->user()->can('manage-users');
        }

        return parent::getIndexOption($option);
    }

    /**
     * @param \A17\Twill\Models\Model $item
     * @return array
     */
    protected function indexItemData($item)
    {

        $user = $this->authFactory->guard('twill_users')->user();
        $canEdit = $user->can('manage-users') || $user->id === $item->id;
        return [
            'edit' => $canEdit ? $this->getModuleRoute($item->id, 'edit') : null,
        ];
    }



    public function userCodeForm()
    {

        return view('admin.users.user_code');
    }


    public function userCodeStore(UserRequest $request)
    {


        return  App::environment(['local', 'staging']) ? $this->userCodeLocal($request) : $this->userCodeLive($request);
    }


    public function userCodeLocal(Request $request)
    {
        $data = [];
        $users = [];

        // $input = $this->validateFormRequest()->all();
        $number = $request->get('numbers');
        $initials = '4G';

        $check = DB::table('twill_users')->where('authentication_type', '=', User::AUTHENTICATION_USERCODE)->latest('id')->first();

        if (empty($check->user_number)) {
            for ($i = 1; $i <= $number; $i++) {

                $randomString =  self::generateRandomString(9);

                $request->session()->put('register_password', $randomString);

                $pass = Hash::make($randomString);

                $topic[] = ['Name' => '4G-Capital UserCodes -' . Carbon::now()];
                $data[] = [
                    'username' =>  $initials . sprintf("%'05d", $i),
                    'Password' => $randomString
                ];

                $users[] = User::create([
                    'name' => $initials . '' . sprintf("%'05d", $i),
                    'first_name' => $initials,
                    'last_name' => sprintf("%'05d", $i),
                    'username' => $initials . sprintf("%'05d", $i),
                    'role' => UserRole::Learner,
                    'email' => $initials . '@' . sprintf("%'05d", $i) . '.com',
                    'company_id' => 1,
                    'user_number' => sprintf("%'05d", $i),
                    'password' => $pass,
                    'email_verified_at' => Carbon::now(),
                    'is_activated' => 1,
                    'published' => 1,
                    'registered_at' => Carbon::now(),
                    'authentication_type' => USER::AUTHENTICATION_USERCODE,
                    'require_new_password' => 1,
                ]);
            }
            foreach ($data as $user) {


                $u = User::where('username', $user['username'])->first();

                $position = new TwillPosition();
                $position->user_id = $u->id;
                $position->position = $user['Password'];

                $position->save();
            }



            $export_name = '4G-Capital-UsersExport_' . Carbon::now() . '.xlsx';



            return (new UserExport($data, $topic))->download($export_name, \Maatwebsite\Excel\Excel::XLSX);

        } else {
            $count = $check->user_number + 1;
            $numbers = $count + $number;

            for ($i = $count; $i <= $numbers - 1; $i++) {

                $randomString =  self::generateRandomString(9);

                $request->session()->put('register_password', $randomString);

                $pass = Hash::make($randomString);

                $topic[] = ['Name' => '4G-Capital UserCodes -' . Carbon::now()];
                $data[] = [
                    'username' =>  $initials . sprintf("%'05d", $i),
                    'Password' => $randomString
                ];

                $users[] = User::create([
                    'name' => $initials . '' . sprintf("%'05d", $i),
                    'first_name' => $initials,
                    'last_name' => sprintf("%'05d", $i),
                    'username' => $initials . sprintf("%'05d", $i),
                    'role' => UserRole::Learner,
                    'email' => $initials . '@' . sprintf("%'05d", $i) . '.com',
                    'company_id' => 1,
                    'user_number' => sprintf("%'05d", $i),
                    'password' => $pass,
                    'email_verified_at' => Carbon::now(),
                    'is_activated' => 1,
                    'published' => 1,
                    'registered_at' => Carbon::now(),
                    'authentication_type' => USER::AUTHENTICATION_USERCODE,
                    'require_new_password' => 1,
                ]);
            }
            foreach ($data as $user) {


                $u = User::where('username', $user['username'])->first();

                $position = new TwillPosition();
                $position->user_id = $u->id;
                $position->position = $user['Password'];

                $position->save();
            }


            $export_name = '4G-Capital-UsersExport_' . Carbon::now() . '.xlsx';

            return (new UserExport($data, $topic))->download($export_name, \Maatwebsite\Excel\Excel::XLSX);
        }
    }


    public function userCodeLive(Request $request)
    {
        $data = [];
        $users = [];

        $number = $request->get('numbers');
        $initials = '4G';
   

        $check = DB::table('twill_users')->where('authentication_type', '=', User::AUTHENTICATION_USERCODE)->latest('id')->first();

        if (empty($check->user_number)) {
            for ($i = 1; $i <= $number; $i++) {

                $randomString = self::generateRandomString(9);

                $request->session()->put('register_password', $randomString);
                $pass = Hash::make($randomString);

                $topic[] = ['Name' => '4G-Capital UserCodes -' . Carbon::now()];
                $data[] = [
                    'username' => $initials . sprintf("%'05d", $i),
                    'Password' => $randomString
                ];

                $users[] = User::create([
                    'name' => $initials . '' . sprintf("%'05d", $i),
                    'first_name' => $initials,
                    'last_name' => sprintf("%'05d", $i),
                    'username' => $initials . sprintf("%'05d", $i),
                    'role' => UserRole::Learner,
                    'email' => $initials . '@' . sprintf("%'05d", $i) . '.com',
                    'company_id' => 1,
                    'user_number' => sprintf("%'05d", $i),
                    'password' => $pass,
                    'email_verified_at' => Carbon::now(),
                    'is_activated' => 1,
                    'published' => 1,
                    'registered_at' => Carbon::now(),
                    'authentication_type' => USER::AUTHENTICATION_USERCODE,
                    'require_new_password' => 1,
                ]);
            }

            foreach ($users as $v) {
                $edxuser = EdxAuthUser::where('username', $v->username)->first();

        
                if ($edxuser === null) {
                    return "No such user";
                } else {
                    $edxuser->is_active = 1;
                    $edxuser->save();
                }
            }

            foreach ($data as $user) {


                $u = User::where('username', $user['username'])->first();

                $position = new TwillPosition();
                $position->user_id = $u->id;
                $position->position = $user['Password'];

                $position->save();
            }


            $export_name = '4G-Capital-UsersExport_' . Carbon::now() . '.xlsx';

            return (new UserExport($data, $topic))->download($export_name, \Maatwebsite\Excel\Excel::XLSX);
        } else {
            $count = $check->user_number + 1;
            $numbers = $count + $number;

            for ($i = $count; $i <= $numbers - 1; $i++) {

                $randomString = self::generateRandomString(9);

                $request->session()->put('register_password', $randomString);

                $pass = Hash::make($randomString);

                $topic[] = ['Name' => '4G-Capital UserCodes -' . Carbon::now()];
                $data[] = [
                    'username' => $initials . sprintf("%'05d", $i),
                    'Password' => $randomString
                ];

                $users[] = User::create([
                    'name' => $initials . '' . sprintf("%'05d", $i),
                    'first_name' => $initials,
                    'last_name' => sprintf("%'05d", $i),
                    'username' => $initials . sprintf("%'05d", $i),
                    'role' => UserRole::Learner,
                    'email' => $initials . '@' . sprintf("%'05d", $i) . '.com',
                    'company_id' => 1,
                    'user_number' => sprintf("%'05d", $i),
                    'password' => $pass,
                    'email_verified_at' => Carbon::now(),
                    'is_activated' => 1,
                    'published' => 1,
                    'registered_at' => Carbon::now(),
                    'authentication_type' => USER::AUTHENTICATION_USERCODE,
                    'require_new_password' => 1,
                ]);
            }

            foreach ($users as $v) {
                $edxuser = EdxAuthUser::where('username', $v->username)->first();
                if ($edxuser === null) {
                    return "No such user";
                } else {
                    $edxuser->is_active = 1;
                    $edxuser->save();
                }
            }
            foreach ($data as $user) {


                $u = User::where('username', $user['username'])->first();

                $position = new TwillPosition();
                $position->user_id = $u->id;
                $position->position = $user['Password'];

                $position->save();
            }



            $export_name = '4G-Capital-UsersExport_' . Carbon::now() . '.xlsx';

            return (new UserExport($data, $topic))->download($export_name, \Maatwebsite\Excel\Excel::XLSX);
        }
    }





    protected function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    protected function validateFormRequest()
    {
        $unauthorizedFields = Collection::make($this->fieldsPermissions)->filter(function ($permission, $field) {
            return Auth::guard('twill_users')->user()->cannot($permission);
        })->keys();

        $unauthorizedFields->each(function ($field) {
            $this->request->offsetUnset($field);
        });

        return App::make($this->getFormRequestClass());
    }
}
