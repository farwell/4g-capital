<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\Course;
use App\Exports\CourseReflectionExport;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class CourseReflectionController extends ModuleController
{
    protected $moduleName = 'courseReflections';
    
    protected $titleColumnKey = 'username';

    
    protected $filters = [
        'course_id' => 'course_id',
    
    ];
   
    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
        'permalink' => false,
        'publish' =>false,
        'delete' => false,
    ];

    protected $indexColumns = [
        
        'username' => [ // field column
            'title' => 'User Name',
            'field' => 'username',
        ],

         'course_id' => [ // relation column
            'title' => 'Course',
            'sort' => true,
            'relationship' => 'courses',
            'field' => 'name'
        ],
        
        'reflection' => [ // relation column
            'title' => 'Reflection',
            'field' => 'reflection'
        ],
       
    ];


    protected function indexData($request)
    {
        return [
            'course_idList' =>  Course::where('status',1)->orderBy('name')->get(),
            
        ];
    }


    public function export(Request $request)
    {

        $data = json_decode($request->input('tableData'));
       
        $topic[] = ['Name' => 'Course Reflection Reports 4G Capital_'.Carbon::now()];

        return Excel::download(new CourseReflectionExport($data,$topic), 'Course Reflection Reports 4G Capital_'.Carbon::now().'.xlsx');
    }
    
    
}
