<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class MessageTypeController extends BaseModuleController
{
    protected $moduleName = 'messageTypes';

    protected $indexOptions = [
        'permalink' => false,
    ];
}
