<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ResourceThemeController extends ModuleController
{
    protected $moduleName = 'resourceThemes';
    
}
