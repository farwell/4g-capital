<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class MenuTypeController extends ModuleController
{
    protected $moduleName = 'menuTypes';
    
}
