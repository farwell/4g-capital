<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Models\Branch;
use App\Models\Sector;
use App\Repositories\BranchRepository;
use App\Repositories\CountryRepository;
use App\Repositories\CourseRepository;
use Illuminate\Http\Request;

class SectorController extends BaseModuleController
{
    protected $moduleName = 'sectors';

    protected $indexOptions = [
        'reorder' => true,
];


protected $indexColumns = [
        
    'title' => [ // field column
        'title' => 'Title',
        'field' => 'title',
    ],
   
    'branches_selected' => [ // field column
        'title' => 'Branches',
        'field' => 'branches_selected',
    ],

    'courses_selected' => [ // field column
        'title' => 'Mandatory Courses',
        'field' => 'courses_selected',
    ],
];



protected function formData($request)
{

    return [
        'countryList' => app(CountryRepository::class)->listAll('title')->sort(),
        'branchList' =>app(BranchRepository::class)->listAll('title')->sort(),
        'courseList' => app(CourseRepository::class)->listAll('name')->sort(),
    ];
}
}
