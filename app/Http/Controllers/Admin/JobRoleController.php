<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\CourseRepository;
use App\Models\JobRole;
use Illuminate\Support\Collection;

class JobRoleController extends ModuleController
{
    protected $moduleName = 'jobRoles';

    protected $indexOptions = [
        'reorder' => true,
];


protected $indexColumns = [
        
    'title' => [ // field column
        'title' => 'Title',
        'field' => 'title',
    ],
   
    'courses_selected' => [ // field column
        'title' => 'Compulsory Courses',
        'field' => 'courses_selected',
    ],
];


protected function formData($request)
{

    return [
        'courseList' => app(CourseRepository::class)->listAll('name')->sort(),
        
    ];

}







    
}
