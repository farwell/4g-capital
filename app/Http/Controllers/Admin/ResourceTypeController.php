<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ResourceTypeController extends ModuleController
{
    protected $moduleName = 'resourceTypes';

    protected $indexColumns = [
        
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        
        'icon_name' => [ // relation column
            'title' => 'Icon Name',
            'field' => 'icon_name'
        ],
        
    ];
    
}
