<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class CompanyController extends ModuleController
{
    protected $moduleName = 'companies';
    
    protected $titleColumnKey = 'name';
}
