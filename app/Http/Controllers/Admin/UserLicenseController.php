<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\Course;
use App\Models\CourseCreation;
use Illuminate\Support\Collection;
use App\Repositories\UserLicenseRepository;
use App\Models\JobRole;
use App\Models\JobroleCourse;

class UserLicenseController extends ModuleController
{
    protected $moduleName = 'userLicenses';

    protected $perPage = 500;

    protected $titleColumnKey = 'username';
    
    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
        'permalink' => false,
        'publish' =>false,
    ];

    protected $filters = [
        'course_id'=> 'course_id',
        'enrollment_date'=> 'enrollment_date'
    ];

    protected $indexColumns = [

        'username' => [ // relation column
            'title' => 'Name',
            'field' => 'username'
        ],
        'email' => [ // field column
            'title' => 'Email',
            'sort' => true,
            'relationship' => 'user',
            'field' => 'email_value'
        ],

         'course_id' => [ // relation column
            'title' => 'Course',
            'sort' => true,
            'relationship' => 'course',
            'field' => 'display_name'
        ],
    
        'created_at' => [ // field column
            'title' => 'Enrollment Date',
            'field' => 'created_at',
        ],
    ];


    protected function indexData($request)
    {
        return [
            'course_idList' =>  CourseCreation::where('status', 1)->orderBy('display_name')->get(),
            'enrollment_dateList' => '',
        ];
    }
    


    public function addAll(){

        $roles = JobRole::all();
    
        foreach( $roles as $role){
        JobroleCourse::where('job_role_id', '=', $role->id)->where('course_id', 52)->delete();
    
        
         $job = new JobroleCourse();
         $job->job_role_id = $role->id;
         $job->course_id = 51;
         $job->save();
        
        }
    
        return "Yes";
    
    }
}
