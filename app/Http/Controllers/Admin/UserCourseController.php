<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Repositories\CourseRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class UserCourseController extends BaseModuleController
{
    protected $moduleName = 'userCourses';

    protected $indexOptions = [
        'permalink' => false,
    ];

    protected $indexColumns = [
        
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
       
        'courses_selected' => [ // field column
            'title' => 'Recommended Courses',
            'field' => 'courses_selected',
        ],

        
    ];

    protected function formData($request)
    {
     
        $CompanyhrRoles = ['Company HR'];
        
        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ))){
    
         $userList = app(UserRepository::class)->where('published',1)->where('company_id', Auth::user()->company_id)->where('id','!=',1)->pluck('name','id');
        }else{
    
            $userList = app(UserRepository::class)->where('published',1)->where('id','!=',1)->pluck('name','id');
        }
    
        return [
            'userList' => $userList,
            'courseList' => app(CourseRepository::class)->where('status',1)->pluck('name','id'),
            
        ];
    
    }
}
