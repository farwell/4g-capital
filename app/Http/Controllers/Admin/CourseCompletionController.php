<?php

namespace App\Http\Controllers\Admin;


use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Models\Branch;
use App\Models\LearningStatus;
use App\Models\Course;
use App\Models\JobRole;
use App\Models\Country;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use App\Exports\CourseCompletionExport;
use App\Models\CourseCreation;
use App\Models\Sector;
use Illuminate\Support\Collection;
use App\Repositories\CourseCompletionRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class CourseCompletionController extends ModuleController
{
    protected $moduleName = 'courseCompletions';

    protected $titleColumnKey = 'username';

    protected $perPage = 100;

    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
        'permalink' => false,
        'publish' =>false,
    ];

    protected $filters = [
        'course_id' => 'course_id',
        'country_id' => 'country_id',
        'job_role_id' => 'job_role_id',
        'branch_id' => 'branch_id',
        'status_type'=> 'status_type',
        'enrollment_date'=> 'enrollment_date',
        'completion_date'=> 'completion_date'

    ];

    protected $indexColumns = [

        'username' => [ // field column
            'title' => 'User Name',
            'field' => 'username',
        ],

        'email' => [ // field column
            'title' => 'Email',
            'field' => 'email',
        ],
        'country_id' => [ // relation column
            'title' => 'Country',
            'sort' => true,
            'relationship' => 'countries',
            'field' => 'title'
        ],
        'job_role_id' => [ // relation column
            'title' => 'Job Role',
            'sort' => true,
            'relationship' => 'roles',
            'field' => 'title'
        ],
        'branch_id' => [ // relation column
            'title' => 'Branch',
            'sort' => true,
            'relationship' => 'branches',
            'field' => 'title'
        ],

         'course_id' => [ // relation column
            'title' => 'Course',
            'sort' => true,
            'relationship' => 'courses',
            'field' => 'display_name'
        ],

        'completion_status' => [ // relation column
            'title' => 'Status',
            'field' => 'completion_status'
        ],
        'enrollment_date' => [ // field column
            'title' => 'Enrollment Date',
            'field' => 'enrollment_date',
        ],
        'created_at' => [ // field column
            'title' => 'Completion Date',
            'field' => 'created_at',
        ],
    ];


    protected function indexData($request)
    {
        return [
            'course_idList' =>  CourseCreation::published()->where('status',1)->orderBy('display_name')->get(),
            'country_idList' =>  Country::published()->orderBy('Title')->get(),
            'job_role_idList' =>  JobRole::published()->get(),
            'branch_idList' =>  Branch::published()->get(),
            'sector_idList' =>  Sector::published()->get(),
            'status_typeList' =>  LearningStatus::all(),
            'completion_dateList' => '',
            'enrollment_dateList' => '',
        ];
    }

    public function dashboard(){
        $launchYear = "2022";
        $emonth = date('m');
        $cmonth = date('m');

        $eyear = date('Y');
        $cyear = date('Y');

        $genderChartOptions = [
			'chart_title' => 'Onboarded users report by Gender',
            'subtitle' => 'Onboarded Users with completed profiles',
			'chart_type' => 'pie',
			'series_data'=>$this->repository->getCompleteProfileGender(),
		];

        $completionChartOptions = [
			'chart_title' => 'Onboarded Users vs User with Completed Compulsory Modules ',
			'subtitle' => 'Onboarded Users vs User with Completed Compulsory Modules ',
			'chart_type' => 'pie',
			'series_data'=>$this->repository->getCompleteComplusoryModules(),
		];


        $completionFemaleChartOptions = [
			'chart_title' => 'Female Onboarded Users vs User with Completed Compulsory Modules ',
			'subtitle' => 'Female Onboarded Users vs User with Completed Compulsory Modules',
			'chart_type' => 'pie',
			'series_data'=>$this->repository->getFemaleCompleteComplusoryModules(),
		];

        $completionMaleChartOptions = [
			'chart_title' => 'Male Onboarded Users vs User with Completed Compulsory Modules ',
			'subtitle' => 'Male Onboarded Users vs User with Completed Compulsory Modules',
			'chart_type' => 'pie',
			'series_data'=>$this->repository->getMaleCompleteComplusoryModules(),
		];

       
       



       $completedRegistrationoption = $this->repository->getCompleteRegistrations();


       $pendingRegistrationoption = $this->repository->getPendingRegistrations();


       /** Top 5 courses Enrollments */
   $yearlyEnrollmentGraph = [
    'chart_title' => 'Top 5 courses enrolled into to date',
    'enrollment_data'=>$this->repository->getYearlyTopEnrollments($eyear,$launchYear ),
    ];

  /** End Top 5 courses */

    /** Top 5 courses Completions */
    $yearlyCompletionGraph = [
        'chart_title' => 'Top 5 courses completed to date',
        'completion_data' => $this->repository->getYearlyTopCompletions($cyear,$launchYear),
    ];

    /** End Top 5 courses */


       $enrollmentCompletionGraph = [
        'chart_title' => 'Monthly Enrollments vs Completions',
        'enrollment_data'=>$this->repository->getTopEnrollments($emonth),
        'completion_data' => $this->repository->getTopCompletions( $cmonth),
    ];

    $licenseReports = $this->getLicenseReport(4);

  

 
    if ($licenseReports !== null) {
        // Data retrieval was successful
        // Process the retrieved license reports data as needed
        // For example, you can iterate over the reports and display them
        $licenseReport = $licenseReports;;
    } else {
        // Data retrieval failed
        echo "Error fetching license reports.";
    }

 




     
    $userCompletions = $this->repository->getTopUserCompletions();

    return view('admin.courseCompletions.dashboard',compact('completionChartOptions', 'completedRegistrationoption', 'pendingRegistrationoption' ,'enrollmentCompletionGraph', 'yearlyEnrollmentGraph','genderChartOptions','yearlyCompletionGraph','userCompletions','completionFemaleChartOptions','completionMaleChartOptions','licenseReport'));
  }

  private function getLicenseReport($id)
{
    // Fetch license report data for the specific ID from the API
    $response = Http::get('https://erevuka.org/api/licenses/report/'.$id); 

    // Check if the request was successful (status code 2xx)
    if ($response->successful()) {
        // Extract license report data from the response
        return $response->json();
    } else {
        // Handle the case when the request was not successful
        return null;
    }
}

 
    public function eresults(Request $request){

        $nmonth = date('m',strtotime($request->emonth));

        return $this->repository->getTopMonthEnrollments($nmonth);

    }


    public function cresults(Request $request){


        $nmonth = date('m',strtotime($request->cmonth));

        return $this->repository->getTopMonthCompletions($nmonth);

    }


    public function branchCompletion(Request $request){


        if ($request->isMethod('post')) {

            if(empty($request->input('country')) && empty($request->input('search'))){

                $errors = "Please Select one option";

                return Redirect::back()->withErrors($errors);
          }else{

          if ($request->input('country')) {

              $branches = Branch::where('country_id', $request->input('country'))->published()->paginate(50);

          } else if ($request->input('search')) {


            $keyword = $request->input('search');

            $branches = Branch::where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->paginate(50);

          }
        }


        }else{

            $branches = Branch::published()->paginate(50);

        }



        return view('admin.courseCompletions.branchCompletion', compact('branches'));
    }






    public function sectorCompletion(Request $request){


        if ($request->isMethod('post')) {

            if(empty($request->input('country')) && empty($request->input('search'))){

                $errors = "Please Select one option";

                return \Redirect::back()->withErrors($errors);
          }else{

          if ($request->input('country')) {

              $sectors = Sector::where('country_id', $request->input('country'))->published()->paginate(50);

          } else if ($request->input('search')) {


            $keyword = $request->input('search');

            $sectors = Sector::where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->paginate(50);

          }
        }


        }else{

            $sectors = Sector::published()->paginate(50);

        }



        return view('admin.courseCompletions.sectorCompletion', compact('sectors'));
    }

    public function jobroleCompletion(Request $request){

        $jobRoles = JobRole::with(['courses', 'courses.userLicenses', 'courses.completions'])->paginate(100);



 



        // if ($request->isMethod('post')) {

        //     if(empty($request->input('country')) && empty($request->input('search'))){

        //         $errors = "Please Select one option";

        //         return \Redirect::back()->withErrors($errors);
        //   }else{

        //   if ($request->input('country')) {

        //       $sectors = Sector::where('country_id', $request->input('country'))->published()->paginate(50);

        //   } else if ($request->input('search')) {


        //     $keyword = $request->input('search');

        //     $sectors = Sector::where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->paginate(50);

        //   }
        // }


        // }else{

        //     $sectors = Sector::published()->paginate(50);

        // }



        return view('admin.courseCompletions.jobroleCompletion', compact('jobRoles'));
    }

    // sudo nohup php /var/www/html/4g-capital/artisan queue:work — daemon > /var/www/html/4g-capital/storage/logs/laravel.log & 2>&1 &








    public function export(Request $request)
    {


        $data = json_decode($request->input('tableData'));

        $topic[] = ['Name' => 'Course Completion Reports 4G Capital_'.Carbon::now()];

        return Excel::download(new CourseCompletionExport($data,$topic), 'Course Completion Reports 4G Capital_'.Carbon::now().'.xlsx');
    }

    



     public function yearlyEResults(Request $request){

        $nyear = $request->eyear;

        return $this->repository->getTopYearlyEnrollments($nyear);

    }



    public function yearlyCRresults(Request $request){
        $nyear = $request->cyear;

        return $this->repository->getTopYearlyCompletions($nyear);

    }

    

}
