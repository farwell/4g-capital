<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use A17\Twill\Services\Forms\Fields\Input;
use A17\Twill\Services\Forms\Fields\Wysiwyg;
use A17\Twill\Services\Forms\Form;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Response;
use A17\Twill\Helpers\FlashLevel;
use Carbon\Carbon;
use App\Edx\EdxAuthUser;
use Auth;
use App;
use DB;
use App\Models\CourseCreation;
use ZipArchive;

class CourseCreationController extends ModuleController
{
    protected $moduleName = 'courseCreations';
    protected $titleColumnKey = 'display_name';

    protected $indexOptions = [
        'permalink' => false
    ];

    protected $indexColumns = [

        'display_name' => [ // field column
            'title' => 'Name',
            'field' => 'display_name'
        ],

        'start' => [ // field column
            'title' => 'Start Date',
            'field' => 'start',
        ],

        'end' => [ // relation column
            'title' => 'End Date',
            'field' => 'end'
        ],

        'effort' => [ // relation column
            'title' => 'Effort',
            'field' => 'effort'
        ],
        'due_date' => [ // relation column
            'title' => 'Due Date',
            'field' => 'due_date'
        ],


    ];



    public function getCreateForm(): Form
    {
        return Form::make([
            Input::make()
                ->name('title')
                ->label('My title field')
                ->translatable()
                ->onChange('formatPermalink'),
            Wysiwyg::make()
                ->name('description')
                ->label('Descrription')
                ->translatable(),
            Input::make()
                ->name('slug')
                ->label(twillTrans('twill::lang.modal.permalink-field'))
                ->translatable()
                ->ref('permalink')
                ->prefix($this->getPermalinkPrefix($this->getPermalinkBaseUrl())),
        ]);
    }



    /**
     * @param int|null $parentModuleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($parentModuleId = null)
    {
        $parentModuleId = $this->getParentModuleIdFromRequest($this->request) ?? $parentModuleId;

        $input = $this->validateFormRequest()->all();
        $optionalParent = $parentModuleId ? [$this->getParentModuleForeignKey() => $parentModuleId] : [];

        if (isset($input['cmsSaveType']) && $input['cmsSaveType'] === 'cancel') {
            return $this->respondWithRedirect(moduleRoute(
                $this->moduleName,
                $this->routePrefix,
                'create'
            ));
        }

        //    $edxUser = EdxAuthUser::where('username', Auth::user()->username)->first();

        //    if(!$edxUser->is_staff){

        //     return $this->respondWithError(trans('admin.course_errors.edx_premissions'));
        //    }



        $item = App::environment(['local', 'staging']) ?  $this->repository->createLocal($input + $optionalParent) :  $this->repository->create($input + $optionalParent);

        if ($item['status'] == false) {

            return $this->respondWithError($item['data']);
        }
        activity()->performedOn($item)->log('created');

        $this->fireEvent($input);

        Session::put($this->moduleName . '_retain', true);

        if ($this->getIndexOption('editInModal')) {
            return $this->respondWithSuccess(twillTrans('twill::lang.publisher.save-success'));
        }

        if (isset($input['cmsSaveType']) && Str::endsWith($input['cmsSaveType'], '-close')) {
            return $this->respondWithRedirect($this->getBackLink());
        }

        if (isset($input['cmsSaveType']) && Str::endsWith($input['cmsSaveType'], '-new')) {
            return $this->respondWithRedirect(moduleRoute(
                $this->moduleName,
                $this->routePrefix,
                'create'
            ));
        }

        return $this->respondWithRedirect(moduleRoute(
            $this->moduleName,
            $this->routePrefix,
            'edit',
            [Str::singular(last(explode('.', $this->moduleName))) => $this->getItemIdentifier($item)]
        ));
    }



    public function settings($parentModuleId = null)
    {

        $input = $this->validateFormRequest()->all();
        $optionalParent = $parentModuleId ? [$this->getParentModuleForeignKey() => $parentModuleId] : [];



        if (empty($input['start']) || $input['start'] == null) {

            //return Redirect::back()->withError('The Course Start Date is required');
            Session::flash('status', 'The Course Start Date is required');
            return Redirect::back();
        }
        if (empty($input['end']) || $input['end'] == null) {

            //return Redirect::back()->withError('The Course End Date is required');
            Session::flash('status', 'The Course End Date is required');
            return Redirect::back();
        }

        if (empty($input['enrol_start']) || $input['enrol_start'] == null) {

            //return Redirect::back()->withError('The Course Start Date is required');
            Session::flash('status', 'The Course Enrollment Start Date is required');
            return Redirect::back();
        }

        if (empty($input['enrol_end']) || $input['enrol_end'] == null) {

            //return Redirect::back()->withError('The Course Start Date is required');
            Session::flash('status', 'The Course Enrollment End Date is required');
            return Redirect::back();
        }


        if (isset($input['save']) && $input['save'] === 'cancel') {
            return $this->respondWithRedirect(moduleRoute(
                $this->moduleName,
                $this->routePrefix,
                'create'
            ));
        }

        $item = App::environment(['local', 'staging']) ?  $this->repository->updateSettingsLocal($input + $optionalParent) :  $this->repository->updateSettings($input + $optionalParent);

        if ($item == false) {

            //  Session::flash( ['message' => 'Something Went wrong! Please check the inputs and try again','variant' => FlashLevel::ERROR]);
            return Redirect::back()->withErrors('Something Went wrong! Please check the inputs and try again');;
        } else {

        Session::flash('status', 'Course Settings Updated');
        return Redirect::back()->withInput(['tab' => 'settings']);
        }
    }



    public function advanced($parentModuleId = null)
    {

        $input = $this->validateFormRequest()->all();
        $optionalParent = $parentModuleId ? [$this->getParentModuleForeignKey() => $parentModuleId] : [];


        if (isset($input['save']) && $input['save'] === 'cancel') {
            return $this->respondWithRedirect(moduleRoute(
                $this->moduleName,
                $this->routePrefix,
                'create'
            ));
        }

        $item = App::environment(['local', 'staging']) ?  $this->repository->updateAdvancedLocal($input + $optionalParent) :  $this->repository->updateAdvanced($input + $optionalParent);

        if ($item == false) {

            //  Session::flash( ['message' => 'Something Went wrong! Please check the inputs and try again','variant' => FlashLevel::ERROR]);
            return Redirect::back()->withErrors('Something Went wrong! Please check the inputs and try again');;
        } else {

        Session::flash('status', 'Course Advanced Settings Updated');
        return Redirect::back()->withInput(['tab' => 'advanced']);
        }
    }





    public function grading($parentModuleId = null)
    {

        if (isset($input['save']) && $input['save'] === 'cancel') {
            return $this->respondWithRedirect(moduleRoute(
                $this->moduleName,
                $this->routePrefix,
                'create'
            ));
        }

        $input = $this->validateFormRequest()->all();
        $optionalParent = $parentModuleId ? [$this->getParentModuleForeignKey() => $parentModuleId] : [];

        //dd($input);
        if (!isset($input['blocks']) || !$input['blocks']) {
            Session::flash('status', 'Please Add Assignment Types by clicking the Add content button ');
            return Redirect::back()->withInput(['tab' => 'grading']);
        }
        //    dd($request->all());

        $item = App::environment(['local', 'staging']) ?  $this->repository->updateGradingLocal($input + $optionalParent) :  $this->repository->updateGrading($input + $optionalParent);



        Session::flash('status', 'Course Advanced Settings Updated');
        return Redirect::back()->withInput(['tab' => 'grading']);
        
    }


    public function blocks($parentModuleId = null)
    {

        if (isset($input['save']) && $input['save'] === 'cancel') {
            return $this->respondWithRedirect(moduleRoute(
                $this->moduleName,
                $this->routePrefix,
                'create'
            ));
        }

        $input = $this->validateFormRequest()->all();
        $optionalParent = $parentModuleId ? [$this->getParentModuleForeignKey() => $parentModuleId] : [];


        //dd($input);

        $item = App::environment(['local', 'staging']) ?  $this->repository->updateBlocksLocal($input + $optionalParent) :  $this->repository->updateBlocks($input + $optionalParent);

        // dd($item);


        //
        if ($item == false
        ) {

            //  Session::flash( ['message' => 'Something Went wrong! Please check the inputs and try again','variant' => FlashLevel::ERROR]);
            return Redirect::back()->withErrors('Something Went wrong! Please check the inputs and try again');;
        } else {

        if ($input['category'] === 'chapter' || $input['category'] === 'Section') {

            $cType = 'Section';
        } else if ($input['category'] === 'sequential' || $input['category'] === 'Subsection') {

            $cType = 'Subsection';
        } else if ($input['category'] === 'vertical' || $input['category'] === 'unit') {

            $cType = 'Unit';
        }
        if ($input['type'] === 'edit') {

            Session::flash('status', $cType . ' edit successful');
        } else if ($input['type'] === 'delete') {

            Session::flash('status', $cType . ' deleted successfully');
        } else {

            Session::flash('status', $cType . ' created successfully');
        }


        if ($input['category'] === 'vertical' || $input['category'] === 'unit' && $input['type'] != 'delete') {

            return Redirect::route('admin.unit.creation', ['item' => $item['0'], 'locator' => $item['1']]);
        }


        return Redirect::back()->withInput(['tab' => 'content']);
    }
    }


    public function unitCreation($item, $locator)
    {

        $structure = CourseCreation::where('id', $item)->first();

        return view('admin.courseCreations.components.unitComponents', ['item' => $item, 'locator' => $locator, 'structure' => $structure]);
    }




    public function createUnitComponent(Request $request)
    {

        if ($request) {

            $input = $request->all();
            $item = App::environment(['local', 'staging']) ?  $this->repository->createLocal($input) :  $this->repository->createUnitComponent($input);


            if ($item == true) {
                $msg = "The Component created successfully.";
                return response()->json(array('success' => $msg), 200);
            } else {
                $error = "The was an error.";
                return response()->json(array('error' => $error), 200);
            }
        }
    }









    public function unitBlocks(Request $request, $parentModuleId = null)
    {

        if (isset($input['save']) && $input['save'] === 'cancel') {
            return $this->respondWithRedirect(moduleRoute(
                $this->moduleName,
                $this->routePrefix,
                'create'
            ));
        }

        $input = $this->validateFormRequest()->all();
        $optionalParent = $parentModuleId ? [$this->getParentModuleForeignKey() => $parentModuleId] : [];

        //dd($input);

        $item = App::environment(['local', 'staging']) ?  $this->repository->updateBlocksLocal($input + $optionalParent) :  $this->repository->updateUnitBlocks($input + $optionalParent);




         if($item == false){

            //  Session::flash( ['message' => 'Something Went wrong! Please check the inputs and try again','variant' => FlashLevel::ERROR]);
             return Redirect::back()->withErrors('Something Went wrong! Please check the inputs and try again');;

         }else{


            if ($input['type'] === 'edit') {

                Session::flash('status',  ' Component edit successful');
            } else if ($input['type'] === 'delete') {

                Session::flash('status', 'Component deleted successfully');
            }


            return Redirect::route('admin.unit.creation', ['item' => $item['0'], 'locator' => $item['1']]);
         }
        //

        // return Redirect::back()->withInput(['tab' => 'content']);
    }



    public function unitPublish($parentModuleId = null)
    {

        if (isset($input['save']) && $input['save'] === 'cancel') {
            return $this->respondWithRedirect(moduleRoute(
                $this->moduleName,
                $this->routePrefix,
                'create'
            ));
        }

        $input = $this->validateFormRequest()->all();
        $optionalParent = $parentModuleId ? [$this->getParentModuleForeignKey() => $parentModuleId] : [];


        //dd($input);

        $item = publishUnit($input['unit_id']);



        Session::flash('status',  'Unit Published');


        return Redirect::route('admin.unit.creation', ['item' => $input['item_id'], 'locator' => $input['unit_id']]);



        // return Redirect::back()->withInput(['tab' => 'content']);
    }


    protected function respondWithError($message)
    {
        return $this->respondWithJson($message, FlashLevel::ERROR);
    }

    protected function respondWithJson($message, $variant)
    {
        return Response::json([
            'message' => $message,
            'variant' => $variant,
        ]);
    }


    public function courseSync(){

        $edx_courses = $this->edxgetCourses();

        foreach ($edx_courses as $key => $edx_course) {

            //dd($key, $edx_course);

            $slug = str_replace(' ', '-', $edx_course['name']);
            $slug2 = str_replace('-–-', '-', $slug);

        if (CourseCreation::where('course_id', $edx_course['id'])->count()){

            DB::table('course_creations')
                ->where('course_id', $edx_course['id'])
                ->update([
                    'course_id' => $edx_course['id'],
                    'display_name' =>  $edx_course['name'],
                    'short_description' => $edx_course['short_description'],
                    'overview' => $edx_course['overview'],
                    'effort' => $edx_course['effort'],
                    'start' => $edx_course['start'],
                    'end' => $edx_course['end'],
                    'enrol_start' => $edx_course['enrollment_start'],
                    'enrol_end' => $edx_course['enrollment_end'],
                    'course_image_uri' => $edx_course['course_image_uri'],
                    'course_video_uri' => $edx_course['course_video_uri'],
                    'short_name' => $slug2,
                    'self_paced' => $edx_course['self_paced'],
                    'mobile_available' => $edx_course['mobile_available'],
                    'pass_mark' => $edx_course['pass_mark'],
                    'advanced_modules' => $edx_course['advanced_modules'],
                    'enable_subsection_gating' => $edx_course['enable_subsection_gating'],
                    'certificates_display_behavior'=> $edx_course['certificates_display_behavior'],

                ]);
            }else{
                CourseCreation::create([
                    'course_id' => $edx_course['id'],
                    'display_name' =>  $edx_course['name'],
                    'short_description' => $edx_course['short_description'],
                    'overview' => $edx_course['overview'],
                    'effort' => $edx_course['effort'],
                    'start' => $edx_course['start'],
                    'end' => $edx_course['end'],
                    'enrol_start' => $edx_course['enrollment_start'],
                    'enrol_end' => $edx_course['enrollment_end'],
                    'course_image_uri' => $edx_course['course_image_uri'],
                    'course_video_uri' => $edx_course['course_video_uri'],
                    'short_name' => $slug2,
                    'self_paced' => $edx_course['self_paced'],
                    'mobile_available' => $edx_course['mobile_available'],
                    'pass_mark' => $edx_course['pass_mark'],
                    'advanced_modules' => $edx_course['advanced_modules'],
                    'enable_subsection_gating' => $edx_course['enable_subsection_gating'],
                    'certificates_display_behavior' => $edx_course['certificates_display_behavior']

                ]);

            }

        }

        Session::flash('status', 'Courses have been synced successfully.');
        return redirect('admin/courseCreations');
    }


    private function edxgetCourses()
    {
        $configLms = config()->get("settings.lms.live");
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', $configLms['LMS_BASE'] . '/api/courses/v1/courses/?page=1&page_size=500');

            $courses =  json_decode($response->getBody()->getContents())->results;


            foreach ($courses as $key => $value) {

                $course = (array)$value;
                $settings = $this->getAdvanced($course);

                $courses[$key] = (array)$courses[$key];
                $course['overview'] = $this->getOverview($course);
                //Remove unwanted fields
                $course['course_video_uri'] = $course['media']->course_video->uri;
                $course['course_image_uri'] =  $configLms['LMS_BASE'] . $course['media']->course_image->uri;
                unset($course['media']);
                unset($course['course_id']);
                //Format datetime
                $vowels = array("T", "Z");
                $course['start'] =  date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ", $course['start'])));
                $course['end'] = date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ", $course['end'])));
                $course['enrollment_start'] = date('Y-m-d H:m:i', strtotime($course['enrollment_start']));
                $course['enrollment_end'] = date('Y-m-d H:m:i', strtotime($course['enrollment_end']));
                $course['pass_mark'] = $this->getGrading($course);
                $course['advanced_modules'] = (isset($settings['advanced_modules'])) ? implode(',', $settings['advanced_modules']['value']) : '';
                $course['enable_subsection_gating']= $settings['enable_subsection_gating']['value'];
                $course['mobile_available'] = $settings['mobile_available']['value'];
                $course['certificates_display_behavior'] = $settings['certificates_display_behavior']['value'];
                $course['self_paced'] = $course['pacing'];
                //Format time
                $exploded_effort = explode(":", $course['effort']);
                switch (count($exploded_effort)) {
                    case '3':
                        $course['effort'] = Carbon::createFromTime($exploded_effort[0], $exploded_effort[1], $exploded_effort[2])->toTimeString();
                        break;
                    case '2':
                        $course['effort'] = Carbon::createFromTime(0, $exploded_effort[0], $exploded_effort[1])->toTimeString();
                        break;
                }
                $courses[$key] = $course;
            }
            return $courses;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            //dd($response);
        }
    }

    private function getOverview($course)
    {
        $configLms = config()->get("settings.lms.live");
        $client = new \GuzzleHttp\Client();
        try {
            //Get course description
            $request = $client->request('GET', $configLms['LMS_BASE'] . '/api/courses/v1/courses/' . $course['id']);
            $response = json_decode($request->getBody()->getContents());


            return $response->overview;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            return Toastr::error("Error enrolling into course");
            return false;
        }
    }


    private function getGrading($course)
    {
        $configLms = config()->get("settings.lms.live");
        $userinfo = array();

        $user = EdxAuthUser::where('username', Auth::user()->username)->first();
        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        // use Client ti process a get method from the studio

        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->user = json_encode($userinfo);

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',

                ]
            ]
        );

        try {
            $response = $enrollClient->request('GET', $configLms['CMS_BASE'] . '/course/settings/grading/' . $course['id'], [
                \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
            ]);

            $res = json_decode($response->getBody()->getContents(), true);

            return $res['minimum_grade_credit'];

        } catch (\GuzzleHttp\Exception\ClientException $e) {


            return false;
        } catch (\Exception $e) {
            return false;

        }
    }

    private function getAdvanced($course)
    {
        $configLms = config()->get("settings.lms.live");
        $userinfo = array();

        $user = EdxAuthUser::where('username', Auth::user()->username)->first();
        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        // use Client ti process a get method from the studio

        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->user = json_encode($userinfo);

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',

                ]
            ]
        );

        try {
            $response = $enrollClient->request('GET', $configLms['CMS_BASE'] . '/course/settings/advanced/' . $course['id'], [
                \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
            ]);

            $res = json_decode($response->getBody()->getContents(), true);

            return $res;
        } catch (\GuzzleHttp\Exception\ClientException $e) {


            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
