<?php

namespace App\Http\Requests\Admin;

use A17\Twill\Http\Requests\Admin\Request;

class CompanyRequest extends Request
{
    public function rulesForCreate()
    {
        return [];
    }

    public function rulesForUpdate()
    {
        return [
        'contact_first_name' => 'required',
        'contact_last_name' => 'required',
        'contact_email' => 'required',
        ];
    }
}
