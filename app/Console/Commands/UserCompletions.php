<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Edx\EdxAuthUser;
use App\Edx\StudentCourseEnrollment;
use App\Edx\StudentCourseCompletion;
use App\Models\UserLicense;
use App\Models\CourseCompletion;
use App\Models\TwillPosition;
use Auth;

class UserCompletions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:completions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get user completions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $licenses = UserLicense::all();
            foreach ($licenses as $license) {

                if ($license->enrolled_at) {

                  if($license->user){
                    $token = TwillPosition::where('user_id', $license->user->id)->first();

                    if($token){
                    if($token->etock){
                        if($license->course ){
                                $result = $this->getCourseProgress($license->course->course_id, $license->user->email, $token->etock);

                                $quiz = CourseCompletion::where('user_id', $license->user->id)->where('course_id', $license->course->id)->first();


                                if ($result['grade'] > 0) {
                                    if ($quiz) {
                                        $quiz->user_id = $license->user->id;
                                        $quiz->course_id = $license->course->id;
                                        $quiz->username = $license->user->name;
                                        $quiz->email = $license->user->email;
                                        $quiz->branch_id = $license->user->branch_id;
                                        $quiz->score = $result['grade'];
                                        $quiz->max_score = 1;
                                        $quiz->completion_date = $result['completion'];
                                        $quiz->enrollment_date = $result['enrollment'];
                                        $quiz->job_role_id = $license->user->job_role_id;
                                        $quiz->country_id = $license->user->country_id;
                                        $quiz->user_license_id = $license->id;
                                        $quiz->status = $result['status'];
                                        $quiz->save();
                                    } else {
                                        $quiz = new CourseCompletion();
                                        $quiz->user_id = $license->user->id;
                                        $quiz->course_id = $license->course->id;
                                        $quiz->username = $license->user->name;
                                        $quiz->email = $license->user->email;
                                        $quiz->branch_id = $license->user->branch_id;
                                        $quiz->score = $result['grade'];
                                        $quiz->max_score = 1;
                                        $quiz->completion_date = $result['completion'];
                                        $quiz->enrollment_date = $result['enrollment'];
                                        $quiz->job_role_id = $license->user->job_role_id;
                                        $quiz->country_id = $license->user->country_id;
                                        $quiz->user_license_id = $license->id;
                                        $quiz->status = $result['status'];
                                        $quiz->save();
                                    }
                                }
                        }

                }
            }
                }
            }


            }

        echo 'done';
    }

        private function getCourseProgress($courseId, $email, $token)
    {


        $user = EdxAuthUser::where('email', $email)->first();

        $enrollment = StudentCourseEnrollment::where(['user_id' => $user->id, 'course_id' => $courseId])->first();
        $completion = StudentCourseCompletion::where(['student_id' => $user->id, 'module_type' => 'problem', 'course_id' => $courseId])->orderBy('id', 'DESC')->first();
        if ($enrollment) {

            $gradeApiObject = $enrollment->getGenCertificate($courseId, $user->username, $token );

            if ($gradeApiObject[0]->percent >= 0.80) {
                $gradeApiObject[0]->letter_grade = 'Completed';
                $completion = date('Y-m-d H:i:s', strtotime($completion->modified));
            } elseif ($gradeApiObject[0]->percent <= 0.79 && $gradeApiObject[0]->percent >= 0.10) {

                $gradeApiObject[0]->letter_grade = 'Completed but Failed';
                $completion = date('Y-m-d H:i:s', strtotime($completion->modified));
            } else {
                $gradeApiObject[0]->letter_grade = 'In Progress';
                $completion = '';
            }

            return [
                'status' => $gradeApiObject[0]->letter_grade,
                'grade' => (float)$gradeApiObject[0]->percent,
                'completion' => $completion,
                'enrollment' => $enrollment->created,
            ];
        } else {
            return ['status' => 'Pending', 'grade' => '0'];
        }
    }
}
