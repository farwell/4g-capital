<?php

namespace App\Edx;

use App\Edx\GeneratedCertificate;
use App\Edx\StudentCourseware;
use Illuminate\Database\Eloquent\Model;
use DB;
use Cookie;

class StudentCourseEnrollment extends Model
{
  //set connection for model
  protected $connection = 'edx_mysql';

  protected $fillable = ['course_id', 'created', 'is_active', 'mode', 'user_id'];

  //Set table for model
  protected $table = 'student_courseenrollment';

  public $timestamps = false;


  public function edx_user(){
    return $this->belongsTo('App\Edx\EdxAuthUser','user_id');
  }

  public function course(){
    return $this->belongsTo('App\Courses');
  }

  public function getGenCert(){

    $configLms = config()->get("settings.lms.live");

    $client = new \GuzzleHttp\Client(
      [
        'verify'=>env('VERIFY_SSL',true),
        'headers'=>['Authorization'=>'Bearer '. $_COOKIE['edinstancexid']
      ]
    ]);
      $request =  $client->request('GET', $configLms['LMS_BASE'].'/api/grades/v0/course_grade/'.$this->course_id.'/users/?username='.$this->edx_user->username);
         if ($request) {
             return json_decode($request->getBody()->getContents());
           }
          return false;
    //    $genCert = GeneratedCertificate::where('course_id',$this->course_id)->where('user_id',$this->user_id)->first();
    //
    //
    //    if($genCert){
    //       $this->g = $genCert->grade;
    //       $this->grade = ($genCert->grade * 100).'%';
    //      switch ($genCert->status) {
    //        case 'downloadable':
    //           $this->status = "Completed";
    //          break;
    //        case 'notpassing':
    //        default:
    //          $this->status = "Completed but Failed";
    //          break;
    //      }
    //
    //    }else{
    //      $this->status = 'In Progress';
    //      $this->grade = (0).'%';
    //    }
    //
    // return $genCert;
  }


    public function getGenCertificate($course, $username, $token)
    {

        $configLms = config()->get("settings.lms.live");

        $client = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]
        );
        $request =  $client->request('GET', $configLms['LMS_BASE'] . '/api/grades/v0/course_grade/' . $course . '/users/?username=' . $username);
        if ($request) {
            return json_decode($request->getBody()->getContents());
        }
        return false;

    }

  public function getCompletion($user, $courses){
  $grades= array();
  $found = null;
  $datas = StudentCourseware::where('student_id',$user)->where('module_type','=','problem')->where('grade','!=','')->get();
  if($datas){
    foreach($datas as $data){
      $grades[] = $data->course_id;
    }
  foreach($grades as $g){
    if (in_array($g,$courses)) {
         $found['qty'] = true;
     }
    }
    return $found;
  }
}

  public function getGenCerts($user, $course){
     // var_dump($user);
     $grades=array();
    $datas = StudentCourseware::where('course_id',$course)->where('student_id',$user)->where('module_type','=','problem')->get();
    if($datas){
      foreach($datas as $data){
         $grades[] = $data->grade;
      }
      $g = (array_sum($grades)/ 5);
      $this->grade = ((array_sum($grades)/ 5) * 100). '%';
        if ($g >= 0.80){
           $this->status = 'Completed';
        }elseif($g <= 0.79 && $g >= 0.20){
         $this->status = 'Completed but Failed';
       }else{
          $this->status = 'In Progress';
        $this->grade = (0).'%';

       }
      if($course === 'course-v1:SLBA+Mod_6+2018'){
         $this->status = 'Enrolled';
          $this->grade = 'Not Gradable';
        }

     }
    return $datas;
  }


  public function getGenRep(){
       $genCert = GeneratedCertificate::where('course_id',$this->course_id)->where('user_id',$this->user_id)->first();


       if($genCert){
          $this->g = $genCert->grade;
          $this->grade = ($genCert->grade * 100).'%';
         switch ($genCert->status) {
           case 'downloadable':
              $this->status = "Completed";
             break;
           case 'notpassing':
           default:
             $this->status = "Completed but Failed";
             break;
         }

       }else{
         $this->status = 'In Progress';
         $this->grade = (0).'%';
       }

    return $genCert;
  }

  public function getGenRept(){
    $genRept = GeneratedCertificate::where('user_id',$this->user_id)->where('status','downloadable')->first();
    return $genRept;
  }


}
