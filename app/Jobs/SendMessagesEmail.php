<?php

namespace App\Jobs;

use App\Mail\EmailForQueuing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMessagesEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $users;
    protected $sender;

    protected $subject;
    protected $msg;
  
    public function __construct($sender,$users,$subject,$msg)
    {
        $this->users = $users;
         $this->sender = $sender;
        $this->subject = $subject;
        $this->msg = $msg;
       
        // dd($this->users);

        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user) {

            
            $sender = $this->sender;
            $subject = $this->subject;
           $name = $user->name;
            $msg = $this->msg;
            
    
            $data = [
                'sender' => $sender,
                'subject' => $subject,
                'name' =>$name,
                'msg' => $msg,
                
            ];
    
            $email = new EmailForQueuing($data);
            Mail::to($user->email, $name)->send($email);
        }
    }
}
