<?php

if (!function_exists('routeLocalized')) {
    function routeLocalized($name, $parameters = [], $locale =null, $absolute =false)
    {
        $locale = substr(($locale == null ? app()->getLocale() : $locale), 0, 2);

        $url = route("{$name}", $parameters, false);
        if ($locale != null && $locale != config('app.defaultLocale')) {
            $url .= "?language={$locale}";
        }

        return ($absolute ? Request::getScheme().'://'. config('app.url') . $url : $url);
    }
}
