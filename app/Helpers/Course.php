<?php

use App\Edx\EdxAuthUser;
use A17\Twill\Models\Block;
use App\Models\CourseCreation;


if (!function_exists('courseEdxCreate')) {
    function courseEdxCreate($fields)
    {

        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");
        $userinfo = array();
        $user = EdxAuthUser::where('username', Auth::user()->username)->first();

        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];


        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->display_name = $fields['display_name'];
        $enrollmentInfoObject->org = $fields['org'];
        $enrollmentInfoObject->number = $fields['number'];
        $enrollmentInfoObject->run = $fields['run'];
        $enrollmentInfoObject->user = json_encode($userinfo);
        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => ' application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',
                ]
            ]
        );

        try {
            $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/create', [
                \GuzzleHttp\RequestOptions::JSON => $enrollmentInfoObject
            ]);


            $res = json_decode($response->getBody()->getContents(), true);



            if (isset($res['course_key'])) {
                $sendback = ['data' => $res['course_key'], 'status' => true];
                return $sendback;
            } elseif (isset($res['ErrMsg'])) {

                $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                return  $sendback;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}



if (!function_exists('courseEdxSettings')) {
    function courseEdxSettings($fields)
    {

        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");
        $userinfo = array();


        $user = EdxAuthUser::where('username', Auth::user()->username)->first();

        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        //  if(!empty($fields['medias'])){

        //   $upload =   uploadAssets($fields['medias']['cover_image'], $fields['course_id']);

        //   if($upload != true){

        //     return $upload;
        //   }

        //  }





        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->start_date = $fields['start'];
        $enrollmentInfoObject->end_date = $fields['end'];
        $enrollmentInfoObject->enrollment_start = $fields['enrol_start'];
        $enrollmentInfoObject->enrollment_end = $fields['enrol_end'];
        $enrollmentInfoObject->effort = $fields['effort'];
        $enrollmentInfoObject->short_description = $fields['short_description'];
        $enrollmentInfoObject->overview = $fields['overview'];
        $enrollmentInfoObject->self_paced = ($fields['self_paced'] === "1") ? 1 : 0;
        $enrollmentInfoObject->intro_video = '';
        $enrollmentInfoObject->user = json_encode($userinfo);


        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',
                    'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );

        try {
            $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/settings/details/' . $fields['course_id'], [
                \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
            ]);

            $res = json_decode($response->getBody()->getContents(), true);


            if ($response->getStatusCode() == 200) {

                $sendback = ['data' => $res, 'status' => true];
                return $sendback;
            } else {

                $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                return  $sendback;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}



if (!function_exists('courseEdxAdvanced')) {
    function courseEdxAdvanced($fields)
    {

        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");
        $userinfo = array();


        $user = EdxAuthUser::where('username', Auth::user()->username)->first();

        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        $default_advanced = "openassessment,scormxblock,library_content,freetextresponse,videoalpha";

        $modules = [
            'deprecated' => false,
            'display_name' => "Advanced Module List",
            'value' => ($fields['advanced_modules']) ? $fields['advanced_modules'] : $default_advanced
        ];

        $displayName = [
            'deprecated' => false,
            'display_name' => "Course Display Name",
            'value' =>  $fields['display_name']
        ];

        $certificate = [
            'deprecated' => false,
            'display_name' => "Certificates Display Behavior",
            'value' =>  $fields['certificates_display_behavior']
        ];

        $mobile = [
            'deprecated' => false,
            'display_name' => "Mobile Course Available",
            'value' =>  $fields['mobile_available']
        ];

        $prerequisite = [
            "deprecated" => false,
            "display_name" => "Enable Subsection Prerequisites",
            "value" =>  $fields['enable_subsection_gating']
        ];



        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->advanced_modules = $modules;
        $enrollmentInfoObject->display_name = $displayName;
        $enrollmentInfoObject->certificates_display_behavior = $certificate;
        $enrollmentInfoObject->mobile_available = $mobile;
        $enrollmentInfoObject->enable_subsection_gating = $prerequisite;
        $enrollmentInfoObject->user = json_encode($userinfo);


        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',
                    'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );

        try {
            $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/settings/advanced/' . $fields['course_id'], [
                \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
            ]);

            $res = json_decode($response->getBody()->getContents(), true);

            if ($response->getStatusCode() == 200) {

                $sendback = ['data' => $res, 'status' => true];
                return $sendback;
            } else {

                $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                return  $sendback;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}



if (!function_exists('courseEdxGrading')) {
    function courseEdxGrading($fields)
    {

        $configLms = config()->get("settings.lms.live");
        $userinfo = array();

        $graders = array();
        $user = EdxAuthUser::where('username', Auth::user()->username)->first();

        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        $s = CourseCreation::where('id', '=', $fields['item_id'])->first();

        if ($s->pass_mark === $fields['pass_mark']) {

            $pass = ['pass' => $s->pass_mark];
        } else {

            $pass = ['pass' => ($fields['pass_mark'] / 100)];
        }



        if (isset($fields['blocks'])) {
            foreach ($fields['blocks'] as $key => $value) {

                $graders[] = $value;
            }
        } else {

            $blocks = Block::where('blockable_id', '=', $fields['item_id'])->get();
            if ($blocks) {
                foreach ($blocks as $b) {
                    if (!empty($b->content)) {
                        $graders[] = $b->content;
                    }
                }
            }
        }

        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->grade_cutoffs = $pass;
        $enrollmentInfoObject->graders = $graders;
        $enrollmentInfoObject->minimum_grade_credit = 0.8;
        $enrollmentInfoObject->grace_period = null;
        $enrollmentInfoObject->user = json_encode($userinfo);


        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',
                    'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );

        try {
            $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/settings/grading/' . $fields['course_id'], [
                \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
            ]);

            $res = json_decode($response->getBody()->getContents(), true);


            if ($response->getStatusCode() == 200) {

                $sendback = ['data' => $res, 'status' => true];
                return $sendback;
            } else {

                $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                return  $sendback;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();

            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}



if (!function_exists('getCourseChapters')) {
    function getCourseChapters($courseId)
    {

        $configLms = config()->get("settings.lms.live");

        // Split the course ID to remove course-v1:
        $split = explode(':', $courseId);

        //create block Id by concatenating the second part of the course ID split
        $blockId = 'block-v1:' . $split['1'] . '+type@course+block@course';


        // use Client ti process a get method from the studio

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',

                ]
            ]
        );

        try {
            $response = $enrollClient->request('GET', $configLms['CMS_BASE'] . '/course/xblock/outline/' . $blockId);
            $res = json_decode($response->getBody()->getContents(), true);


            return $res;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}




if (!function_exists('createCourseChapters')) {
    function createCourseChapters($fields)
    {

        $configLms = config()->get("settings.lms.live");
        $userinfo = array();

        $user = EdxAuthUser::where('username', Auth::user()->username)->first();
        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        if ($fields['parent']) {

            $parent = $fields['parent'];
        } else {
            // Split the course ID to remove course-v1:
            $split = explode(':', $fields['course_id']);
            //create block Id by concatenating the second part of the course ID split
            $parent = 'block-v1:' . $split['1'] . '+type@course+block@course';
        }



        if ($fields['type'] === 'edit') {

            //dd($fields);

            $name = ['display_name' => $fields['section_name']];

            $enrollmentInfoObject = new \stdClass();
            $enrollmentInfoObject->metadata = $name;
            $enrollmentInfoObject->graderType = (isset($fields['grader'])) ? $fields['grader'] : null;
            $enrollmentInfoObject->prereqUsageKey = (isset($fields['prereq'])) ? $fields['prereq'] : null;
            $enrollmentInfoObject->isPrereq = (isset($fields['is_prereq'])) ? $fields['is_prereq'] : false;
            $enrollmentInfoObject->prereqMinScore = (isset($fields['prereq_min_score'])) ? $fields['prereq_min_score'] : '';
            $enrollmentInfoObject->prereqMinCompletion = (isset($fields['prereq_min_completion'])) ? $fields['prereq_min_completion'] : '';

            $enrollmentInfoObject->user = json_encode($userinfo);

            $enrollClient = new \GuzzleHttp\Client(
                [
                    'verify' => env('VERIFY_SSL', true),
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Origin' => $configLms['CMS_BASE'],
                        'Referer' => $configLms['CMS_BASE'],
                        'X-Requested-With' => ' XMLHttpRequest',
                        'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                        'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                    ]
                ]
            );

            try {
                $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/xblock/' . $parent, [
                    \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
                ]);
                $res = json_decode($response->getBody()->getContents(), true);

                //dd($res);

                if ($response->getStatusCode() == 200) {

                    $sendback = ['data' => $res, 'status' => true];
                    return $sendback;
                } else {

                    $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                    return  $sendback;
                }
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $responseJson = $e->getResponse();
                $response = json_decode($responseJson->getBody()->getContents(), true);
                //Error, delete user

                return $response;
            } catch (\Exception $e) {
                //Error, reactivate reset

            return false;
                // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
            }
        } elseif ($fields['type'] === 'delete') {

            $enrollClient = new \GuzzleHttp\Client(
                [
                    'verify' => env('VERIFY_SSL', true),
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Origin' => $configLms['CMS_BASE'],
                        'Referer' => $configLms['CMS_BASE'],
                        'X-Requested-With' => ' XMLHttpRequest',
                        'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                        'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                    ]
                ]
            );

            try {
                $response = $enrollClient->request('DELETE', $configLms['CMS_BASE'] . '/course/xblock/' . $parent);

                $res = json_decode($response->getBody()->getContents(), true);


                if ($response->getStatusCode() == 200) {

                    $sendback = ['data' => $res, 'status' => true];
                    return $sendback;
                } else {

                    $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                    return  $sendback;
                }
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $responseJson = $e->getResponse();
                $response = json_decode($responseJson->getBody()->getContents(), true);
                //Error, delete user

                return $response;
            } catch (\Exception $e) {
                //Error, reactivate reset

            return false;
                // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
            }
        } else {
            $enrollmentInfoObject = new \stdClass();
            $enrollmentInfoObject->category = $fields['category'];
            $enrollmentInfoObject->display_name = $fields['section_name'];
            $enrollmentInfoObject->parent_locator = $parent;
            $enrollmentInfoObject->user = json_encode($userinfo);


            $enrollClient = new \GuzzleHttp\Client(
                [
                    'verify' => env('VERIFY_SSL', true),
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Origin' => $configLms['CMS_BASE'],
                        'Referer' => $configLms['CMS_BASE'],
                        'X-Requested-With' => ' XMLHttpRequest',
                        'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                        'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                    ]
                ]
            );


            try {
                $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/xblock/', [
                    \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
                ]);
                $res = json_decode($response->getBody()->getContents(), true);

                if ($response->getStatusCode() == 200) {

                    $sendback = ['data' => $res, 'status' => true];
                    return $sendback;
                } else {

                    $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                    return  $sendback;
                }
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $responseJson = $e->getResponse();
                $response = json_decode($responseJson->getBody()->getContents(), true);
                //Error, delete user

                return $response;
            } catch (\Exception $e) {
                //Error, reactivate reset

            return false;
                // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
            }
        }
    }
}


if (!function_exists('getCourseGrader')) {
    function getCourseGrader($courseId)
    {

        $configLms = config()->get("settings.lms.live");

        $userinfo = array();

        $user = EdxAuthUser::where('username', Auth::user()->username)->first();
        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        // use Client ti process a get method from the studio

        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->user = json_encode($userinfo);

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',

                ]
            ]
        );

        try {
            $response = $enrollClient->request('GET', $configLms['CMS_BASE'] . '/course/settings/grading/' . $courseId, [
                \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
            ]);



            $res = json_decode($response->getBody()->getContents(), true);


            return $res;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}


if (!function_exists('getUnitOutline')) {
    function getUnitOutline($Id)
    {

        $configLms = config()->get("settings.lms.live");
        // use Client ti process a get method from the studio

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',

                ]
            ]
        );

        try {
            $response = $enrollClient->request('GET', $configLms['CMS_BASE'] . '/course/xblock/' . $Id);
            $res = json_decode($response->getBody()->getContents(), true);


            return $res;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}


if (!function_exists('getSingleComponents')) {
    function getSingleComponents($Id)
    {


        $configLms = config()->get("settings.lms.live");


        // use Client ti process a get method from the studio

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',

                ]
            ]
        );

        try {
            $response = $enrollClient->request('GET', $configLms['CMS_BASE'] . '/course/xblock/' . $Id);

            $res = json_decode($response->getBody()->getContents(), true);

            return $res;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}



if (!function_exists('courseUnitComponents')) {
    function courseUnitComponents($fields)
    {

        $configLms = config()->get("settings.lms.live");
        $userinfo = array();
        $user = EdxAuthUser::where('username', Auth::user()->username)->first();

        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];


        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->category = $fields['type_id'];
        $enrollmentInfoObject->parent_locator = $fields['block_id'];
        $enrollmentInfoObject->user = json_encode($userinfo);

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => ' application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',
                ]
            ]
        );



        try {
            $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/xblock/', [
                \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
            ]);


            $res = json_decode($response->getBody()->getContents(), true);


            if ($response->getStatusCode() == 200) {

                $sendback = ['data' => $res, 'status' => true];
                return $sendback;
            } else {

                $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                return  $sendback;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}







if (!function_exists('createUnitBlocks')) {
    function createUnitBlocks($fields)
    {

        $configLms = config()->get("settings.lms.live");
        $userinfo = array();

        $user = EdxAuthUser::where('username', Auth::user()->username)->first();
        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        if ($fields['parent']) {

            $parent = $fields['parent'];
        } else {
            // Split the course ID to remove course-v1:
            $split = explode(':', $fields['course_id']);
            //create block Id by concatenating the second part of the course ID split
            $parent = 'block-v1:' . $split['1'] . '+type@course+block@course';
        }



        if ($fields['type'] === 'edit') {
            if ($fields['category'] === 'html') {

                $meta = ['graded' => false];
                $enrollmentInfoObject = new \stdClass();
                $enrollmentInfoObject->data = $fields['componentData'];
                $enrollmentInfoObject->metadata = $meta;
                $enrollmentInfoObject->graded = false;
                $enrollmentInfoObject->user = json_encode($userinfo);

                $enrollClient = new \GuzzleHttp\Client(
                    [
                        'verify' => env('VERIFY_SSL', true),
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Origin' => $configLms['CMS_BASE'],
                            'Referer' => $configLms['CMS_BASE'],
                            'X-Requested-With' => ' XMLHttpRequest',
                            'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                            'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                        ]
                    ]
                );

                try {
                    $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/xblock/' . $parent, [
                        \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
                    ]);


                    $res = json_decode($response->getBody()->getContents(), true);


                    if ($response->getStatusCode() == 200) {

                        $sendback = ['data' => $res, 'status' => true];
                        return $sendback;
                    } else {

                        $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                        return  $sendback;
                    }
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    $responseJson = $e->getResponse();
                    $response = json_decode($responseJson->getBody()->getContents(), true);
                    //Error, delete user
                    // dd($responseJson);
                    return $response;
                } catch (\Exception $e) {
                    //Error, reactivate reset

                return false;
                    // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
                }
            } else if ($fields['category'] === 'library_content') {

                $meta = ['max_count' => $fields['count'], 'source_library_id' => $fields['blockOptions'], 'display_name' => $fields['display_name']];

                $enrollmentInfoObject = new \stdClass();
                $enrollmentInfoObject->metadata = $meta;
                $enrollmentInfoObject->user = json_encode($userinfo);

                $enrollClient = new \GuzzleHttp\Client(
                    [
                        'verify' => env('VERIFY_SSL', true),
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Origin' => $configLms['CMS_BASE'],
                            'Referer' => $configLms['CMS_BASE'],
                            'X-Requested-With' => ' XMLHttpRequest',
                            'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                            'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                        ]
                    ]
                );

                try {
                    $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/xblock/unit/' . $parent, [
                        \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
                    ]);


                    $res = json_decode($response->getBody()->getContents(), true);




                    if ($response->getStatusCode() == 200) {

                        $sendback = ['data' => $res, 'status' => true];
                        return $sendback;
                    } else {

                        $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                        return  $sendback;
                    }
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    $responseJson = $e->getResponse();
                    $response = json_decode($responseJson->getBody()->getContents(), true);
                    //Error, delete user
                     return false;

                } catch (\Exception $e) {
                    //Error, reactivate reset

                    return false;
                    // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
                }
            } else if ($fields['category'] === 'scormxblock') {

                if (isset($fields['scorm_file']) && !empty($fields['scorm_file'])) {


                    $path = '/edx/var/edxapp/media/scorm/';

                    $targetdir = $path . generateRandomString();

                    if (!is_dir($targetdir)) {

                        mkdir($targetdir, 0777);
                    }

                    $zip = new ZipArchive();
                    if ($zip->open($fields['scorm_file']) === TRUE) {

                        $zip->extractTo($targetdir);
                        $zip->close();
                    }
                }

                $scorm_split = explode('edxapp', $targetdir);

                $scorm_file = $scorm_split[1] . '/scormdriver/indexAPI.html';
                $path_index_page = '/scormdriver/indexAPI.html';

                $meta = ['has_score' => 'false', 'height' => $fields['height'], 'display_name' => $fields['display_name'], 'scorm_file' => $scorm_file, 'path_index_page' => $path_index_page];

                $enrollmentInfoObject = new \stdClass();
                $enrollmentInfoObject->metadata = $meta;
                $enrollmentInfoObject->user = json_encode($userinfo);

                $enrollClient = new \GuzzleHttp\Client(
                    [
                        'verify' => env('VERIFY_SSL', true),
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Origin' => $configLms['CMS_BASE'],
                            'Referer' => $configLms['CMS_BASE'],
                            'X-Requested-With' => ' XMLHttpRequest',
                            'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                            'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                        ]
                    ]
                );

                try {
                    $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/xblock/' . $parent, [
                        \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
                    ]);


                    $res = json_decode($response->getBody()->getContents(), true);




                    if ($response->getStatusCode() == 200) {

                        $sendback = ['data' => $res, 'status' => true];
                        return $sendback;
                    } else {

                        $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                        return  $sendback;
                    }
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    $responseJson = $e->getResponse();
                    $response = json_decode($responseJson->getBody()->getContents(), true);
                    //Error, delete user
                    // dd($responseJson);
                    return false;
                } catch (\Exception $e) {
                    //Error, reactivate reset

                    return false;
                    // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
                }
            }
        } elseif ($fields['type'] === 'delete') {

            $enrollClient = new \GuzzleHttp\Client(
                [
                    'verify' => env('VERIFY_SSL', true),
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Origin' => $configLms['CMS_BASE'],
                        'Referer' => $configLms['CMS_BASE'],
                        'X-Requested-With' => ' XMLHttpRequest',
                        'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                        'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                    ]
                ]
            );

            try {
                $response = $enrollClient->request('DELETE', $configLms['CMS_BASE'] . '/course/xblock/unit/' . $parent);

                $res = json_decode($response->getBody()->getContents(), true);


                if ($response->getStatusCode() == 200) {

                    $sendback = ['data' => $res, 'status' => true];
                    return $sendback;
                } else {

                    $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                    return  $sendback;
                }
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $responseJson = $e->getResponse();
                $response = json_decode($responseJson->getBody()->getContents(), true);
                //Error, delete user

                return $response;
            } catch (\Exception $e) {
                //Error, reactivate reset

            return false;
                // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
            }
        }
    }
}




if (!function_exists('publishUnit')) {
    function publishUnit($id)
    {

        $configLms = config()->get("settings.lms.live");
        $userinfo = array();

        $user = EdxAuthUser::where('username', Auth::user()->username)->first();
        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->publish = 'make_public';
        $enrollmentInfoObject->user = json_encode($userinfo);


        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',
                    'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );

        try {
            $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/xblock/' . $id, [
                \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
            ]);
            $res = json_decode($response->getBody()->getContents(), true);

            if ($response->getStatusCode() == 200 && $res) {

                return true;
            } else {

                return false;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}

if (!function_exists('getLibraries')) {
    function getLibraries()
    {


        $configLms = config()->get("settings.lms.live");

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',

                ]
            ]
        );

        try {
            $response = $enrollClient->request('GET', $configLms['CMS_BASE'] . '/course/library');

            $res = json_decode($response->getBody()->getContents(), true);
            return $res;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}


if (!function_exists('uploadAssets')) {
    function uploadAssets($image, $parent)
    {
        $configLms = config()->get("settings.lms.live");

        $user = EdxAuthUser::where('username', Auth::user()->username)->first();
        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->file = $image;
        $enrollmentInfoObject->user = json_encode($userinfo);

        $name = $image[0]['name'];
        $path = $image[0]['original'];

        $fileinfo = array(

        );

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',
                    'Cookie' => ' csrftoken=' . $_COOKIE['csrftoken'],
                    'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
                ]
            ]
        );

        try {
            $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/assets/' .$parent.'/', [
                'multipart' => [
                   [
                        'Content-type' => 'multipart/form-data',
                        'name' => 'file',
                        'contents' => file_get_contents($path),
                        'filename' =>  $name
                   ]
                ]
            ]);
            $res = json_decode($response->getBody()->getContents(), true);

             if($res['msg'] === "Upload completed"){

                return true;

             }else{

                return $res['msg'];

             }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

        return false;
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}

function generateRandomString($length = 32)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}
