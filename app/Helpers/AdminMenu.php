<?php
if (!function_exists('adminMenu')) {
	function adminMenu()
	{
		return [


			'content' => [
				'title' => 'Content',
				'route' => 'admin.content.pages.index',
				'primary_navigation' => [
					'pages' => [
						'title' => 'Pages',
						'module' => true,

					],
					'menuses' => [
						'title' => 'Menus',
						'module' => true,
						'secondary_navigation' => [
							'menuses' => [
								'title' => 'Menus',
								'module' => true
							],
							'menuTypes' => [
								'title' => 'Menu Types',
								'module' => true,

							],
						]

					],
					'footerTexts' => [
						'title' => 'FooterTexts',
						'module' => true
					],

					//  'socialmedia' => [
					// 	'title' => 'Social Media Links',
					// 	 'module' => true,

					// ],
					// 'projecttopics' => [
					// 	'title' => 'Project Topics',
					// 	 'module' => true,

					// ],
				],
			],



			// 'resources' => [
			// 	'title' => 'Resources',
			// 	'route' => 'admin.resources.resources.index',
			// 	'primary_navigation' => [

			// 		'resources' => [
			// 			'title' => 'Resources',
			// 			'module' => true,

			// 		],
			// 		'resourceTypes' => [
			// 			'title' => 'Resource Types',
			// 			'module' => true,

			// 		],

			// 	],
			// ],



			'courses' => [
				'title' => 'Courses',
				'route' => 'admin.courses.index',

			],
            'courseCreations' => [
                'title' => 'Course Creation',
                'module' => true
            ],
			'userCourses' => [
				'title' => 'HR Recommended',
				'module' => true,
			],
			'sessions' => [
				'title' => 'Live Sessions',
				'module' => true,

			],

			'reports' => [
				'title' => 'Reports',
				'route' => 'admin.graph.dashboard',
				'primary_navigation' => [
					'graph' => [
						'title' => 'Dashboard',
						'route' => 'admin.graph.dashboard',
					],
					'courseCompletions' => [
						'title' => 'Course Completions',
						'module' => true,

					],

					'jobroleCompletions' => [
						'title' => 'Job Role Completions',
						'route' => 'admin.jobrole.completion',

					],

					'branch' => [
						'title' => 'Branch Completion',
						'route' => 'admin.branch.completion',
					],
					'userLicenses' => [
						'title' => 'Course Enrollments',
						'module' => true
					],
					'courseReflections' => [
						'title' => 'Course Reflection',
						'module' => true,

					],
				]
			],

			'jobRoles' => [
				'title' => 'Job Roles',
				'module' => true
			],
			'branches' => [
				'title' => 'Branches',
				'module' => true
			],
			'sectors' => [
				'title' => 'Sectors',
				'module' => true,
			],
			'countries' => [
				'title' => 'Countries',
				'module' => true
			],

			'evaluation' => [
				'title' => 'Evaluation',
				'route' => 'admin.evaluation.userEvaluations.index',
				'primary_navigation' => [
					'userEvaluations' => [
						'title' => 'User Evaluations',
						'module' => true,
					],

					'evaluations' => [
						'title' => 'Evaluation Questions',
						'module' => true
					],
					'evaluationOptions' => [
						'title' => 'Evaluation Options',
						'module' => true
					],

				]
			],
		    'messages' => [
				'title' => 'Messages',
				'route' => 'admin.messages.platformMessages.index',
				'primary_navigation' => [

					'platformMessages' => [
						'title' => 'Platform Messages',
						'module' => true
					],
					'messageTypes' => [
						'title' => 'MessageTypes',
						'module' => true,
					],
				]
			],
			'groups' => [
				'title' => 'Groups',
				'route' => 'admin.groups.groups.index',
				'primary_navigation' => [
					'groups' => [
						'title' => 'Groups',
						'module' => true,

					],
					'groupFormats' => [
						'title' => 'Group Formats',
						'module' => true,

					]
				]
			],

		];
	}
}


if (!function_exists('adminMenus')) {
    function adminMenus()
    {
        return [


            'content' => [
                'title' => 'Content',
                'route' => 'admin.content.pages.index',
                'primary_navigation' => [
                    'pages' => [
                        'title' => 'Pages',
                        'module' => true,

                    ],
                    'menuses' => [
                        'title' => 'Menus',
                        'module' => true,
                        'secondary_navigation' => [
                            'menuses' => [
                                'title' => 'Menus',
                                'module' => true
                            ],
                            'menuTypes' => [
                                'title' => 'Menu Types',
                                'module' => true,

                            ],
                        ]

                    ],
                    'footerTexts' => [
                        'title' => 'FooterTexts',
                        'module' => true
                    ],

                    //  'socialmedia' => [
                    // 	'title' => 'Social Media Links',
                    // 	 'module' => true,

                    // ],
                    // 'projecttopics' => [
                    // 	'title' => 'Project Topics',
                    // 	 'module' => true,

                    // ],
                ],
            ],



            // 'resources' => [
            // 	'title' => 'Resources',
            // 	'route' => 'admin.resources.resources.index',
            // 	'primary_navigation' => [

            // 		'resources' => [
            // 			'title' => 'Resources',
            // 			'module' => true,

            // 		],
            // 		'resourceTypes' => [
            // 			'title' => 'Resource Types',
            // 			'module' => true,

            // 		],

            // 	],
            // ],



            'courses' => [
                'title' => 'Courses',
                'route' => 'admin.courses.index',

            ],
            'courseCreations' => [
                'title' => 'Course Creation',
                'module' => true
            ],
            'userCourses' => [
                'title' => 'HR Recommended',
                'module' => true,
            ],
            'sessions' => [
                'title' => 'Live Sessions',
                'module' => true,

            ],

            'reports' => [
                'title' => 'Reports',
                'route' => 'admin.graph.dashboard',
                'primary_navigation' => [
                    'graph' => [
                        'title' => 'Dashboard',
                        'route' => 'admin.graph.dashboard',
                    ],
                    'courseCompletions' => [
                        'title' => 'Course Completions',
                        'module' => true,

                    ],

                    'jobroleCompletions' => [
                        'title' => 'Job Role Completions',
                        'route' => 'admin.jobrole.completion',

                    ],

                    'branch' => [
                        'title' => 'Branch Completion',
                        'route' => 'admin.branch.completion',
                    ],
                    'userLicenses' => [
                        'title' => 'Course Enrollments',
                        'module' => true
                    ],
                    'courseReflections' => [
                        'title' => 'Course Reflection',
                        'module' => true,

                    ],
                ]
            ],

            'jobRoles' => [
                'title' => 'Job Roles',
                'module' => true
            ],
            'branches' => [
                'title' => 'Branches',
                'module' => true
            ],
            'sectors' => [
                'title' => 'Sectors',
                'module' => true,
            ],
            'countries' => [
                'title' => 'Countries',
                'module' => true
            ],

            'evaluation' => [
                'title' => 'Evaluation',
                'route' => 'admin.evaluation.userEvaluations.index',
                'primary_navigation' => [
                    'userEvaluations' => [
                        'title' => 'User Evaluations',
                        'module' => true,
                    ],

                    'evaluations' => [
                        'title' => 'Evaluation Questions',
                        'module' => true
                    ],
                    'evaluationOptions' => [
                        'title' => 'Evaluation Options',
                        'module' => true
                    ],

                ]
            ],
            'messages' => [
                'title' => 'Messages',
                'route' => 'admin.messages.platformMessages.index',
                'primary_navigation' => [

                    'platformMessages' => [
                        'title' => 'Platform Messages',
                        'module' => true
                    ],
                    'messageTypes' => [
                        'title' => 'MessageTypes',
                        'module' => true,
                    ],
                ]
            ],
            'groups' => [
                'title' => 'Groups',
                'route' => 'admin.groups.groups.index',
                'primary_navigation' => [
                    'groups' => [
                        'title' => 'Groups',
                        'module' => true,

                    ],
                    'groupFormats' => [
                        'title' => 'Group Formats',
                        'module' => true,

                    ]
                ]
            ],

        ];
    }
}

if (!function_exists('hrMenu')) {
	function hrMenu()
	{
		return [
			'courses' => [
				'title' => 'Courses',
				'route' => 'admin.courses.index',

			],
			'jobRoles' => [
				'title' => 'JobRoles',
				'module' => true
			],


			'reports' => [
				'title' => 'Reports',
				'route' => 'admin.reports.courseCompletions.index',
				'primary_navigation' => [
					'courseCompletions' => [
						'title' => 'Course Completions',
						'module' => true,

					],
					'jobroleCompletions' => [
						'title' => 'Job Role Completions',
						'route' => 'admin.jobrole.completion',

					],

					'userLicenses' => [
						'title' => 'UserLicenses',
						'module' => true
					]
				]
			],
			'messages' => [
				'title' => 'Messages',
				'route' => 'admin.messages.platformMessages.index',
				'primary_navigation' => [

					'platformMessages' => [
						'title' => 'Platform Messages',
						'module' => true
					],

				]
			],

		];
	}
}
