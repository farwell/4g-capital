<?php

use App\Edx\EdxAuthUser;
use A17\Twill\Models\Block;
use App\Models\CourseStructure;


if (!function_exists('courseLibraryCreate')) {
function courseLibraryCreate($fields)
{

        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");
        $userinfo = array();
        $user = EdxAuthUser::where('username', Auth::user()->username)->first();

        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];


        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->display_name = $fields['title'];
        $enrollmentInfoObject->org = $fields['org'];
        $enrollmentInfoObject->number = $fields['number'];
        $enrollmentInfoObject->user = json_encode($userinfo);

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => ' application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',
                ]
            ]
        );



        try {
            $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/library/', [
                \GuzzleHttp\RequestOptions::JSON => $enrollmentInfoObject
            ]);
            
          
            $res = json_decode($response->getBody()->getContents(), true);

        
            if (isset($res['library_key'])) {
                $sendback = ['data' => $res, 'status' => true];
                return $sendback;
            } elseif (isset($res['ErrMsg'])) {
                $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                return  $sendback;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

            return $e->getMessage();
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }


}
}



if (!function_exists('getLibraryComponents')) {
    function getLibraryComponents($courseId)
    {

      
        $configLms = config()->get("settings.lms.live");

        // Split the course ID to remove course-v1:
        $split = explode(':', $courseId);

        //create block Id by concatenating the second part of the course ID split
        $blockId = 'lib-block-v1:' . $split['1'] . '+type@library+block@library';


        // use Client ti process a get method from the studio

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',

                ]
            ]
        );

        try {
            $response = $enrollClient->request('GET', $configLms['CMS_BASE'] . '/course/library/' . $courseId);
            
            $res = json_decode($response->getBody()->getContents(), true);
            return $res;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

            return $e->getMessage();
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}



if (!function_exists('getSingleLibraryComponents')) {
    function getSingleLibraryComponents($Id)
    {


        $configLms = config()->get("settings.lms.live");


        // use Client ti process a get method from the studio

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',

                ]
            ]
        );

        try {
            $response = $enrollClient->request('GET', $configLms['CMS_BASE'] . '/course/xblock/' . $Id);

            $res = json_decode($response->getBody()->getContents(), true);

            return $res;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

            return $e->getMessage();
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }
    }
}








if (!function_exists('courseLibraryComponents')) {
function courseLibraryComponents($fields)
{

        $configLms = config()->get("settings.lms.live");
        $userinfo = array();
        $user = EdxAuthUser::where('username', Auth::user()->username)->first();

        $userinfo = ['id' => $user->id, 'username' => $user->username, 'is_staff' => $user->is_staff, 'is_superuser' => $user->is_superuser, 'email' => $user->email, 'is_active' => $user->is_active];

        // if ($fields['parent']) {
        //     $parent = $fields['parent'];

        // } else {
        //     // Split the course ID to remove course-v1:
        //     $split = explode(':', $fields['library_key']);
        //     //create block Id by concatenating the second part of the course ID split
        //     $parent = 'lib-block-v1:' . $split['1'] . '+type@library+block@library';
        // }

        // Split the course ID to remove course-v1:
        $split = explode(':', $fields['library_key']);
        //create block Id by concatenating the second part of the course ID split
        $parent = 'lib-block-v1:' . $split['1'] . '+type@library+block@library';
  

        if($fields['type_id'] === 'checkbox_hints'){
            $name = "checkboxes_response_hint.yaml";
        }elseif($fields['type_id'] === 'blank_advanced'){
            $name = "";
        }elseif($fields['type_id'] === 'multichoice_hints'){

            $name = "multiplechoice_hint.yaml";

        }


        $enrollmentInfoObject = new \stdClass();
        $enrollmentInfoObject->boilerplate = $name;
        $enrollmentInfoObject->category = "problem";
        $enrollmentInfoObject->parent_locator = $parent;
        $enrollmentInfoObject->user = json_encode($userinfo);

        $enrollClient = new \GuzzleHttp\Client(
            [
                'verify' => env('VERIFY_SSL', true),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => ' application/json',
                    'Origin' => $configLms['CMS_BASE'],
                    'Referer' => $configLms['CMS_BASE'],
                    'X-Requested-With' => ' XMLHttpRequest',
                ]
            ]
        );



        try {
            $response = $enrollClient->request('POST', $configLms['CMS_BASE'] . '/course/xblock/', [
                \GuzzleHttp\RequestOptions::JSON =>  $enrollmentInfoObject
            ]);


            $res = json_decode($response->getBody()->getContents(), true);

            if ($response->getStatusCode() == 200) {

                $sendback = ['data' => $res, 'status' => true];
                return $sendback;
            } else {

                $sendback = ['data' => $res['ErrMsg'], 'status' => false];
                return  $sendback;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user

            return $response;
        } catch (\Exception $e) {
            //Error, reactivate reset

            return $e->getMessage();
            // return redirect()->back()->withErrors("There was a problem resetting your password. Please try again later or report to support.");
        }


 

}}