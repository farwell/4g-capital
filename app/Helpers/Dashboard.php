<?php
if (!function_exists('adminDashboard')) {
    function adminDashboard() {

		return  [
			'modules' => [
				// 'jobRoles' => [
				// 	'name' => 'jobRoles',
				// 	'label' => 'Job Roles',
				// 	'label_singular' => 'Job Role',
				// 	'activity' => true,
				// 	'search' => true,
				// 	'draft' => true,
				// 	'count' => true,
				// 	'create' => false,
				// ],
				'users' => [
					'name' => 'users',
					'label' => 'Userlist',
					'label_singular' => 'Userlist',
					'search' => true,
					'count' => true,
					'create' => false,
				],

			],
			
		];
    }
}

if (!function_exists('hrAdminDashboard')) {
    function hrAdminDashboard() {
		return  [
			'modules' => [
				'jobRoles' => [
					'name' => 'jobRoles',
					'label' => 'Job Roles',
					'label_singular' => 'Job Role',
					'activity' => true,
					'search' => true,
					'draft' => true,
					'count' => true,
					'create' => false,
				],
				
				
			],
			
		];
    }
}
