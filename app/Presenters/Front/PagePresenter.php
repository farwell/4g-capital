<?php

namespace App\Presenters\Front;

class PagePresenter extends BasePresenter
{
    public function image()
    {
        return $this->imageMedia('cover');
    }

    public function imageSeo()
    {
        return $this->image();
	}

	public function imageHero()
    {
        return $this->imageMedia('cover', 'hero');
	}

	public function url()
    {
        return "#";
	}

	public function text()
    {
		return $this->entity->body;
	}
}