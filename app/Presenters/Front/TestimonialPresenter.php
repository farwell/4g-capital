<?php

namespace App\Presenters\Front;

class TestimonialPresenter extends BasePresenter
{
	public function source()
    {
        return $this->entity->firstname . ' '. $this->entity->lastname ;
	}

	public function description()
    {
        return $this->entity->description;
	}


}
