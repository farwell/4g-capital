<?php

namespace App\Presenters\Front;

class UserEvaluationPresenter extends BasePresenter
{

    public function answer1()
    {
        return $this->entity->answer1;
	}

    
    public function question1()
    {
        return $this->entity->question1;
	}

}