<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\ModuleRepository;
use A17\Twill\Repositories\Behaviors\HandleBlocks;
use App\Models\Company;

class CompanyRepository extends ModuleRepository
{
    use HandleTranslations, HandleSlugs, HandleMedias, HandleBlocks;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function afterSave($object, $fields)
    {
      $this->updateRepeater($object, $fields, 'domain', 'Domain');
      parent::afterSave($object, $fields);
    }

    public function getFormFields($object){
        $fields = parent::getFormFields($object);
        $fields = $this->getFormFieldsForRepeater($object, $fields, 'domain', 'Domain');
        return $fields;
    }

    public function prepareFieldsBeforeCreate($fields){
        $fields['layout'] ='regular';
        return parent::prepareFieldsBeforeCreate($fields);
    }
}
