<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Jobs\SendMessagesEmail;
use App\Models\PlatformMessage;
use Illuminate\Support\Arr;
use App\Models\Notification;
use App\Models\User;
use App\Notifications\IndividualNotification;
use Illuminate\Support\Facades\DB;
use Auth;

// use DB;

class PlatformMessageRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions;

    public function __construct(PlatformMessage $model)
    {
        $this->model = $model;
    }




    public function update($id, $fields)
    {

     
        
        DB::transaction(function () use ($id, $fields) {

            $object = $this->model->findOrFail($id);

            $this->beforeSave($object, $fields);

            $fields = $this->prepareFieldsBeforeSave($object, $fields);

            $object->fill(Arr::except($fields, $this->getReservedFields()));

            $object->save();



               $individualMessage =[
                'type'=> Notification::MESSAGE,
                'subject'=> $object->subject,
                'model_id' => $object->id,
                'message' =>strip_tags($object->message) ,
               ];

               $sender = Auth::user()->name;
               $subject =$object->subject;
               $msg= strip_tags($object->message);

               if($fields['message_type_id'] == 3 && is_array($fields['target_id']) )
               {
                   $targets = DB::table('targets')->whereIn('id', $fields['target_id'])->pluck('title');
               
                   $users = User::select('id', 'name', 'email')
                       ->whereIn('role', $targets)
                       //->where('company_id', $fields['company_id'])
                       ->get();

                       try {

                        SendMessagesEmail::dispatch($sender, $users, $subject,$msg);
                      
                      } catch (\Exception $e) {
                      
                          return $e->getMessage();
                      }
               
                  
               }


               if($fields['message_type_id'] == 2 && is_array($fields['job_role_id']) )
               {
              

                if (in_array(0, $fields['job_role_id'])) {
                    $users = User::all();
                   
                }
                   $users = User::select('id', 'name', 'email')
                       ->whereIn('job_role_id', $fields['job_role_id'])
                       ->get();


                       try {

                        SendMessagesEmail::dispatch($sender, $users, $subject,$msg);
                      
                      } catch (\Exception $e) {
                      
                          return $e->getMessage();
                      }
                   
               }
               
        

           if( $fields['message_type_id'] == 1 && isset($fields['email'])){

        
        
            $users = User::whereIn('id', $fields['email'])->get();
           
            
         
            try {

                SendMessagesEmail::dispatch($sender, $users, $subject,$msg);
              
              } catch (\Exception $e) {
              
                  return $e->getMessage();
              }


           }


            $this->afterSave($object, $fields);
        }, 3);
}
    


    public function updateBasic($id, $values, $scopes = [])
    {

       
        return DB::transaction(function () use ($id, $values, $scopes) {
            // apply scopes if no id provided
            if (is_null($id)) {
                $query = $this->model->query();

                foreach ($scopes as $column => $value) {
                    $query->where($column, $value);
                }
            
                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });
                
                
                return true;
            }

            // apply to all ids if array of ids provided
            if (is_array($id)) {
                $query = $this->model->whereIn('id', $id);
                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });

                return true;
            }

            if (($object = $this->model->find($id)) != null) {
                
                $object->update($values);
                $this->afterUpdateBasic($object, $values);
                 
             $update = Menus::where('key','=', $object->key)->first();

            // dd($object->published);

             if($object->published){

                $update->published = true;
                $update->active = 1;
                $update->save();
             
             }else{
                $update->published = false;
                $update->active = 0;
                $update->save();
             
             }
                return true;
            }

            return false;
        }, 3);
    }
}
