<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Sector;
use App\Models\SectorBranch;
use App\Models\SectorCourse;
use Illuminate\Support\Facades\DB;
use IlluminateAgnostic\Arr\Support\Arr;

class SectorRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleFiles, HandleRevisions;

    public function __construct(Sector $model)
    {
        $this->model = $model;
    }

    public function update($id, $fields)
{
    DB::transaction(function () use ($id, $fields) {
        $object = $this->model->findOrFail($id);

        $this->beforeSave($object, $fields);

        $fields = $this->prepareFieldsBeforeSave($object, $fields);

        $object->fill(Arr::except($fields, $this->getReservedFields()));

        $object->save();

        if (isset($fields['branch_id']) && !empty($fields['branch_id'])) {
            $object->branches()->detach();
            foreach ($fields['branch_id'] as $branchId) {
                $sectorBranch = new SectorBranch();
                $sectorBranch->sector_id = $object->id;
                $sectorBranch->branch_id = $branchId;
                $sectorBranch->save();
            }
        }

        if (isset($fields['courses']) && !empty($fields['courses'])) {
            $object->courses()->detach();
            foreach ($fields['courses'] as $courseId) {
                $sectorCourse = new SectorCourse();
                $sectorCourse->sector_id = $object->id;
                $sectorCourse->course_id = $courseId;
                $sectorCourse->save();
            }
        }

        $this->afterSave($object, $fields);
    }, 3);
}

    
}
