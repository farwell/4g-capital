<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\UserLicense;
use DB;
use PDO;
use Carbon\Carbon;

class UserLicenseRepository extends ModuleRepository
{
    

    public function __construct(UserLicense $model)
    {
        $this->model = $model;
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $scopes
     * @return \Illuminate\Database\Query\Builder
     */
    public function filter($query, array $scopes = [])
    {
        
       
        $likeOperator = $this->getLikeOperator();

        foreach ($this->traitsMethods(__FUNCTION__) as $method) {
            $this->$method($query, $scopes);
        }

        unset($scopes['search']);

        if (isset($scopes['exceptIds'])) {
            $query->whereNotIn($this->model->getTable() . '.id', $scopes['exceptIds']);
            unset($scopes['exceptIds']);
        }
         //dd($scopes);
        foreach ($scopes as $column => $value) {

         if($column === 'enrollment_date'){
            
            $range = explode(' - ', $value);

            $query->whereBetween('created_at', [Carbon::parse($range[0]),Carbon::parse($range[1])]);

                //$query->where('created_at', 'LIKE', '%' . $value . '%');

             }else{
                if (method_exists($this->model, 'scope' . ucfirst($column))) {
               
                    $query->$column();
            } else {
    
                   
                    if (is_array($value)) {
                        $query->whereIn($column, $value);
                    } elseif ($column[0] == '%') {
                        $value && ($value[0] == '!') ? $query->where(substr($column, 1), "not $likeOperator", '%' . substr($value, 1) . '%') : $query->where(substr($column, 1), $likeOperator, '%' . $value . '%');
                    } elseif (isset($value[0]) && $value[0] == '!') {
                        $query->where($column, '<>', substr($value, 1));
                    } elseif ($value !== '') {
                        $query->where($column, $value);
                    }
                }


             }

         
        }

        return $query;
    }
  

    protected function getLikeOperator()
    {
        if (DB::connection()->getPDO()->getAttribute(PDO::ATTR_DRIVER_NAME) === 'pgsql') {
            return 'ILIKE';
        }

        return 'LIKE';
    }



}
