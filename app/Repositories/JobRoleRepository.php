<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use Illuminate\Support\Arr;
use App\Models\JobRole;
use App\Models\JobroleCourse;
use App\Models\Course;
use DB;

class JobRoleRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions;

    public function __construct(JobRole $model)
    {
        $this->model = $model;
    }
   
    public function update($id, $fields)
    {
        
        DB::transaction(function () use ($id, $fields) {
            
            $object = $this->model->findOrFail($id);

            $this->beforeSave($object, $fields);

            $fields = $this->prepareFieldsBeforeSave($object, $fields);

            $object->fill(Arr::except($fields, $this->getReservedFields()));

            $object->save();

            if($fields['courses']){
            
            // delete records with job role 
            JobroleCourse::where('job_role_id', '=', $object->id)->delete();

            // Loop through the courses array 

            foreach($fields['courses'] as $course){

             // Save records to raltionship table

             $job = new JobroleCourse();
             $job->job_role_id = $object->id;
             $job->course_id = $course;

             $job->save();

            }
            }

            $this->afterSave($object, $fields);
        }, 3);
    }




}
