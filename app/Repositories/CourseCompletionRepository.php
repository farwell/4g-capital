<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\CourseCompletion;
use App\Models\User;
use App\Models\JobroleCourse;
use App\Models\Course;
use App\Models\CourseCreation;
use App\Models\UserLicense;
use DB;
use PDO;
use Carbon\Carbon;

class CourseCompletionRepository extends ModuleRepository
{


    public function __construct(CourseCompletion $model)
    {
        $this->model = $model;
    }


    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $scopes
     * @return \Illuminate\Database\Query\Builder
     */
    public function filter($query, array $scopes = [])
    {


        $likeOperator = $this->getLikeOperator();

        foreach ($this->traitsMethods(__FUNCTION__) as $method) {
            $this->$method($query, $scopes);
        }

        unset($scopes['search']);

        if (isset($scopes['exceptIds'])) {
            $query->whereNotIn($this->model->getTable() . '.id', $scopes['exceptIds']);
            unset($scopes['exceptIds']);
        }
         //dd($scopes);
        foreach ($scopes as $column => $value) {

          if($column === 'status_type'){

             if($value === '1'){

                $query->where('score', '>=', 0.8);

             }elseif($value === '2'){

                $query->whereBetween('score', [0.2, 0.79]);

             }elseif($value === '3'){

                $query->where('score', '<', 0.2);
             }

             }elseif($column === 'completion_date'){

                $range = explode(' - ', $value);

                $query->whereBetween('completion_date', [Carbon::parse($range[0]),Carbon::parse($range[1])]);

                 //$query->where('enrollment_date', 'LIKE', '%' . $value . '%');

             }elseif($column === 'enrollment_date'){

                $range = explode(' - ', $value);

                $query->whereBetween('enrollment_date', [Carbon::parse($range[0]),Carbon::parse($range[1])]);

                 //$query->where('enrollment_date', 'LIKE', '%' . $value . '%');

             }else{
                if (method_exists($this->model, 'scope' . ucfirst($column))) {

                    $query->$column();
                } else {


                    if (is_array($value)) {
                        $query->whereIn($column, $value);
                    } elseif ($column[0] == '%') {
                        $value && ($value[0] == '!') ? $query->where(substr($column, 1), "not $likeOperator", '%' . substr($value, 1) . '%') : $query->where(substr($column, 1), $likeOperator, '%' . $value . '%');
                    } elseif (isset($value[0]) && $value[0] == '!') {
                        $query->where($column, '<>', substr($value, 1));
                    } elseif ($value !== '') {
                        $query->where($column, $value);
                    }
                }


             }


        }

        return $query;
    }


    protected function getLikeOperator()
    {
        if (DB::connection()->getPDO()->getAttribute(PDO::ATTR_DRIVER_NAME) === 'pgsql') {
            return 'ILIKE';
        }

        return 'LIKE';
    }



    public function getCompleteRegistrations(){

     $registrations = array();

     $months = ['Jan' => '1', 'Feb' => '2', 'Mar' => '3', 'Apr' => '4', 'May' => '5', 'Jun' => '6',
     'Jul' => '7', 'Aug' => '8', 'Sep' => '9', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12'];

     foreach ($months as $key => $value){

     $c = User::published()->whereNotNull('job_role_id')->whereNotNull('country_id')->whereMonth('registered_at', $value)->whereYear('registered_at', '=', date('Y'))->get();

     $registrations[$key] = count($c);

     }


    $data = collect($registrations);

     return $data;


    }


    public function getPendingRegistrations(){

        $pending = array();

        $months = ['Jan' => '1', 'Feb' => '2', 'Mar' => '3', 'Apr' => '4', 'May' => '5', 'Jun' => '6',
        'Jul' => '7', 'Aug' => '8', 'Sep' => '9', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12'];

        foreach ($months as $key => $value){

        $c = User::published()->whereNull('job_role_id')->orWhere('job_role_id','=','')->whereNull('country_id')->orWhere('country_id','=','')->whereMonth('registered_at', $value)->whereYear('registered_at', '=', date('Y'))->get();

        $pending[$key] = count($c);

        }
        $data = collect($pending);

       return $data;

       }

       public function getTopEnrollments($month){
        $enrollments = array();
        $courses = CourseCreation::where('published',1)->get();
        foreach($courses as $key => $course){
            $userlicenses = UserLicense::where('course_id', $course->id)->whereMonth('created_at','=', $month)->get();


            if(count($userlicenses) > 0 ){
                $enrollments[] = [
                    'name' => $course->display_name,
                    'total' => count($userlicenses)
                   ];
            }else{
              unset($courses[$key]);
            }


        }

        $data = collect($enrollments);
		return $data;


    }


     public function getTopCompletions($month){
       $completions = array();
        $courses = CourseCreation::where('published',1)->get();
        foreach($courses as $key => $course){
            $completed = CourseCompletion::where('course_id','=',$course->id)->where('score','>=', 0.80)->whereMonth('created_at','=', $month)->get();

            if(count($completed) > 0 ){
                $completions[] = [
                    'name' => $course->display_name,
                    'total' => count($completed)
                   ];
            }else{
              unset($courses[$key]);
            }


        }
        
        $data = collect($completions);
       
		return $data;

     }


     public function getTopMonthEnrollments($month){
        $enrollments = array();
        $courses = CourseCreation::where('published',1)->get();
        foreach($courses as $key => $course){
            $userlicenses = UserLicense::where('course_id', $course->id)->whereMonth('created_at','=', $month)->get();

            if(count($userlicenses) > 0 ){
                $enrollments[] = [
                'name' => $course->display_name,
                'total' => count($userlicenses)];

            }else{
              unset($courses[$key]);
            }


        }

        // $data = collect();
		return $enrollments;


    }


    public function getTopMonthCompletions($month){
        $completions = array();
        $courses = CourseCreation::where('published',1)->get();
        foreach($courses as $key => $course){
            $completed = CourseCompletion::where('course_id','=',$course->id)->where('score','>=', 0.80)->whereMonth('created_at','=', $month)->get();

            if(count($completed) > 0 ){
                $completions[] = [
                    'name' => $course->display_name,
                    'total' => count($completed)
                   ];
            }else{
              unset($courses[$key]);
            }


        }
        // $data = collect($completions);
		return $completions;

    }



    public function getCompleteComplusoryModules()
    {
         $users = User::where('id', '!=', 1)->published()->get();
    
        $totalCount = $users->count();
    
        $completed = self::getCertified();

       
    
        $percentageComplete = ($completed['count'] / $totalCount) * 100;
    
        $ditribution = [
            'incomplete' => [
                'name' => 'Incomplete Compulsory',
                'value' => $percentageComplete,
                'total' => ($totalCount - $completed['count']),
            ],
            'complete' => [
                'name' => 'Completed Compulsory',
                'value' => (100 - $percentageComplete),
                'total' => $completed['count'],
            ],
        ];
    
        $data = collect($ditribution);
        return $data;
    }
    
    



    public function getFemaleCompleteComplusoryModules()
	{
        $users = User::where('id','!=', 1)
        ->where(function ($query)  {
            $query->where('gender_id', 2)->published();
        })->get();

    
		$totalCount = $users->count();

        

        $completed = self::getFemaleCertified();

       

     

		//dd((count($completed)/$totalCount) * 100);


		$percentageComplete =($completed['count']/$totalCount) * 100;
        
		//$ditribution = ['incomplete'=>$percentageIncomplete, 'complete'=>(100 -$percentageIncomplete)];
		$ditribution = [
			'incomplete'=>[
                'name'=>'Incomplete Compulsory',
                'value'=>$percentageComplete,
				'total'=> ($totalCount - $completed['count']),
			],
			'complete'=>[
                'name'=>'Completed Compulsory',
                'value'=>(100 -$percentageComplete),
				'total'=> $completed['count'],

			]
		];
		$data = collect($ditribution );
		return $data;
	}


    public function getMaleCompleteComplusoryModules()
	{
		$users = User::where('id','!=',1)->where('gender_id',1)->published()->get();
		$totalCount = $users->count();

        $completed = self::getMaleCertified();

		//dd((count($completed)/$totalCount) * 100);


		$percentageComplete =($completed['count']/$totalCount) * 100;

		//$ditribution = ['incomplete'=>$percentageIncomplete, 'complete'=>(100 -$percentageIncomplete)];
		$ditribution = [
			'incomplete'=>[
                'name'=>'Incomplete Compulsory',
                'value'=>$percentageComplete,
				'total'=> ($totalCount - $completed['count']),
			],
			'complete'=>[
                'name'=>'Completed Compulsory',
                'value'=>(100 -$percentageComplete),
				'total'=> $completed['count'],

			]
		];
		$data = collect($ditribution );
		return $data;
	}

    public function getCompleteProfileGender()
    {
        $users = User::where('id', '!=', 1)->published()->get();
        $totalCount = $users->count();


        $female = User::where('id', '!=', 1)->where('gender_id', 2)->published()->get();
        $male = User::where('id', '!=', 1)->where('gender_id', 1)->published()->get();
        $undefined = User::where('id', '!=', 1)->where('gender_id', 3)->published()->get();

        $notundefined = User::where('id', '!=', 1)->where('gender_id', null)->published()->get();

        //dd((count($completed)/$totalCount) * 100);


        $femaleComplete = (count($female) / $totalCount) * 100;
        $maleComplete = (count($male) / $totalCount) * 100;
        $undefinedComplete = (count($undefined) / $totalCount) * 100;
        $notundefinedComplete = (count($notundefined) / $totalCount) * 100;

        //$ditribution = ['incomplete'=>$percentageIncomplete, 'complete'=>(100 -$percentageIncomplete)];
        $ditribution = [
            'female' => [
                'name' => 'Female Users',
                'value' => $femaleComplete,
                'total' => $female->count(),
            ],
            'male' => [
                'name' => 'Male Users',
                'value' => $maleComplete,
                'total' => $male->count(),

            ],

            'undefined' => [
                'name' => 'Undefined Users',
                'value' =>  $undefinedComplete,
                'total' =>  $undefined->count(),

            ],

            'notundefined' => [
                'name' => 'Incomplete User Profiles',
                'value' =>  $notundefinedComplete,
                'total' =>  $notundefined->count(),

            ]
        ];
        $data = collect($ditribution);
        return $data;
    }

    public function getYearlyTopEnrollments($year, $launchYear)
    {
        $enrollments = array();
        $completed = UserLicense::whereYear('created_at', '>=', $launchYear)->whereYear('created_at', '<=', $year)
            ->selectRaw('id, course_id, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();

        foreach ($completed as $key => $course) {

            $license =  UserLicense::where('course_id', $course->course_id)->get();

            $enrollments[] = [
                'name' => $course->course->display_name,
                'total' => count($license)
            ];
        }

        $data = collect($enrollments);
        return $data;
    }

    public function getYearlyTopCompletions($year, $launchYear)
    {
      
        $completions = array();
        $completed = CourseCompletion::whereYear('created_at', '>=', $launchYear)->whereYear('created_at', '<=', $year)
            ->selectRaw('id, course_id,score, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();

        

        foreach ($completed as $key => $course) {
            $licenses =  CourseCompletion::where('course_id', $course->course_id)->get();
            $completions[] = [
                'name' => $course->courses->display_name,
                'total' => count($licenses)
            ];
        }
        $data = collect($completions);

        
        return $data;
    } 

    public function getTopYearlyEnrollments($year)
    {
       

        $launchYear = "2022";
        $eyear = date('Y');
        $enrollments = array();

        if($year == 0){
            $completed = UserLicense::whereYear('created_at', '>=', $launchYear)
            ->selectRaw('id, course_id, count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();
        }else{
            $completed = UserLicense::whereYear('created_at', '=', $year)
            ->selectRaw('id, course_id,count(*) as count')
            ->groupBy('course_id')
            ->orderBy('count', 'desc')
            ->take(5)
            ->get();

       

        }

        foreach ($completed as $key => $course) {

            $userlicenses = UserLicense::where('course_id', $course->course_id)->whereYear('created_at', '=', $launchYear)->get();
           
            if (count($userlicenses) > 0) {
                $enrollments[] = [
                    'name' => $course->course->display_name,
                    'total' => count($userlicenses)
                ];
            } else {
                unset($completed[$key]);
            }
        }

       

        // $data = collect();
        return $enrollments;
    }

  /** End of Enrollments Graph Repository */
  public function getTopYearlyCompletions($year)
  {
    
      $launchYear = "2022";
      $eyear = date('Y');
    
      $completions = array();


      if($year == 0){
          $courses = CourseCompletion::whereYear('created_at', '>=', $launchYear)
          ->selectRaw('id, course_id,score, count(*) as count')
          ->groupBy('course_id')
          ->orderBy('count', 'desc')
          ->take(5)
          ->get();
       

      }else{
          $courses = CourseCompletion::whereYear('created_at', '=', $year)
          ->selectRaw('id, course_id,score, count(*) as count')
          ->groupBy('course_id')
          ->orderBy('count', 'desc')
          ->take(5)
          ->get();

      }
     
      foreach ($courses as $key => $course) {
          $completed = CourseCompletion::where('course_id', '=', $course->course_id)->whereYear('created_at', '=', $launchYear)->get();

          if (count($completed) > 0) {
              $completions[] = [
                  'name' => $course->courses->display_name,
                  'total' => count($completed)
              ];
          } else {
              unset($courses[$key]);
          }
      }
     
      // $data = collect($completions);
      return $completions;
  }


  public function getYearlyRegistrations($year)
  {
    
      $launchYear = "2022";
      $eyear = date('Y');
    
      $completions = array();


      if($year == 0){
          $courses = CourseCompletion::whereYear('created_at', '>=', $launchYear)
          ->selectRaw('id, course_id,score, count(*) as count')
          ->groupBy('course_id')
          ->orderBy('count', 'desc')
          ->take(5)
          ->get();
       

      }else{
          $courses = CourseCompletion::whereYear('created_at', '=', $year)
          ->selectRaw('id, course_id,score, count(*) as count')
          ->groupBy('course_id')
          ->orderBy('count', 'desc')
          ->take(5)
          ->get();

      }
     
      foreach ($courses as $key => $course) {
          $completed = CourseCompletion::where('course_id', '=', $course->course_id)->whereYear('created_at', '=', $launchYear)->get();

          if (count($completed) > 0) {
              $completions[] = [
                  'name' => $course->courses->display_name,
                  'total' => count($completed)
              ];
          } else {
              unset($courses[$key]);
          }
      }
     
      // $data = collect($completions);
      return $completions;
  }




  public function getTopUserCompletions(){
     
    $completions = array();
    $completed = CourseCompletion::where('user_id','!=',1)
         ->selectRaw('id, user_id,score, count(*) as count')
        ->groupBy('user_id')
        ->orderBy('count', 'desc')
        ->take(10)
        ->get();

    

    foreach ($completed as $key => $course) {
        $licenses =  CourseCompletion::where('user_id', $course->user_id)->get();
        $completions[] = [
            'name' => $course->users->first_name.' '.$course->users->last_name,
            'total' => count($licenses)
        ];
    }
    $data = collect($completions);
    return $data;


 }


    // public static function getCertified()
    // {

    //     $compulsories = array();
    //     $complete = array();
    //     $total = array();

    //     $users = User::where('published','=',1)->get();

    //     foreach( $users as $key => $user){
    //         $compulsory = JobroleCourse::where('job_role_id','=',$user->job_role_id)->get();
    //         $completed = CourseCompletion::where('user_id','=',$user->id)->get();



    //             if(count($completed) > 0){
    //                 foreach($compulsory as $comp){
    //                     $compulsories[] = $comp->course_id;
    //                 }
    //                 foreach($completed as $compl){
    //                     $complete[] = $compl->course_id;

    //                 }

    //                 if(count($compulsories) != count($complete)){
    //                     unset($users[$key]);

    //                 }else{

    //                     $total[] += 1;
    //                 }

    //             }else{

    //                 unset($users[$key]);
    //             }
    //         }

    //         return $total;

    //   }

    public static function getCertified()
    {
        $completed = [];
    
        $users = User::where('published', '=', 1)->get();
    
        foreach ($users as $user) {
            $compulsory = JobroleCourse::where('job_role_id', '=', $user->job_role_id)->pluck('course_id')->toArray();
            $userCompleted = CourseCompletion::where('user_id', '=', $user->id)->pluck('course_id')->toArray();
    
            if (count($compulsory) === count($userCompleted) && count(array_diff($compulsory, $userCompleted)) === 0) {
                $completed[] = $user->id;
            }
        }
    
        return [
            'count' => count($completed),
            'users' => $completed,
        ];
    }

    public static function getFemaleCertified()
    {
        $completed = [];
    
       
        $users = User::where('id','!=', 1)
        ->where(function ($query)  {
            $query->orWhere('gender_id', 2);
        })->get();
    
        
    
        foreach ($users as $user) {
            $compulsory = JobroleCourse::where('job_role_id', '=', $user->job_role_id)->pluck('course_id')->toArray();
            $userCompleted = CourseCompletion::where('user_id', '=', $user->id)->pluck('course_id')->toArray();
    
            if (count($compulsory) == count($userCompleted) && count(array_diff($compulsory, $userCompleted)) == 0) {
                $completed[] = $user->id;
            }
        }

      
    
        return [
            'count' => count($completed),
            'users' => $completed,
        ];
    }


    public static function getMaleCertified()
    {
        $completed = [];
    
       
        $users = User::where('id','!=', 1)
        ->where(function ($query)  {
            $query->where('gender_id', 1)->published();
        })->get();
    
        foreach ($users as $user) {
            $compulsory = JobroleCourse::where('job_role_id', '=', $user->job_role_id)->pluck('course_id')->toArray();
            $userCompleted = CourseCompletion::where('user_id', '=', $user->id)->pluck('course_id')->toArray();
    
            if (count($compulsory) === count($userCompleted) && count(array_diff($compulsory, $userCompleted)) === 0) {
                $completed[] = $user->id;
            }
        }
    
        return [
            'count' => count($completed),
            'users' => $completed,
        ];
    }



}
