<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\GeneralMessage;

class GeneralMessageRepository extends ModuleRepository
{
    

    public function __construct(GeneralMessage $model)
    {
        $this->model = $model;
    }
}
