<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Course;
use App\Models\User;
use App\Models\UserCourse;
use App\Models\UserCourses;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UserCourseRepository extends ModuleRepository
{
    

    public function __construct(UserCourse $model)
    {
        $this->model = $model;
    }

    public function update($id, $fields)
    {
      
        
        DB::transaction(function () use ($id, $fields) {
            
            $object = $this->model->findOrFail($id);

            $this->beforeSave($object, $fields);

            $fields = $this->prepareFieldsBeforeSave($object, $fields);

            $object->fill(Arr::except($fields, $this->getReservedFields()));

            $object->save();

            if($fields['courses']){
            
            // delete records with job role 
            UserCourses::where('module_id', '=', $object->id)->delete();

            $user = User::where('id',$fields['user'])->first();

            // Loop through the courses array 

            foreach($fields['courses'] as $course){

             // Save records to raltionship table

             $job = new UserCourses();
             $job->module_id = $object->id;
             $job->user_id = $fields['user'];
             $job->course_id = $course;
             $job->job_role_id = $user->job_role_id;

             $job->save();

            }
            }

           $course_title = Course::where('id',  $job->course_id)->first();
          
            $name = Auth::user()->name;

             Mail::send('emails.user_course',['name' => $user->name,  'title'=> $course_title->name, 'url' => route('user.login')],
             function ($message) use ($user) {
               $message->from('support@farwell-consultants.com');
               $message->to($user->email, $user->first_name);
               $message->subject('New Course Alert');
             });
          

            $this->afterSave($object, $fields);
        }, 3);
    }
}
