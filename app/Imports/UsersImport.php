<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Edx\EdxAuthUser;
use Ixudra\Curl\Facades\Curl;
use Redirect;
use App\Models\Role;
use App;
use TaylorNetwork\UsernameGenerator\Generator;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Session;


class UsersImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {

      foreach ($rows as $row) 
        {

        if(isset($row['first_name']) && isset($row['last_name']) && isset($row['phone'])){
          $generator = new Generator([ 'separator' => '_' ]);
          $username = $generator->generate($row['first_name'] . ' ' . $row['last_name']);
          

          $usern = EdxAuthUser::where('username', $username)->first();
          
          if(!empty($usern)){
            $username = $username.'_'.rand(0,7);
          }else{
            $username = $username;
          }

          $randomString =  self::generateRandomString(9);
          // \Illuminate\Http\Request::request()->add(['password' => $randomString]);
          $pass = Hash::make($randomString);
          $email = $username.'@ttk.com';

        
          $string = substr($row['phone'], 0, 1);

          if($string === "0"){

           $phone = substr($row['phone'],1);
          }
          else{

           $phone = $row['phone'];
          }

          $topic[] = ['Name' => 'User Export'];
          $data[] = [
            'Phone'=> $phone,
            'Password'=> $randomString
          ];

          
          $user = new User();
          $user->name = $row['first_name'] . ' ' . $row['last_name'];
          $user->email = $email;
          $user->role = 1;
          $user->username= $username;
          $user->password =  $pass;
          $user->company_id = 1;
          $user->phone = $phone;
          $user->phone_locale = '+254';
          $user->first_name = $row['first_name'];
          $user->last_name = $row['last_name'];
          $user->is_activated = 1;
          $user->platform_information = 1;
          $user->contact_consent= 1;
          $user->published = 1;
          $user->is_activated = true;
          $user->registered_at = Carbon::now();
         $user->save();

          $edxResponse =  $this->edxRegister($user,$randomString);
          

          if ($edxResponse !== true) {
          }else{

              $edxuser = EdxAuthUser::where('username', $user->username)->first();
              $edxuser->first_name = $user->first_name;
              $edxuser->last_name = $user->last_name;
              $edxuser->email = $user->email;
              $edxuser->is_active =  1;
              $edxuser->save();
           
          }
          }
      
        }

        $store= $this->storeExcel($data,$topic);

      
        session()->put('excel_download', $store);
    }


    public function storeExcel($data,$topic) 
      {
        $name = Carbon::now().'_UsersExport.xlsx';

        Excel::store(new UsersExport($data,$topic), 'uploads/exports/'.$name, 'real_public');
        
        //Excel::store(new UsersExport($data,$topic), $name);

        // $exporter = app()->makeWith(UsersExport::class, compact('data','topic'));
        
        // $exporter->store($name);
        return $name;

      }


    
    private function edxRegister($user, $password)
    {
      //Validate user
      if ($user === null) {
        return;
      }
      //Package data to be sent
  if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
           $email = $user->email;
         }else{
          $email = $user->email.'@local.com';
    }
      $data = [
        'email' => $email,
        'name' => $user->first_name . ' ' . $user->last_name,
        'username' => $user->username,
        'honor_code' => 'true',
        'password' => $password,
        'country'=> 'KE',
        'terms_of_service'=> 'true',
      ];
  
      $headers = array(
        'Content-Type'=>'application/x-www-form-urlencoded',
        'cache-control' => 'no-cache',
        'Referer'=>env('APP_URL').'/register',
      );
  
      $client = new \GuzzleHttp\Client();
  
      try {
  
        $response = $client->request('POST', env('LMS_REGISTRATION_URL'), [
          'form_params' => $data,
          'headers'=>$headers,
        ]);
  
        return true;
  
      }catch (\GuzzleHttp\Exception\ClientException $e) {
  
        $responseJson = $e->getResponse();
        $response = json_decode($responseJson->getBody()->getContents(),true);
        //Error, delete user
        $user->delete();
  
        //Delete password resets
        //PasswordReset::where('email', '=', $user->email)->delete();
        $errors = [];
        foreach ($response as $key => $error) {
          //Return error
          $errors[] = $error;
        }
        //echo "CATCH 1";
        //dd($errors);
        return $errors[0];
  
  
      }catch ( \Exception $e){
  
        //Error, delete user
        $user->delete();
        //Delete password resets
        //PasswordReset::where('email', '=', $user->email)->delete();
        //echo "CATCH 2";
        //echo $e->getMessage();
        //die;
        return $e->getMessage();
  
      }
    }

    protected function generateRandomString($length = 6)
  	{
  		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  		$charactersLength = strlen($characters);
  		$randomString = '';
  		for ($i = 0; $i < $length; $i++) {
  			$randomString .= $characters[ rand(0, $charactersLength - 1) ];
  		}

  		return $randomString;
    }


}
