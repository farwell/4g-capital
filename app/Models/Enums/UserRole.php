<?php

    namespace A17\Twill\Models\Enums;

    use MyCLabs\Enum\Enum;

    class UserRole extends Enum
    {   
        const VIEWONLY = 'View only';
        const PUBLISHER = 'Publisher';
        const ADMIN = 'Admin';
        const Learner = 'Learner';
        const HR = 'HR';
        const HOD = 'HOD';
        const GROUPHR = 'GROUPHR';
        const COMPANYHR = 'COMPANYHR';
        const BRANCHMANAGER = 'BRANCHMANAGER';
    }