<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobroleCourse extends Model
{
    //

    protected $fillable = ['job_role_id', 'course_id'];


    public function courses(){
      return $this->belongsTo(CourseCreation::class,'course_id','id');
    }
}
