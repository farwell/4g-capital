<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class SessionResource extends Model
{
    //
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'user_id',
        'session_id',
        'title',
        'description',
        'filename',
    ];
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];

    protected $presenterAdmin = 'App\Presenters\Front\SessionResourcePresenter';

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id', 'id');
    }

    public function session()
    {
        return $this->belongsTo(Session::class , 'session_id', 'id');
    }
}
