<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupResource extends Model
{
    //

    protected $fillable = [
        'published',
        'title',
        'description',
        'resource_type_id',
        'resource_theme_id',
        'resource_category',
        'resource_country',
        'cover_image',
        'audio_file',
        'video_file',
        'downloable_file',
        'external_link',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
