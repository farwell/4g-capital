<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\GroupTheme;

class GroupThemeTranslation extends Model
{
    protected $baseModuleModel = GroupTheme::class;
}
