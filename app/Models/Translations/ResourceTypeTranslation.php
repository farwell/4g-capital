<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\ResourceType;

class ResourceTypeTranslation extends Model
{
    protected $baseModuleModel = ResourceType::class;
}
