<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\ContentBucket;

class ContentBucketTranslation extends Model
{
    protected $baseModuleModel = ContentBucket::class;
}
