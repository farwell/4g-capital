<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Projecttopic;

class ProjecttopicTranslation extends Model
{
    protected $baseModuleModel = Projecttopic::class;
}
