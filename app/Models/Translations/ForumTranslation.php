<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Forum;

class ForumTranslation extends Model
{
    protected $baseModuleModel = Forum::class;
}
