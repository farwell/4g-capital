<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\GroupFormat;

class GroupFormatTranslation extends Model
{
    protected $baseModuleModel = GroupFormat::class;
}
