<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\FeatureWeek;

class FeatureWeekTranslation extends Model
{
    protected $baseModuleModel = FeatureWeek::class;
}
