<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\GroupForumTopic;

class GroupForumTopicTranslation extends Model
{
    protected $baseModuleModel = GroupForumTopic::class;
}
