<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\PlatformMessage;

class PlatformMessageTranslation extends Model
{
    protected $baseModuleModel = PlatformMessage::class;
}
