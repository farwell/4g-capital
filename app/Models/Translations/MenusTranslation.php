<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Menus;

class MenusTranslation extends Model
{
    protected $baseModuleModel = Menus::class;
}
