<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Sponsor;

class SponsorTranslation extends Model
{
    protected $baseModuleModel = Sponsor::class;
}
