<?php

namespace App\Models;


use A17\Twill\Models\Model;

class UserCourse extends Model 
{
    
    protected $table = 'user_course';

    protected $fillable = [
        'published',
        'title',
        'description',
    ];

    public function courses(){

        return $this->belongsToMany(Course::class, 'user_courses', 'module_id','course_id');

    }

    public function users(){

        return $this->belongsToMany(User::class, 'user_courses', 'module_id','user_id');

    }

    public function getCoursesSelectedAttribute($value)
    {
        return  $this->courses->implode('name', ', ');
    }


    public function getUserSelectedAttribute($value)
    {
        
        $user= $this->users->groupBy('user_id');

        return $this->id;
    }
    
}
