<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectorCourse extends Model
{
    //
    protected $table = 'sector_course';
    protected $hidden = ['sector_id'];


    public function courses(){
      return $this->belongsTo(CourseCreation::class,'course_id','id');
    }
}
