<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Evaluation extends Model
{
    //
    use HasPosition;
    
    protected $fillable = [
      'published',
      'title',
      'course_id',
      'option_id',
      
  ];


    public function scopeActive($query)
    {
      return $query->where('status', '=', 0);
    }

    public function evaluation_options(){
      return $this->belongsTo(EvaluationOption::class,'option_id','id');
    }
}
