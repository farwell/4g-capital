<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Sector extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];

    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];

    public function scopePublished($query)
    {
        return $query->wherePublished(true)->orderByTranslation('title');
    }

    public function scopeDraft($query)
    {
        return $query->wherePublished(false)->orderByTranslation('title');
    }

    public function scopeOnlyTrashed($query)
    {
        return $query->whereNotNull('deleted_at')->orderByTranslation('title');
    }


    public function branches(){
        return $this->belongsToMany(Branch::class, 'sector_branch','sector_id','branch_id');
    }

    public function courses(){

        return $this->belongsToMany(Course::class, 'sector_course', 'sector_id','course_id');

    }

  

    public function users()
    {
        return $this->hasManyThrough(User::class, Branch::class, 'sector_id', 'branch_id');
    }
    
       public function country()
       {
           return $this->belongsTo(Country::class,'country_id','id');
       }
    
    
       public function completions(){
    
        return $this->hasMany(CourseCompletion::class);
    
       }


    public function  getRegisteredAttribute(){
        $users = array();
    
        foreach($this->users as $user){
           
            $users[] = $user->id;
        }
    
        return count($users);
       
       }
    
    
    
    //    public function getCompletedAttribute()
    //    {
    //        $users = $this->users;
    //        $total = 0;
       
    //        foreach ($users as $key => $user) {
    //            $compulsories = [];
    //            $complete = [];
       
    //            $compulsory = JobroleCourse::where('job_role_id', '=', $user->job_role_id)->get();
    //            $completed = CourseCompletion::where('user_id', '=', $user->id)->get();
       
    //            if (count($completed) > 0) {
    //                foreach ($compulsory as $comp) {
    //                    $compulsories[] = $comp->course_id;
    //                }
    //                foreach ($completed as $compl) {
    //                    $complete[] = $compl->course_id;
    //                }
       
    //                $completedCount = 0;
    //                foreach ($complete as $courseId) {
    //                    if (in_array($courseId, $compulsories)) {
    //                        $completedCount++;
    //                    }
    //                }
       
    //                if ($completedCount === count($compulsories)) {
    //                    $total += 1;
    //                } else {
    //                    unset($users[$key]);
    //                }
    //            } else {
    //                unset($users[$key]);
    //            }
    //        }
       
    //        return $total;
    //    }


    public function getCompletedAttribute()
   {
       $users = $this->users;
       $completedCount = 0;

           foreach ($users as $key => $user) {
                $compulsory = JobroleCourse::where('job_role_id', '=', $user->job_role_id)->pluck('course_id')->toArray();
               
               $completed = CourseCompletion::where('user_id', '=', $user->id)->pluck('course_id')->toArray();
            
               
               if (count($compulsory) === count($completed) && count(array_diff($compulsory, $completed)) === 0) {
                $completedCount++;
            }
           }

           

           return $completedCount;
   }

   public function getCompletedOneAttribute()
   {
       $users = $this->users;
      
   
       $completedCount = 0;

      

           foreach ($users as $key => $user) {
                $compulsory = JobroleCourse::where('job_role_id', '=', $user->job_role_id)->pluck('course_id')->toArray();
           
               $completed = CourseCompletion::where('user_id', '=', $user->id)->pluck('course_id')->toArray();
             
               
               if (count($completed) > 0 || count(array_diff($compulsory, $completed)) === 0) {
                $completedCount++;
            }
           }

           

           return $completedCount;
   }
   

//        public function getCompletedOneAttribute()
//    {
//        $users = $this->users;
      
   
//        $completedCount = 0;

      

//            foreach ($users as $key => $user) {
//                 $compulsory = JobroleCourse::where('job_role_id', '=', $user->job_role_id)->pluck('course_id')->toArray();
           
//                $completed = CourseCompletion::where('user_id', '=', $user->id)->pluck('course_id')->toArray();
             
               
//                if (count($completed) > 0 || count(array_diff($compulsory, $completed)) === 0) {
//                 $completedCount++;
//             }
//            }

           

//            return $completedCount;
//    }


       public function getBranchesSelectedAttribute($value)
       {
           return  $this->branches->implode('title', ', ');
       }

       public function getCoursesSelectedAttribute($value)
       {
           return  $this->courses->implode('name', ', ');
       }
    
    
}
