<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectorBranch extends Model
{
  protected $table = 'sector_branch';
    protected $fillable = ['sector_id', 'branch_id'];

    public function branches(){
      return $this->belongsTo(Sector::class,'branch_id','id');
    }
    
}
