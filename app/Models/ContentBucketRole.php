<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentBucketRole extends Model
{
    //

    protected  $table = 'content_bucket_role';


    public function roles() {
        return $this->belongsTo(Role::class,'role_id','id');
    }
}
