<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCourses extends Model
{
    

    protected $fillable = ['module_id','user_id', 'course_id', 'job_role_id'];
}
