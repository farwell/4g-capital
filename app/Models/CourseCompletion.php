<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Carbon\Carbon;
use App\Models\JobroleCourse;


class CourseCompletion extends Model
{
    //
    use HasPosition;

    protected $fillable = ["course_id", "username", "user_id", "grade", "percent", "passed", "created_at","enrollment_date",'branch_id','job_role_id','country_id'];


    public function courses()
    {
        return $this->belongsTo(CourseCreation::class, 'course_id', 'id');
    }

    

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function branches()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }
    
    public function roles()
    {
        return $this->belongsTo(JobRole::class, 'job_role_id', 'id');
    }

    public function countries()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function getCompletionDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getEnrollmentDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getCompletionStatusAttribute(){

        $completed = $this->checkCompletionsStatus($this->course_id, $this->user_id) ;
       
         return $completed['status'];
    
    
      }


      private function checkCompletionsStatus($course_id, $user_id){

       $completion = CourseCompletion::where('course_id', $course_id)->where('user_id', $user_id)->first();
   
       if($completion){
        
           if($completion->score >= 0.80){
   
               $completionStatus = 1;
               $status = "Completed";
   
           }else{
   
               $completionStatus = 2;
               $status = 'Completed but failed';
           }
       }else{
           $completionStatus = false;
           $status = 'In Progress';
       }
       
       return [
       'status' => $status,
       'completionStatus'=>$completionStatus
        ];
   
   }

  public static function getCertified(){
  
    $compulsories = array();
    $complete = array();
    $total = array();

    $users = User::where('published','=',1)->get();
        
    foreach( $users as $key => $user){
        $compulsory = JobroleCourse::where('job_role_id','=',$user->job_role_id)->get();
        $completed = CourseCompletion::where('user_id','=',$user->id)->get();
        


            if(count($completed) > 0){
                foreach($compulsory as $comp){
                    $compulsories[] = $comp->course_id;
                }
                foreach($completed as $compl){
                    $complete[] = $compl->course_id;
            
                }

                if(count($compulsories) != count($complete)){
                    unset($users[$key]);

                }else{

                    $total[] += 1;
                }
                    
            }else{

                unset($users[$key]);
            }
        }

        return $total;

  }

  
}
