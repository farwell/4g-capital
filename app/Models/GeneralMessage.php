<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Carbon\Carbon;

class GeneralMessage extends Model
{
    //
    use HasPosition;

    protected $fillable = ["name", "email", "contact_phone", "subject", "contact_message"];

}
