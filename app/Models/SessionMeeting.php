<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SessionMeeting extends Model
{
    //
    protected $appends = ['resource_url'];

    public function getResourceUrlAttribute()
    {
        return url('/admin/sessionmeetings/'.$this->getKey());
    }

public function comments(){

    return $this->hasMany(Comment::class)->orderBy('created_at','DESC');
}

    public function course()
    {
        return $this->hasOne(Course::class , 'sessions_id', 'id' );
    }


    public function resources()
    {
        return $this->hasMany(SessionResource::class , 'id', 'session_id');
    }
}
