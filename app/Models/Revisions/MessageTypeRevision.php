<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class MessageTypeRevision extends Revision
{
    protected $table = "message_type_revisions";
}
