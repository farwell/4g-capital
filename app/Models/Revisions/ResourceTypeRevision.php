<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class ResourceTypeRevision extends Revision
{
    protected $table = "resource_type_revisions";
}
