<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class SocialmediaRevision extends Revision
{
    protected $table = "socialmedia_revisions";
}
