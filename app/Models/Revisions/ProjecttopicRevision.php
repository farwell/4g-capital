<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class ProjecttopicRevision extends Revision
{
    protected $table = "projecttopic_revisions";
}
