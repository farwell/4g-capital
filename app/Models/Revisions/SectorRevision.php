<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class SectorRevision extends Revision
{
    protected $table = "sector_revisions";
}
