<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class ContentBucketRevision extends Revision
{
    protected $table = "content_bucket_revisions";
}
