<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class PlatformMessageRevision extends Revision
{
    protected $table = "platform_message_revisions";
}
