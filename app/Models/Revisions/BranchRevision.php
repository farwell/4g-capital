<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class BranchRevision extends Revision
{
    protected $table = "branch_revisions";
}
