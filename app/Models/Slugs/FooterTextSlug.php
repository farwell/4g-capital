<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class FooterTextSlug extends Model
{
    protected $table = "footer_text_slugs";
}
