<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class GroupThemeSlug extends Model
{
    protected $table = "group_theme_slugs";
}
