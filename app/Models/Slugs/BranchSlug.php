<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class BranchSlug extends Model
{
    protected $table = "branch_slugs";
}
