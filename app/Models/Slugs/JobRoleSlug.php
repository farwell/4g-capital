<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class JobRoleSlug extends Model
{
    protected $table = "job_role_slugs";
}
