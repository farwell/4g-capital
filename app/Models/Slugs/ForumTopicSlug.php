<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ForumTopicSlug extends Model
{
    protected $table = "forum_topic_slugs";
}
