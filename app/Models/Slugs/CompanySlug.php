<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class CompanySlug extends Model
{
    protected $table = "company_slugs";
}
