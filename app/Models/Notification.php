<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
    const GROUP=1;
    const ACTIVITY=2;
    const MESSAGE=3;
    const FOLLOW=4;


    protected $table = 'notifications';


    protected $casts = [
    'data' => 'array',
   ];
}
