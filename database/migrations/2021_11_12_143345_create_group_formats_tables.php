<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupFormatsTables extends Migration
{
    public function up()
    {
        Schema::create('group_formats', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('group_format_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'group_format');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('group_format_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'group_format');
        });

        Schema::create('group_format_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'group_format');
        });
    }

    public function down()
    {
        Schema::dropIfExists('group_format_revisions');
        Schema::dropIfExists('group_format_translations');
        Schema::dropIfExists('group_format_slugs');
        Schema::dropIfExists('group_formats');
    }
}
