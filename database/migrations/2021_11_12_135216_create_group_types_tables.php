<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupTypesTables extends Migration
{
    public function up()
    {
        Schema::create('group_types', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('group_type_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'group_type');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('group_type_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'group_type');
        });

        Schema::create('group_type_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'group_type');
        });
    }

    public function down()
    {
        Schema::dropIfExists('group_type_revisions');
        Schema::dropIfExists('group_type_translations');
        Schema::dropIfExists('group_type_slugs');
        Schema::dropIfExists('group_types');
    }
}
