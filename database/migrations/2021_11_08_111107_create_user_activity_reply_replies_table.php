<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserActivityReplyRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_activity_reply_replies', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('user_activity_id');
            $table->integer('user_activity_reply_id');
            $table->text('comment')->nullable();
            $table->json('images')->nullable();
            $table->json('gifs')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_activity_reply_replies');
    }
}
