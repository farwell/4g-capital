<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSessionResourcesTables extends Migration
{
    public function up()
    {
        Schema::create('session_resources', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('session_id')->unsigned()->nullable();
            $table->string('filename')->nullable();
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('session_resource_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'session_resource');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('session_resource_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'session_resource');
        });

        Schema::create('session_resource_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'session_resource');
        });
    }

    public function down()
    {
        Schema::dropIfExists('session_resource_revisions');
        Schema::dropIfExists('session_resource_translations');
        Schema::dropIfExists('session_resource_slugs');
        Schema::dropIfExists('session_resources');
    }
}
