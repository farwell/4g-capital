<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForumTopicsTables extends Migration
{
    public function up()
    {
        Schema::create('forum_topics', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('forum_topic_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'forum_topic');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('forum_topic_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'forum_topic');
        });

        Schema::create('forum_topic_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'forum_topic');
        });
    }

    public function down()
    {
        Schema::dropIfExists('forum_topic_revisions');
        Schema::dropIfExists('forum_topic_translations');
        Schema::dropIfExists('forum_topic_slugs');
        Schema::dropIfExists('forum_topics');
    }
}
