<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTables extends Migration
{
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->bigInteger('group_type_id')->unsigned()->nullable();
            $table->string('group_format')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('title')->nullable();
			$table->text('description')->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('group_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'group');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('group_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'group');
        });

        Schema::create('group_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'group');
        });
    }

    public function down()
    {
        Schema::dropIfExists('group_revisions');
        Schema::dropIfExists('group_translations');
        Schema::dropIfExists('group_slugs');
        Schema::dropIfExists('connect_groups');
    }
}
