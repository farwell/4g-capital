<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentTrainingcommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('training_comments', function (Blueprint $table) {
            $table->bigInteger('parent_id')->unsigned()->nullable();

        });
        Schema::table('training_comments', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('training_comments')->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('training_comments', function (Blueprint $table) {
            $table->dropColumn('parent_id');

        });
    }
}
