<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTitleResourcestable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('resources', function (Blueprint $table) {
            $table->string('title')->nullable();
			$table->text('description')->nullable();
			$table->string('cover_image')->nullable();
            $table->string('audio_file')->nullable();
            $table->string('video_file')->nullable();
            $table->string('downloable_file')->nullable();
            $table->string('external_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('resources', function (Blueprint $table) {
			$table->dropColumn('title');
			$table->dropColumn('description');
			$table->dropColumn('cover_image');
            $table->dropColumn('audio_file');
            $table->dropColumn('video_file');
            $table->dropColumn('downloable_file');
            $table->dropColumn('external_link');
        });
    }
}
