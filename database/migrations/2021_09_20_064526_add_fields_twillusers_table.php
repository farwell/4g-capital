<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsTwillusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $twillUsersTable = config('twill.users_table', 'twill_users');

        Schema::table($twillUsersTable, function (Blueprint $table) {
            $table->string('username');
            $table->string('phone')->nullable();
            $table->string('phone_locale')->nullable();
            $table->string('is_activated')->nullable();
            $table->date('last_login_at')->nullable();
            $table->date('registered_at')->nullable();
            $table->string('require_new_password', 200)->nullable();
            $table->string('company_id')->nullable();
            $table->string('is_company_admin')->nullable();
            $table->string('is_admin')->nullable();
            $table->string('secondary_email')->nullable();
            $table->string('platform_information')->nullable();
            $table->string('profile_pic')->nullable();
		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        $twillUsersTable = config('twill.users_table', 'twill_users');
        Schema::table($twillUsersTable, function (Blueprint $table) {

            $table->dropColumn('username');
            $table->dropColumn('phone');
            $table->dropColumn('phone_locale');
            $table->dropColumn('is_activated');
            $table->dropColumn('last_login_at');
            $table->dropColumn('registered_at');
            $table->dropColumn('require_new_password');
            $table->dropColumn('company_id');
            $table->dropColumn('is_company_admin');
            $table->dropColumn('secondary_email');
            $table->dropColumn('platform_information');
            $table->dropColumn('profile_pic');
			
        });
    }
}
