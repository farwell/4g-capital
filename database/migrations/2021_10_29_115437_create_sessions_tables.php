<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSessionsTables extends Migration
{
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('meeting_id')->nullable();
            $table->string('host_email')->nullable();
            $table->text('topic')->nullable();
            $table->string('status')->nullable();
            $table->string('start_time')->nullable();
            $table->text('agenda')->nullable();
            $table->longText('join_url')->nullable();
            $table->string('meeting_password')->nullable();
            $table->string('session_video_url')->nullable();
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('session_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'session');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('session_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'session');
        });

        Schema::create('session_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'session');
        });
    }

    public function down()
    {
        Schema::dropIfExists('session_revisions');
        Schema::dropIfExists('session_translations');
        Schema::dropIfExists('session_slugs');
        Schema::dropIfExists('sessions');
    }
}
