<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailGeneralmessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('general_messages', function (Blueprint $table) {
            $table->string('email')->nullable();
            $table->string('position')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('general_messages', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('position');
            $table->dropSoftDeletes();
        });
    }
}
