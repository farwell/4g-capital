<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeleteUserlicenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('user_licenses', function (Blueprint $table) {
            $table->softDeletes();
            $table->string('username');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('user_licenses', function (Blueprint $table) {
            $table->dropSoftDeletes();
            $table->dropColumn('username');
        });
    }
}
