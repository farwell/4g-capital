<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeatureWeeksTables extends Migration
{
    public function up()
    {
        Schema::create('feature_weeks', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('feature_week_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'feature_week');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('feature_week_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'feature_week');
        });

        Schema::create('feature_week_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'feature_week');
        });
    }

    public function down()
    {
        Schema::dropIfExists('feature_week_revisions');
        Schema::dropIfExists('feature_week_translations');
        Schema::dropIfExists('feature_week_slugs');
        Schema::dropIfExists('feature_weeks');
    }
}
